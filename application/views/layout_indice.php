<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

$privilegi=$this->users_model->account_privilegi($grupposelezionato);

$percorso_base=$this->config->item('real_path');

$percorso_users=$this->config->item('percorso_users');
$cartella_archivio=$percorso_base.$percorso_users.$stringa_casuale."/content";
$url_archivio="/strimy/users/".$stringa_casuale."/content";
?>

<script language="Javascript">


function checkform() {
	var totale_sequenze=$("#enumero_layout").val();
	var xx=0;
	var errore=0;
	var messaggio_errore="";
	var ora_inizio = new Date();
	var ora_fine = new Date();
	var sequenza="";
	while (xx<totale_sequenze) {
		attiva=$("#eattiva_"+xx).val();
		if (attiva==1) {
			inizio=$("#einizio_"+xx).val();
			fine=$("#efine_"+xx).val();
			
			if (sequenza!="")
				sequenza=sequenza+"x";
			
			sequenza=sequenza+inizio+"-"+fine;
		}
	xx++;
	}

	var sum=0;
    $('.egiorno_selezionato').each(function() {
        sum += Number($(this).val());
    });
    
    if ((sum>1) && ($("#check_giorno_successivo").val()==1)) {
		messaggio_errore="E' presente un layout attivo oltre la mezzanotte!\nIn questo caso è necessario impostare manualmente il palinsesto per i singoli giorni.";
		errore=1;
	}

	if ((errore==0) && (sequenza!="")) {
		var id_dispositivo=$('#eid_dispositivo').val();
		var giorno_oggi=$('#egiorno_oggi').val();
		
		var stringa = '';
		stringa=stringa+"esequenza_orari="+sequenza+"&eid_dispositivo="+id_dispositivo+"&egiorno_oggi="+giorno_oggi;

		$.ajax({
		  url: "<?php echo base_url(); ?>index.php/layout/check_orario_layout", 
		  data: stringa,
		  async: false,
		  dataType: 'html',
		  method: 'POST',
		  success: function(risultato) {
				if (risultato!="0") {
					messaggio_errore="ATTENZIONE: Orari incoerenti o non formattati";
					errore=1;
				}
			  }
		});
	}
	
	if (errore==1) {
		alert(messaggio_errore);
		return false;
	}
	else {
		var messaggio="";
		messaggio=messaggio+"Vuoi proseguire?";
		if (confirm(messaggio)) {
			$('#form_changed').val(0);
			document.getElementById("conferma_layout").submit();
			return true;
			}
		else
			return false;
	}
}



function riordina() {
		$('div#contenitore_sequenze>div.sequenza').tsort('span.orario_inizio');
	}

function mostra_aggiungi(elemento) {
		$("div.elemento_archivio_overlay").fadeOut();
		$("#elemento_archivio_overlay_"+elemento).fadeIn(100);
		
	}

function nascondi_aggiungi(elemento) {
		$("#elemento_archivio_overlay_"+elemento).fadeOut(10);
	}

function aggiorna_orari() {
	var totale_layout=$("#enumero_layout").val();
	var xx=0;
	var yy=0;
	while (xx<totale_layout) {
			attiva=$("#eattiva_"+xx).val();
			
			if (attiva==1) {
			
				$("#span_numero_layout_"+xx).html(yy+1);
				controlla_orari(xx);
				yy++;
				
			}
		xx++;
		}
	}	

function controlla_orari(indice) {
		var inizio = $("#einizio_"+indice).val();
		var fine = $("#efine_"+indice).val();
		var stt = new Date("November 13, 2013 " + inizio);
		stt = stt.getTime();
		var endt = new Date("November 13, 2013 " + fine);
		endt = endt.getTime();
		differenza=endt-stt;
		if (differenza>0) {
			$("#giorno_successivo_"+indice).hide();
			$("#check_giorno_successivo").val(0);
		}
		else {
			$("#giorno_successivo_interno_"+indice).html(fine);
			$("#giorno_successivo_"+indice).show();
			$("#check_giorno_successivo").val(1);
		}
	}

function cambia_layout() {
	var layout=$("#eseleziona_layout").val();
	$(".dettagli_layout").hide();
	$(".image_layout").hide(150);
	$("#image_layout_"+layout).show(150);
	$("#dettagli_layout_"+layout).show();
	}

function aggiungi_nuovo_layout() {
	var layout=$("#eseleziona_layout").val();
	aggiungi_layout(0, layout);
	$("#aggiungi_layout").modal("hide");
	}


function aggiungi_layout(id, tipologia) {
		
		var numero_layout=$("#enumero_layout").val();
		var numero_layout_attivi=$("#enumero_layout_attivi").val();
		if (numero_layout_attivi==10) {
			alert("Attenzione! E' stato raggiunto il limite di "+numero_layout_attivi+" fasce orarie gestibili");
			return false;
		}
		
		var fine_precedente="09:00";
		if (Math.round(numero_layout)>0) {
			numero_layout_precedente=Math.round(numero_layout)-1;
			fine_precedente=$("#efine_"+numero_layout_precedente).val();
		}
		var nuovodiv="";
		
		nuovodiv='<div id="div_layout_'+numero_layout+'" style="position: relative; float: left; clear: both; width: 100%; k;"></div><div id="ultimo_indice"></div>';
		$('#ultimo_indice').replaceWith(nuovodiv);
		
		$('#div_layout_'+numero_layout).load("<?php echo base_url(); ?>index.php/layout/aggiungi_layout/"+id+"/"+"/"+tipologia+"/"+numero_layout+"/"+fine_precedente);
		
		numero_layout=Math.round(numero_layout)+1;
		$("#enumero_layout").val(numero_layout);
		numero_layout_attivi=Math.round(numero_layout_attivi)+1;
		$("#enumero_layout_attivi").val(numero_layout_attivi);
		$('#form_changed').val(1);
		$('#eform_changed').val(1);
	}

function rimuovi_layout(indice) {
	var numero_layout_attivi=$("#enumero_layout_attivi").val();
		if (confirm('Rimuovere il layout?')) {
			$("#eattiva_"+indice).val('0');
			$("#div_layout_"+indice).hide(500);
			$("#div_layout_"+indice +" .playlist_select").remove();
			numero_sequenze_attive=Math.round(numero_layout_attivi)-1;
			$("#enumero_layout_attivi").val(numero_layout_attivi);
			$('#form_changed').val(1);
			$('#eform_changed').val(1);
			aggiorna_orari();
		}
		return false;
	}

function seleziona_dispositivo(casella, indice) {
	if (casella.checked)
		$("#eid_dispositivo_gruppo_selezionato_"+indice).val(1);
	else
		$("#eid_dispositivo_gruppo_selezionato_"+indice).val(0);
}

function seleziona_giorno(casella, indice) {
	if (casella.checked)
		$("#egiorno_selezionato_"+indice).val(1);
	else
		$("#egiorno_selezionato_"+indice).val(0);
}

function checkform_presave() {
	var errore=0;
	var valore=0;
	$('.playlist_select').removeClass('has-error-generic');
	$('.playlist_select').each(function(){
		valore=$(this).val();
		if (valore==0) {
			errore=1;
			$(this).addClass('has-error-generic');
		}
	})
	if (errore==1) {
		alert('Attenzione: verifica di aver specificato tutte le playlist');
		return false
	}
	else {
		$('#salva_layout').modal('show');
	}
}

</script>


<?php

if ($id_dispositivo<0):	// si tratta di un gruppo
	$this->db->from('dispositivi_gruppi');
	$this->db->where('id', ($id_dispositivo*-1));
	$queryDG=$this->db->get();
	$rowDG=$queryDG->row();
	$descrizione_gruppo_dispositivo=$rowDG->descrizione;
	$id_gruppo_dispositivo=$id_dispositivo*-1;
else:	// si tratta di un dispositivo
	$this->db->from('dispositivi');
	$this->db->where('id_dispositivo', $id_dispositivo);
	$queryGRP=$this->db->get();
	if ($queryGRP->num_rows()>0):
		$rowGRP=$queryGRP->row();
		$id_gruppo_dispositivo=$rowGRP->id_gruppo;
		
		$this->db->from('dispositivi_gruppi');
		$this->db->where('id', $id_gruppo_dispositivo);
		$queryDG=$this->db->get();
		$rowDG=$queryDG->row();
		$descrizione_gruppo_dispositivo=$rowDG->descrizione;
	else:
		$id_gruppo_dispositivo=0;
		$descrizione_gruppo_dispositivo="";
	endif;
endif;


echo "<h1><i class=\"fa fa-clock-o\"></i> Palinsesto <small>";
if ($id_dispositivo>0)
	echo "dispositivo ";
echo $this->strimy_model->descrizione_dispositivo($id_dispositivo);
echo "</small></h1>";

echo "<hr>";

$giornoARR[1]="lunedì";
$giornoARR[2]="martedì";
$giornoARR[3]="mercoledì";
$giornoARR[4]="giovedì";
$giornoARR[5]="venerdì";
$giornoARR[6]="sabato";
$giornoARR[7]="domenica";
	

$yy=1;
echo "<ul class=\"nav nav-tabs\">";
foreach ($giornoARR as $giorno_temp):
	if ($yy==$giorno)
		echo "<li class=\"active\">";
	else
		echo "<li>";
	echo "<a href=\"".base_url()."index.php/layout/indice/".$id_dispositivo."/".$yy."\">";
	echo $giorno_temp;
	echo "</a></li>";
	$yy++;
endforeach;
echo "</ul>";


echo "<div id=\"playlist\" class=\"top-buffer\">";




	echo "<form name=\"conferma_layout\" id=\"conferma_layout\" method=\"post\" action=\"".$this->config->item('base_url')."index.php/layout/layout_salva\">";

	echo "<input type=\"hidden\" id=\"check_giorno_successivo\" value=\"0\">";
	echo "<input type=\"hidden\" id=\"egiorno_oggi\" value=\"".$giorno."\">";
	echo "<input type=\"hidden\" id=\"eid_dispositivo\" name=\"eid_dispositivo\" value=\"".$id_dispositivo."\">";
	echo "<input type=\"hidden\" name=\"eid_gruppo_dispositivo\" value=\"".$id_gruppo_dispositivo."\">";
	echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\">";


	// form di salvataggio
	echo "<a data-toggle=\"modal\" data-target=\"#aggiungi_layout\" class=\"btn btn-default\"><i class=\"fa fa-object-group\"></i> Aggiungi layout</a>";
	echo " <a onclick=\"checkform_presave();\" class=\"btn btn-default\"><i class=\"fa fa-floppy-o\"></i> Salva</a>";

		echo "<div id=\"salva_layout\" class=\"modal fade\">";
		echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
		echo "<div class=\"modal-header\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
		echo "<h4 class=\"modal-title\">Salva palinsesto ";
		if ($id_dispositivo>0)
			echo "dispositivo ".$id_dispositivo;
		else
			echo "gruppo ".$rowDG->descrizione;
		echo "</h4>";
		echo "</div>";
		
		echo "<div class=\"modal-body\">";

		// se id_dispositivo è negativo, si tratta di un intero gruppo
		if ($id_dispositivo<0)
			$id_gruppo_dispositivo=$id_dispositivo*-1;
		
		// se $id_gruppo_dispositivo è selezionato, recupera tutti i dispositivi associati
		if ($id_gruppo_dispositivo!=0):
		$dispositivo_gruppo=Array();
		$this->db->from('dispositivi');
		$this->db->where('id_gruppo', $id_gruppo_dispositivo);
		$queryGRP=$this->db->get();
		foreach ($queryGRP->result() as $rowGRP):
			$dispositivo_gruppo[]=$rowGRP->id_dispositivo;
		endforeach;
		
		else:
			$dispositivo_gruppo[]=$id_dispositivo;
		endif;
		
		echo "<h4>Applica palinsesto a...</h4>";
		
		echo "<div style=\"width: 100%; position: relative; float: left; margin-bottom: 10px; clear: both;\">";
		
		// se è stato specificato un intero gruppo, tutti i dispositivi sono automaticamente selezionati e l'elenco sarà nascosto
		echo "<div style=\"float: left; width: 50%; padding-right: 20px;";
		if (($id_dispositivo<0) || ($id_gruppo_dispositivo==0))
			echo " display: none;";
		echo "\">";
		
		if ($id_gruppo_dispositivo!=0)
			echo "Dispositivi del gruppo ".$descrizione_gruppo_dispositivo."<br>";
		$kk=0;
		if ($id_dispositivo<0):
			echo "<input type=\"text\" name=\"eid_dispositivo_gruppo[".$kk."]\" value=\"".$id_dispositivo."\">";
			echo "<input type=\"hidden\" id=\"eid_dispositivo_gruppo_selezionato_".$kk."\" name=\"eid_dispositivo_gruppo_selezionato[".$kk."]\" value=\"1\">";
			echo "<br>";
			$kk++;
		endif;
		
		foreach ($dispositivo_gruppo as $dispositivo_gruppo_temp):
		
			// il dispositivo è automaticamente selezionato se è quello su cui si sta agendo, o se si sta agendo sull'intero gruppo
			if (($id_dispositivo<0) || ($id_dispositivo==$dispositivo_gruppo_temp))
				$dispositivo_selezionato=1;
			else
				$dispositivo_selezionato=0;
				
			echo "<input id=\"edispositivo_gruppo_".$kk."\" type=\"checkbox\"";
			if ($dispositivo_selezionato==1)
				echo " checked=\"checked\"";
			if ($dispositivo_gruppo_temp==$id_dispositivo)
				echo " disabled=\"disabled\"";
			echo " onclick=\"seleziona_dispositivo(this, '".$kk."')\"> ".$dispositivo_gruppo_temp;
			echo "<input type=\"hidden\" name=\"eid_dispositivo_gruppo[".$kk."]\" value=\"".$dispositivo_gruppo_temp."\">";
			echo "<input type=\"hidden\" id=\"eid_dispositivo_gruppo_selezionato_".$kk."\" name=\"eid_dispositivo_gruppo_selezionato[".$kk."]\"";
			if ($dispositivo_selezionato==1)
				echo " value=\"1\"";
			else
				echo " value=\"0\"";
			echo ">";
			echo "<br>";
			$kk++;
		endforeach;
		echo "</div>";

		echo "<div style=\"position: relative; float: left;\">";
		echo "Giorni della settimana<br>";
		$kk=1;
		foreach ($giornoARR as $giornoTEMP):
			echo "<input id=\"eseleziona_giorno_".$kk."\" type=\"checkbox\"";
			if ($kk==$giorno)
				echo " checked=\"checked\" disabled=\"disabled\"";
			echo " onclick=\"seleziona_giorno(this, '".$kk."');\"> ";
			echo $giornoARR[$kk];
			echo "<input type=\"hidden\" id=\"egiorno_selezionato_".$kk."\" name=\"egiorno_selezionato[".$kk."]\" class=\"egiorno_selezionato\"";
			if ($kk==$giorno)
				echo "value=\"1\"";
			else
				echo "value=\"0\"";
			echo ">";
			echo "<br>";
			$kk++;
		endforeach;

		echo "</div>";

		echo "</div>";

		echo "<br>";
		echo "<div class=\"text-right\">";
		echo "<input type=\"button\" value=\"Salva palinsesto\" class=\"btn btn-primary\" onclick=\"return checkform();\">";
		echo "</div>";

		echo "</div>";

		echo "<div class=\"modal-footer\">";
		echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
		echo "</div>";

		echo "</div></div>";
		echo "</div>";


	$timestamp_iniziale=0;
	$timestamp_oggi=$timestamp_iniziale+($giorno-1)*86400;
	// verifica se ci sono layout a cavallo con la mezzanotte precedente
	$differenza_settimana=0;
	$giorno_precedente=$giorno-1;
	if ($giorno_precedente==0):
		$giorno_precedente=7;
		$differenza_settimana=7*86400;
	endif;
	$orario_inizio=$timestamp_oggi;
	$this->db->from('layout_playlist');
	$this->db->where('id_dispositivo', $id_dispositivo);
	$this->db->where('giorno', $giorno_precedente);
	$this->db->where('orario_fine >', ($orario_inizio+$differenza_settimana));
	$this->db->order_by('orario_fine', 'desc');
	$queryPREC=$this->db->get();
	if ($queryPREC->num_rows()>0):
		$rowPREC=$queryPREC->row();
		date_default_timezone_set('UTC');
		echo "<div style=\"color: #666\">";
		echo "Attenzione: &egrave; presente un layout del giorno precedente che termina alle <strong>".date('H:i', $rowPREC->orario_fine)."</strong> odierne";
		echo "</div>";
	endif;

	echo "<div class=\"top-buffer\">";

		echo "<input type=\"hidden\" id=\"enumero_layout\" name=\"enumero_layout\" value=\"0\">";
		echo "<input type=\"hidden\" id=\"enumero_layout_attivi\" name=\"enumero_layout_attivi\" value=\"0\">";
		echo "<div id=\"ultimo_indice\"></div>";
		
		?> <script language="Javascript"> <?php
		
		$this->db->from('layout_playlist');
		$this->db->where('id_account', $grupposelezionato);
		$this->db->where('id_dispositivo', $id_dispositivo);
		$this->db->where('giorno', $giorno);
		$this->db->order_by('ordine', 'asc');
		$queryOPT=$this->db->get();
		if ($queryOPT->num_rows()==0):
		
		else:
			foreach ($queryOPT->result() as $rowOPT):
		?>	aggiungi_layout('<?php echo $rowOPT->id; ?>', '<?php echo $rowOPT->tipologia; ?>'); <?php
			endforeach;
		endif;
		?> </script> <?php

	echo "</div>";

	echo "<input type=\"hidden\" id=\"eform_changed\" name=\"eform_changed\" value=\"0\">"; // questa variabile indica se la form è stata modificata
	echo "<input type=\"hidden\" id=\"form_changed\" name=\"form_changed\" value=\"0\">";  // anche questa variabile indica se la form è stata modificata, ma viene resettata al momento del submit

	echo "</form>";
	
echo "</div>";



?>
<script language="Javascript">
	aggiorna_orari();
</script>
<?php






// form di selezione layout
	
	$layout=$this->config->item('layout');
	$layout_visibility=$this->config->item('layout_visibility');
	$layout_widget=$this->config->item('layout_widget');
	$layout_image=$this->config->item('layout_image');
	$widget=$this->config->item('widget');
	$widget_descrizione=$this->config->item('widget_descrizione');
	
	echo "<div id=\"aggiungi_layout\" class=\"modal fade\">";
	echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
	echo "<div class=\"modal-header\">";
    echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
    echo "<h4 class=\"modal-title\">Aggiungi layout</h4>";
	echo "</div>";
	
	echo "<div class=\"modal-body\">";

	echo "<div style=\"width: 100%; position: relative; float: left; clear: both; margin-bottom: 20px;\">";
	// div selezione
	echo "<div style=\"position: relative; width: 45%; float: left;\">";
	
	echo "Seleziona layout<br>";
	$xx=0;
	$yy=0;
	
	$layout_disponibili=Array();
	$layout_reali_rif=Array();
	foreach ($layout as $layout_temp):
		$zz=array_search($layout_temp, $layout);
		$visualizza[$layout_temp]=1;
		foreach ($layout_widget[$xx] as $layout_widget_temp):
			if ($privilegi[$widget[$layout_widget_temp]]==0)
				$visualizza[$layout_temp]=0;
		endforeach;

		if ((isset($layout_visibility[$zz])) && ($layout_visibility[$zz]==0))
			$visualizza[$layout_temp]=0;

		if ($visualizza[$layout_temp]==1):
			$layout_disponibili[$yy]=$layout_temp;
			$layout_reali_rif[$yy]=$zz;
			$yy++;
		endif;
			
	$xx++;
	endforeach;
	
	$totale_layout_disponibili=$yy;
	
	echo "<select id=\"eseleziona_layout\" onchange=\"cambia_layout();\" class=\"form-control\">";
	$yy=0;
	while ($yy<$totale_layout_disponibili) {
		$xx=$layout_reali_rif[$yy];
		echo "<option value=\"".$xx."\"";
		if ($yy==0) echo " selected";
		echo ">".$layout_disponibili[$yy]."</option>";
		$yy++;
	}
	echo "</select>";

	
	$yy=0;
	while ($yy<$totale_layout_disponibili) {
		$xx=$layout_reali_rif[$yy];
		echo "<div id=\"dettagli_layout_".$xx."\" class=\"dettagli_layout top-buffer\"";
		if ($yy!=0) echo " style=\"display: none;\"";
		echo ">";
		$zz=0;
		foreach ($layout_widget[$xx] as $layout_widget_temp):
			echo "<span class=\"label label-info\" style=\"margin-right: 5px;\">".($zz+1)."</span> <label>".$widget_descrizione[$layout_widget_temp]."</label><br>";
			$zz++;
		endforeach;
		echo "</div>";
		$yy++;
	}
	
	echo "</div>";
	// fine div selezione

	// div anteprima
	echo "<div style=\"float: right; width: 50%; min-height: 181px; background-color: #f9f9f9; position: relative;\">";
	$yy=0;
	while ($yy<$totale_layout_disponibili) {
		$xx=$layout_reali_rif[$yy];
		echo "<div id=\"image_layout_".$xx."\" class=\"image_layout\" style=\"background-image: url('".$this->config->item('base_url')."assets/layout/".$layout_image[$xx]."'); ";
		if ($yy!=0) echo " display: none;";
		echo "\">";
		// if (isset($layout_image[$xx])):
		//	echo "<img src=\"".$this->config->item('base_url')."assets/layout/".$layout_image[$xx]."\" style=\"width: 180px;\">";
		// endif;
		echo "</div>";
		$yy++;
	}
	
	echo "</div>";
	// fine div anteprima
	echo "</div>";
	
	// echo "<br><br><br>";
	echo "<div class=\"text-right\">";
	echo "<input type=\"submit\" value=\"Inserisci layout\" class=\"btn btn-primary\" onclick=\"aggiungi_nuovo_layout()\">";
	echo "</div>";

	echo "</div>";

	echo "<div class=\"modal-footer\">";
    echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
    echo "</div>";

	echo "</div></div>";
	echo "</div>";







?>

<script language="Javascript">
	$('#conferma_layout').change(function(){
		$('#form_changed').val(1);
		$('#eform_changed').val(1);
	});
	
	window.onbeforeunload = function() {
		var form_changed=$('#form_changed').val();
		if (form_changed==1) {
			return 'ATTENZIONE!\nCi sono delle modifiche non salvate!';
				// return true;
		/*	else
				return false; */
		}
				
	};
</script>
