<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
//$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

?>
<script language="Javascript">
	function cancella_gruppo(gruppo) {
		if (confirm('Desideri cancellare la categoria? I dispositivi associati saranno mantenuti.')) {
			var url="<?php echo base_url(); ?>index.php/strimy/gestisci_gruppi_dispositivo_elimina";
			var stringa="eid_gruppo="+gruppo;
			$.ajax({
				url: url, 
				data: stringa,
				async: false,
				dataType: 'html',
				method: 'POST',
				success: function(risultato) {
					if (risultato=="0")
						$("#gruppo_"+gruppo).hide(500);
				}
			});
		}
		return false;
	}

	function verifica_form(gruppo) {
		$(".form-group").removeClass('has-error');
		if ($("#edescrizione_gruppo_"+gruppo).val()=="") {
			$("#form_eid_dispositivo_"+gruppo).addClass('has-error');
			return false;
		}
		else {
			return true;
		}
	}

</script>
<?php

$this->db->from('dispositivi_gruppi');
$this->db->where('id_account', $grupposelezionato);
$this->db->order_by('descrizione', 'asc');
$query=$this->db->get();

foreach ($query->result() as $row):

	echo "<div id=\"gruppo_".$row->id."\">";
	echo "<form id=\"attivazione_".$row->id."\" method=\"post\" class=\"form-horizontal\" action=\"".$this->config->item('base_url')."index.php/strimy/gestisci_gruppi_dispositivo_salva\">";

	echo '<div id="form_eid_dispositivo_'.$row->id.'" class="form-group">
   
    <div class="col-sm-8">
      <input type="text" class="form-control" id="edescrizione_gruppo_'.$row->id.'" name="edescrizione_gruppo" value="'.$row->descrizione.'">
      <input type="hidden" name="eid_gruppo" value="'.$row->id.'">
	</div>
	<div class="col-sm-4">
	<input type="submit" value="aggiorna" class="btn btn-primary" onclick="return verifica_form(\''.$row->id.'\');">
	 <a onclick="return cancella_gruppo(\''.$row->id.'\');" class="btn btn-default"><i class="fa fa-trash"></i></a>
    </div>
  </div>';

	echo "</form>";
	echo "</div>";

endforeach;

echo "<form id=\"attivazione_0\" class=\"form-horizontal\" method=\"post\" action=\"".$this->config->item('base_url')."index.php/strimy/gestisci_gruppi_dispositivo_salva\">";

echo '<div id="form_eid_dispositivo_0" class="form-group">

<div class="col-sm-8">
  <input type="text" class="form-control" id="edescrizione_gruppo_0" name="edescrizione_gruppo" placeholder="descrizione del nuovo gruppo">
  <input type="hidden" name="eid_gruppo" value="0">
</div>
<div class="col-sm-4">
<input type="submit" value="inserisci" class="btn btn-primary" onclick="return verifica_form(\'0\');">
</div>
</div>';

echo "</form>";


?>

