<?php

$mp3_get_tags=$this->config->item('base_url')."php-lib/mp3_get_tags.php";

include($mp3_get_tags);

?>

<script language="javascript">
	
	function upload_file(div) {
		$(".upload_file").hide();
		$("#upload_file_"+div).show(50);
		$(".pulsante_upload_file").show();
		$("#pulsante_upload_file_"+div).hide();
	}

	function attiva_file(id, status) {
		if (status==0) {
			$("#file_attivo0_"+id).hide();
			$("#file_attivo1_"+id).show();
		}
		if (status==1) {
			$("#file_attivo1_"+id).hide();
			$("#file_attivo0_"+id).show();
		}
		
		var stringa = '';
		stringa=stringa+"eid_file="+id+"&edisattiva="+status;
	
		$.ajax({
		  url: "<?php echo base_url(); ?>index.php/mp3/gestione_file_mp3_attiva", 
		  data: stringa,
		  async: true,
		  dataType: 'html',
		  method: 'POST',
		  success: function(risultato) {
			  if (risultato!="")
				alert(risultato);
			  }
		});
		
	}

</script>

<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

$percorso_base=$this->config->item('real_path');
$percorso_users=$this->config->item('percorso_users');

$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/content";
$url_base="/strimy/users/".$stringa_casuale."/content";

$chiave=$this->config->item('chiave');
$genere=$this->config->item('genere');

if (!isset($tipologia_file))
	$tipologia_file="audio";

if ($tipologia_file=="audio"):
	$tipo_file=0;
	$descrizione_pulsante="Aggiungi brano";
	$pulsante_classifica=1;
	$titolo="Archivio Radio";
elseif ($tipologia_file=="video"):
	$tipo_file=1;
	$descrizione_pulsante="Aggiungi video";
	$pulsante_classifica=0;
	$titolo="Archivio MusicTV";
endif;

echo "<h1>".$titolo."</h1>";

echo '<div class="btn-group">';
echo "<a href=\"/index.php/mp3/gestione_file_mp3/gestisci_brano/0/".$tipologia_file."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><button type=\"button\" class=\"btn btn-default fa fa-cloud-upload\">".$descrizione_pulsante."</button></a>";
if ($pulsante_classifica==1)
	echo "<a href=\"".base_url()."index.php/mp3/gestione_file_mp3/carica_classifica/0\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><button type=\"button\" class=\"btn btn-default fa fa-bar-chart\">Importa classifica</button></a>";
echo "<a href=\"".base_url()."index.php/mp3/gestione_file_mp3/gestisci_categorie/0/".$tipologia_file."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><button type=\"button\" class=\"btn btn-default fa fa-tags\">Gestisci categorie jingle</button></a>";
echo "</div>";


if ($risultato==1):
	echo "<div id=\"messaggio_alert\">";
	echo $messaggio;
	echo "</div>";
endif;

if ($risultato==2):
	echo "<div id=\"messaggio_conferma\">";
	echo $messaggio;
	echo "</div>";
endif;



echo "<br>";


$giorni_tolleranza_classifica=210;
$data_oggi=time();


$this->db->from('archivio_radio');
$this->db->where('id_account', $grupposelezionato);
$this->db->where('tipologia_file', $tipo_file);
$query=$this->db->get();
$totale_numero_brani=$query->num_rows();

$this->db->from('archivio_radio');
$this->db->where('id_account', $grupposelezionato);
$this->db->where('nome_file !=', '');
$this->db->where('disattivo', 0);
$this->db->where('jingle', 0);
$this->db->where('punteggio !=', 0);
$this->db->where('tipologia_file', $tipo_file);
$query=$this->db->get();
$totale_numero_brani_classifica=$query->num_rows();

$this->db->from('archivio_radio');
$this->db->where('id_account', $grupposelezionato);
$this->db->where('jingle', 1);
$this->db->where('tipologia_file', $tipo_file);
$query=$this->db->get();
$totale_numero_brani_jingle=$query->num_rows();

$this->db->from('archivio_radio');
$this->db->where('id_account', $grupposelezionato);
$this->db->where('nome_file !=', '');
$this->db->where('disattivo', 0);
$this->db->where('jingle', 0);
$this->db->where('tipologia_file', $tipo_file);
$query=$this->db->get();
$totale_numero_brani_presenti=$query->num_rows();

$this->db->from('archivio_radio');
$this->db->where('id_account', $grupposelezionato);
$this->db->where('nome_file !=', '');
$this->db->where('disattivo', 0);
$this->db->where('jingle', 0);
$this->db->where('tipologia_file', $tipo_file);
$query=$this->db->get();
$totale_numero_brani_attivi=$query->num_rows();

$this->db->from('archivio_radio');
$this->db->where('id_account', $grupposelezionato);
$this->db->where('nome_file !=', '');
$this->db->where('disattivo', 0);
$this->db->where('data_classifica <', ($data_oggi-$giorni_tolleranza_classifica*86400));
$this->db->where('jingle', 0);
$this->db->where('tipologia_file', $tipo_file);
$query=$this->db->get();
$totale_numero_brani_datati=$query->num_rows();


$dataIMP['tipologia_file']=$tipologia_file;
$dataRES=$this->mp3_model->mp3_archivio_recupera($dataIMP);

$xx=0;


	
$this->db->from('archivio_radio_classifica');
$this->db->where('id_account', $grupposelezionato);
$queryUA=$this->db->get();

if ($queryUA->num_rows()>0):
	$rowUA=$queryUA->row();
	echo "<div id=\"messaggio_stato\" style=\"margin-top: -80px;\">";
	echo "<span style=\"font-size: 0.8em\">";
	echo "Totale di <b>".$totale_numero_brani."</b> elementi in archivio, di cui:<br>";
	echo "<b>".($totale_numero_brani-$totale_numero_brani_jingle)."</b> brani e <b>".$totale_numero_brani_jingle."</b> jingle<br>";
	echo "<b>".$totale_numero_brani_attivi."</b> brani disponibili per la playlist<br>";
	echo "<b>".$totale_numero_brani_classifica."</b> brani in classifica e <b>".($totale_numero_brani_presenti-$totale_numero_brani_classifica)."</b> fuori classifica<br>";
	echo "<span style=\"color: red\"><b>".$totale_numero_brani_datati."</b> brani in classifica da pi&ugrave; di ".$giorni_tolleranza_classifica." giorni</span><br>";
	// echo "<span style=\"color: red\">I brani evidenziati sono presenti in classifica da pi&ugrave; di ".$giorni_tolleranza_classifica." giorni.</span> ";
	echo "Ultimo aggiornamento classifica: <strong>".date('d/m/y', $rowUA->ultimo_aggiornamento)."</strong>";
	echo "</span>";
	echo "</div>";
endif;


// echo "<div class=\"sequenza\" style=\"position: relative;\">";


if ($dataRES['numero_risultati']>0):

	echo "<table class=\"table table-striped\">";
	echo "<thead><tr>";
	echo "<th></th>";
	echo "<th></th>";
	echo "<th>artista</th>";
	echo "<th>titolo</th>";
	echo "<th>inserimento in classifica</th>";
	echo "<th>punteggio</th>";
	echo "<th>genere</th>";
	echo "<th>b.p.m.</th>";
	echo "<th>chiave</th>";
	echo "<th>emozione</th>";
	echo "<th>data di inserimento</th>";
	echo "<th>ultima modifica</th>";
	echo "<th colspan=\"2\"></th></tr></thead>";

	echo "<tbody>";
	while ($xx<$dataRES['numero_risultati']) {
		
		echo "<tr>";
		
		if ($dataRES['jingle'][$xx]==0):
			echo "<td>".($xx+1)."</td>";
			echo "<td>";
			if ($tipologia_file=="audio")
				echo "<i class=\"fa fa-music\" style=\"width: 20px;\"></i>";
			if ($tipologia_file=="video")
				echo "<i class=\"fa fa-video-camera\" style=\"width: 20px;\"></i>";
			echo "</td>";
			echo "<td><strong>".$dataRES['artista'][$xx]."</strong></td>";
			echo "<td>".$dataRES['titolo'][$xx]."</td>";
			echo "<td>";
			if ($dataRES['data_classifica_raw'][$xx]>0)
				echo $dataRES['data_classifica'][$xx];
			echo "</td>";
			echo "<td>";
			if ($dataRES['punteggio'][$xx]!=0)
				echo $dataRES['punteggio'][$xx];
			echo "</td>";
			echo "<td>";
			if ($dataRES['genere_1'][$xx]!=0)
				echo $genere[$dataRES['genere_1'][$xx]];
			if ($dataRES['genere_2'][$xx]!=0)
				echo "/".$genere[$dataRES['genere_2'][$xx]];
			if ($dataRES['genere_3'][$xx]!=0)
				echo "/".$genere[$dataRES['genere_2'][$xx]];
			if ($dataRES['genere_4'][$xx]!=0)
				echo "/".$genere[$dataRES['genere_4'][$xx]];
			echo "</td>";
			echo "<td>";
			if ($dataRES['bpm'][$xx]!=0)
				echo $dataRES['bpm'][$xx];
			echo "</td>";
			echo "<td>";
			if ($dataRES['chiave'][$xx]!=0)
				echo $dataRES['chiave_testo'][$xx];
			echo "</td>";
			echo "<td>";
			if ($dataRES['power'][$xx]!=0)
				echo $dataRES['power'][$xx];
			echo "</td>";
			echo "<td>";
			if ($dataRES['data_inserimento'][$xx]!=0)
				echo "<small>".date('d/m/y', $dataRES['data_inserimento'][$xx])."</small>";
			echo "</td>";
			echo "<td>";
			if ($dataRES['data_modifica'][$xx]!=0)
				echo "<small>".date('d/m/y', $dataRES['data_modifica'][$xx])."</small>";
			echo "</td>";
			
			echo "<td>";
			$file_mp3=$percorso_base.$percorso_users.$stringa_casuale."/radio/".$dataRES['file'][$xx];
			if (file_exists($file_mp3))
				$id3_tags = mp3_get_tags($file_mp3);
			echo "</td>";

			
		else:
			echo "<td>".($xx+1)."</td>";
			echo "<td><i class=\"fa fa-microphone\" style=\"width: 20px;\"></i></td>";
			echo "<td><strong>".$dataRES['file'][$xx]."</strong></td>";
			echo "<td colspan=\"4\">";
			
			$desc_jingle=trim($dataRES['artista'][$xx]." - ".$dataRES['titolo'][$xx]);
			if ($desc_jingle!="-")
				echo $desc_jingle."<br>";
			
			$this->db->from('archivio_radio_jingle_categorie');
			$this->db->where('id', $dataRES['jingle_categoria'][$xx]);
			$this->db->where('id_account', $grupposelezionato);
			$queryCAT=$this->db->get();
			if ($queryCAT->num_rows()>0):
				$rowCAT=$queryCAT->row();
				echo "<small>categoria: ".$rowCAT->descrizione."</small>";
			endif;
			echo "</td>";
			echo "<td colspan=\"5\"></td>";
		endif;
		
		echo "<td>";
		echo "<a href=\"/index.php/mp3/gestione_file_mp3/gestisci_brano/".$dataRES['id_file'][$xx]."/".$tipologia_file."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><i class=\"fa fa-pencil\" style=\"width: 20px;\"></i></a> ";
		echo "<a href=\"".base_url()."index.php/mp3/gestione_file_mp3_cancella/".$dataRES['id_file'][$xx]."/".$tipologia_file."\" onClick=\"return confirm('Confermi l\'eliminazione?');\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
		
		echo "<span id=\"file_attivo0_".$dataRES['id_file'][$xx]."\"";
		if ($dataRES['disattivo'][$xx]==0) echo " style=\"display: none;\"";
		echo ">";
		echo "<a href=\"javascript:attiva_file('".$dataRES['id_file'][$xx]."','0');\">";
		echo "<i class=\"fa fa-square-o\" style=\"width: 20px;\"></i>";
		echo "</a>";
		echo "</span>";
		
		echo "<span id=\"file_attivo1_".$dataRES['id_file'][$xx]."\"";
		if ($dataRES['disattivo'][$xx]==1) echo " style=\"display: none;\"";
		echo ">";
		echo "<a href=\"javascript:attiva_file('".$dataRES['id_file'][$xx]."','1');\">";
		echo "<i class=\"fa fa-check-square-o\" style=\"width: 20px;\"></i>";
		echo "</a>";
		echo "</span>";

		echo "</td>";
		
		echo "<td>";
		if ($dataRES['presente'][$xx]==0)
			echo "<span style=\"color: red;\"><i class=\"fa fa-exclamation-circle\" style=\"width: 20px;\"></i></span>";
		else	
			echo "<a href=\"".base_url()."index.php/mp3/player/".$dataRES['id_file'][$xx]."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><i class=\"fa fa-play\" style=\"width: 20px;\"></i></a>";
			// echo "<a href=\"".base_url()."index.php/mp3/player/".$dataRES['id_file'][$xx]e."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><i class=\"fa fa-pencil\" style=\"width: 20px;\"></i></a> ";
		echo "</td>";

		echo "</tr>";
		
	$xx++;	
	}

	echo "</tbody>";
	echo "</table>";

else:

	echo "Archivio vuoto";

endif;

// echo "</div>";

?>

