<?php

$current_url=$this->uri->segment(1)."/".$this->uri->segment(2);
$login=$this->flexi_auth->is_logged_in();

if (($current_url!="/") && ($this->uri->segment(1)!="pub") && ($current_url!="users/login") && ($current_url!="users/logout") && (!$login)):
	redirect("users/login");
elseif ($login):
	$gruppo=$this->flexi_auth->get_user_group_id();
	$tipologia=$this->strimy_model->account_tipologia($gruppo);
	$grupposelezionato=$gruppo;
	$utente=$this->flexi_auth->get_user_id();
	$amministratore=$this->strimy_model->utente_tipologia($utente);
	$privilegi=$this->users_model->account_privilegi($grupposelezionato);
	
endif;

if (!isset($gruppo) || ($gruppo==""))
	$gruppo=0;

if ( (!isset($grupposelezionato)) || ($grupposelezionato==0) )
	$grupposelezionato=$gruppo;


if ($this->config->item('base_url')=="http://www.westrim.it/"): // è il portale di Westrim
	$site_title="Westrim by Strimy";
	$site_logo="logo_web_westrim.png";

elseif ($this->config->item('base_url')=="http://test.strimy.it/"):	// è il portale di test
	$site_title="StrimyTest (developer environment)";
	$site_logo="logo_web.png";

else: // è il portale di Strimy
	$site_title="Strimy";
	$site_logo="logo_web.png";

endif;


$crediti_residui=$this->users_model->crediti_residui($grupposelezionato);

?>

<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->


<html xmlns="http://www.w3.org/1999/xhtml" lang="it">
<head>

<title><?php echo $site_title; ?></title>


  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
  <!-- Fogli di stile -->
  
  <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/bootstrap/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/js/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/plugins/flexslider/flexslider.css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/bootstrap/css/bootstrap.custom.css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/bootstrap/css/bootstrap-sortable.css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/plugins/fancybox/jquery.fancybox.css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>assets/css/stili-custom.css" />

  <link type="text/css" href="<?php echo $this->config->item('base_url'); ?>js/jplayer/skin/blue.monday/scss/jplayer.blue.monday.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo $this->config->item('base_url'); ?>js/cropper-master/dist/cropper.css" rel="stylesheet" />

  <!-- respond.js per IE8 -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>style/font-awesome/css/font-awesome.min.css">




 <script src="http://code.jquery.com/jquery.js"></script>
 <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>js/jquery-ui.min.js"></script>
 <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>js/cropper-master/dist/cropper.min.js"></script>
 <script src="<?php echo $this->config->item('base_url'); ?>assets/bootstrap/js/bootstrap.min.js"></script>
 <script src="<?php echo $this->config->item('base_url'); ?>assets/bootstrap/plugin/bootstrap-hover-dropdown.min.js"></script>
 <script src="<?php echo $this->config->item('base_url'); ?>assets/bootstrap/plugin/bootstrap-sortable.js"></script>
 


<script language="Javascript">
$(document).on('hidden.bs.modal', function (e) {
    $(e.target).removeData('bs.modal');
    // chiude le istanze di jPlayer, se presenti
    if ($('#jquery_jplayer_1').length) {
		$('#jquery_jplayer_1').jPlayer('destroy');
	}
});

function spazio_account() {
	$('#span_occupazione').load("<?php echo base_url(); ?>index.php/users/spazio_account");
}

function messaggio_errore(messaggio) {
	$('#emessaggio_conferma').hide();
	$('#emessaggio_errore').html(messaggio);
	$('#emessaggio_errore').fadeIn(500);	
}

function messaggio_conferma(messaggio) {
	$('#emessaggio_errore').hide();
	$('#emessaggio_conferma').html(messaggio);
	$('#emessaggio_conferma').fadeIn(500);	
}

</script>

</head>

<body>

<div class="container">

<!-- <header> -->
<nav class="navbar navbar-default navbar-collapse navbar-responsive-collapse navbar-fixed-top">
	<div class="container-fluid">
	
		<div class="navbar-header">
			<a href="<?php echo $this->config->item('base_url'); ?>"><img src="<?php echo $this->config->item('base_url'); ?>style/images/<?php echo $site_logo; ?>"></a>
		</div> 
 
  
		<div class="collapse navbar-collapse navbar-responsive-collapse">

			<?php if ($login): ?>

			<ul class="nav navbar-nav">			
			
				<li><a class="voce_menu_header a" href="<?php echo $this->config->item('base_url'); ?>index.php/strimy/frontpage/"><i class="fa fa-cubes"></i>dashboard</a></li>
			   
			   
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
						<i class="fa fa-cloud"></i>contenuti<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
					   
					   	<?php if (($tipologia==0) && ($amministratore==1)): ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/strimy/gestione_archivio_sistema/"><i class="fa fa-gear"></i>Archivio di sistema</a></li>
						<?php endif; ?>
					   
						<?php if ($privilegi[2]==1): ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/strimy/gestione_archivio/"><i class="fa fa-folder-open-o"></i>Archivio generale</a></li>
						<?php endif; ?>
						<?php if ($privilegi[0]==1): ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/mp3/gestione_radio/audio"><i class="fa fa-music"></i>Radio</a></li>
						<?php endif; ?>
						<?php if ($privilegi[1]==1): ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/mp3/gestione_radio/video"><i class="fa fa-video-camera"></i>MusicTV</a></li>
						<?php endif; ?>

					</ul>
				</li>  

			 	<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
						<i class="fa fa-list"></i>playlist<b class="caret"></b>
					</a>
			 		<ul class="dropdown-menu">
					
					<?php
						$widget=$this->config->item('widget');
						$widget_descrizione=$this->config->item('widget_descrizione');
						$widget_icona=$this->config->item('widget_icona');
						foreach ($widget as $widget_temp):
							if ($privilegi[$widget_temp]==1):
								$descrizione=array_search($widget_temp, $widget);
								
								if ($widget_temp==2):  // è un widget di tipo Slideshow: deve visualizzare i sottomenù
									echo "<li class=\"dropdown-submenu\">";
									echo "<a href=\"#\">";
									echo "<i class=\"fa ".$widget_icona[$descrizione]."\"></i>".$widget_descrizione[$descrizione]."</a>";
								
										echo "<ul class=\"dropdown-menu\">";
										echo "<li><a href=\"".$this->config->item('base_url')."index.php/strimy/gestione_playlist/".$widget_temp."/2\">";
										echo "<i class=\"fa fa-photo\"></i>immagini</a></li>";
										echo "<li><a href=\"".$this->config->item('base_url')."index.php/strimy/gestione_playlist/".$widget_temp."/1\">";
										echo "<i class=\"fa fa-video-camera\"></i>video</a></li>";
										echo "<li><a href=\"".$this->config->item('base_url')."index.php/strimy/gestione_playlist/".$widget_temp."/0\">";
										echo "<i class=\"fa fa-music\"></i>audio</a></li>";
										echo "</ul>";
									echo "</li>";
								
								else:	// è un widget di altro tipo
									echo "<li>";
									echo "<a href=\"".$this->config->item('base_url')."index.php/strimy/gestione_playlist/".$widget_temp."\">";
									echo "<i class=\"fa ".$widget_icona[$descrizione]."\"></i>".$widget_descrizione[$descrizione];
									echo "</a>";
									echo "</li>";
								endif;
								
								
							endif;
						endforeach;
						
					?>
					
					</ul>
					
				</li>
			 
			 
		<!--		<li><a class="voce_menu_header a" href="<?php echo $this->config->item('base_url'); ?>index.php/strimy/programmazione/"><i class="fa fa-clock-o"></i>programmazione</a></li> -->
	
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
						<i class="fa fa-clock-o"></i>palinsesto<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
					   	
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/layout/programmazione/"><i class="fa fa-clock-o"></i>Tutti i dispositivi</a></li>

						<?php
						$this->db->from('dispositivi_gruppi');
						$this->db->where('id_account', $grupposelezionato);
						$queryGRUPPO=$this->db->get();
						if ($queryGRUPPO->num_rows()>0):
							echo "<li class=\"dropdown-submenu\">";
							echo "<a href=\"#\">";
							echo "<i class=\"fa fa-cubes\"></i>Gruppi</a>";
							
							echo "<ul class=\"dropdown-menu\">";
							foreach ($queryGRUPPO->result() as $rowGRUPPO):
							
								echo "<li><a href=\"".$this->config->item('base_url')."index.php/layout/programmazione/".$rowGRUPPO->id."\">";
								echo "<i class=\"fa fa-clock-o\"></i>".$rowGRUPPO->descrizione."</a></li>";
							
							endforeach;
							echo "</ul>";
							echo "</li>";
							
						endif;
						?>

					</ul>
				</li>  

			 
				<?php if ($amministratore==1): ?>
				<!-- <li><i class="fa fa-desktop"></i> <a href="<?php echo $this->config->item('base_url'); ?>index.php/strimy/gestione_archivio_sistema/">sistema</a></li> -->
				<?php endif; ?>

			</ul>

			<?php
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $gruppo);
			$queryGRP=$this->db->get();
			$rowGRP=$queryGRP->row();
			?>
			<ul class="nav navbar-nav navbar-right">
			
				<li><a class="voce_menu_header a" href="<?php echo $this->config->item('base_url'); ?>assets/download/strimy_guida_rapida.pdf" target="_blank"><i class="fa fa-question-circle"></i>guida rapida</a></li>
			
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
						<i class="fa fa-gear"></i><?php echo $rowGRP->ugrp_name; ?><b class="caret"></b>
					</a>
					<ul class="dropdown-menu">

					<li style="font-size: 0.8em; width: 200px; padding: 10px;">
						<?php
							echo "<table class=\"table table-condensed\" style=\"margin: 0px\">";
							echo "<tr><td><i class=\"fa fa-user\"></i></td><td>".$this->flexi_auth->get_user_identity()."</td></tr>";
							echo "<tr><td><i class=\"fa fa-database\"></i></td><td><strong><big><big><big><big>".$crediti_residui."</big></big></big></big></strong> crediti</td></tr>";
							echo "<tr><td><i class=\"fa fa-cloud\"></i></td><td><span id=\"span_occupazione\"></span></td></tr>";
							echo "</table>";
						?>
					</li>

					<li><a style="padding: 0px;"></a></li>
					
					<?php if ($amministratore==1): ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/users/gestione_account/"><i class="fa fa-euro"></i>crediti e fatturazione</a></li>
					<?php endif; ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/users/indice_utenti/"><i class="fa fa-users"></i>utenti</a></li>

					<?php if (($tipologia==0) && ($amministratore==1)): ?>
						<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/users/indice_account/"><i class="fa fa-suitcase"></i>account</a></li>  
					<?php endif; ?>

					<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/users/logout/" onclick="return confirm('Desideri uscire?');"><i class="fa fa-sign-out"></i>logout</a></li>
					
					</ul>
				</li>

			</ul>

		
		
			<?php elseif (!$login): ?>
			<ul class="nav navbar-nav navbar-right">
			<li><a href="<?php echo $this->config->item('base_url'); ?>index.php/users/login/"><i class="fa fa-sign-in"></i>login</a></li>
			</ul>
			<?php endif; ?>

		</div>

 
 </div><!-- /.containter-fluid -->
 

</nav><!-- /.navbar -->



<script language="Javascript">
	spazio_account();
</script>

<div class="container" style="position: relative; clear: both; top: 65px; margin-bottom: 100px">

