</div>  <!-- chiusura container interno -->


</div> <!-- chiusura container esterno -->


<!-- inizio finestra modale -->
<div id="finestra-modale" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-body"></div>
 
  </div>
 </div>
</div>
<!-- fine finestra modale -->



<?php /////////////////// SCRIPT PER IL DISCLAIMER SULLA COOKIE LAW
// il file rpc_sito_visitato.php (che si occupa di memorizzare il cookie di assenso) va posizionato nella root del sito

$cookie_pagina_informazioni=$this->config->item('base_url')."index.php/pub/privacy";
// indica la pagina di destinazione per l'informativa completa, se esiste
// (non modificare null'altro)

$cookie_sito_visitato="";
if (isset($_COOKIE['sito_visitato']))
	$cookie_sito_visitato=$_COOKIE['sito_visitato'];
if ($cookie_sito_visitato==""):
?>
<br><br>
<div id="cookie_sito_visitato" style="display: none; position: fixed; bottom: 0; float: left; width: 100%; text-align: center; background-color: #eee; color: #222; opacity: 0.95; fint-size: 0.9em; padding: 8px; border-top: 4px solid black;">
Utilizziamo i cookie per raccogliere statistiche e migliorare l'esperienza di navigazione. Proseguendo nel sito ne accetti le condizioni
<input type="button" style="background-color: #333; color: white; border-radius: 5px; padding: 5px 10px 5px 10px; margin-left: 10px;" value="OK" onclick="cookie_sito_visitato();">
<?php if ($cookie_pagina_informazioni!=""): ?>
<input type="button" style="background-color: #333; color: white; border-radius: 5px; padding: 5px 10px 5px 10px; margin-left: 10px;" value="maggiori informazioni" onclick="cookie_maggiori_informazioni()";>
<?php endif; ?>
</div>

<script language="javascript">
	
	$('#cookie_sito_visitato').slideUp();

	function cookie_sito_visitato() {
		var stringa="";
		$.ajax({
		  url: "/rpc_sito_visitato.php?visitato=ok", 
		  data: stringa,
		  async: true,
		  dataType: 'html',
		  method: 'POST',
		  success: function(risultato) {
			  if (risultato=="")
				$('#cookie_sito_visitato').slideUp();
			  }
		});
	}
	
	function cookie_maggiori_informazioni() {
		var url="<?php echo $cookie_pagina_informazioni; ?>";
		location.href=url;
	}

</script>

<?php endif;
/////////////////// FINE SCRIPT ?>


<footer class="footer">
  <div class="container">
	<p class="text-muted text-center"><strong>Strimy</strong> by Atcetera - &copy; 2014-<?php echo date("Y");?></p>
  </div>
</footer>

</body>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-1085974-16', 'auto');
	  ga('set', 'anonymizeIp', true);   
	  ga('send', 'pageview');

	</script>

</html>
