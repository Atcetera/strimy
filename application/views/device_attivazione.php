<script language="Javascript">

function checkform() {

	var messaggio="";

	$(".form-group").removeClass('has-error');
    if ($("#elocalita").val()=="")
			$("#form_elocalita").addClass('has-error');

	$.ajax({

	  url: "<?php echo base_url() ?>index.php/strimy/dispositivo_attivazione", 
	  data: $("#attivazione").serialize(),
	  async: false,
	  dataType: 'html',
	  method: 'POST',
	  success: function(risultato) {
			if (risultato=="0") {
				$(location).attr('href',"<?php echo base_url() ?>index.php/strimy/frontpage");
			}
			else if ((risultato=="1") || (risultato=="2")) {
				errore=1;
				messaggio="Codice dispositivo non specificato o non valido";
				$("#form_eid_dispositivo").addClass('has-error');
				return false;
			}
			else if (risultato=="3") {
				errore=1;
				messaggio="ATTENZIONE: dispositivo già attivato!";
				$("#form_eid_dispositivo").addClass('has-error');
				return false;
			}
			else if (risultato=="4") {
				errore=1;
				messaggio="Non &egrave; stata indicata la localit&agrave;";
				$("#form_elocalita").addClass('has-error');
				return false;
			}
			else {
				errore=1;
				messaggio="Errore generico. Contattare il supporto tecnico";
				return false;
			}
			
	  }
	});
	
	if (errore==1)
		messaggio_errore(messaggio);
	
	return false;

	}

</script>

<?php

$grupposelezionato=$this->strimy_model->accountselezionato();

echo "<form id=\"attivazione\" class=\"form-horizontal\">";

echo '<div id="form_eid_dispositivo" class="form-group">
    <label for="eid_dispositivo" class="col-sm-2 control-label">Codice</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="eid_dispositivo" name="eid_dispositivo" placeholder="inserisci il codice mostrato sullo schermo">
    </div>
  </div>';

echo '<div id="form_elocalita" class="form-group">
    <label for="elocalita" class="col-sm-2 control-label">Localit&agrave;</label>
    <div class="col-sm-10">
      <input type="text" class="form-control has-error" id="elocalita" name="elocalita" placeholder="la citt&agrave; in cui &egrave; installato il dispositivo">
    </div>
  </div>';

echo '<div class="form-group">
    <label for="edescrizione" class="col-sm-2 control-label">Descrizione</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="edescrizione" name="edescrizione">
    </div>
  </div>';


echo "<input type=\"hidden\" name=\"eaccount\" value=\"".$grupposelezionato."\" readonly=\"readonly\">";
echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";

echo "<span id=\"emessaggio_errore\" class=\"messaggio_errore\" style=\"float: right\"></span><br>";
echo "<div class=\"text-right\">";
echo "<input type=\"button\" value=\"conferma attivazione\" class=\"btn btn-primary\" onClick=\"checkform('modulo');\">";
echo "</div>";

echo "</form>";

?>

