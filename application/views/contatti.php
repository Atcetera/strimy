<?php
/*
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">-->
<html>
  <head>
    <title>Civico</title>
    <meta http-equiv="content-language" content="it" />
    <meta name="revisit-after" content="7 days" />
    <link href="../template/style_lp.css" rel="stylesheet" type="text/css" >

    <!--[if IE 6]>
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <![endif]-->

    <!--[if IE 6]>
    <link href="./style_lp_ie.css" rel="stylesheet" type="text/css" >
    <![endif]-->
*/
?>

	<style>
		span.avviso{
			visibility: hidden;
			font-size: 0.6em;
		//	display: none;
			color: red;
		}
		
		td.tabletd {
			height: 10px;
			vertical-align: top;
		}
	</style>


<script language="Javascript">
function resetavvisi() {
	$('span#avvisonome').css("visibility", "hidden");
	$('span#avvisoemail').css("visibility", "hidden"); 
	$('span#avvisoprivacy').css("visibility", "hidden");
	}

function checkform() {

	enome=this.document.getElementById('enome');
	eazienda=this.document.getElementById('eazienda');
	email=this.document.getElementById('email');
	etelefono=this.document.getElementById('etelefono');
	emessaggio=this.document.getElementById('emessaggio');
	eprivacy=this.document.getElementById('eprivacy');
	einvia=this.document.getElementById('einvia');
	eannulla=this.document.getElementById('eannulla');

	resetavvisi();
	var colore_errore='#ff6666';
	var newsletter=0;
	errore=0;
	
	risultato="Attenzione: vi preghiamo di verificare i dati inseriti\n\n";
	var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$/; 

	if (enome.value=='') {
	errore = 1;
	enome.style.borderColor=colore_errore;
	$('span#avvisonome').css("visibility", "visible");  
	}
	if (emessaggio.value=='') {
	errore = 1;
	emessaggio.style.borderColor=colore_errore;
	$('span#avvisomessaggio').css("visibility", "visible");  
	}
	if (email.value.search(emailFilter) == -1) {
	errore=1;
   	email.style.borderColor=colore_errore;
   	$('span#avvisoemail').css("visibility", "visible");  
	}
	if (!echeckprivacy.checked) {
	errore = 1;
	$('span#avvisoprivacy').css("visibility", "visible");  
	}

    if (errore==1) {
		return false;
	}
	
	else {

//		einvia.style.backgroundColor="#AA2222";
		einvia.disabled = true;
		eannulla.disabled = true;

		var ajax;

		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
		    ajax = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
		    ajax = new ActiveXObject("Microsoft.XMLHTTP");
		}

		ajax.open("POST", "<?php echo base_url() ?>index.php/pub/contatti_invia", false);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		ajax.setRequestHeader("connection", "close");
		var link;
	
		link = "";
		link += "&enome=" + enome.value;
		link += "&eazienda=" + eazienda.value;
		link += "&email=" + email.value;
		link += "&etelefono=" + etelefono.value;
		link += "&emessaggio=" + emessaggio.value;
		link += "&econferma=ok";

		ajax.send(link);
		risultato=ajax.responseText;

		if (risultato==0) {
			$('div#formcompila').hide(0);
			$('div#formconferma').fadeIn(100);
			$.fancybox.center();
		}
		if (risultato==1) {
			alert("Si è verificato un problema nell'invio del messaggio.\nRiprova fra qualche minuto");
		}

		if ((risultato!=0) && (risultato!=1))
			alert(risultato);

//		einvia.style.backgroundColor="#AA2222";
		einvia.disabled = false;
		eannulla.disabled = false;

		return false;
	
	}

}


function chiudi() {
	$.fancybox.close();
}

</script>

<?php
/*
</head>


*/
?>


<div id="divform" style="margin: 0px;">
<div id="formcompila" style="margin-left: 10px; margin-right: 15px;">
<span id="descrizione_popup">
<h3>Sei interessato a <?php echo $this->config->item('nome_prodotto'); ?>?</h3>

<p style="margin-bottom: 8px; text-align: left;">
Inviaci i tuoi dati: saremo felici di rispondere alle tue domane e di tenerti<br>informato sugli sviluppi del serivizo.<br><br>
</p>
</span>
<form name="modulo" id="modulo">
<table>

<tr><td style="200px" class="tabletd" nowrap><p>Nome *</p></td><td style="200px" class="tabletd" nowrap><input name="enome" id="enome" class="forminput" maxlength="30" style="width: 300px;" onFocus="this.style.backgroundColor='white'"> <span class="avviso" id="avvisonome">il campo &egrave; obbligatorio</span></td></tr>

<tr><td style="200px" class="tabletd" nowrap><p>Azienda</p></td><td style="200px" class="tabletd" nowrap><input name="eazienda" id="eazienda" class="forminput" maxlength="30" style="width: 300px;" onFocus="this.style.backgroundColor='white'"> <span class="avviso" id="avvisoazienda">il campo &egrave; obbligatorio</span></td></tr>

<tr><td style="200px" class="tabletd" nowrap><p>Telefono</p></td><td style="200px" class="tabletd" nowrap><input name="etelefono" id="etelefono" class="forminput" maxlength="30" style="width: 300px;" onFocus="this.style.backgroundColor='white'"> <span class="avviso" id="avvisotelefono">il campo &egrave; obbligatorio</span></td></tr>

<tr><td class="tabletd" nowrap><p>E-mail *</p></td><td class="tabletd"><input name="email" id="email" class="forminput" style="width: 300px;" maxlength="100" onFocus="this.style.backgroundColor='white'"> <span class="avviso" id="avvisoemail">l'indirizzo non &egrave; corretto</span>
</td></tr>

<tr><td class="tabletd">Messaggio *</td><td class="tabletd"><textarea name="emessaggio" id="emessaggio" class="forminput" cols="34" rows="10" style="width: 500px; resize: none;" onFocus="this.style.backgroundColor='white'"></textarea><br><span class="avviso" id="avvisomessaggio">e&grave; necessario inserire il testo del messaggio</span></td></tr>

<tr><td class="tabletd" colspan="2">
<p style="font-size: 11px;">I campi contrassegnati dall'asterisco sono obbligatori<br>
<input type="checkbox" name="echeckprivacy" id="echeckprivacy" style="position: relative; top: 2px;"> dichiaro di aver preso visione dell'<a href="<?php echo base_url() ?>index.php/pub/privacy" target="_blank">informativa sulla privacy</a><br>
<span class="avviso" id="avvisoprivacy" style="top: -5px; left: 20x; display: block; font-size: 1em">&egrave; obbligatorio prendere visione dell'informativa</span>
</p></td></tr>

</table>

<p align="center" style="margin-top: 0px; margin-bottom: 0xp;">
<input type="button" value="annulla" name="eannulla" id="eannulla" class="formsubmit" style="margin-right: 50px; color: #999;" onClick="chiudi();">
<input type="button" value="invia" name="einvia" id="einvia" class="formsubmit" onClick="checkform('modulo');">
</p>
</form>
</div>


<div id="formconferma" style="display: none;">
<p>
Grazie per averci contattato!<br>
Provvederemo a risponderti quanto prima.
</p>

<p align="center" style="margin-top: 30px; margin-bottom: 0xp;">
<input type="button" value="chiudi" class="formsubmit" style="color: #999;" onClick="chiudi();">
</p>

<br><br><br><br>

</div>


</div>

<?php
/*
</body>
</html>
*/
?>
