<script language="Javascript">
 jQuery(document).ready(function () {
	jQuery('a#contatti').colorbox({width: "600px", closeButton: false, scrolling: false});
});

</script>

<style type="text/css"> 
   body {background: black;}
   
  </style>

<div class="contenitore_paragrafo" style="padding: 0;">
	
	<div class="paragrafo" style="width: 960px;">
		<!-- <table class="table">
			<tr>
			<td><img src="<?php echo base_url() ?>/style/images/monitor_speakers.png"></td>
			<td><div style="margin-left: 70px; font-size: 1.3em;"><span style="font-size: 1.5em; color: white; line-height: 1.7em;">Westrim</span><br>Musica, contenuti e intrattenimento digitale personalizzati per la tua attività.</div>
			<!-- <div style="text-align: center; margin-left: auto; margin-right: auto;"> <a href="#" onClick="openFancybox('<?php echo base_url() ?>index.php/pub/contatti');"><input type="submit" value=" Tienimi informato " style="margin-top: 40px; font-size: 1.5em; font-weight: 300;"></a></div></td> -->
		<!--	<div style="text-align: center; margin-left: auto; margin-right: auto;"> <a href="<?php echo base_url() ?>/downloads/westrim_brochure.pdf"><input type="submit" value=" Scarica la brochure " style="margin-top: 40px; font-size: 1.5em; font-weight: 300;"></a></div></td>
			</tr>
		</table>
		<div id="payoff">dai <span style="color: #FF5500;">Volume</span> al tuo business!</div>
		-->
		<img src="<?php echo base_url() ?>/style/images/web_front.png">
	</div>
</div>



<div class="contenitore_paragrafo_antracite">
	<div class="paragrafo">
		<div class="titolo_paragrafo">Cos'è ?</div>
			<table class="table">
			<tr>
			<td>
			Un condensato di tecnologia in appena <strong>3 x 6 x 9 cm</strong>: il media box più piccolo ed efficiente al mondo, semplice e di facile installazione.
			Posizionalo sotto al bancone o dietro al televisore per risparmiare spazio e nasconderlo alla vista.
			In pochi istanti musica, video e contenuti digitali saranno trasmessi nel tuo negozio.<br><br>

			<span style="color: #FF6600;">La novità?</span>
			Westrim <strong>non è un servizio in streaming</strong>: funziona anche con brevi assenze di segnale ADSL e limita il consumo di banda durante le ore di apertura dell'attività. 
			</td>
			<td><div class="icona_paragrafo_right"><img src="<?php echo base_url() ?>/style/images/westrim_device.png" width="250px"></div></td>
			</tr>
			</table>
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo">
		<table class="table">
			<tr>
				<td width="320px">
					<p align="center">
						<img src="<?php echo base_url() ?>/style/images/radio_icon.png"><br>
						<span class="titolo_paragrafo">Radio In Store</span>
					</p>
				</td>
				<td width="320px">
					<p align="center">
						<img src="<?php echo base_url() ?>/style/images/visual_radio_icon.png"><br>
						<span class="titolo_paragrafo">Visual Radio</span>
					</p>
				</td>
				<td width="320px">
					<p align="center">
						<img src="<?php echo base_url() ?>/style/images/musictv_icon.png"><br>
						<span class="titolo_paragrafo">Music TV</span>
					</p>
				</td>
			</tr>
			<tr>
				<td width="320px" valign="top" style="padding: 25px;">
					<p align="left" style="font-size: 0.9em; line-height: 1.4em;">
						Questa soluzione è rivolta a coloro che vogliono <strong>personalizzare la comunicazione d'ambiente</strong>. 
						Un palinsesto musicale studiato da esperti del settore e composto in base alle vostre esigenze e al vostro target. Il catalogo musicale è vastissimo grazie agli accordi con <strong>Siae</strong> ed <strong>SCF</strong>.
						Quotidianamente il nostro staff programma i vostri <strong>jingles</strong> e/o <strong>messaggi promozionali</strong>, fornendovi un'assistenza completa, con possibilità di <strong>intervento in tempo reale</strong>. 
					</p>
				</td>
				<td width="320px" valign="top" style="padding: 25px;">
					<p align="left" style="font-size: 0.9em; line-height: 1.4em;">
						Hai un televisore? E' disponibile l'opzione <strong>Visual Radio</strong>, che ti permette di abbinare la riproduzione di contenuti visuali quali <strong>messaggi</strong>, <strong>immagini</strong> o <strong>filmati</strong>.
						<strong>Senza bisogno di dispositivi aggiuntivi!</strong>
					</p>
				</td>
				<td width="320px" valign="top" style="padding: 25px;">
					<p align="left" style="font-size: 0.9em; line-height: 1.4em;">
						<strong>Westrim Music Television</strong> vi propone i migliori video musicali del momento, selezionati quotidianamente dai nostri responsabili artistici secondo le vostre indicazioni.
						E' sufficiente collegare il media box al televisore tramite un comune cavo <strong>HDMI</strong>: il dispositivo riprodurrà il palinsesto, <strong>accendendo e spegnendo il monitor agli orari prestabiliti</strong>.
					</p>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo_centrato">
		Westrim by <strong>Music.lab</strong> - p. iva 04431060260 - <a href="mailto:info@westrim.it">info@westrim.it</a> - <a href="<?php echo base_url() ?>index.php/pub/privacy">privacy</a>
	</div>
</div>

<!--
<div class="contenitore_paragrafo">
	<div class="paragrafo">
		<table class="table">
			<tr>
			<td><div class="icona_paragrafo"><i class="fa fa-gears"></i></div></td>
			<td><div class="titolo_paragrafo">Come funziona</div>
			Accedi al tuo pannello di controllo su www.strimy.it e personalizza la tua scaletta 
			con orari di accensione e spegnimento.<br> Carica foto, video, annunci personalizzati e notizie, oppure scegli stream di notizie o playlist di video e musica.<br>
			Strimy si aggiorna in automatico e riproduce i tuoi contenuti agli orari stabiliti, accendendo e spegnendo la TV quando serve!<br>
			E quando non ci sono contenuti programmati, Strimy mostra l'homescreen con orario, informazioni meteo e notizie utili.</td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo_antracite">
	<div class="paragrafo">
		<table class="table">
			<tr>
			<td><div class="titolo_paragrafo">Dovunque, in ogni momento</div>
			Con Strimy non devi più essere in negozio o contattare il tuo tecnico per comunicare con i clienti. Devi chiudere per un imprevisto? Imposta l'avviso per comunicarlo nelle ore in cui non puoi esserci! Hai un nuovo prodotto in promozione giovedì? Hai diversi negozi o un'intera catena?<br>
			Non hai tempo di occupartene durante l'orario di apertura?<br>
			Nessun problema: puoi controllare le scalette di riproduzione di uno o cento schermi, dallo stesso pc, in qualsiasi momento, anche dal divano di casa!</td>
			<td><div class="icona_paragrafo_right"><i class="fa fa-laptop"></i></div></td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo">
		<table class="table">
			<tr>
			<td><div class="icona_paragrafo"><i class="fa fa-check-square-o"></i></div></td>
			<td><div class="titolo_paragrafo">Pronto in 5 minuti</div>
			L'idea di base di Strimy è che tutto dev'essere semplice: non serve chiamare un tecnico né essere esperti di informatica. Anzi, abbiamo inventato Strimy proprio per chi è negato coi computer.<br>
			Il kit contiene già tutto il necessario: basta collegare i tre cavi alla tv, alla rete internet e a una presa di corrente.<br>
			Alla prima accensione, sullo schermo appare il codice di attivazione. Vai su www.strimy.it, registrati e inserisci il codice: il tuo Strimy è pronto per trasmettere!<br></td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo_antracite">
	<div class="paragrafo">
		<table class="table">
			<tr>
			<td><div class="titolo_paragrafo">Piccolo, economico, ecologico</div>
			Strimy sfrutta la potenza e versatilità di <a href="http://www.raspberrypi.org">Raspberry Pi</a> per offrirti il digital signage più piccolo ed efficiente al mondo. È grande come una carta di credito: posizionalo dietro al monitor per risparmiare spazio e nasconderlo alla vista.<br>
			Strimy consuma 20 volte meno di un sistema tradizionale con computer.
			In un anno, un normale pc acceso 24 ore al giorno consuma 132,00 € di elettricità*: Strimy ne consuma 6,60!<br>
			Inoltre, Strimy accende e spegne la tv agli orari prestabiliti e, se la spegni manualmente, Strimy entra in modalità di basso consumo.<br>
			<span style="font-size: 0.8em; line-height: 1.1em; color: #999;"><br>*Considerando il consumo annuo di un normale pc (circa 880kw/h) a una tariffa energetica media nazionale (circa 0,15 € per Kw/h).</span></td>
			<td><div class="icona_paragrafo_right"><i class="fa fa-plug"></i></div></td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo_centrato">
		Strimy è un prodotto di <a href="http://www.atcetera.it">Atcetera</a> - p. iva 04582860260 - <a href="mailto:info@strimy.it">info@strimy.it</a> - <a href="<?php echo base_url() ?>index.php/pub/privacy">privacy</a>
	</div>

-->
