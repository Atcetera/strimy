<div class="paragrafo" style="margin-top: 30px; margin-bottom: 30px;">
<big><b>Nota informativa sulla privacy</b></big><br>
Informativa ai sensi dell’art. 13 D.L. n. 196 del 30/06/03 in materia di tutela dei dati personali<br>
<br>
ATCETERA tratta dati personali di clienti, fornitori e collaboratori e di soggetti che hanno volontariamente comunicato i propri dati anagrafici (direttamente o tramite telefono, fax, e-mail o altro mezzo).<br>
<br>
Il trattamento di tali dati avviene nel rispetto dei diritti degli interessati.<br>
<br>
Il Titolare del trattamento è:<br>
<br>
<b>
Atcetera s.a.s.<br>
di Matteo D’Agord e Luca Civiero & C.<br>
sede legale:<br>
Via Francia, 37<br>
31033 Castelfranco Veneto (TV)</b><br>
<br>
Nella persona del suo rappresentante legale.<br>
<br>
I dati comunicati dai soggetti interessati sono trattati esclusivamente per finalità connesse all’attività economica dell’azienda, quali:<br>
- elaborazione di statistiche interne;<br>
- redazione ed emissione di preventivi relativi ai servizi richiesti da clienti, anche potenziali;<br>
- inserimento nei database anagrafici interni;<br>
- elaborazioni di documenti legali e contabili inerenti la natura stessa dell’attività;<br>
nonché a tutti i soggetti cui la facoltà di accesso a tali dati sia riconosciuta dalle norme in vigore.<br>
<br>
I dati personali degli interessati potranno essere comunicati a soggetti esterni, qualora necessario, per finalità connesse all’attività aziendale, quali:<br>
- dipendenti, fornitori ed altri collaboratori nell’ambito delle relative mansioni, se inerenti i rapporti commerciali con gli interessati;<br>
- banche, studi legali, società di recupero credito;<br>
- agli uffici postali o a corrieri privati per l’invio di documentazione e/o materiale;<br>
- alle persone fisiche e/o giuridiche, quando necessario allo svolgimento della nostra attività o ai fini della gestione della contabilità aziendale;<br>
nonché a tutti i soggetti cui la facoltà di accesso a tali dati sia riconosciuta dalle norme in vigore.<br>
<br>
Il mancato conferimento dei propri dati da parte dei soggetti interessati può comportare l’impossibilità di erogare (parzialmente o totalmente) i servizi richiesti o la necessità di interrompere il rapporto contrattuale.<br>
<br>
Il trattamento dei dati personali avviene esclusivamente all’interno della nostra sede operativa, tramite supporti cartacei e informatici, adottando tutte le ragionevoli precauzioni che ne garantiscano la riservatezza e la sicurezza.<br>
<br>
L’interessato ha diritto di ottenere, in qualsiasi momento, la conferma dell’esistenza o meno di dati personali che lo riguardano, nonché la loro comunicazione, la rettifica, l’integrazione o la cancellazione.

<br><br>

<big><b>Utilizzo dei cookies</b></big><br>
I cookie sono file di testo che vengono memorizzati nel computer del visitatore per tracciarne la navigazione all'interno del sito e migliorare il servizio offerto.
Questo sito utilizza cookie di terze parti e in particolare quelli di Google Analytics; i dati cos&igrave; raccolti vengono archiviati ed elaborati dai server di Google e potrebbero essere usati per finalit&agrave; statistiche e commerciali (maggiori informazioni a <a href="http://www.google.com/intl/it/policies/privacy/">questo indirizzo</a>).
<br>
L'utente pu&ograve; disattivare in qualsiasi momento i cookie agendo sulle impostazioni del browser.
</div>
