<script language="Javascript">
	function cambia_amministratore(casella) {
		if (casella.checked)
			$("#eamministratore").val('1');
		else
			$("#eamministratore").val('0');
	}
	
	function cambia_storage() {
		if ($('#estorage').val()==0) {
			$('#storage0').hide();
			$('#storage1').hide();
		}
	else {
			$('#storage0').show();
			$('#storage1').show();
		}
	}
	
</script>


<?php

$utente=$this->flexi_auth->get_user_id();
$gruppo=$this->flexi_auth->get_user_group_id();
// $privilegi=$this->users_model->account_privilegi($gruppo);
$tipologia=$this->strimy_model->account_tipologia($gruppo);
$amministratore=$this->strimy_model->utente_tipologia($utente);
$tipologie=$this->config->item("tipologie");
$widget=$this->config->item("widget");
$widget_descrizione=$this->config->item("widget_descrizione");
$widget_icona=$this->config->item("widget_icona");

if ($operazione=="indice_account"):

	echo "<h1><i class=\"fa fa-suitcase\"></i> Gestione account</h1>";

	echo "<hr>";

	$this->db->from('user_groups');
	if ($tipologia==1):
		$this->db->where('ugrp_id', $gruppo);
		$this->db->or_where('ugrp_gruppo_riferimento', $gruppo);
	endif;
	if ($tipologia==2):
		$this->db->where('ugrp_id', $gruppo);
	endif;
	$query=$this->db->get();
	$row=$query->row();

	echo "<a href=\"".$this->config->item('base_url')."index.php/users/edita_account/0\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-plus\"></i> Crea nuovo account</a>";


	echo '<table class="table table-striped top-buffer">';
	echo '<thead><tr>';
	echo '<th>account</th>';
	echo '<th>descrizione</th>';
	echo '<th>e-mail</th>';
	echo '<th>tipologia</th>';
	echo '<th>privilegi</th>';
	echo '<th>n.utenti</th>';
	echo '<th>n.dispositivi</th>';
	echo '<th>crediti</th>';
	echo '<th>archivio contenuti/radio</th>';
	echo '<th>storage</th>';
	echo '<th></th></tr></thead>';
	
	echo "<tbody>";
	
	
	$yy=0;
	foreach ($query->result() as $row):
	
		echo "<tr>";
	//	echo "<td>".$row->ugrp_id."</td>";
		echo "<td><b>".$row->ugrp_name."</b></td>";
		echo "<td>".$row->ugrp_desc."</td>";
		echo "<td>".$row->ugrp_email."</td>";
		echo "<td>".$tipologie[$row->ugrp_tipologia];
		if ($row->ugrp_tipologia==2):	// se è un account cliente, indica qual è l'accout rivenditore di riferimento
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $row->ugrp_gruppo_riferimento);
			$queryGF=$this->db->get();
			$rowGF=$queryGF->row();
			echo " di <b>".$rowGF->ugrp_name."</b>";
		endif;
		echo "</td>";
		
		echo "<td>";
		$gruppo_privilegi_temp=$this->users_model->account_privilegi($row->ugrp_id);
		// $xx=0;
		foreach ($widget as $widget_temp):
			$icona_temp=array_search($widget_temp, $widget);
			if ($gruppo_privilegi_temp[$widget_temp]==1)
				echo "<i class=\"fa ".$widget_icona[$icona_temp]."\" style=\"margin-right: 2px;\" title=\"".$widget_descrizione[$icona_temp]."\" alt=\"".$widget_descrizione[$icona_temp]."\"></i>";
		endforeach;
		echo "</td>";
		
		echo "<td>";
		$this->db->from('user_accounts');
		$this->db->where('uacc_group_fk', $row->ugrp_id);
		$queryUNU=$this->db->get();
		if ($queryUNU->num_rows()>0)
			echo $queryUNU->num_rows();
		echo "</td>";
		
		echo "<td>";
		$this->db->from('dispositivi');
		$this->db->where('id_account', $row->ugrp_id);
		$queryNF=$this->db->get();
		if ($queryNF->num_rows()>0)
			echo $queryNF->num_rows();
		else
			echo "";
		echo "</td>";
		
		echo "<td>";
		if ($row->crediti_scala==1)
			echo $row->crediti;
		echo "</td>";
		
		echo "<td>";
		$this->db->from('archivio_file');
		$this->db->where('id_account', $row->ugrp_id);
		$queryNF=$this->db->get();
		echo $queryNF->num_rows()." file/";
		$this->db->from('archivio_radio');
		$this->db->where('id_account', $row->ugrp_id);
		$this->db->where('nome_file !=', '');
		$queryNF=$this->db->get();
		echo $queryNF->num_rows()." <small>file</small>";
		echo "</td>";
		
		echo "<td>";
		if ($row->ugrp_storage==0):
			echo "interno";
		elseif ($row->ugrp_storage==1):
			echo "Amazon S3";
			echo " (".trim($row->ugrp_bucket).")";
		elseif ($row->ugrp_storage==2):
			echo "Google Cloud Storage";
			echo " (".trim($row->ugrp_bucket).")";
		endif;
		echo "</td>";
		
		echo "<td class=\"text-right\">";
		echo '<div class="btn-group">';
		// echo "<a href=\"".base_url()."index.php/strimy/configurazione_dispositivo/".$rowDS->id_dispositivo."\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-gear\" style=\"width: 20px;\"></i></a>";
		echo "<a href=\"".$this->config->item('base_url')."index.php/users/edita_account/".$row->ugrp_id."\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
		// <a href=\"javascript:openFancybox('".$this->config->item('base_url')."index.php/users/edita_account/".$row->ugrp_id."');\"><i class=\"fa fa-pencil\" style=\"width: 20px;\"></i></a>";
		if ($row->ugrp_id!=$gruppo)
			echo "<a href=\"".$this->config->item('base_url')."index.php/users/elimina_account/".$row->ugrp_id."\" onClick=\"return confirm('Confermi l\'eliminazione?');\" class=\"btn btn-default\"><i class=\"fa fa-trash\"></i></a>";
		echo "</div>";
		echo "</td>";
		echo "</tr>";
		
		$yy++;
	endforeach;
	echo "</tbody>";
	echo "</table>";
	

elseif ($operazione=="edita_account"):
	?>

	<script language="Javascript">
		
	function cambia_privilegio(casella, privilegio) {
		if (casella.checked)
			$("#eprivilegi"+privilegio).val('1');
		else
			$("#eprivilegi"+privilegio).val('0');
	}
	
	</script>
	
	<?php

	if ($id_account==0):
		$nome_gruppo="";
		$descrizione_gruppo="";
		$tipologia_gruppo=2;
		$gruppo_privilegi=$privilegi;
		$cstorage=0;
		$cbucket_amazon="";
		$curl="";
		$cemail="";
		$ccrediti=15;
		$ccrediti_scala=1;
	else:
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $id_account);
		$queryGRP=$this->db->get();
		$rowGRP=$queryGRP->row();
		$nome_gruppo=$rowGRP->ugrp_name;
		$descrizione_gruppo=$rowGRP->ugrp_desc;
		$tipologia_gruppo=$rowGRP->ugrp_tipologia;
		$gruppo_privilegi=$this->users_model->account_privilegi($id_account);
		$cstorage=$rowGRP->ugrp_storage;
		$cbucket=$rowGRP->ugrp_bucket;
		$curl=$rowGRP->ugrp_url;
		$cemail=$rowGRP->ugrp_email;
		$ccrediti=$rowGRP->crediti;
		$ccrediti_scala=$rowGRP->crediti_scala;
	endif;
	
	echo "<form id=\"config\" action=\"".$this->config->item('base_url')."index.php/users/salva_account\" method=\"post\">";
	echo "<table>";
	echo "<tr><td style=\"width: 130px;\">Nome account</td><td><input type=\"text\" name=\"enome\" value=\"".$nome_gruppo."\" size=\"20\"></td></tr>";
	echo "<tr><td>Descrizione</td><td><input type=\"text\" name=\"edescrizione\" value=\"".$descrizione_gruppo."\" size=\"40\"></td></tr>";
	echo "<tr><td>E-mail</td><td><input type=\"text\" name=\"eemail\" value=\"".$cemail."\" size=\"40\"></td></tr>";
	echo "<tr><td>Tipologia</td><td>";
	
	if ($gruppo!=$id_account):
		echo "<select name=\"etipologia\">";
		$xx=0;
		foreach ($tipologie as $tipologie_temp):
			if (($tipologia==0) || ($tipologia<$xx)):	// elenca le sole tipologie compatibili con la tipologia dell'account con cui si è loggati
				echo "<option value=\"".$xx."\"";
				if ($xx==$tipologia_gruppo)
					echo " selected";
				echo ">".$tipologie_temp;	
				echo "</option>";
			endif;
			$xx++;
		endforeach;
		echo "</select>";
	
	else:
		echo "<b>".$tipologie[$tipologia_gruppo]."</b>";
		echo "<input type=\"hidden\" name=\"etipologia\" value=\"".$tipologia_gruppo."\" readonly=\"readonly\">";
	endif;
	echo "</td></tr>";

	$xx=0;
	foreach($widget as $widget_temp):
		echo "<tr><td>";
		if ($xx==0) echo "Privilegi";
		echo "</td><td>";
		
		if ($id_account!=0):
			if (!isset($gruppo_privilegi[$widget_temp])):
				$gruppo_privilegi[$widget_temp]=0;
			endif;
		else:
			// se è un nuovo account, tutte le funzionalità saranno attive di default ad esclusione di quelle inerenti Westrim (radio e musictv)
			if ($widget_temp>1):
				$gruppo_privilegi[$widget_temp]=1;
			else:
				$gruppo_privilegi[$widget_temp]=0;
			endif;
		endif;
		
		echo "<input type=\"checkbox\" name=\"eprivilegi".$widget_temp."_checkbox\"";
		if ($gruppo_privilegi[$widget_temp]==1) echo " checked=\"checked\"";
		if (($tipologia!=0) && ($amministratore==0)) echo " disabled=\"disabled\"";
		echo " onClick=\"cambia_privilegio(this, '".$widget_temp."');\"> gestione ".$privilegi_temp;
		echo "<input type=\"hidden\" id=\"eprivilegi".$widget_temp."\" name=\"eprivilegi[".$widget_temp."]\" value=\"".$gruppo_privilegi[$widget_temp]."\">";
		$descrizione_temp=array_search($widget_temp, $widget);
		$descrizione=$widget_descrizione[$descrizione_temp];
		echo $descrizione;
		echo "</td>";
		echo "</tr>";
		$xx++;
	endforeach;
	
	/*
	foreach($gruppo_privilegi as $privilegi_temp):
		echo "<tr><td>";
		if ($xx==0) echo "Privilegi";
		echo "</td><td>";
		echo "<input type=\"checkbox\" name=\"e".$privilegi_temp."_checkbox\"";
		if ($gruppo_privilegi[$privilegi_temp]==1) echo " checked=\"checked\"";
		if (($tipologia!=0) && ($amministratore==0)) echo " disabled=\"disabled\"";
		echo " onClick=\"cambia_privilegio(this, '".$privilegi_temp."');\"> gestione ".$privilegi_temp." ".$key;
		echo "<input type=\"hidden\" id=\"e".$privilegi_temp."\" name=\"e".$privilegi_temp."\" value=\"".$gruppo_privilegi[$privilegi_temp]."\">";
		echo "</td>";
		echo "</tr>";
		$xx++;
	endforeach;
	*/
	echo "<tr><td>Storage</td><td>";
	echo "<select name=\"estorage\" id=\"estorage\" onchange=\"cambia_storage();\">";
	echo "<option value=\"0\"";
	if ($cstorage==0)
		echo " selected";
	echo ">interno</option>";
	echo "<option value=\"1\"";
	if ($cstorage==1)
		echo " selected";
	echo ">Amazon S3</option>";
	echo "<option value=\"2\"";
	if ($cstorage==2)
		echo " selected";
	echo ">Google Cloud Storage</option>";
	echo "</select>";
	echo "</td></tr>";
	
	echo "<tr id=\"storage0\"";
	if ($cstorage==0)
		echo " style=\"display: none;\"";
	echo "><td>Nome del bucket<td>";
	echo "<input type=\"text\" name=\"ebucket\" value=\"".$cbucket."\" style=\"width: 150px;\" maxlength=\"50\"></td></tr>";
	
	echo "<tr id=\"storage1\"";
	if ($cstorage==0)
		echo " style=\"display: none;\"";
	echo "><td>Url pubblico<td>";
	echo "<input type=\"text\" name=\"eurl\" value=\"".$curl."\" style=\"width: 300px;\" maxlength=\"200\"></td></tr>";


	echo "<tr><td>Crediti residui</td><td>";
	echo "<input type=\"text\" name=\"ecrediti\" value=\"".$ccrediti."\" size=\"5\" maxlenght=\"5\">";
	echo " <input type=\"checkbox\" name=\"ecrediti_scala\"";
	if ($ccrediti_scala==1)
		echo " checked=\"checked\"";
	echo "> <small>scala crediti</small>";
	echo "</td></tr>";

	echo "</table>";
	
	echo "<br><br>";
	echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
	echo "<input type=\"hidden\" name=\"eid_account\" value=\"".$id_account."\" readonly=\"readonly\">";
	
		
//	echo "<tr><td></td><td>";
	if ($id_account==0)
		$etichetta="inserisci account";
	else
		$etichetta="aggiorna dati";
	
	$permesso=0;
	
	if ($tipologia!=2)
		$permesso=1;
	elseif ($id_account==$gruppo)
		$permesso=1;
	
	if ($permesso==1)
		echo "<input type=\"submit\" value=\"".$etichetta."\">";
	
//	echo "</td></tr></table>";
	echo "</form>";


elseif ($operazione=="indice_user"):

	echo "<h1><i class=\"fa fa-user\"></i> Gestione utenti</h1>";
	echo "<hr>";

	$this->db->select('user_accounts.uacc_id as uacc_id, user_accounts.uacc_email, user_accounts.uacc_username as uacc_username, user_accounts.uacc_amministratore as uacc_amministratore, user_accounts.uacc_group_fk as uacc_group_fk, user_groups.ugrp_name as ugrp_name');
	$this->db->from('user_accounts');
	$this->db->join('user_groups', 'user_groups.ugrp_id=user_accounts.uacc_group_fk');
	if ($tipologia<2):
		$this->db->where('user_groups.ugrp_id', $gruppo);
		$this->db->or_where('user_groups.ugrp_gruppo_riferimento', $gruppo);
	endif;
	if ($tipologia==2):
		$this->db->where('user_accounts.uacc_group_fk', $gruppo);
	endif;
	$this->db->order_by('user_accounts.uacc_username', 'asc');
	$this->db->order_by('user_accounts.uacc_email', 'asc');
	$queryACC=$this->db->get();


	if ($amministratore==1)
		echo "<a href=\"".$this->config->item('base_url')."index.php/users/edita_user/0\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-plus\"></i> Aggiungi nuovo utente</a>";
	
	
	echo '<table class="table table-striped top-buffer">';
	echo '<thead><tr>';
	echo '<th></th>';
	echo '<th>username</th>';
	echo '<th>email</th>';
	echo '<th>account</th>';
	echo '<th>tipologia</th>';
	echo '<th></th></tr></thead>';
	echo "<tbody>";
	
	$yy=0;
	foreach ($queryACC->result() as $rowACC):
		echo "<tr>";
		if ($rowACC->uacc_id==$utente)
			echo "<td><i class=\"fa fa-user\"\"></i></td>";
		else
			echo "<td></td>";
		echo "<td><b>".$rowACC->uacc_username."</b>";
		echo "</td>";
		echo "<td>".$rowACC->uacc_email."</td>";
		echo "<td>".$rowACC->ugrp_name."</td>";
		echo "<td>";
		if ($rowACC->uacc_amministratore==1)
			echo "amministratore";
		else
			echo "utente";
		echo "</td>";
		
		echo "<td class=\"text-right\">";
		echo '<div class="btn-group">';
		
		if (($amministratore==1) || ($rowACC->uacc_id==$utente))
		// echo "<a href=\"#\" onClick=\"openFancybox('".base_url()."index.php/mp3/gestione_file_mp3/gestisci_brano/".$dataRES['id_file'][$xx]."');\">
		echo "<a href=\"".$this->config->item('base_url')."index.php/users/edita_user/".$rowACC->uacc_id."\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-pencil\" style=\"width: 20px;\"></i></a>";
		if (($amministratore==1) && ($rowACC->uacc_id!=$utente))
			echo "<a href=\"".$this->config->item('base_url')."index.php/users/elimina_user/".$rowACC->uacc_id."\" onClick=\"return confirm('Confermi l\'eliminazione?');\" class=\"btn btn-default\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
		echo "</div>";
		echo "</td>";		
		echo "</tr>";
		
		$yy++;
	endforeach;
	echo "</tbody>";
	echo "</table>";

elseif ($operazione=="edita_user"):
	?>

	<script language="Javascript">
		function mostra_disclaimer_password() {
			if ($("#password1").val().length>0)
				$("#disclaimer_password").hide();
			else
				$("#disclaimer_password").show();
		}


	function check_form_utente() {
		var stringa = $('#form_utente').serialize();
		
		var errore=0;
		var messaggio="";
		var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$/; 
		
		var id_account=$("#eid_account").val();
		
		var email=$("#eemail").val();
		if ((email=="") || (email.search(emailFilter) == -1)) {
			messaggio="Indirizzo email assente o non formattato";
			errore=1;
		}

		if ($("#password1").val()!=$("#password2").val()) {
			messaggio="Le password non coincidono";
			errore=1;
		}

		if (id_account!=0) {

			if (($("#password1").val().length<8) && ($("#password1").val().length>0)) {
					messaggio="Password non specificata o troppo corta";
					errore=1;
				}
		}

		if (id_account==0) {
		
			if ($("#password1").val().length<8) {
				messaggio="Password non specificata o troppo corta";
				errore=1;
			}
			
			$.ajax({

			  url: "<?php echo base_url() ?>index.php/users/check_user", 
			  data: "username="+$("#eusername").val()+"&email="+$("#eemail").val(),
			  async: false,
			  dataType: 'html',
			  method: 'POST',
			  success: function(risultato) {
					if (risultato=="1") {
						errore=1;
						messaggio="Utente gi&agrave; presente! Inserisci un username diverso";
					}
					if (risultato=="2") {
						errore=1;
						messaggio="Email gi&agrave; registrata! Inserisci un indirizzo diverso";
					}
				  }
				});
			
			if ($("#eusername").val()=="") {
				messaggio="Username non specificato";
				errore=1;
			}
			
		}
		
		if (errore==1) {
			messaggio_errore(messaggio);
			return false;
		}
		else
			return true;
		
	}
	
	function utente_amministratore(check) {
		// var amministratore=$("#eamministratore_checkbox");
		if (check.checked)
			$("#eamministratore").val(1);
		else
			$("#eamministratore").val(0);
	
		}
	</script>


	<?php
	if ($id_user==0):
		$username="";
		$email="";
		$grupporiferimento="";
		$utente_amministratore=0;
	else:
		$this->db->from('user_accounts');
		$this->db->where('uacc_id', $id_user);
		$queryACC=$this->db->get();
		$rowACC=$queryACC->row();
		$username=$rowACC->uacc_username;
		$email=$rowACC->uacc_email;
		$grupporiferimento=$rowACC->uacc_group_fk;
		$utente_amministratore=$rowACC->uacc_amministratore;
	endif;
	
	echo "<form action=\"".$this->config->item('base_url')."index.php/users/salva_utente\" method=\"post\" id=\"form_utente\" class=\"form-horizontal\" onsubmit=\"return check_form_utente()\">";
	echo "<fieldset>";
	
	echo '<div class="form-group"><label class="col-md-4 control-label" for="textinput">Username</label><div class="col-md-4">';
	if ($id_user==0)
		echo '<input id="eusername" name="eusername" type="text" class="form-control input-md" size="20" maxlength="12">';
	else
		echo '<input id="eusername" name="eusername" type="text" class="form-control input-md" size="20" placeholder="'.$username.'" readonly="readonly">';
	echo '</div></div>';
	
	echo '<div class="form-group"><label class="col-md-4 control-label" for="textinput">Email</label><div class="col-md-4">';
	echo '<input id="eemail" name="eemail" type="text" class="form-control input-md" size="40" maxlength="80" value="'.$email.'">';
	echo '</div></div>';
	
	echo '<div class="form-group"><label class="col-md-4 control-label" for="pwd">Password</label><div class="col-md-6">';
	echo '<input id="password1" name="epassword" type="password" class="form-control input-md" size="20"';
	if ($id_user!=0)
		echo ' placeholder="se vuoto, la password rimarr&agrave; invariata"';
	echo '><span class="help-block">minimo 8 caratteri</span>';
	echo '</div></div>';

	echo '<div class="form-group"><label class="col-md-4 control-label" for="pwd">Password (ridigita)</label><div class="col-md-6">';
	echo '<input id="password2" type="password" class="form-control input-md" size="40">';
	echo '</div></div>';
	
	if (($tipologia==0) && ($amministratore==1) && ($id_user!=$utente)):
		echo '<div class="form-group">
		<label class="col-md-4 control-label" for="checkboxes"></label>
		<div class="col-md-4">
			<label class="checkbox-inline" for="checkboxes-0">
			<input type="checkbox" id="eamministratore_checkbox" name="eamministratore_checkbox"';
			if ($utente_amministratore) echo ' checked="checked"';
			echo ' onclick="utente_amministratore(this);">
			amministratore
			</label>
			</div>
		</div>';
		echo '<div class="form-group">
				<label class="col-md-4 control-label" for="selectbasic">Account</label>
				<div class="col-md-4">
				<select name="account_selezionato" class="form-control">';
				$dataSELECT['gruppo_selezionato']=$grupporiferimento;
				$this->users_model->select_lista_account($dataSELECT);
		echo '</select></div></div>';
	else:
		echo "<input type=\"hidden\" name=\"account_selezionato\" value=\"".$gruppo."\" readonly=\"readonly\">";	
	endif;


	echo "<input type=\"hidden\" id=\"eamministratore\" name=\"eamministratore\" value=\"".$utente_amministratore."\">";
	echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
	echo "<input type=\"hidden\" name=\"eid_account\" id=\"eid_account\" value=\"".$id_user."\" readonly=\"readonly\">";
//	echo "<input type=\"hidden\" name=\"account_selezionato\" value=\"".$gruppo."\" readonly=\"readonly\">";	
	

	echo '</fieldset>';

	echo "<div class=\"text-right\">";

	echo "<span id=\"emessaggio_errore\" class=\"messaggio_errore\"></span><br>";
	if ($id_user==0)
		$etichetta="inserisci utente";
	else
		$etichetta="aggiorna dati";
	echo "<input type=\"submit\" value=\"".$etichetta."\" class=\"btn btn-primary\">";

	echo "</div>";
	echo "</form>";
	

endif;
?>

