<script language="Javascript">
	function playlist_cancella(indirizzo, id, tabella) {
		if (confirm('Vuoi eliminare la playlist?')) {
			var url=indirizzo+'/'+id;
			$.ajax({
			  url: url
			});
			$("#list_"+tabella+"_"+id).hide(500);
		}
	}
</script>


<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

$privilegi=$this->users_model->account_privilegi($grupposelezionato);

// function elenca_playlist($data) {
	
	// $this=$data['this'];
	$widget=$this->config->item('widget');
	$widget_descrizione=$this->config->item('widget_descrizione');
	$widget_icona=$this->config->item('widget_icona');
	// $data['descrizione']=$widget($tipologia_playlist);
	
	$descrizione=array_search($tipologia_playlist, $widget);


	if (($tipologia_playlist==0) || ($tipologia_playlist==1)):
		// è una playlist radio/video
		$link_edit=$this->config->item('base_url')."index.php/mp3/radio_magic_new_gestisci";
		$link_del=$this->config->item('base_url')."index.php/mp3/radio_magic_new_gestisci_cancella";
		$link_suffisso="";
		$db_table="magic_temporanea_indice";
	elseif ($tipologia_playlist==2):
		// è una playlist di contenuti generici
		$link_edit=$this->config->item('base_url')."index.php/strimy/playlist_generica_gestisci";
		$link_del=$this->config->item('base_url')."index.php/strimy/playlist_generica_gestisci_cancella";
		$link_suffisso="";
		$db_table="playlist_generica_indice";
	else:
		// è un widget senza contenuti (es. news)
		$link_edit=$this->config->item('base_url')."index.php/strimy/playlist_misc_gestisci";
		$link_del=$this->config->item('base_url')."index.php/strimy/playlist_generica_gestisci_cancella";
		$link_suffisso=" data-toggle=\"modal\" data-target=\"#finestra-modale\"";
		$db_table="playlist_generica_indice";
	// else:
	//	$db_table="playlist_generica_indice";
	endif;

	
	if (($tipologia_playlist==2) && ($tipologia_file==0)):
		$icona="fa-music";
		$desc=" audio";
	elseif (($tipologia_playlist==2) && ($tipologia_file==1)):
		$icona="fa-video-camera";
		$desc=" video";
	elseif (($tipologia_playlist==2) && ($tipologia_file==2)):
		$icona="fa-photo";
		$desc=" immagini";
	else:
		$icona=$widget_icona[$descrizione];
	endif;

	echo "<h1><i class=\"fa ".$icona."\"></i> Playlist <small>".$widget_descrizione[$descrizione].$desc."</small></h1>";
	
	echo "<hr>";
	
	$link_aggiungi=$link_edit."/0/".$tipologia_playlist."";
	if ( ($tipologia_playlist==2) && (isset($tipologia_file)) )
				$link_aggiungi=$link_aggiungi."/".$tipologia_file;
	// echo "\"".$link_suffisso.">nuova</a> ]<br>";
	
	// echo '<div class="btn-group">';
	echo "<a href=\"".$link_aggiungi."\"".$link_suffisso."\" class=\"btn btn-default\"><i class=\"fa fa-plus\"></i> Nuova playlist</a>";
	// echo '</div>';
	
	$this->db->from($db_table);
	$this->db->where('tipologia', $tipologia_playlist);
	if ($tipologia_playlist==2)
		$this->db->where('tipologia_file', $tipologia_file);
	$this->db->where('id_account', $grupposelezionato);
	$query=$this->db->get();
	
	if ($query->num_rows()>0):
		echo '<table class="table table-striped top-buffer">';
		echo '<thead><tr>';
		echo '<th>descrizione</th>';
		echo '<th>ultima modifica</th>';
		if ($tipologia_playlist==2)
			echo '<th>numero elementi</th>';
		echo '<th></th></tr></thead>';	
		
		echo "<tbody>";
		foreach ($query->result() as $row):
			echo "<tr id=\"list_".$db_table."_".$row->id."\">";

			echo "<td>".$row->descrizione."</td>";

			echo "<td>";
			if ($row->ultima_modifica!=0)
				echo date('d M Y', $row->ultima_modifica)." alle ore ".date('H:i', $row->ultima_modifica);
			echo "</td>";

			if ($tipologia_playlist==2):
				$this->db->from('playlist_generica_elementi');
				$this->db->where('id_playlist', $row->id);
				$queryELE=$this->db->get();
				echo "<td>".$queryELE->num_rows()."</td>";
			endif;
			
			echo "<td class=\"text-right\">";
			echo "<div class=\"btn-group\">";
			echo "<a href=\"".$link_edit."/".$row->id."/".$tipologia_playlist."";
			echo "\"".$link_suffisso." class=\"btn btn-default\"><i class=\"fa fa-edit\" style=\"width: 20px;\"></i></a>";
			echo "<a href=\"javascript:playlist_cancella('".$link_del."','".$row->id."','".$db_table."');\" class=\"btn btn-default\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
			echo "</div>";
			echo "</td>";
			echo "</tr>";
		endforeach;
		echo "</tbody>";
		echo "</table>";
	endif;
	echo "<br>";

?>

<script src="<?php echo $this->config->item('base_url'); ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
