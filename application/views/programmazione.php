<?php

$utente=$this->flexi_auth->get_user_id();
$amministratore=$this->strimy_model->utente_tipologia($utente);
$grupposelezionato=$this->strimy_model->accountselezionato();

if ($id_gruppo!=0):
	$this->db->from('dispositivi_gruppi');
	$this->db->where('id', $id_gruppo);
	$this->db->where('id_account', $grupposelezionato);
	$queryGRUPPO=$this->db->get();
	$rowGRUPPO=$queryGRUPPO->row();
	$descrizione_gruppo=$rowGRUPPO->descrizione;
else:
	$descrizione_gruppo="Tutti i dispositivi";
endif;


$giornoARR[1]="lun";
$giornoARR[2]="mar";
$giornoARR[3]="mer";
$giornoARR[4]="gio";
$giornoARR[5]="ven";
$giornoARR[6]="sab";
$giornoARR[7]="dom";

echo "<h1><i class=\"fa fa-clock-o\"></i> Palinsesto <small>gruppo ".$descrizione_gruppo."</small></h1>";

echo "<hr>";

if ($id_gruppo!=0)
	echo "<a href=\"".base_url()."index.php/layout/indice/".($id_gruppo*-1)."\" class=\"btn btn-default\"><i class=\"fa fa-list\"></i> Programmazione di gruppo</a>";

$yy=0;
$zz=0;


	$this->db->from('dispositivi');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('attivato', 1);
	if ($id_gruppo!=0)
		$this->db->where('id_gruppo', $id_gruppo);
	$this->db->order_by('id', 'asc');

	$queryDS=$this->db->get();

	if ($queryDS->num_rows()>0):
		
		$xx=0;

		echo '<table class="table table-striped top-buffer">';
		echo '<thead><tr>';
		echo '<th style="width: 25%;">dispositivo</th>';
		echo '<th style="width: 20%;">descrizione</th>';
		echo '<th style="width: 20%;">ultima modifica</th>';
		echo '<th>programmazione</th>';
		echo '<th></th></tr></thead>';
		
		echo "<tbody>";
		
		$programmazione_personalizzata_tot=0;
		foreach ($queryDS->result() as $rowDS):
			
			$this->db->from('layout_indice');
			$this->db->where('id_dispositivo', $rowDS->id_dispositivo);
			$queryPRP=$this->db->get();
			if ($queryPRP->num_rows()>0):
				$rowPRP=$queryPRP->row();
				if (($rowPRP->programmazione_personalizzata==1) && ($id_gruppo!=0)):
					$programmazione_personalizzata=1;
					$programmazione_personalizzata_tot=1;
				endif;
				$ultima_modifica=date('d M Y', $rowPRP->ultima_modifica_manuale)." alle ore ".date('H:i', $rowPRP->ultima_modifica_manuale);
			else:
				$programmazione_personalizzata=0;
				$ultima_modifica="";
			endif;

			if ($programmazione_personalizzata==1)
				echo "<tr class=\"warning\">";
			else
				echo "<tr>";
			
			echo "<td>";
			echo $rowDS->id_dispositivo;
			echo "</td>";
			
			echo "<td>";
			echo $rowDS->descrizione;
			echo "</td>";

			$programmazione_settimana="";
			$kk=1;
			foreach ($giornoARR as $giorno_temp):
				$this->db->from('layout_playlist');
				$this->db->where('id_dispositivo', $rowDS->id_dispositivo);
				$this->db->where('giorno', $kk);
				$queryUMO=$this->db->get();
				if ($queryUMO->num_rows()>0):
					$rowUMO=$queryUMO->row();
					if ($programmazione_settimana!="")
						$programmazione_settimana=$programmazione_settimana.", ";
					$programmazione_settimana=$programmazione_settimana.$giorno_temp;
				endif;
				$kk++;
			endforeach;

			echo "<td>";
			echo $ultima_modifica;
			echo "</td>";
			
			echo "<td>";
			echo $programmazione_settimana;
			echo "</td>";
		
			echo "<td class=\"text-right\">";
			echo "<input type=\"hidden\" id=\"id_dispositivo_".$zz."\" name=\"id_dispositivo[]\" value=\"".$rowDS->id_dispositivo."\" readonly=\"readonly\">";
			
			echo '<div class="btn-group">';
			echo "<a href=\"".$this->config->item('base_url')."index.php/layout/indice/".$rowDS->id_dispositivo."\" class=\"btn btn-default\"><i class=\"fa fa-list\" style=\"width: 20px;\"></i></a>";
			echo "</div>";

			echo "</td>";

			echo "</tr>";

			$xx++;
			$zz++;
		endforeach;

		if ($programmazione_personalizzata_tot==1):
			echo "<tfoot>";
			echo "<tr class=\"warning\"><td colspan=\"6\">";
			echo "<span style=\"color: #666\">i dispositivi evidenziati hanno una programmazione differente rispetto a quella di gruppo</span>";
			echo "</td></tr>";
			echo "</tfoot>";
		endif;

		echo "</tbody>";
		echo "</table>";

	endif;

	$yy++;


echo "<input type=\"hidden\" id=\"totale_dispositivi\" name=\"totale_dispositivi\" value=\"".$zz."\">";

?>
