<?php

if ($operazione=="gestione"):

	?>

	<script language="Javascript">
	function salva_dati_anagrafici() {
		var stringa = $('#dettagli_account').serialize();

		$(".form-group").removeClass('has-error');
		
		var errore=0;
		var messaggio="";
		var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$/; 
		
		if ($("#nome_cognome").val()=="") {
			$("#form_enome_cognome").addClass('has-error');
		}

		if ($("#ragione_sociale").val()=="") {
			$("#form_ragione_sociale").addClass('has-error');
		}

		if ($("#codice_fiscale").val()=="") {
			$("#form_codice_fiscale").addClass('has-error');
		}

		if ($("#partita_iva").val()=="") {
			$("#form_partita_iva").addClass('has-error');
		}

		if (($("#partita_iva").val()=="") && ($("#codice_fiscale").val()=="")) {
			messaggio="E' necessario specificare almeno CODICE FISCALE oppure PARTITA IVA";
			errore=1;
		}

		if (($("#nome_cognome").val()=="") && ($("#ragione_sociale").val()=="")) {
			messaggio="E' necessario specificare almeno NOME E COGNOME oppure RAGIONE SOCIALE";
			errore=1;
		}

		if (errore==1) {
			messaggio_errore(messaggio);
			return false;
		} else {
			$.ajax({

			  url: "<?php echo base_url(); ?>index.php/users/gestione_account_dati_salva", 
			  data: stringa,
			  async: false,
			  dataType: 'html',
			  method: 'POST',
			  success: function(risultato) {
					window.location.href = '<?php echo base_url(); ?>index.php/users/gestione_account';
					}
			});
			return false;
		}
		
	}
	</script>

	<?php
	$gruppo=$this->flexi_auth->get_user_group_id();
	echo "<form onSubmit=\"return salva_dati_anagrafici();\" class=\"form-horizontal\" id=\"dettagli_account\">";
	
	$this->db->from('user_groups');
	$this->db->where('ugrp_id', $gruppo);
	$queryACC=$this->db->get();
	$rowACC=$queryACC->row();
	
	$this->db->from('user_groups_dettagli');
	$this->db->where('id_account', $gruppo);
	$queryANA=$this->db->get();
	$rowANA=$queryANA->row();
	
	echo '<div id="form_enome_cognome" class="form-group">
    <label for="enome_cognome" class="col-sm-4 control-label">Nome e cognome</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="nome_cognome" name="nome_cognome" placeholder="necessari se persona fisica" value="'.$rowANA->nome_cognome.'">
    </div>
  </div>';

	echo '<div id="form_ragione_sociale" class="form-group">
    <label for="eragione_sociale" class="col-sm-4 control-label">Ragione sociale</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="ragione_sociale" name="ragione_sociale" placeholder="necessaria se persona giuridica" value="'.$rowANA->ragione_sociale.'">
    </div>
  </div>';

	echo '<div id="form_indirizzo" class="form-group">
    <label for="eindirizzo" class="col-sm-4 control-label">Indirizzo</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="indirizzo" name="indirizzo" placeholder="" value="'.$rowANA->indirizzo.'">
    </div>
  </div>';

	echo '<div id="form_citta" class="form-group">
    <label for="ecitta" class="col-sm-4 control-label">Citt&agrave;</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="citta" name="citta" placeholder="" value="'.$rowANA->citta.'">
    </div>
  </div>';

	echo '<div id="form_cap" class="form-group">
    <label for="ecap" class="col-sm-4 control-label">C.A.P.</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="cap" name="cap" placeholder="" value="'.$rowANA->cap.'">
    </div>
  </div>';

	echo '<div id="form_provincia" class="form-group">
    <label for="eprovincia" class="col-sm-4 control-label">Provincia</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="provincia" name="provincia" placeholder="" value="'.$rowANA->provincia.'">
    </div>
  </div>';

	echo '<div id="form_codice_fiscale" class="form-group">
    <label for="epartita_iva" class="col-sm-4 control-label">Codice fiscale</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="codice_fiscale" name="codice_fiscale" placeholder="necessario se persona fisica" value="'.$rowANA->codice_fiscale.'">
    </div>
  </div>';

	echo '<div id="form_partita_iva" class="form-group">
    <label for="epartita_iva" class="col-sm-4 control-label">Partita IVA</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="partita_iva" name="partita_iva" placeholder="necessaria se persona giuridica" value="'.$rowANA->partita_iva.'">
    </div>
  </div>';

	echo '<div class="form-group">
    <label class="col-sm-4 control-label">E-mail</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" readonly=\"readonly\" value="'.$rowACC->ugrp_email.'">
    </div>
  </div>';

	
	echo "<div class=\"text-right\">";
	echo "<span id=\"emessaggio_errore\" class=\"messaggio_errore\"></span><br>";
	echo "<input type=\"hidden\" name=\"etipo_form\" value=\"dati_anagrafici\">";
	echo "<input type=\"submit\" class=\"btn btn-primary\" value=\"Conferma\">";
	echo "</form>";
	echo "</div>";

elseif ($operazione=="pagamento"):

?>

<script language="Javascript">
	function pagamento() {
		var importo=$("#eimporto_pagamento").val();
		var crediti=$("#crediti").val();

		var stringa = '';
		stringa=stringa+"eimporto="+importo;
		stringa=stringa+"&ecrediti="+crediti;
		
		$.ajax({

		  url: "<?php echo base_url(); ?>index.php/users/gestione_account_pagamento", 
		  data: stringa,
		  async: false,
		  dataType: 'html',
		  method: 'POST',
		  success: function(risultato) {
					$('#price-box-outer').fadeOut(50);
					$("#conferma_pagamento").html(risultato);
					$("#conferma_pagamento").fadeIn(500);
				}
		});
		return false;
	}
</script>

<?php

echo "<h1><i class=\"fa fa-euro\"></i> Amministrazione account</h1>";

echo "<hr>";


	$gruppo=$this->flexi_auth->get_user_group_id();
	$tipologia=$this->strimy_model->account_tipologia($gruppo);
	$grupposelezionato=$gruppo;
	$utente=$this->flexi_auth->get_user_id();
	$amministratore=$this->strimy_model->utente_tipologia($utente);
	$privilegi=$this->users_model->account_privilegi($grupposelezionato);

	$crediti_residui=$this->users_model->crediti_residui($grupposelezionato);
	
	echo "<a href=\"".base_url()."index.php/users/gestione_account_dati\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-edit\"></i> Modifica anagrafica</a>";
	echo " <a href=\"".base_url()."index.php/users/gestione_account_dati\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-trash\"></i> Elimina account</a>";

	

?>	

<div class="col-sm-12">

<div id="dati_fatturazione" class="col-sm-5">
	<?php
	// elenca i dettagli anagrafici
	
	$this->db->from('user_groups_dettagli');
	$this->db->where('id_account', $grupposelezionato);
	$query=$this->db->get();
	$row=$query->row();
		
	$this->db->from('user_groups');
	$this->db->where('ugrp_id', $grupposelezionato);
	$queryACC=$this->db->get();
	$rowACC=$queryACC->row();

	echo '<div class="panel panel-default top-buffer">';
	echo '<div class="panel-heading">I tuoi dati anagrafici</div>';
	echo "<table class=\"table\">";
	echo "<tr><td>Nome e cognome</td><td>".$row->nome_cognome."</td></tr>";
	echo "<tr><td>Ragione sociale</td><td>".$row->ragione_sociale."</td></tr>";
	echo "<tr><td>Indirizzo</td><td>".$row->indirizzo."</td></tr>";
	echo "<tr><td>Citt&agrave;</td><td>".$row->citta."</td></tr>";
	echo "<tr><td>C.A.P.</td><td>".$row->cap."</td></tr>";
	echo "<tr><td>Provincia</td><td>".$row->provincia."</td></tr>";
	echo "<tr><td>Codice fiscale</td><td>".$row->codice_fiscale."</td></tr>";
	echo "<tr><td>Partita IVA</td><td>".$row->partita_iva."</td></tr>";
	echo "<tr><td>E-mail</td><td>".$rowACC->ugrp_email."</td></tr>";
	echo "</table>";
	echo "</div>";

	$this->db->from('paypal_pagamenti');
	$this->db->where('pagato', 1);
	$this->db->where('id_account', $grupposelezionato);
	$this->db->order_by('timestamp', 'desc');
	$queryPAG=$this->db->get();
	if ($queryPAG->num_rows()>0):
	
		echo '<div class="panel panel-default top-buffer">';
		echo '<div class="panel-heading">Le tue fatture</div>';
		echo "<table class=\"table\">";
		echo "<thead>";
		echo "<tr><th>Numero</th><th>Data</th><th>Crediti acquistati</th><th>Importo</th><th></th></tr>";
		echo "</thead><tbody>";
		foreach ($queryPAG->result() as $rowPAG):
			echo "<tr>";
			echo "<td>".$rowPAG->codice." / ".date('y', $rowPAG->data_pagamento)."</td>";
			echo "<td>".date('d/m/y', $rowPAG->data_pagamento)."</td>";
			echo "<td>".$rowPAG->crediti."</td>";
			echo "<td>€ ".$rowPAG->importo_totale."</td>";
			echo "<td>";
				echo '<div class="btn-group">';
				echo "<a href=\"".$this->config->item('base_url')."index.php/users/stampa_fattura/".$rowPAG->id."\" class=\"btn btn-default\"><i class=\"fa fa-file-pdf-o\" style=\"width: 20px;\"></i></a>";
				echo "</div>";
			echo "</td>";
			echo "</tr>";
		endforeach;
		echo "</tbody>";
		echo "</table>";
		echo "</div>";

	endif;

	// echo "</div>";
	?>
</div>

	 <div id="price-box" class="col-sm-6 col-sm-offset-1">

	 <div id="price-box-outer">

      <div class="price-box">

        <form class="form-horizontal form-pricing" role="form" onsubmit="return pagamento();">

          <div class="price-form text-center">

			<h4 class="great" style="margin-left: 0px">Ricarica il tuo account</h4>

            <div class="form-group text-center top-buffer">
              <div class="col-sm-4 text-right" style="padding-top: 10px">
				  <i class="fa fa-database" style="font-size: 1.8em"></i>
			  </div>
              <div class="col-sm-4 text-center">
                <input type="text" id="crediti" class="form-control input-lg" style="height: 1.8em; font-size: 1.8em; font-weight: bold; margin: 0 auto" onfocus="$('.price-slider-outer').hide(); $('.price-slider-outer-2').show();" onkeyup="calcola_importo()">

                <span class="price"></span>
              </div>
              <div class="col-sm-4 text-left" style="padding-top: 20px">
				  crediti
			  </div>
            </div>


			<div class="price-slider-outer top-buffer" style="width: 100%;">

				<div class="col-sm-2" style="top: -5px; color: #999">
					<i class="fa fa-database"></i>50
				</div>
				
				<div class="price-slider col-sm-6 col-sm-offset-1" style="margin-bottom: 5px">
					<div class="col-sm-12">
					  <div id="slider"></div>
					</div>
				</div>

				<div class="col-sm-3" style="top: -5px; color: #999">
					<i class="fa fa-database"></i>1500
				</div>

			</div>

			<div class="price-slider-outer" style="width: 100%; clear: both; color: #999;">
				utilizza il cursore o inserisci la quantit&agrave;
			</div>
			
			<div class="price-slider-outer-2" style="display: none; width: 100%; clear: both; color: #999;">
				inserisci la quantit&agrave;
			</div>
			
			

            <div class="form-group top-buffer">
              <label for="amount" class="col-sm-6 control-label">Importo €: </label>
              <span class="help-text" style="font-size: 0.8em;">1 credito = <span id="costo_credito"></span> €</span>
              <div class="col-sm-6">
                <input type="hidden" id="amount" class="form-control">
                <p class="price lead" id="amount-label"></p>
                <span class="price">,00 + iva</span>
              </div>
            </div>


          </div>

          <div class="form-group">
            <div class="col-sm-12">
              <button type="submit" class="btn btn-primary btn-lg btn-block">Prosegui ></button>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <img src="http://amirolahmad.github.io/bootstrap-pricing-slider/images/payment.png" class="img-responsive payment" />
            </div>
          </div>


		<input type="hidden" id="eimporto_pagamento" name="eimporto_pagamento">

        </form>

      </div>

    </div>

	</div>

	<div id="conferma_pagamento" style="display: none;">
	<!-- form di conferma del pagamento -->
	</div>	

</div>




<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>

<script language="Javascript">

$(document).ready(function() {
          $("#slider").slider({
              animate: true,
              value:1,
              min: 50,
              max: 1500,
              step: 50,
              slide: function(event, ui) {
                  update(1,ui.value); //changed
              }
          });

          $("#crediti").val(50);
          update();
      });

      function update(slider,val) {
		
		var $crediti = slider == 1?val:$("#crediti").val();  
		$( "#crediti" ).val($crediti);
        calcola_importo();
        
      }
      
      function check_crediti() {
		  var $crediti = $("#crediti").val();  
		  // $crediti=Math.round($crediti);
		  if (($crediti<1) || (!$.isNumeric($crediti)) )
				$crediti=0;
		  if ($crediti>10000)
			$crediti=10000;
		 $crediti=Math.round($crediti);
		  $("#crediti").val($crediti);
		}
      
      function calcola_importo() {
		
		check_crediti();
		 
		var $crediti = $("#crediti").val();  
        var par0=-0.00002833;
        var par1=1.324;
        var par2=20.29;
        
        var importo_temp=par0*$crediti*$crediti + par1*$crediti + par2;
        
        var $importo_totale=Math.round(importo_temp/5)*5;
        
        if ($importo_totale<0)
			$importo_totale="-";
        
        if (Math.round($crediti)==0)
			$importo_totale=0;
        
        var $amount = slider == 1?val:$("#amount").val();
        var $duration = slider == 2?val:$("#duration").val();
		var $crediti_residui=<?php echo $crediti_residui; ?>;
		var $crediti_totali=Math.floor($crediti_residui)+Math.round($crediti);

		var $credito_singolo=$importo_totale/$crediti;
		$credito_singolo=Math.ceil($credito_singolo*1000)/1000;

        $( "#amount-label" ).text($importo_totale);
        if (Math.round($crediti)>0)
			$( "#costo_credito" ).text($credito_singolo);
        else
			$( "#costo_credito" ).text('-');
		
		$( "#crediti_totali-label" ).text($crediti_totali);
		
		$( "#eimporto_pagamento" ).val($importo_totale);
		
        $('#slider a').html('<label><span class="glyphicon glyphicon-chevron-left" style="top: -2px;"></span><span class="glyphicon glyphicon-chevron-right" style="top: -2px;"></span></label>');
      }
</script>

<?php endif; ?>
