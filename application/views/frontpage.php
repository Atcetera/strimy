<?php

echo "<h1><i class=\"fa fa-cubes\"></i> Dashboard</h1>";

echo "<hr>";

$utente=$this->flexi_auth->get_user_id();
$amministratore=$this->strimy_model->utente_tipologia($utente);
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataRET=$this->strimy_model->recupera_gruppi_dispositivi();
$id_gruppo=$dataRET['id_gruppo'];
$desc_gruppo=$dataRET['desc_gruppo'];

echo "<a href=\"".base_url()."index.php/strimy/attivazione_dispositivo\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-hdd-o\"></i> Attiva dispositivo</a>";
echo " <a href=\"".base_url()."index.php/strimy/gestisci_gruppi_dispositivo/\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-cubes\"></i> Gestisci gruppi</a>";


$yy=0;
$zz=0;

foreach ($id_gruppo as $id_gruppo_temp):
	$this->db->from('dispositivi');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('attivato', 1);
	$this->db->where('id_gruppo', $id_gruppo_temp);
	$this->db->order_by('id', 'asc');

	$queryDS=$this->db->get();

	if ($queryDS->num_rows()>0):

		if ($id_gruppo[$yy]!=0):
			echo "<div class=\"panel panel-info top-buffer\">";
			echo "<div class=\"panel-heading\">Gruppo ".$desc_gruppo[$yy]."</div>";
			echo "<div class=\"panel-body\">";
		endif;

		$xx=0;

		echo '<table class="table table-striped';
		if ($id_gruppo[$yy]==0)
			echo " top-buffer";
		echo '">';
		echo '<thead><tr>';
		echo '<th style="width: 15%;">dispositivo</th>';
		echo '<th style="width: 20%;">descrizione</th>';
		echo '<th>stato</th>';
		echo '<th></th></tr></thead>';
		
		echo "<tbody>";

		foreach ($queryDS->result() as $rowDS):
			
			echo "<tr>";

			echo "<td>";
			echo $rowDS->id_dispositivo;
			echo "</td>";
			
			echo "<td>";
			echo $rowDS->descrizione;
			echo "</td>";
			
			echo "<td>";
			echo "<span id=\"".$rowDS->id_dispositivo."\"></span>";
			echo "</td>";
			
			echo "<td class=\"text-right\">";
			echo "<input type=\"hidden\" id=\"id_dispositivo_".$zz."\" name=\"id_dispositivo[]\" value=\"".$rowDS->id_dispositivo."\" readonly=\"readonly\">";
			
			echo '<div class="btn-group">';
			// echo "<a href=\"".$this->config->item('base_url')."index.php/layout/indice/".$rowDS->id_dispositivo."/0\" class=\"btn btn-default\"><i class=\"fa fa-list\" style=\"width: 20px;\"></i></a>";
			echo "<a href=\"".base_url()."index.php/strimy/configurazione_dispositivo/".$rowDS->id_dispositivo."\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-gear\"></i></a>";
			echo "<a href=\"".$this->config->item('base_url')."index.php/strimy/dispositivo_comando_remoto/".$rowDS->id_dispositivo."\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><i class=\"fa fa-bolt\"></i></a>";
			echo "</div>";

			echo ' <div class="btn-group">';
			echo "<a href=\"".$this->config->item('base_url')."index.php/strimy/dispositivo_elimina/".$rowDS->id_dispositivo."\" class=\"btn btn-default\" onClick=\"return confirm('Confermi l\'eliminazione?');\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
			echo "</div>";
			
			/*
			if ($amministratore==1):
				echo ' <div class="btn-group">';
				echo "<a href=\"".$this->config->item('base_url')."index.php/strimy/check_dispositivo/playlist/".$rowDS->id_dispositivo."\" alt=\"playlist\" target=\"_blank\" class=\"btn btn-default\"><i class=\"fa fa-bolt\"></i></a>";
				echo "<a href=\"".$this->config->item('base_url')."index.php/strimy/check_dispositivo/elenca_contenuti/".$rowDS->id_dispositivo."\" alt=\"elenca contenuti\" target=\"_blank\" class=\"btn btn-default\"><i class=\"fa fa-bolt\"></i></a>";
				echo "</div>";
			endif;
			*/
			echo "</td>";

			echo "</tr>";

			$xx++;
			$zz++;
		endforeach;

		echo "</tbody>";
		echo "</table>";

		if ($id_gruppo[$yy]!=0):
			echo "</div></div>"; // chiude il pannello
		endif;


	endif;

	$yy++;
endforeach;

echo "<input type=\"hidden\" id=\"totale_dispositivi\" name=\"totale_dispositivi\" value=\"".$zz."\">";

?>

<script language="Javascript">

	var timer=60000; // pausa tra i ping, espressa in millisecondi (valore consigliato: 60000)

	function check_all() {
		var totale_dispositvi=document.getElementById("totale_dispositivi").value;
		var valore="";
		var ii=0;
		while (ii < totale_dispositvi) {
			valore=document.getElementById("id_dispositivo_"+ii).value;
			risultato=check(valore);
			document.getElementById(valore).innerHTML=risultato;
			ii++;
		}
	}

	function check(dispositivo_id) {
		var result = null;
		var scriptUrl = "<?php echo $this->config->item('base_url'); ?>index.php/strimy/check_dispositivo/leggi/"+dispositivo_id;
		     $.ajax({
			url: scriptUrl,
			type: 'get',
			dataType: 'html',
			async: false,
			success: function(data) {
			    risultato = data;
				} 
		    });
		    return risultato;
	}

	check_all();
	var intervallo=setInterval(check_all,timer);
	
</script>


