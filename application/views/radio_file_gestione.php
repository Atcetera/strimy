<script language="Javascript">
	function cambia_cancellazione(casella, privilegio) {
		if (casella.checked)
			$("#ecancella").val('1');
		else
			$("#ecancella").val('0');
	}
	
	function check_cancella() {
		cancellazione=$("#ecancella").val();
		if (cancellazione==1) {
			if (confirm('Desideri cancellare il file?'))
				return true;
			else
				return false;
		}
		return true;
	}
	
	function cambia_tipologia(casella) {
		if (casella.value==0) {
			$("#tabella_brano").show(50);
			$("#tabella_categoria").hide(10);
			$(".asterisco").show();
		}
		if (casella.value==1) {
			$("#tabella_brano").hide(10);
			$("#tabella_categoria").show(50);
			$(".asterisco").hide();
		}
		$("#ejingle").val(casella.value);
	}
	
</script>

<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
$privilegi=$this->strimy_model->account_privilegi($grupposelezionato);

$percorso_base=$this->config->item('real_path');

if ($tipologia_file=="audio"):
	$tipo_file=0;
	$descrizione="brano musicale";
elseif ($tipologia_file=="video"):
	$tipo_file=1;
	$descrizione="video musicale";
endif;

$dataRET=$this->strimy_model->recupera_gruppi_dispositivi();
$id_gruppo=$dataRET['id_gruppo'];
$desc_gruppo=$dataRET['desc_gruppo'];

if ($operazione=="gestisci_brano"): // form di inserimento/gestione dei file

	$upload_max_filesize = (int)(ini_get('upload_max_filesize'));

	$genere=$this->config->item('genere');
	$chiave=$this->config->item('chiave');
	$power=$this->config->item('power');

	if ($id_file==0):
		$cnome_file="";
		$cartista="";
		$ctitolo="";
		$canno_pubblicazione="";
		$cgenere[0]=0;
		$cgenere[1]=0;
		$cgenere[2]=0;
		$cgenere[3]=0;
		$cbpm=0;
		$cchiave=0;
		$cpower=0;
		$cjingle=0;
		$cjingle_categoria=0;
		$cpunteggio=0;
		$ctipologia_file=$tipo_file;
	else:
		$this->db->from('archivio_radio');
		$this->db->where('id', $id_file);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		$row=$query->row();
		$cnome_file=$row->nome_file;
		$cartista=$row->artista;
		$ctitolo=$row->titolo;
		$canno_pubblicazione=$row->anno_pubblicazione;
		if ($canno_pubblicazione==0) $canno_pubblicazione="";
		$cbpm=$row->bpm;
		$cgenere[0]=$row->genere_1;
		$cgenere[1]=$row->genere_2;
		$cgenere[2]=$row->genere_3;
		$cgenere[3]=$row->genere_4;
		$cchiave=$row->chiave;
		$cpower=$row->power;
		$cjingle=$row->jingle;
		$cjingle_categoria=$row->jingle_categoria;
		$cpunteggio=$row->punteggio;
		$ctipologia_file=$row->tipologia_file;
	endif;

	echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/gestione_file_mp3_salva\" enctype=\"multipart/form-data\" onsubmit=\"return check_cancella()\">";

	if ($id_file==0):
		echo "<table>";
		echo "<tr><td></td><td></td></tr>";
		echo "<tr><td></td>Tipologia<td>";
		echo "<input type=\"radio\" name=\"tipologia\" value=\"0\" onclick=\"cambia_tipologia(this);\" checked=\"checked\"> ".$descrizione;
		echo "<input type=\"radio\" name=\"tipologia\" value=\"1\" onclick=\"cambia_tipologia(this);\"> jingle pubblicitario";
		echo "</td></tr>";
		echo "</table>";
	endif;

	// selezione brano
	echo "<table>";
	echo "<tr>";
	echo "<td style=\"width: 130px;\">Artista<span class=\"asterisco\"";
	if ($cjingle==1) echo " style=\"display: none\"";	
	echo "> *</span></td>";
	echo "<td><input type=\"text\" name=\"eartista\" value=\"".$cartista."\"></td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td>Titolo<span class=\"asterisco\"";
	if ($cjingle==1) echo " style=\"display: none\"";	
	echo "> *</span></td>";
	echo "<td><input type=\"text\" name=\"etitolo\" value=\"".$ctitolo."\"></td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td>Anno</td>";
	echo "<td><input type=\"text\" name=\"eanno_pubblicazione\" value=\"".$canno_pubblicazione."\" style=\"width: 50px;\" maxlength=\"4\"></td>";
	echo "</tr>";

	echo "</table>";
	
	echo "<table id=\"tabella_brano\"";
	if ($cjingle==1) echo " style=\"display: none\"";	
	echo ">";

	$xx=0;
	while ($xx<4) {
		echo "<tr>";
		echo "<td style=\"width: 130px;\">";
		if ($xx==0) echo "Genere";
		else echo "Sottogenere n.".$xx;
		echo "</td>";
		echo "<td>";
		echo "<select name=\"egenere_".$xx."\">";
		$yy=0;
		foreach ($genere as $genere_temp):
			echo "<option value=\"".$yy."\"";
			if ($cgenere[$xx]==$yy)
				echo " selected";
			echo ">";
			echo $genere_temp;
			echo "</option>";
			$yy++;
		endforeach;
		echo "</select>";
		echo "</td>";
		echo "</tr>";
	$xx++;
	}

	echo "<tr>";
	echo "<td>Bpm</td>";
	echo "<td><input type=\"text\" name=\"ebpm\" value=\"".$cbpm."\" style=\"width: 50px;\" maxlength=\"4\"></td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td>Chiave</td>";
	echo "<td>";
	echo "<select name=\"echiave\">";
	$yy=0;
	foreach ($chiave as $chiave_temp):
		echo "<option value=\"".$yy."\"";
		if ($cchiave==$yy)
			echo " selected";
		echo ">";
		echo $chiave_temp;
		echo "</option>";
		$yy++;
	endforeach;
	echo "</select>";
	echo "</td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td>Emozione</td>";
	echo "<td>";
	echo "<select name=\"epower\">";
	$yy=0;
	foreach ($power as $power_temp):
		echo "<option value=\"".$yy."\"";
		if ($cpower==$yy)
			echo " selected";
		echo ">";
		echo $power_temp;
		echo "</option>";
		$yy++;
	endforeach;
	echo "</select>";
	echo "</td>";
	echo "</tr>";
	
	echo "<tr>";
	echo "<td>Punteggio</td>";
	echo "<td><input type=\"text\" name=\"epunteggio\" value=\"".$cpunteggio."\" style=\"width: 120px;\" maxlength=\"15\"></td>";
	echo "</tr>";
	
	echo "</table>";

	// seleziona categoria jingle
	echo "<table id=\"tabella_categoria\"";
	if ($cjingle==0) echo " style=\"display: none\"";
	echo ">";
	echo "<tr>";
	echo "<td style=\"width: 130px;\">Categoria</td>";
	echo "<td>";
	echo "<select name=\"ejingle_categoria\">";
	echo "<option value=\"0\"";
	if ($cjingle_categoria==0) echo " selected";
	echo ">--</option>";
	$this->db->from('archivio_radio_jingle_categorie');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('tipologia_file', $tipo_file);
	$this->db->order_by('descrizione', 'asc');
	$queryCAT=$this->db->get();
	foreach ($queryCAT->result() as $rowCAT):
		echo "<option value=\"".$rowCAT->id."\"";
		if ($cjingle_categoria==$rowCAT->id) echo " selected";
		echo ">".$rowCAT->descrizione."</option>";
	endforeach;
	echo "</select>";
	echo "</td>";
	echo "</tr>";
	echo "</table>";


	echo "<table>";
	
	echo "<tr>";
	echo "<td>File<br>";
	echo "<small>(max ".$upload_max_filesize."mb)</small>";
	echo "</td>";
	echo "<td>";
	if ($cnome_file!=""):
		echo "<span style=\"width: 300px; overflow: hidden; float: left; position: relative;\">".$cnome_file."</span>";
		if ($cjingle==0) echo " (<input type=\"checkbox\" name=\"ecancella_checkbox\" onClick=\"cambia_cancellazione(this);\"> cancella)";
		echo "<br>sostituisci ";
	endif;
		echo "<input type=\"file\" name=\"efile\" />";
	echo "</td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td></td>";
	echo "<td>";
	echo "<input type=\"hidden\" name=\"etipologia_file\" value=\"".$ctipologia_file."\">";
	echo "<input type=\"hidden\" id=\"ecancella\" name=\"ecancella\" value=\"0\">";
	echo "<input type=\"hidden\" id=\"ejingle\" name=\"ejingle\" value=\"".$cjingle."\">";
	echo "<input type=\"hidden\" name=\"eid_file\" value=\"".$id_file."\">";
	echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\">";
	echo "<br><input type=\"submit\" value=\"conferma\">";
	echo "</td>";

	echo "</tr>";
	echo "</table>";
	echo "</form>";


elseif ($operazione=="carica_classifica"):
	// echo "<div style=\"position: relative; float: left;\">";
	echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/aggiorna_classifica\" enctype=\"multipart/form-data\">";
	echo "aggiorna classifica <input type=\"file\" name=\"efile\" />";
	echo "<br>";
	echo "numero di brani da importare <input type=\"text\" name=\"emax_records\" value=\"150\">";
	echo "<br><br>";
	echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
	echo "<input type=\"submit\" value=\"Upload\" />";
	echo "</form>";
	// echo "</div>";

elseif ($operazione=="gestisci_categorie"):
	$this->db->from('archivio_radio_jingle_categorie');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('tipologia_file', $tipo_file);
	$this->db->order_by('descrizione', 'asc');
	$query=$this->db->get();
	echo "<table>";
	foreach ($query->result() as $row):
		echo "<tr><td>";
		echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/aggiorna_categoria\">";
		echo "<input type=\"text\" name=\"edescrizione_categoria\" value=\"".$row->descrizione."\">";
		echo " <select name=\"eid_gruppo_dispositivo\" style=\"width: 150px;\">";
		$xx=0;
		foreach ($id_gruppo as $temp):
			if ($temp==0)
				$desc_gruppo[$xx]="(tutti i dispositivi)";
			echo "<option value=\"".$temp."\"";
			if ($row->id_gruppo_dispositivo==$temp)
				echo " selected";
			echo ">".$desc_gruppo[$xx]."</option>";
			$xx++;
		endforeach;
		echo "</select>";
		echo "<input type=\"hidden\" name=\"eid_categoria\" value=\"".$row->id."\">";
		echo "<input type=\"hidden\" name=\"etipologia_file\" value=\"".$tipo_file."\">";
		echo " <input type=\"submit\" value=\"aggiorna\">";
		echo " <a href=\"".base_url()."index.php/mp3/elimina_categoria/".$row->id."\" onClick=\"return confirm('Confermi l\'eliminazione?');\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
		echo "</form>";
		echo "</td></tr>";
	endforeach;
	echo "<tr><td>";
	echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/aggiorna_categoria\">";
	echo "<input type=\"text\" name=\"edescrizione_categoria\" value=\"\">";
	echo " <select name=\"eid_gruppo_dispositivo\" style=\"width: 150px;\">";
		$xx=0;
		foreach ($id_gruppo as $temp):
			if ($temp==0)
				$desc_gruppo[$xx]="(tutti i dispositivi)";
			echo "<option value=\"".$temp."\"";
			if ($temp==0)
				echo " selected";
			echo ">".$desc_gruppo[$xx]."</option>";
			$xx++;
		endforeach;
		echo "</select>";
	echo "<input type=\"hidden\" name=\"eid_categoria\" value=\"0\">";
	echo "<input type=\"hidden\" name=\"etipologia_file\" value=\"".$tipo_file."\">";
	echo " <input type=\"submit\" value=\"inserisci\">";
	echo "</form>";
	echo "</td></tr>";
	echo "</table>";
	
endif;

?>

