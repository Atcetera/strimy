<script language="Javascript">
	function file_cancella(origine, id) {
		if (confirm('Vuoi eliminare il file?')) {
			var url='<?php echo base_url(); ?>index.php/strimy/file_cancella/'+origine+'/'+id;
			$.ajax({
			  url: url
			});
			$("#file_"+id).hide(500);
			spazio_account();
		}
	}
</script>

<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

if ($origine=="archivio_file"):
	$dataLISTA['directory_contenuto']="content";
	$dataLISTA['amministratore']=0;
	$titolo="Archivio generale";
	$icon="fa-folder-open-o";
	$link="gestione_archivio";
elseif ($origine=="archivio_sistema"):
	$dataLISTA['directory_contenuto']="../common";
	$dataLISTA['amministratore']=1;
	$titolo="Archivio di sistema";
	$icon="fa-gears";
	$link="gestione_archivio_sistema";
endif;

echo "<h1><i class=\"fa ".$icon."\"></i> ".$titolo."</h1>";

echo "<hr>";

$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);

$url_base=$dataSTORAGE['full_path'].$this->config->item('percorso_users').$stringa_casuale."/".$dataLISTA['directory_contenuto'];


echo "<a href=\"".base_url()."index.php/strimy/file_gestisci/".$origine."/0/".$tipologia_file."\" data-toggle=\"modal\" data-target=\"#finestra-modale\" class=\"btn btn-default\"><i class=\"fa fa-cloud-upload\"></i> Carica file</a>";

if ($messaggio==1):
	echo "<div id=\"messaggio_alert\">";
	echo "Errore in fase di salvataggio";
	echo "</div>";
endif;


// definisce le tipologie di contenuti
$dataTIP[-1]="tutti i file";
$dataTIP[0]="audio";
$dataTIP[1]="video";
$dataTIP[2]="immagini";

// definisce l'ordine di visualizzazione
$dataTIP_O[0]=-1;
$dataTIP_O[1]=2;
$dataTIP_O[2]=1;
$dataTIP_O[3]=0;

echo "<br><br>";
echo "<ul class=\"nav nav-tabs top-buffer\">";
$xx=0;
while ($xx<count($dataTIP_O)) {
	$yy=$dataTIP_O[$xx];
	if ($yy==$tipologia_file)
		echo "<li class=\"active\">";
	else
		echo "<li>";
	echo "<a href=\"".base_url()."index.php/strimy/".$link."/".$yy."\">";
	echo $dataTIP[$yy];
	echo "</a></li>";
	$xx++;
}
echo "</ul>";

$dataLISTA['tipologia_file']=$tipologia_file;
$dataRET=$this->strimy_model->lista_file_archivio($dataLISTA);


if ($dataRET['totale_file']==0):

	echo "<div class=\"top-buffer\">";
	echo "Archivio vuoto";
	echo "</div>";

else:

	echo '<table class="table table-striped sortable top-buffer">';
	echo '<thead><tr><th data-defaultsort="disabled">anteprima</th>';
	if ($tipologia_file==-1)
		echo '<th style="width: 120px;">tipologia</th>';
	echo '<th data-defaultsort="asc">descrizione</th>';
	echo '<th class="text-right" style="width: 130px; padding-right: 20px;">data upload</th>';
	echo '<th data-defaultsort="disabled" style="width: 80px;"></th></tr></thead>';
	

	echo "<tbody>";
	
	$xx=0;
	foreach ($dataRET['elenco_file'] as $file):
		
		echo "<tr id=\"file_".$dataRET['elenco_file_id'][$xx]."\">";
		echo "<td style=\"width: 150px\">";
	
		if ($dataRET['elenco_file_tipologia'][$xx]=="immagini"):
			// $file_anteprima=$url_base."/".$file."_thumb.jpg";
			$file_icona="fa fa-camera";
		elseif ($dataRET['elenco_file_tipologia'][$xx]=="video"):
			// $file_anteprima=$url_base."/".$file."_thumb.jpg";
			$file_icona="fa-video-camera";
		elseif ($dataRET['elenco_file_tipologia'][$xx]=="audio"):
			// $file_anteprima=base_url()."/style/icone/archive_music.jpg";
			$file_icona="fa fa-volume-up";
		endif;
		$file_anteprima=$url_base."/".$dataRET['elenco_file_thumb'][$xx];
		echo "<div class=\"elemento_archivio_gestione on_hover\" href=\"".$this->config->item('base_url')."index.php/strimy/player/".$origine."/".$dataRET['elenco_file_id'][$xx]."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"";
		echo " style=\"background-image: url('".$file_anteprima."');\"";
		echo ">";
		
		echo "</td>";
		
		if ($tipologia_file==-1):
			echo "<td>";
			echo "<span class=\"label label-info\"";
			if ($dataRET['elenco_file_tipo'][$xx]==0)
				echo " style=\"background-color: orange\"";
			elseif ($dataRET['elenco_file_tipo'][$xx]==1)
				echo " style=\"background-color: lightgreen\"";
			echo ">";
			echo $dataRET['elenco_file_tipologia'][$xx];
			echo "</span>";
			echo "</td>";
		endif;
		
		echo "<td style=\"width: 60%; overflow: hiddem; margin-right: 20px;\">".str_replace("_", " _", $dataRET['elenco_file_descrizione'][$xx])."</td>";
		
		echo "<td class=\"text-right\" data-dateformat=\"DD-MM-YY\">";
		if ($dataRET['elenco_file_data_upload'][$xx]!=0)
			echo date('d/m/y', $dataRET['elenco_file_data_upload'][$xx]);
		echo "</td>";
	
		echo "<td class=\"text-right\" style=\"width: 120px;\">";
		echo "<div class=\"btn-group\">";
		echo "<a href=\"".base_url()."index.php/strimy/file_gestisci/".$origine."/".$dataRET['elenco_file_id'][$xx]."/".$dataRET['elenco_file_tipo'][$xx]."\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><i class=\"fa fa-pencil\" style=\"width: 20px;\"></i></a>";
		echo " <a href=\"javascript:file_cancella('".$origine."','".$dataRET['elenco_file_id'][$xx]."');\" class=\"btn btn-default\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
	//	echo "<a href=\"".base_url()."index.php/strimy/file_cancella/".$origine."/".$dataRET['elenco_file_id'][$xx]."\" class=\"btn btn-default\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
		echo "</div>";
		
		echo "</td>";
		
		echo "</tr>";
		
			
			
		$xx++;
	endforeach;
	
	echo "</tbody>";
	echo "</table>";
	
	

endif;
	

?>

