<script language="Javascript">

function paypal_pagamento() {

	/*
	var stringa = $('#dettagli_account').serialize();
	
	$.ajax({

	  url: "<?php echo base_url(); ?>index.php/users/gestione_account_dati_salva", 
	  data: stringa,
	  async: false,
	  dataType: 'html',
	  method: 'POST',
	  success: function(risultato) {
			$("#conferma_pagamento").fadeOut(50);
			$("#conferma_pagamento").html(risultato);
			$("#conferma_pagamento").fadeIn(500);
			}
	});
	*/
	
	window.open('', 'popup','height=600,width=800,left=100,top=100,status=no,menu=no,scrollbars=no');
	window.location.href = '<?php echo base_url(); ?>index.php/users/gestione_account_pagamento/conferma';
	
	return false;
	
	}

</script>

<?php
	$gruppo=$this->flexi_auth->get_user_group_id();
	$tipologia=$this->strimy_model->account_tipologia($gruppo);
	$grupposelezionato=$gruppo;
	$utente=$this->flexi_auth->get_user_id();
	$amministratore=$this->strimy_model->utente_tipologia($utente);

	$crediti_residui=$this->users_model->crediti_residui($grupposelezionato);


	if ($operazione=="pagamento"):

		$importo=$this->input->post("eimporto");
		$crediti=$this->input->post("ecrediti");

		$iva=$this->config->item('iva');
		$importo_iva=$importo*$iva/100;
		$importo_totale=$importo+$importo_iva;

		echo "Importo: ".$importo."<br>";
		echo "Importo totale: ".($importo_totale)."<br>";
		echo "Crediti: ".$crediti."<br>";
		
		$timestamp=time();
		
		if ($this->config->item('paypal_sandbox')==0):
			$paypal_account=$this->config->item('paypal_account');
			$paypal_url="https://www.paypal.com/cgi-bin/webscr";
			$test_pagamento=0;
		else:
			$paypal_account=$this->config->item('paypal_sandbox_account');
			$paypal_url="https://www.sandbox.paypal.com/cgi-bin/webscr";
			$test_pagamento=1;
		endif;

		$descrizione_oggetto="Strimy - acquisto ".$crediti." crediti";
		
	//	$sessione="x";  // valore deprecato e mantenuto per compatibilità

		$data_serializzato=$importo_totale."///".$grupposelezionato."///".$crediti."///".$timestamp;

		echo "<form action=\"".$paypal_url."\" style=\"margin: 0px; text-align: center\" method=\"post\" target=\"popup\" onSubmit=\"paypal_pagamento();\">";
		echo "<input type=\"hidden\" name=\"cmd\" value=\"_xclick\">";
		echo "<input type=\"hidden\" name=\"currency_code\" value=\"EUR\" />";
		echo "<input type=\"hidden\" name=\"lc\" value=\"IT\" />";
		echo "<input type=\"hidden\" name=\"item_name\" value=\"".$descrizione_oggetto."\" />";
		echo "<input type=\"hidden\" name=\"amount\" value=\"".$importo_totale."\" />";
		echo "<input type=\"hidden\" name=\"business\" value=\"".$paypal_account."\" />";
		echo "<input type=\"hidden\" name=\"notify_url\" value=\"".base_url()."index.php/paypal/ricevi_pagamento\" />";
		echo "<input type=\"hidden\" name=\"custom\" value=\"".$data_serializzato."\" />";
		echo "<input type=\"submit\" value=\"Paga con PayPal\">";
		echo "</form>";
		
		$dataPAG['id_account']=$grupposelezionato;
		// $dataPAG['session_id']=$sessione;
		$dataPAG['timestamp']=$timestamp;
		$dataPAG['descrizione']=$descrizione_oggetto;
		$dataPAG['percentuale_iva']=$iva;
		$dataPAG['importo_totale']=$importo_totale;
		// $dataPAG['importo_imponibile_normale']=$importo_imponibile_normale;
		$dataPAG['importo_imponibile']=$importo;
		$dataPAG['importo_iva']=$importo_iva;
		$dataPAG['crediti']=$crediti;
		$dataPAG['pagato']=0;
		$dataPAG['test_pagamento']=$test_pagamento;

		$this->db->from('user_groups_dettagli');
		$this->db->where('id_account', $grupposelezionato);
		$queryANA=$this->db->get();
		if ($queryANA->num_rows()>0):
			$rowANA=$queryANA->row();
			$dataPAG['ana_nome_cognome']=$rowANA->nome_cognome;
			$dataPAG['ana_ragione_sociale']=$rowANA->ragione_sociale;
			$dataPAG['ana_partita_iva']=$rowANA->partita_iva;
			$dataPAG['ana_codice_fiscale']=$rowANA->codice_fiscale;
			$dataPAG['ana_indirizzo1']=$rowANA->indirizzo;
			$dataPAG['ana_indirizzo2']=$rowANA->cap." ".$rowANA->citta." ".$rowANA->provincia;
		endif;

		$this->db->from('paypal_pagamenti');
		$this->db->where('id_account', $grupposelezionato);
		$this->db->where('pagato', 0);
		$queryCP=$this->db->get();
		if ($queryCP->num_rows()==0):
			$this->db->insert('paypal_pagamenti', $dataPAG);
		else:
			$rowCP=$queryCP->row();
			$this->db->where('id', $rowCP->id);
			$this->db->update('paypal_pagamenti', $dataPAG);
		endif;

	
	elseif ($operazione=="conferma"):
	
		echo "<div style=\"width: 100%; text-align: center; margin-top: 100px; font-size: 1.1em;\">";
		echo "Segui le istruzioni sul sito di PayPal: una volta completato il pagamento, <a href=\"".base_url()."index.php/users/gestione_account\">clicca qui</a> per tornare alla schermata di gestione dell'account";
		echo "</div>";
	
	endif;
	
?>	

