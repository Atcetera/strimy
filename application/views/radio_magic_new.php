<?php


$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
$privilegi=$this->strimy_model->account_privilegi($grupposelezionato);

$percorso_base=$this->config->item('real_path');

/*
$this->db->from('dispositivi');
$this->db->where('id_dispositivo', $id_dispositivo);
$this->db->where('id_account', $grupposelezionato);
$queryDISP=$this->db->get();
$rowDISP=$queryDISP->row();

if ($id_gruppo_dispositivo!=0):
	$this->db->from('dispositivi_gruppi');
	$this->db->where('id', $id_gruppo_dispositivo);
	$this->db->where('id_account', $grupposelezionato);
	$queryGRUPPO=$this->db->get();
	$rowGRUPPO=$queryGRUPPO->row();
	$descrizione_gruppo_dispositivo=$rowGRUPPO->descrizione;
endif;
*/

$chiave=$this->config->item('chiave');
$genere=$this->config->item('genere');

$giorno_settimana=Array();
$giorno_settimana[]="luned&igrave;";
$giorno_settimana[]="marted&igrave;";
$giorno_settimana[]="mercoled&igrave;";
$giorno_settimana[]="gioved&igrave;";
$giorno_settimana[]="venerd&igrave;";
$giorno_settimana[]="sabato";
$giorno_settimana[]="domenica";


if ($operazione=="creazione"):

	function inserisci_jingle($data) {
			
		$this2=$data['this'];
		$indice=$data['indice'];
		$grupposelezionato=$data['grupposelezionato'];

		$this2->db->from('archivio_radio_jingle_categorie');
		$this2->db->where('id_account', $grupposelezionato);
		$this2->db->order_by('id', 'asc');
		$query=$this2->db->get();
			
		echo "jingle n.".($indice+1)." <select name=\"esequenza_jingle_categoria[".$indice."]\">";

		echo "<option value=\"0\" selected>nessuno</option>";
		foreach ($query->result() as $row):
			$this2->db->from('archivio_radio');
			$this2->db->where('id_account', $grupposelezionato);
			$this2->db->where('jingle', 1);
			$this2->db->where('disattivo', 0);
			$this2->db->where('jingle_categoria', $row->id);
			$queryCNT=$this2->db->get();
			echo "<option value=\"".$row->id."\">".$row->descrizione." (".$queryCNT->num_rows().")</option>";
		endforeach;
		echo "</select>";

		echo " intervallo <input type=\"text\" name=\"esequenza_jingle_intervallo[".$indice."]\" size=\"3\" value=\"5\">";
		echo "<input type=\"hidden\" name=\"esequenza_jingle_id[".$indice."]\" value=\"0\">";

	}

/*
	$this->db->from('dispositivi_gruppi');
	$this->db->where('id', $id_gruppo_dispositivo);
	
	$queryGRP=$this->db->get();
	if ($queryGRP->num_rows()==0)
		die('non è possibile accedere ai dati');
		
	$rowGRP=$queryGRP->row();

	$descrizione_gruppo_dispositivo=$rowGRP->descrizione;
*/
	
	?>
	<script language="Javascript">
		function cambia(definizione, indice, valore) {
			definizione=definizione+"_"+indice;
			valore_attuale=$("#"+definizione+"_"+valore).val();
			if (valore_attuale==0)
				$("#"+definizione+"_"+valore).val('1');
			else
				$("#"+definizione+"_"+valore).val('0');
			totale_brani(indice);
		}
		
		function checkform() {
			var totale_sequenze=$("#enumero_sequenze").val();
			var xx=0;
			var errore=0;
			while (xx<totale_sequenze) {
				attiva=$("#eattiva_"+xx).val();
				if (attiva==1) {
					durata=$("#edurata_"+xx).val();
					// fine=$("#efine_"+xx).val();
					
					durata_check = new Date("01/01/2013 "+durata);
					// ora_fine = new Date("01/01/2013 "+fine);
					
					if ((durata.length!=5) || (durata=="00:00") || (durata_check=="Invalid Date"))
						errore=1;
				}
			xx++;
			}
			if (errore==1) {
				alert("ATTENZIONE: specificare una durata corretta");
				return false;
			}
			else {
				var messaggio="L'operazione non va a interessare le playlist attualmente in esecuzione e non comporta alcuna interruzione di servizio. ";
				messaggio=messaggio+"Nella schermata successiva verrà visualizzata un'anteprima della playlist generata, che potrà essere revisionata e modificata prima di essere resa attiva.";
				messaggio=messaggio+"\nVuoi proseguire?";
				if (confirm(messaggio)) {
					document.getElementById("conferma_anteprima").submit();
					return true;
					}
				else
					return false;
			}
		}

		
		function rimuovi_sequenza(indice) {
			var numero_sequenze_attive=$("#enumero_sequenze_attive").val();
			if (numero_sequenze_attive==1) {
				alert("Attenzione! E' necessario specificare almeno una fascia oraria");
				return false;
			}
			else {
				if (confirm('Rimuovere la sequenza?')) {
					$("#eattiva_"+indice).val('0');
					$("#div_sequenza_"+indice).hide();
					numero_sequenze_attive=Math.round(numero_sequenze_attive)-1;
					$("#enumero_sequenze_attive").val(numero_sequenze_attive);
					aggiorna_orari();
				}
				else
					return false;
			}
		}

		function aggiorna_orari() {
			var totale_sequenze=$("#enumero_sequenze").val();
			// var sequenza_attuale;
			var xx=0;
			var yy=0;
			while (xx<totale_sequenze) {
				attiva=$("#eattiva_"+xx).val();
				if (attiva==1) {
					if (yy>0) {
						$("#einizio_"+xx).val(ultima_fine);
						$("#einizio_"+xx).attr("readonly", true);
					}
					else {
						$("#einizio_"+xx).attr("readonly", false);
					}
					$("#span_numero_sequenza_"+xx).html(yy+1);
					ultima_fine=$("#efine_"+xx).val();
					yy++;
				}
				xx++;
			}
		}

		function cambia_origine(casella, indice) {
			var valore=casella.options[casella.selectedIndex].value;
			if (valore==2)
				$("#div_rotazione_"+indice).hide(10);
			else
				$("#div_rotazione_"+indice).show(200);
			totale_brani(indice);
		}

		function totale_brani(indice) {
			var xx=0;
			var yy=0;
			var valore="";
			var valore1="";
			var power="";
			var numero_brani="";
			var ripetizioni_brani="";
			var genere = new Array();
			var tipologia_file=$("#etipologia_file_"+indice).val();
			var origine=$("#eorigine_"+indice).val();
			//var origine=casella.options[casella.selectedIndex].value;

			while (xx<4) {
				valore=$("#enumero_brani_"+xx+"_"+indice).val();
				valore1=$("#eripetizioni_brani_"+xx+"_"+indice).val();
				if (xx>0) {
					numero_brani=numero_brani+"-";
					ripetizioni_brani=ripetizioni_brani+"-";
				}
				numero_brani=numero_brani+valore;
				ripetizioni_brani=ripetizioni_brani+valore1;
				xx++;
			}
			
			
			xx=0;
			while (xx<99) {
				if ($("#epower_"+indice+"_"+xx).length) {
					valore=$("#epower_"+indice+"_"+xx).val();
					if (valore==1) {
						if (power!="")
							power=power+"-";
						power=power+xx;
					}
				}
				else {
					xx=100;
				}
				xx++;
			}
			
			while (yy<99) {
				
				if ($("#egenere_"+yy+"_"+indice+"_0").length) {
					genere[yy]="";
					xx=0;
					while (xx<99) {
						
						if ($("#egenere_"+yy+"_"+indice+"_"+xx).length) {
							valore=$("#egenere_"+yy+"_"+indice+"_"+xx).val();
							if (valore==1) {
								if (genere[yy]!="")
									genere[yy]=genere[yy]+"-";
								genere[yy]=genere[yy]+xx;
							}
						}
						else {
							xx=100;
						}
						xx++;
					}
					
				}
					
				else {
					yy=100;
				}
				
				yy++;
			}
			
			var stringa = '';
			stringa=stringa+"etipologia_file="+tipologia_file;
			stringa=stringa+"&eorigine="+origine;
			stringa=stringa+"&enumero_brani="+numero_brani;
			stringa=stringa+"&eripetizioni_brani="+ripetizioni_brani;
			stringa=stringa+"&epower="+power;
			stringa=stringa+"&egenere_1="+genere[0];
			stringa=stringa+"&egenere_2="+genere[1];
			stringa=stringa+"&egenere_3="+genere[2];
			stringa=stringa+"&egenere_4="+genere[3];

			// alert(stringa);
			
			$.ajax({

			  url: "<?php echo base_url(); ?>index.php/mp3/radio_magic_new/0/numero_brani", 
			  data: stringa,
			  async: false,
			  dataType: 'html',
			  method: 'POST',
			  success: function(risultato) {
						risultato=risultato.split(";");
						$("#totale_brani_"+indice).html("Brani selezionati: "+risultato[0]+" - "+"Durata totale: "+risultato[1]);
					}
			});
			return false;


		}

		function aggiungi_sequenza(id, tipologia_file) {
			var numero_sequenze=$("#enumero_sequenze").val();
			var numero_sequenze_attive=$("#enumero_sequenze_attive").val();
			if (numero_sequenze_attive==10) {
				alert("Attenzione! E' stato raggiunto il limite di "+numero_sequenze_attive+" fasce orarie gestibili");
				return false;
			}
			var fine_precedente="09:00";
			if (Math.round(numero_sequenze)>0) {
				numero_sequenza_precedente=Math.round(numero_sequenze)-1;
				fine_precedente=$("#efine_"+numero_sequenza_precedente).val();
			}
			var nuovodiv="";
			nuovodiv='<div id="div_sequenza_'+numero_sequenze+'" style="position: relative; float: left; clear: both; width: 100%; k;"></div><div id="ultimo_indice"></div>';
			$('#ultimo_indice').replaceWith(nuovodiv);
			$('#div_sequenza_'+numero_sequenze).load("<?php echo base_url(); ?>index.php/mp3/radio_magic_new_aggiungi_sequenza/"+id+"/"+"/"+tipologia_file+"/"+numero_sequenze+"/"+fine_precedente, "null", function() {
			});
			numero_sequenze=Math.round(numero_sequenze)+1;
			$("#enumero_sequenze").val(numero_sequenze);
			numero_sequenze_attive=Math.round(numero_sequenze_attive)+1;
			$("#enumero_sequenze_attive").val(numero_sequenze_attive);
			
		}

	</script>

	<?php
		
	if ($id_playlist!=0):
		$this->db->from('magic_temporanea_indice');
		$this->db->where('id', $id_playlist);
		$this->db->where('id_account', $grupposelezionato);
		$this->db->where('tipologia', $tipologia_playlist);
		$queryDESC=$this->db->get();
		$rowDESC=$queryDESC->row();
		$cdescrizione_playlist=$rowDESC->descrizione;
	else:
		$cdescrizione_playlist="";
	endif;



	echo "<form name=\"conferma_anteprima\" id=\"conferma_anteprima\" method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_salva_new\">"; // onsubmit=\"return checkform();\">";
	
	
	// form di salvataggio
	echo "<div id=\"salva_playlist\" class=\"modal fade\">";
	echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
	echo "<div class=\"modal-header\">";
    echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
    echo "<h4 class=\"modal-title\">";
    if ($id_playlist==0)
		echo "Crea playlist";
	else
		echo "Salva modifiche playlist";
    echo "</h4>";
	echo "</div>";
	
	echo "<div class=\"modal-body\">";
	echo "Titolo playlist:<br>";
	echo "<input type=\"text\" name=\"edescrizione_playlist\" value=\"".$cdescrizione_playlist."\">";
	echo "<input type=\"hidden\" name=\"eid_playlist\" value=\"".$id_playlist."\">";
	echo "<input type=\"hidden\" name=\"etipologia_playlist\" value=\"".$tipologia_playlist."\">";
	echo "<br>";
	echo "<input type=\"submit\" onclick=\"return checkform();\" value=\"salva\">";
	echo "</div>";

	echo "<div class=\"modal-footer\">";
    echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
    echo "</div>";
	echo "</div></div>";
	echo "</div>";
	// fine form di salvataggio

	
	echo "<div id=\"playlist\" style=\"margin-bottom: 60px; margin-top: -20px; overflow-x: hidden;\">";


		echo "<div id=\"contenitore_intestazione_playlist\" style=\"position: relative; float: left;\">&nbsp;&nbsp;";
			
			echo "<div id=\"intestazione_playlist\">";
				
				echo "<div class=\"btn-group\">";
				if ($tipologia_playlist==0)
					echo "<button type=\"button\" class=\"btn btn-default fa fa-music\" onclick=\"aggiungi_sequenza('0', '0');\">aggiungi sequenza audio</button>";
				if ($tipologia_playlist==1)
					echo "<button type=\"button\" class=\"btn btn-default fa fa-video-camera\" onclick=\"aggiungi_sequenza('0', '1');\">aggiungi sequenza video</button>";
		//		if ($operazione=="revisione")
		//			echo "<a href=\"".base_url()."index.php/mp3/radio_magic_new/".$id_dispositivo."/revisione/1\"><button type=\"button\" class=\"btn btn-default fa fa-reply\">annulla modifiche</button></a>";
				echo "<a data-toggle=\"modal\" data-target=\"#salva_playlist\"><button type=\"button\" class=\"btn btn-default fa fa-floppy-o\">salva</button></a>";
			//	echo "<button type=\"button\" class=\"btn btn-default fa fa-floppy-o\" onclick=\"checkform();\">salva</button>";
				echo "</div>";
				

				echo "<div style=\"position: relative; float: right;  margin-left: 20px; font-size: 0.8em\">";

					echo "<input type=\"hidden\" name=\"eoperazione\" value=\"crea\">";
					echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\">";

				echo "</div>";

		
				echo "<div style=\"clear: both;\"></div>";
				echo "<br>";
	
			echo "</div>";
	
		echo "</div>";


		echo "<div style=\"position: relative; float: left;\">";

			echo "<input type=\"hidden\" id=\"enumero_sequenze\" name=\"enumero_sequenze\" value=\"0\">";
			echo "<input type=\"hidden\" id=\"enumero_sequenze_attive\" name=\"enumero_sequenze_attive\" value=\"0\">";
			echo "<div id=\"ultimo_indice\"></div>";
			
			?> <script language="Javascript"> <?php
			$this->db->from('magic_temporanea_sequenze');
			$this->db->where('id_account', $grupposelezionato);
			// $this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->where('id_playlist', $id_playlist);
			$this->db->where('id_playlist !=', 0);
			$this->db->order_by('ordine', 'asc');
			$queryOPT=$this->db->get();
			
			if ($queryOPT->num_rows()==0):
			?>	aggiungi_sequenza('0', '<?php echo $tipologia_playlist; ?>'); <?php
			else:
				foreach ($queryOPT->result() as $rowOPT):
				?>	
				aggiungi_sequenza('<?php echo $rowOPT->id; ?>', '0');
				<?php
				endforeach;
			endif;
			?> </script> <?php

		echo "</div>";
	
		// echo "</form>";

	
	echo "</div>";

	echo "</form>";



elseif ($operazione=="revisione"):

	?>
	<script language="Javascript">
		function seleziona_dispositivi_gruppo() {
			var dispositivi_selezionati=$("#edispositivi_selezionati_temp").val();
			$("#edispositivi_selezionati").val(dispositivi_selezionati);
		}
		function checkform() {
			if (confirm("Confermando l'anteprima verranno sovrascritte le playlist attualmente in esecuzione per i dispositivi selezionati, per l'intera settimana.\nL'operazione è irreversibile: vuoi continuare?"))
				document.formcompila.submit();
			else
				return false;
		}
	</script>
	<?php

	$this->db->from('magic_temporanea_indice');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('tipologia', $tipologia_playlist);
	$this->db->where('id', $id_playlist);
	$queryNUS=$this->db->get();
	$rowNUS=$queryNUS->row();
	$cdescrizione_playlist=$rowNUS->descrizione;
	$id_playlist_definitiva=$rowNUS->rif_id_playlist_definitiva;

	echo "<div id=\"playlist\" style=\"margin-bottom: 100px;\">";

		echo "<div id=\"contenitore_intestazione_playlist\">&nbsp;";
			echo "<div id=\"intestazione_playlist\">";

	
				// form di salvataggio
				echo "<div id=\"genera_playlist\" class=\"modal fade\">";
				echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
				echo "<div class=\"modal-header\">";
				echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
				echo "<h4 class=\"modal-title\">";
				echo "Genera playlist";
				echo "</h4>";
				echo "</div>";
				
				echo "<div class=\"modal-body\">";
				echo "Titolo playlist:<br>";
				echo "<form method=\"post\" name=\"formcompila\" action=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_salva_new\">";
				echo "<input type=\"text\" name=\"edescrizione_playlist\" value=\"".$cdescrizione_playlist."\">";
				echo "<input type=\"hidden\" name=\"eid_playlist_temporanea\" value=\"".$id_playlist."\">";
				echo "<input type=\"hidden\" name=\"eid\" value=\"".$id_playlist_definitiva."\">";
				echo "<input type=\"hidden\" name=\"etipologia_playlist\" value=\"".$tipologia_playlist."\">";
				echo "<input type=\"hidden\" name=\"eoperazione\" value=\"compila\">";
				echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\">";
				echo "</form>";
				
				echo "<br>";
				echo "<input type=\"submit\" onclick=\"return checkform();\" value=\"conferma\">";
				echo "</div>";

				echo "<div class=\"modal-footer\">";
				echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
				echo "</div>";
				echo "</div></div>";
				echo "</div>";
		

				echo "<div class=\"btn-group\" style=\"width: 100%;\">";
		
					echo "<div style=\"position: relative; float: left\">";
						echo "<a href=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_new_gestisci/".$id_playlist."/".$tipologia_playlist."/creazione\"><button type=\"button\" class=\"btn btn-default fa fa-pencil\">nuova playlist</button></a>";
						echo "<a data-toggle=\"modal\" data-target=\"#aggiungi_brano\"><button type=\"button\" class=\"btn btn-default fa fa-music\">aggiungi brano</button></a>";
						echo "<a data-toggle=\"modal\" data-target=\"#aggiungi_jingle\"><button type=\"button\" class=\"btn btn-default fa fa-volume-up\">gestisci jingle</button></a>";	
					echo "</div>";
		
				echo "<div style=\"position: relative; float: right;\">";
					echo "<a data-toggle=\"modal\" data-target=\"#genera_playlist\"><button type=\"button\" class=\"btn btn-default fa fa-floppy-o\">conferma</button></a>";
				echo "</div>";
			
			echo "</div>";
		echo "</div>";
	
	echo "</div>";


	echo "<div id=\"linea\" style=\"width: 100%; border-bottom: 1px solid #F3F3F3; clear: both; margin-bottom: 10px;\"></div>";


	$sequenza_descrizione=Array();
	$this->db->from('magic_temporanea_sequenze');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('id_playlist', $id_playlist);
	$this->db->order_by('ordine', 'asc');
	$queryNUS=$this->db->get();
	foreach ($queryNUS->result() as $rowNUS):
		$sequenza_descrizione[]=$rowNUS->orario_inizio." - ".$rowNUS->orario_fine;
	endforeach;



	
	
	
	// form di aggiunta brano
	echo "<div id=\"aggiungi_brano\" class=\"modal fade\">";
	echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
	echo "<div class=\"modal-header\">";
    echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
    echo "<h4 class=\"modal-title\">Inserisci brano</h4>";
	echo "</div>";
	
	echo "<div class=\"modal-body\">";
	$dataOP['jingle']=0;
	$dataOP['ordine']="alfabetico";
	$dataOP['esiste_file']=1;
	$dataRES=$this->mp3_model->mp3_archivio_recupera($dataOP);
	if ($dataRES['numero_risultati']>0):
		
		echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_new_inserisci_brano\">";
		$dataOP['jingle']=0;
		
		echo "<select id=\"eid_brano\" name=\"eid_brano\" style=\"width: 190px;\">";		
		$xx=0;
		while ($xx<$dataRES['numero_risultati']) {
			echo "<option value=\"".$dataRES['id_file'][$xx]."\"";
			if ($xx==0)
				echo " selected";
			echo ">";
			$descrizione=$dataRES['artista'][$xx];
			$descrizione=$descrizione." - ".$dataRES['titolo'][$xx];
			
			
			$genere_temp="";
			if ($dataRES['genere_1'][$xx]!=0)
				$genere_temp=$genere_temp.$genere[$dataRES['genere_1'][$xx]];
			if ($dataRES['genere_2'][$xx]!=0)
				$genere_temp=$genere_temp."/".$genere[$dataRES['genere_2'][$xx]];
			if ($dataRES['genere_3'][$xx]!=0)
				$genere_temp=$genere_temp."/".$genere[$dataRES['genere_2'][$xx]];
			if ($dataRES['genere_4'][$xx]!=0)
				$genere_temp=$genere_temp."/".$genere[$dataRES['genere_4'][$xx]];

			if ($genere_temp!="")
				$descrizione=$descrizione." (".$genere_temp.")";
			
			$descrizione=$descrizione." - ".$dataRES['punteggio'][$xx];
			
			echo $descrizione;
			echo "</option>";
			$xx++;
		}
		echo "</select>";
		
		echo "<br><br>";
		echo "dopo la posizione n.:<br>";
		echo "<input type=\"text\" name=\"ebrano_posizione\" value=\"1\" size=\"5\" maxlength=\"3\">";
		echo "<input type=\"hidden\" name=\"egiorno\" value=\"".$giorno."\">";
		echo "<input type=\"hidden\" name=\"eid_dispositivo\" value=\"".$id_dispositivo."\">";
		echo "<br>";
		echo "<input type=\"submit\" value=\"inserisci\">";
		echo "</form>";
	
	else:
		echo "Nessun brano in archivio";
		
	
	endif;
	echo "</div>";
	echo "<div class=\"modal-footer\">";
    echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
    echo "</div>";

	echo "</div></div>";
	echo "</div>";


	// form di gestione jingle
	echo "<div id=\"aggiungi_jingle\" class=\"modal fade\">";
	echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
	echo "<div class=\"modal-header\">";
    echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
    echo "<h4 class=\"modal-title\">Gestisci jingle</h4>";
	echo "</div>";
	
	echo "<div class=\"modal-body\">";
	
	$jingle_categoria_id=Array();
	$jingle_categoria_descrizione=Array();
	$this->db->from('archivio_radio_jingle_categorie');
	$this->db->where('id_account', $grupposelezionato);
//	$this->db->where('id_gruppo_dispositivo', 0);
//	$this->db->or_where('id_gruppo_dispositivo', $id_gruppo_dispositivo);
	$this->db->order_by('descrizione', 'asc');
	$queryJIN=$this->db->get();
	if ($queryJIN->num_rows()>0):
		echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_new_inserisci_jingle\">";
	
		foreach ($queryJIN->result() as $rowJIN):

			$this->db->from('archivio_radio');
			$this->db->where('jingle', 1);
			$this->db->where('jingle_categoria', $rowJIN->id);
			$this->db->where('id_account', $grupposelezionato);
			$queryJIC=$this->db->get();
			if ($queryJIC->num_rows()>0):
				echo "<input type=\"checkbox\" name=\"ejingle_selezionato[]\" value=\"".$rowJIN->id."\"> <small>".$rowJIN->descrizione."</small><br>";
			endif;
			
			// nella playlist attuale esistono già jingle associati a questo gruppo
			// e offre la possibilità di rimuoverli
			$this->db->from('magic_temporanea_elementi', $giorno);
			$this->db->where('jingle_categoria', $rowJIN->id);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('id_playlist', $id_playlist);
			$queryTOTJIN=$this->db->get();
			if ($queryTOTJIN->num_rows()>0):
				$jingle_categoria_id[]=$rowJIN->id;
				$jingle_categoria_descrizione[]=$rowJIN->descrizione;
			endif;
			
			
		endforeach;
	//	echo "</select>";
		echo "<br>";
		echo "intervallo canzoni:<br>";
		echo "<input type=\"text\" name=\"ejingle_intervallo\" value=\"2\" size=\"5\" maxlength=\"3\">";
		echo "<br>";
		echo "fascia oraria:<br>";
		echo "<select name=\"esequenza\">";
		echo "<option value=\"-1\">tutto il giorno</option>";
		$ff=0;
		foreach ($sequenza_descrizione as $sequenza_descrizione_temp):
			echo "<option value=\"".$ff."\">".$sequenza_descrizione_temp."</option>";
			$ff++;
		endforeach;
		echo "</select>";
		echo " <input type=\"checkbox\" name=\"esettimana\" checked=\"checked\"><small>tutta la settimana</small>";
		echo "<br><br>";
		echo "<input type=\"hidden\" name=\"egiorno\" value=\"".$giorno."\">";
		echo "<input type=\"hidden\" name=\"eid_dispositivo\" value=\"".$id_dispositivo."\">";
		echo "<input type=\"submit\" value=\"inserisci\">";
		echo "</form>";
		
		// elenca le categorie di jingle da rimuovere dalla playlist, se ce ne sono
		$zz=0;
		foreach ($jingle_categoria_id as $jingle_temp):
			echo "<br>";
			echo $jingle_categoria_descrizione[$zz];
			echo "<br>";
			echo "<small>";
			echo " [ <a href=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_new_cancella_jingle/".$id_dispositivo."/".$jingle_temp."/".$giorno."/".$giorno."\">rimuovi oggi</a> |";
			echo " <a href=\"".$this->config->item('base_url')."index.php/mp3/radio_magic_new_cancella_jingle/".$id_dispositivo."/".$jingle_temp."/".$giorno."/0\">rimuovi tutti</a> ]";
			echo "</small>";
			$zz++;
		endforeach;
		
	endif;
	echo "</div>";
	echo "<div class=\"modal-footer\">";
    echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
    echo "</div>";

	echo "</div></div>";
	echo "</div>";



	$this->db->from('magic_temporanea_sequenze');
	$this->db->where('id_account', $grupposelezionato);
	// $this->db->where('id_dispositivo', $id_dispositivo);
//	$this->db->where('id_gruppo_dispositivo', $id_gruppo_dispositivo);
	// $this->db->where('giorno', $giorno);
	$this->db->where('id_playlist', $id_playlist);
	$this->db->order_by('ordine', 'asc');
	$querySEQ=$this->db->get();

	// echo "<div style=\"float: left; margin: 30px 30px 30px 80px; width: 90%; top: 238px; position: absolute; padding: 10px; border: 1px solid black;\">";
	echo "<div class=\"sequenza\" style=\"position: relative;\">";
		
		echo "<div style=\"clear: both;\">";
			echo "<div style=\"font-size: 0.9em; font-weight: bold; clear: both;\">";
				echo "<span style=\"width: 80px; float: left;\">posizione</span><span style=\"width: 120px; float: left;\">orario stimato</span><span style=\"width: 450px; float: left;\">brano</span><span style=\"width: 100px; float: left; margin-left: 10px;\">durata</span>";
			echo "</div>";
		echo "<div>";

		$zz=0;
		$xx=0;
		$kk=0;
		echo "<form>";
		// echo "<ul id=\"playtemp\" class=\"sortable\" style=\"max-width: 1500px; padding: 0px;\">";
		
	//	die("totali: ".$querySEQ->num_rows());
		
		foreach ($querySEQ->result() as $rowSEQ):
			
		//	echo $zz."<br>";
			
			if ($xx==0)
				$playlist_inizio=$rowSEQ->orario_inizio;
				
			$playlist_fine=$rowSEQ->orario_fine;
		
			if (round($zz/2)==($zz/2))
				$colore_sfondo="#FFF";
			else
				$colore_sfondo="#F6F6F6";
			
			$tipologia_sequenza[$zz]=$rowSEQ->tipologia_file;
		
		/*
			if ($zz==0):
				$tempo_attuale_formattato=$rowSEQ->orario_inizio;
				$dataDUR['durata']=$tempo_attuale_formattato.":00";
				$dataDURET=$this->mp3_model->durata_secondi($dataDUR);
				$tempo_attuale=$dataDURET['durata_secondi'];
			else:
				$dataDUR['durata']=$tempo_attuale;
				$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
				$tempo_attuale_formattato=$dataDURET['durata_formattata'];
			endif;
	*/

			if ($zz==0)
				$tempo_attuale=-3600;

			if ($tempo_attuale>86400)
				$tempo_attuale=$tempo_attuale-86400;

			$tempo_attuale=round($tempo_attuale);

			if (!isset($tipologia_sequenza[($zz-1)])) $tipologia_sequenza[($zz-1)]=-1;
			
			if ($tipologia_sequenza[$zz]!=$tipologia_sequenza[($zz-1)]):
				if ($zz>0)
					echo "</ul>";
				echo "<ul class=\"sortable\" style=\"max-width: 1400px; padding: 0px; border: 2px solid black;\">";
			endif;
			
			// recupera i brani dall'archivio
			$this->db->select('archivio_radio.punteggio as punteggio, archivio_radio.nome_file as nome_file, archivio_radio.id as id_file, archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.jingle as jingle, archivio_radio.bpm as bpm, archivio_radio.power as power, archivio_radio.chiave as chiave, archivio_radio.durata as durata, archivio_radio.genere_1 as genere_1, archivio_radio.genere_2 as genere_2, archivio_radio.genere_3 as genere_3, archivio_radio.genere_4 as genere_4, magic_temporanea_elementi.jingle_categoria as jingle_categoria, magic_temporanea_elementi.id as id, magic_temporanea_elementi.ordine as ordine, magic_temporanea_elementi.ripetizione as ripetizione');
			$this->db->from('archivio_radio');
			$this->db->join('magic_temporanea_elementi', 'magic_temporanea_elementi.id_file=archivio_radio.id');
			// $this->db->where('magic_temporanea_elementi.giorno', $giorno);
			$this->db->where('magic_temporanea_elementi.id_sequenza', $rowSEQ->id);
			// $this->db->where('playlist_temporanea.sequenza', $zz);
			$this->db->where('magic_temporanea_elementi.id_playlist', $id_playlist);
			$this->db->where('archivio_radio.id_account', $grupposelezionato);
			$this->db->where('archivio_radio.nome_file !=', '');
			$this->db->order_by('magic_temporanea_elementi.ordine', 'asc');
			// $this->db->where('archivio_radio.disattivo', 0);
			
			$query=$this->db->get();
			
			// echo $query->num_rows()."<br>";
			
			$ff=0;
			foreach ($query->result() as $row):
			
			/*
				$dataDUR['durata']=$tempo_attuale;
				$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
				$tempo_attuale_formattato=$dataDURET['durata_formattata'];
				if ($tempo_attuale<3601)
					$tempo_attuale_formattato="00:".$tempo_attuale_formattato;
	*/
				$dataDUR['durata']=$row->durata;
				$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
				
				$opacity=($row->power*2)/10;
				$genere=$this->config->item('genere');
				
				echo "<div id=\"brano_".$row->id."\" style=\"clear: both; height: 20px; background-color: ".$colore_sfondo."; border-bottom: 1px solid #dedede; position: relative; float: left; padding-top: 4px; padding-bottom: 4px;\">";
				
					echo "<span style=\"width: 60px; float: right; margin-top: 2px;\">";
					echo "<a href=\"javascript:cancella_brano('".$id_gruppo_dispositivo."','".$row->id."');\"><i class=\"fa fa-trash\" style=\"width: 20px;\"></i></a>";
					echo "<a href=\"".base_url()."index.php/mp3/player/".$row->id_file."\" data-toggle=\"modal\" data-target=\"#finestra-modale\"><i class=\"fa fa-play\" style=\"width: 20px;\"></i></a>";
					echo "</span>";
					
					echo "<span style=\"display: none;\"><input type=\"text\" name=\"eid\" value=\"".$row->id."\"></span>";

					echo "<span id=\"ordine_".$row->id."\" style=\"width: 45px; float: left; font-size: 0.8em;\">".($xx+1)."</span>";
					
					echo "<span style=\"float: left; width: 35px;\">";
					if ($rowSEQ->tipologia_file==0)
						$icona_anteprima="fa fa-music";
					if ($rowSEQ->tipologia_file==1)
						$icona_anteprima="fa fa-video-camera";
					if ($row->jingle==1)
						$icona_anteprima="fa fa-microphone";
					echo "<i class=\"".$icona_anteprima."\"></i>";
					echo "</span>";
					
					echo "<span style=\"width: 120px; float: left;\">".date("H:i", $tempo_attuale)."</span>";
					
					echo "<span style=\"width: 450px; float: left; overflow: hidden; white-space: nowrap;\">";
						if ($row->jingle==0):
							$descrizione=$row->artista." - ".$row->titolo;
							if ($row->ripetizione==1):
								$descrizione="<span style=\"color: #c00;\">".$descrizione."</span>";
							endif;
							echo $descrizione;
						else:
							echo "<small>".$row->nome_file."</small>";
						endif;
					echo "</span>";
					
					echo "<span style=\"width: 100px; float: left; padding-left: 10px;\">".$dataDURET['durata_formattata_secondi']."</span>";
					
					if ($row->jingle==0):
						echo "<span style=\"width: 300px; float: left; font-size: 0.8em; height: 15px; margin-top: 2px;\">";
							echo "<span style=\"width: 15px; height: 15px; float: left; margin-right: 20px; color: black; padding-left: 5px; background-color: red; opacity: ".$opacity.";\">".$row->power."</span>";
							echo "<span style=\"float: left; min-width: 90px;\">".$row->punteggio."</span>";
							if ($row->genere_1!=0) echo $genere[$row->genere_1];
							if ($row->genere_2!=0) echo "/".$genere[$row->genere_2];
							if ($row->genere_3!=0) echo "/".$genere[$row->genere_3];
							if ($row->genere_4!=0) echo "/".$genere[$row->genere_4];
							echo "</span>";
						echo "</span>";
					else:
						$this->db->from('archivio_radio_jingle_categorie');
						$this->db->where('id', $row->jingle_categoria);
						$this->db->where('id_account', $grupposelezionato);
						$queryJIN=$this->db->get();
						$rowJIN=$queryJIN->row();
						echo "<span style=\"float: left; width: 300px; position: relative; top: 3px; font-size: 0.8em;\">".$rowJIN->descrizione."</span>";
					endif;

				echo "</div>";

				$tempo_attuale=$tempo_attuale+round($row->durata);
				$xx++;
				$ff++;
			endforeach;

			$kk++;		
			$zz++;
		endforeach;
		if ($zz>0)
			echo "</ul>";

		echo "<input type=\"hidden\" id=\"etotale_elementi\" value=\"".($xx+1)."\">";
		echo "<input type=\"hidden\" id=\"einizio_temp\" value=\"".$playlist_inizio."\">";
		echo "<input type=\"hidden\" id=\"efine_temp\" value=\"".$playlist_fine."\">";
		echo "</form>";
	
	echo "</div>";
	
endif;

?>

<script src="<?php echo $this->config->item('base_url'); ?>js/sortable/jquery.sortable.js" type="text/javascript"></script>

<script language="Javascript">
    $('.sortable').sortable({
     items: ':not(.disabled)'
    });
	$('.sortable').sortable().bind('sortupdate', function() {
			aggiorna_ordine_brani();

			var stringa = '';
			var xx=0;
			var elementi = new Array();
				$("input:text[name=eid]").each(function(){
				elementi[xx]=$(this).val();
				xx++;
			});
			
			stringa="eid="+elementi;
			// alert(stringa);
			
			$.ajax({

			  url: "<?php echo base_url(); ?>index.php/mp3/radio_magic_new/0/aggiorna_ordine_brani", 
			  data: stringa,
			  async: true,
			  dataType: 'html',
			  method: 'POST',
			  success: function(risultato) {
				//  alert(risultato);
				}
			});
			return false;
			
	});

	$("#einizio").val($("#einizio_temp").val());
	$("#efine").val($("#efine_temp").val());
	
	
	function aggiorna_ordine_brani() {
		xx=0;
		var elementi = new Array();
			$("input:text[name=eid]").each(function(){
			idtemp=$(this).val();
			$("#ordine_"+idtemp).html(xx+1);
			xx++;
		});
	}
	
	function cancella_brano(id_gruppo_dispositivo, brano_id) {
		if (confirm("Elimino il brano?")) {
			$.ajax({
			  url: "<?php echo base_url(); ?>index.php/mp3/radio_magic_new/"+id_gruppo_dispositivo+"/elimina_brano/"+brano_id,
			  async: true,
			  method: 'GET',
			  success: function(risultato) {
				//	alert(risultato);
				}
			});
			$("#brano_"+brano_id).remove();
			aggiorna_ordine_brani();
		}
	}
	
</script>

