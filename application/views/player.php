<?php

$grupposelezionato=$this->strimy_model->accountselezionato();

$this->db->from($origine);
$this->db->where('id', $id);
if ($origine=="archivio_file")
	$this->db->where('id_account', $grupposelezionato);
$query=$this->db->get();
$row=$query->row();

$this->db->from('user_groups');
$this->db->where('ugrp_id', $grupposelezionato);
$query2=$this->db->get();
$row2=$query2->row();
$stringa_casuale=$row2->ugrp_stringa;

$percorso_base=$this->config->item('archivio_real_path');
$percorso_users=$this->config->item('percorso_users');

$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);

if ($origine=="archivio_file")
	$cartella_base=$dataSTORAGE['full_path'].$percorso_users."user_".$grupposelezionato."_".$stringa_casuale."/content";
else
	$cartella_base=$dataSTORAGE['full_path'].$percorso_users."common";

// $descrizione=$row->artista." - ".$row->titolo;

$file=$cartella_base."/".$row->nome_file;
$descrizione="";

$valori=unserialize($row->valori);

$larghezza=568;

if ($row->tipologia_file!=0):
	$dimensioni=explode(";", $valori['dimensioni']);
	$altezza=round($larghezza*$dimensioni[1]/$dimensioni[0]);
	
	if ($altezza>450):
		$altezza=450;
		$larghezza=round($altezza*$dimensioni[0]/$dimensioni[1]);
	endif;
	
	$dimensioni_finali="width: \"".$larghezza."px\", height: \"".$altezza."px\"";
endif;

?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>js/jplayer/jquery.jplayer.js"></script>

<?php
if ($row->tipologia_file==2): // è un'immagine

	echo "<div style=\"display: table; width: ".$larghezza."px; height: ".$altezza."px; background-size: 100% 100%; background-repeat: no-repeat; background-position: center center; background-image: url(".$file.");\">";
	echo "</div>";

elseif ($row->tipologia_file==0): // è un file audio ?>

<script type="text/javascript">
   $(document).ready(function(){

      $("#jquery_jplayer_1").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            title: "<?php echo $descrizione; ?>",
			  mp3: "<?php echo $file; ?>"
          }).jPlayer("play");
        },
        cssSelectorAncestor: "#jp_container_1",
        swfPath: "/js/jplayer",
        supplied: "mp3",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: false,
        keyEnabled: true,
        remainingDuration: true,
        toggleDuration: true
      });
    });
    
</script>

<div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player" style="width: <?php echo ($larghezza+2) ?>px;">
  <div class="jp-type-single">
    <div class="jp-gui jp-interface">
      <div class="jp-controls">
        <button class="jp-play" role="button" tabindex="0">play</button>
        <button class="jp-stop" role="button" tabindex="0">stop</button>
      </div>
      <div class="jp-progress">
        <div class="jp-seek-bar">
          <div class="jp-play-bar"></div>
        </div>
      </div>
      <div class="jp-volume-controls">
        <button class="jp-mute" role="button" tabindex="0">mute</button>
        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
      </div>
      <div class="jp-time-holder">
        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
        <div class="jp-toggles">
          <button class="jp-repeat" role="button" tabindex="0">repeat</button>
        </div>
      </div>
    </div>
    <div class="jp-details">
      <div class="jp-title" aria-label="title">&nbsp;</div>
    </div>
    <div class="jp-no-solution">
      <span>Update Required</span>
      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
    </div>
  </div>
</div>


<?php elseif ($row->tipologia_file==1): // è un file video ?>

<script type="text/javascript">
   $(document).ready(function(){

      $("#jquery_jplayer_1").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            title: "<?php echo $descrizione; ?>",
			  m4v: "<?php echo $file; ?>"
          }).jPlayer("play");
        },
        cssSelectorAncestor: "#jp_container_1",
        swfPath: "/js/jplayer",
        supplied: "m4v",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: false,
        keyEnabled: true,
        remainingDuration: true,
        toggleDuration: true,
        size: { <?php echo $dimensioni_finali; ?> }
      });
    });
    
</script>


<div id="jp_container_1" class="jp-video " role="application" aria-label="media player" style="width: <?php echo ($larghezza+2) ?>px;">
  <div class="jp-type-single">
    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
    <div class="jp-gui">
      <div class="jp-video-play">
        <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
      </div>
      <div class="jp-interface">
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
        <div class="jp-controls-holder">
          <div class="jp-controls">
            <button class="jp-play" role="button" tabindex="0">play</button>
            <button class="jp-stop" role="button" tabindex="0">stop</button>
          </div>
          <div class="jp-volume-controls">
            <button class="jp-mute" role="button" tabindex="0">mute</button>
            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
            <div class="jp-volume-bar">
              <div class="jp-volume-bar-value"></div>
            </div>
          </div>
          <div class="jp-toggles">
            <button class="jp-repeat" role="button" tabindex="0">repeat</button>
            <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
          </div>
        </div>
        <div class="jp-details">
          <div class="jp-title" aria-label="title">&nbsp;</div>
        </div>
      </div>
    </div>
    <div class="jp-no-solution">
      <span>Update Required</span>
      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
    </div>
  </div>
</div>

<?php endif; ?>


