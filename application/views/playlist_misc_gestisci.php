<script language="Javascript">
function checkform() {
	
	var errore=0;

	$(".form-group").removeClass('has-error');
    if ($("#edescrizione").val()=="") {
			$("#form_edescrizione").addClass('has-error');
			errore=1;
	}

	if ( ($("#url").length>0) && ($("#url").val()=="") ) {
			$("#form_url").addClass('has-error');
			errore=1;	
	}
	
	if ( ($("#city").length>0) && ($("#city").val()=="") ) {
			$("#form_city").addClass('has-error');
			errore=1;	
	}
	

	if (errore==1)
		return false;
	else
		return true;

	}
</script>

<?php


$grupposelezionato=$this->strimy_model->accountselezionato();


$dataVALORI=Array();
if ($id_playlist==0):
	$cdescrizione="";
	$dataVALORI['background_color']="#06AFF2";
else:
	$this->db->from('playlist_generica_indice');
	$this->db->where('id', $id_playlist);
	$this->db->where('id_account', $grupposelezionato);
	$query=$this->db->get();
	$row=$query->row();
	$cdescrizione=$row->descrizione;
	if ($row->valori!=""):
		$dataVALORI=unserialize($row->valori);
	endif;

	if (!isset($dataVALORI['background_color']) || ($dataVALORI['background_color']=="")):
		$dataVALORI['background_color']="#06AFF2";
	else:
		$dataVALORI['background_color']=$this->strimy_model->rgb2hex($dataVALORI['background_color']);
	endif;


endif;

echo "<form method=\"post\" action=\"".$this->config->item('base_url')."index.php/strimy/playlist_misc_gestisci_salva\" class=\"form-horizontal\" enctype=\"multipart/form-data\">";


echo '<div id="form_edescrizione" class="form-group">
    <label for="label_edescrizione" class="col-sm-3 control-label">Titolo</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="edescrizione" name="edescrizione" value="'.$cdescrizione.'">
    </div>
  </div>';



if ($tipologia_playlist==50):	// è un widget news
	
	if (!isset($dataVALORI['url']))
		$dataVALORI['url']="";

	echo '<div id="form_url" class="form-group">
    <label for="label_url" class="col-sm-3 control-label">News feed url</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="url" name="url" value="'.$dataVALORI['url'].'">
    </div>
	</div>';

	echo "<div id=\"form-background-color\" class=\"form-group\">";
	echo '<label for="label_url" class="col-sm-3 control-label">Colore background</label>';
	echo "<input type=\"hidden\" name=\"background_color\" id=\"background_color\" size=\"50\" value=\"".$dataVALORI['background_color']."\"/>";
	echo '<div class="col-sm-9">';
	echo "<span class=\"input-group-addon\" style=\"width: 30px;\"><i></i></span>";
	echo "</div>";
	echo "</div>";


elseif ($tipologia_playlist==51):	// è un widget meteo
	
	if (!isset($dataVALORI['city']))
		$dataVALORI['city']="";
	
	echo '<div id="form_city" class="form-group">
    <label for="label_city" class="col-sm-3 control-label">Citt&agrave;</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="city" name="city" value="'.$dataVALORI['city'].'">
    </div>
	</div>';


	echo "<div id=\"form-background-color\" class=\"form-group\">";
	echo '<label for="label_url" class="col-sm-3 control-label">Colore background</label>';
	echo "<input type=\"hidden\" name=\"background_color\" id=\"background_color\" size=\"50\" value=\"".$dataVALORI['background_color']."\"/>";
	echo '<div class="col-sm-9">';
	echo "<span class=\"input-group-addon\" style=\"width: 30px;\"><i></i></span>";
	echo "</div>";
	echo "</div>";

endif;

echo "<div class=\"text-right\">";
echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
echo "<input type=\"hidden\" name=\"etipologia_playlist\" value=\"".$tipologia_playlist."\" readonly=\"readonly\">";
echo "<input type=\"hidden\" name=\"eid_playlist\" value=\"".$id_playlist."\" readonly=\"readonly\">";

echo "<input type=\"submit\" value=\"Salva playlist\" class=\"btn btn-primary\" onclick=\"return checkform();\"/>";
echo "</form>";
echo "</div>";

?>

<script>
	$(function(){
		$('#form-background-color').colorpicker({
				colorSelectors: {
				'#06AFF2': '#06AFF2',
				'#777777': '#777777',
				'#337ab7': '#337ab7',
				'#5cb85c': '#5cb85c',
				'#5bc0de': '#5bc0de',
				'#f0ad4e': '#f0ad4e',
				'#d9534f': '#d9534f'
			}
		});
	});
</script>

	
