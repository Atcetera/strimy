<script language="javascript">
function checkform() {

	var errore=0;

	$(".form-group").removeClass('has-error');
    if ($("#euser").val()=="") {
		$("#form_euser").addClass('has-error');
		errore=1;
	}
	
	if ($("#epassword").val()=="") {
		$("#form_epassword").addClass('has-error');
		errore=1;
	}

	if (errore==1)
		return false;
	else
		return true;

}
</script>
<?php

if (isset($messaggio)):
	echo '<div class="col-sm-4 col-sm-offset-4 top-buffer" style="z-index: 1000">';
	echo $messaggio;
	echo "</div>";
endif;


echo "<div style=\"padding: 0; display: table; height: 100%; width: 100%;\">";

echo "<div style=\"height: 100%; display:table-cell; vertical-align: middle;\">";

echo "<div id=\"login_form\">";


echo "<form id=\"login\" class=\"form-horizontal\" action=\"".$this->config->item('base_url')."index.php/users/login_check\" method=\"post\">";

echo '<div class="text-center">';
echo "<img src=\"".$this->config->item('base_url')."/assets/img/icon.png\" style=\"width: 96px; margin-top: 10px; margin-bottom: 30px\" />";
echo '</div>';

echo '<div id="form_euser" class="form-group top-buffer">
    <!-- <label for="label_euser" class="col-sm-4 control-label">username</label> -->
    <div class="col-sm-12">
    <input type="text" class="form-control input-lg" id="euser" name="euser" placeholder="username">
    </div>
  </div>';

echo '<div id="form_epassword" class="form-group">
    <!-- <label for="label_epassword" class="col-sm-4 control-label">password</label> -->
    <div class="col-sm-12">
    <input type="password" class="form-control input-lg" id="epassword" name="epassword" placeholder="password">
    </div>
  </div>';

echo "<div class=\"text-center\">";
echo "<input type=\"submit\" value=\"Login\" class=\"btn btn-primary\" style=\"margin-top: 20px; margin-bottom: 10px; zoom: 1.5\" onclick=\"return checkform()\">";
echo "</div>";

echo "</form>";

echo "</div>";
echo "</div>";
echo "</div>";

?>
<script language="Javascript">
function altezza() {
	var altezza=window.innerHeight;
    altezza=Math.round(altezza/2)-200;
    $("#login_form").offset({ top: altezza});	
}
window.onresize = function() {
	altezza();
}
altezza();
</script>
