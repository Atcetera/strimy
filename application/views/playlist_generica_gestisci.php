<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);


// $privilegi=$this->strimy_model->account_privilegi($grupposelezionato);

$percorso_base=$this->config->item('real_path');

$percorso_users=$this->config->item('percorso_users');
$cartella_archivio=$percorso_base.$percorso_users.$stringa_casuale."/content";
$url_archivio=$this->config->item("archivio_base_url").$this->config->item("percorso_users").$stringa_casuale."/content";

// visualizza l'attuale playlist
$percorso_devices=$this->config->item('percorso_devices');
$cartella_device=$percorso_base.$percorso_devices.$device_id;
$file=$cartella_device."/playlist";

$widget_aspect_ratio=$this->config->item('widget_aspect_ratio');
$widget_aspect_ratio_descrizione=$this->config->item('widget_aspect_ratio_descrizione');

?>

<!-- <script src="<?php echo $this->config->item('base_url'); ?>js/sortable/jquery.sortable.min.js" type="text/javascript"></script> -->

<script language="Javascript">

	var widget_aspect_ratio=[];
	var widget_aspect_ratio_descrizione=[];
	<?php
	$xx=0;
	foreach ($widget_aspect_ratio as $widget_aspect_ratio_temp):
		if (isset($widget_aspect_ratio[$xx])):
			echo "widget_aspect_ratio[".$xx."]=".$widget_aspect_ratio[$xx].";\n";
			echo "widget_aspect_ratio_descrizione[".$xx."]='".$widget_aspect_ratio_descrizione[$xx]."';\n";
		endif;
		$xx++;
	endforeach;
	?>

	function checkform() {
			$(".form-group").removeClass('has-error');
			if ($("#edescrizione_playlist").val()=="") {
				$("#form_edescrizione_playlist").addClass('has-error');
				return false;
			}
	}
	
	function aggiungi_file(id, id_file, tipologia_file, animazione) {
			var numero_file=$("#enumero_file").val();

			if (numero_file>0) {
				$('ul.sortable').sortable('destroy');
				$('ul.sortable').unbind();
			}
			
			var nuovodiv="";
			nuovodiv='<div id="div_file_'+numero_file+'" style="position: relative; float: left; clear: both; width: 100%;"></div>';
			$('ul.sortable').append(nuovodiv);
			$('#div_file_'+numero_file).load("<?php echo base_url(); ?>index.php/strimy/playlist_generica_gestisci_aggiungi/"+numero_file+"/"+id+"/"+id_file+"/"+tipologia_file);


		    $('ul.sortable').sortable({		
				placeholder: "ui-state-highlight",
				cancel: ".ui-state-disabled"
			});

			// $('ul.sortable').sortable( "option", "handle", "> .handlem" );

			$('ul.sortable').sortable().bind('sortupdate', function() {
				ordina_file();
			});

			if (animazione==1) {
				$('html, body').animate({
					scrollTop: $('#div_file_'+numero_file).offset().top
				}, 500);
			}

			numero_file=Math.round(numero_file)+1;
			$("#enumero_file").val(numero_file);			
	}
	
	function cancella_file(ordine) {
			if (confirm('La cancellazione sarà effettiva solo al momento del salvataggio. Il file rimarrà comunque in archivio.\nVuoi proseguire?')) {
				$('#eattivo_'+ordine).val(0);
				ordina_file();
				$('#divfile_'+ordine).hide(500);
			}
	}
	
	function ordina_file() {
			var xx=1;
			var valore=1;
			$("ul.sortable div div.panel-body").each(function() {
				valore=$(this).find('.efile_attivo').val();
				if (valore==1) {
					$(this).find("span.campo_playlist_ordine").html(xx);
					xx++;
				}
			});
	}
	
	function mask_ridimensiona(tipo, ordine) {
		
		var immagine='div#divfile_interno_'+ordine+' > img';

		var canvas=$(immagine).cropper('getContainerData');

		if ( (typeof widget_aspect_ratio[tipo] === 'undefined') || (widget_aspect_ratio[tipo] === null) )
			tipo=0;

		if (tipo==0) {
			var dimensioni = {
				left: 0,
				top: 0,
				width: canvas.width,
				height: canvas.height
			};
			$(immagine).cropper('setAspectRatio', NaN);
			$(immagine).cropper('setCropBoxData', dimensioni);
			}
		
	
		else {
			$(immagine).cropper('setAspectRatio', widget_aspect_ratio[tipo]);
		}

		$('#evalori_crop_ratio_'+ordine).val(tipo);
		mask_recupera_dati(ordine);
	
	}

	function mask_recupera_dati(ordine) {
			
			var immagine='div#divfile_interno_'+ordine+' > img';
			var valore=$(immagine).cropper('getData');
			
			var coord_x=Math.round(valore.x);
			if (coord_x<0) coord_x=0;
			var coord_y=Math.round(valore.y);
			if (coord_y<0) coord_y=0;
			var width=Math.round(valore.width);
			if (width<0) width=0;
			var height=Math.round(valore.height);
			if (height<0) height=0;
			
			var stringa=coord_x+";"+coord_y+";"+width+";"+height;
			
			$('#evalori_crop_mask_'+ordine).val(stringa);
	}
	
	function mask_ridimensiona_tutti(tipo) {
			var numero_file=$("#enumero_file").val();
			var xx=0;
			while (xx<numero_file) {
				mask_ridimensiona(tipo, xx);
				xx++;
			}
			$("#ecrop_ratio").val(tipo);
			$("span.mask_tipo").css('visibility', 'hidden');
			$("span#mask_tipo_"+tipo).css('visibility', 'visible');
	}
	
</script>
<?php

if ($id_playlist==0):
	$cdescrizione_playlist="";
	$valori['crop_ratio']=0;
	$titolo="Creazione playlist Carosello";
else:
	$this->db->from('playlist_generica_indice');
	$this->db->where('id_account', $grupposelezionato);
	$this->db->where('id', $id_playlist);
	$queryDES=$this->db->get();
	$rowDES=$queryDES->row();
	$cdescrizione_playlist=$rowDES->descrizione;
	$tipologia_file=$rowDES->tipologia_file;
	$valori=unserialize($rowDES->valori);
	$titolo="Modifica playlist Carosello <small>".$rowDES->descrizione."</small>";
endif;


echo "<h1><i class=\"fa fa-edit\"></i> ".$titolo."</h1>";

echo "<hr>";

$widget_aspect_ratio=$this->config->item('widget_aspect_ratio');
$widget_aspect_ratio_descrizione=$this->config->item('widget_aspect_ratio_descrizione');



echo "<div id=\"playlist\">";
	echo "<form id=\"playlist_form\" name=\"playlist_form\" class=\"form-horizontal\" method=\"post\" action=\"".base_url()."index.php/strimy/playlist_generica_gestisci_salva\">";


		echo "<a data-toggle=\"modal\" data-target=\"#aggiungi_contenuto\" class=\"btn btn-default\"><i class=\"fa fa-plus\"></i> Aggiungi</a>";
		
		if ($tipologia_file!=0):
			echo ' <div class="btn-group">';
			echo '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">';
			echo '<i class="fa fa-crop"></i> Crop <span class="caret"></span>';
			echo '</button>';
			echo '<ul class="dropdown-menu">';

			$kk=0;
			$ordine=0;
			foreach ($widget_aspect_ratio_descrizione as $widget_aspect_ratio_descrizione_temp):
				if (isset($widget_aspect_ratio[$kk])):
					echo "<li>";
					echo "<a tabindex=\"-1\" href=\"javascript:mask_ridimensiona_tutti('".$kk."');\">";
					echo "<span id=\"mask_tipo_".$kk."\" class=\"mask_tipo\"";
					if ($valori['crop_ratio']!=$kk)
						echo " style=\"visibility: hidden;\"";
					echo "><i class=\"fa fa-check\"></i> </span>".$widget_aspect_ratio_descrizione[$kk];
					echo "</a>";
					echo "</li>";
				endif;
				$kk++;
			endforeach;
			echo "</ul>";
			echo "</div>";
		endif;
		
		echo " <a data-toggle=\"modal\" data-target=\"#salva_playlist\" class=\"btn btn-default\"><i class=\"fa fa-floppy-o\"></i> Salva</a>";		

		// form di salvataggio
		echo "<div id=\"salva_playlist\" class=\"modal fade\">";
		echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
		echo "<div class=\"modal-header\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
		echo "<h4 class=\"modal-title\">";
		if ($id_playlist==0)
			echo "Crea playlist";
		else
			echo "Salva modifiche playlist";
		echo "</h4>";
		echo "</div>";

		echo "<div class=\"modal-body\">";
		
		echo '<div id="form_edescrizione_playlist" class="form-group">
		<label for="label_edescrizione_playlist" class="col-sm-2 control-label">Titolo</label>
		<div class="col-sm-10">
		<input type="text" class="form-control" id="edescrizione_playlist" name="edescrizione_playlist" value="'.$cdescrizione_playlist.'">
		</div>
		</div>';

		
		// echo "Titolo playlist:<br>";
		// echo "<input type=\"text\" name=\"edescrizione_playlist\" value=\"".$cdescrizione_playlist."\">";
		echo "<div class=\"text-right\">";
		echo "<input type=\"hidden\" name=\"eid_playlist\" value=\"".$id_playlist."\">";
		echo "<input type=\"hidden\" name=\"etipologia_playlist\" value=\"".$tipologia_playlist."\">";
		echo "<input type=\"hidden\" name=\"etipologia_file\" value=\"".$tipologia_file."\">";
		echo "<input type=\"hidden\" name=\"ecrop_ratio\" id=\"ecrop_ratio\" value=\"".$valori['crop_ratio']."\">";
		echo "<br>";
		echo "<input type=\"submit\" onclick=\"return checkform();\" class=\"btn btn-primary\" value=\"Salva playlist\">";
		echo "</div>";
		
		echo "</div>";

		echo "<div class=\"modal-footer\">";
		echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
		echo "</div>";
		echo "</div></div>";
		echo "</div>";
		// fine form di salvataggio



		echo "<div id=\"contenitore_sequenze\" class=\"top-buffer\">";	// questo div contiene gli elementi della playlist
			echo "<input type=\"hidden\" id=\"enumero_file\" name=\"enumero_file\" value=\"0\">";
			echo "<ul class=\"sortable\">";
			/* elementi della playlist */
			echo "</ul>";
		echo "</div>";


		echo "<script language=\"javascript\">";
		$this->db->select('playlist_generica_elementi.id as id, playlist_generica_elementi.id_file as id_file');
		$this->db->from('playlist_generica_elementi');
		$this->db->join('playlist_generica_indice', 'playlist_generica_indice.id=playlist_generica_elementi.id_playlist');
		$this->db->where('playlist_generica_indice.id_account', $grupposelezionato);
		$this->db->where('playlist_generica_indice.id', $id_playlist);
		$this->db->where('playlist_generica_indice.tipologia', $tipologia_playlist);
		$this->db->order_by('playlist_generica_elementi.ordine', 'asc');
		$queryPL=$this->db->get();
		foreach ($queryPL->result() as $rowPL):
			?>
			aggiungi_file('<?php echo $rowPL->id; ?>', '<?php echo $rowPL->id_file; ?>', '<?php echo $tipologia_file; ?>', '0');
			<?php
		endforeach;
		echo "</script>";

		echo "<div style=\"clear: both;\"></div>";
		echo "<input type=\"hidden\" id=\"econferma\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
	echo "</form>";

echo "</div>";

// visualizza l'archivio


echo "<div id=\"aggiungi_contenuto\" class=\"modal fade\">";
echo "<div class=\"modal-dialog\"><div class=\"modal-content\">";
echo "<div class=\"modal-header\">";
echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
echo "<h4 class=\"modal-title\">Seleziona contenuto</h4>";
echo "</div>";

echo "<div class=\"modal-body\">";

$dataLISTA['gruppo']=$grupposelezionato;
$dataLISTA['tipologia_file']=$tipologia_file;
$dataRET=$this->strimy_model->lista_file_archivio($dataLISTA);


if ($dataRET['totale_file']==0):

	echo "Archivio vuoto";

else:

	echo "<div style=\"position: relative; max-height: 550px; overflow-y: scroll; overflow-x: hidden;\">";
	echo "<table class=\"table\">";
	
	$xx=0;
	foreach ($dataRET['elenco_file'] as $file):
			echo "<tr style=\"cursor: pointer;\" onclick=\"aggiungi_file('0','".$dataRET['elenco_file_id'][$xx]."','".$tipologia_file."', '1');\">";
			$file_anteprima=$url_archivio."/".$dataRET['elenco_file_thumb'][$xx];
				
		//	echo "\n<div class=\"item\" style=\"position: relative; clear: both;\">";
				
				echo "<td>";
				echo "<div class=\"elemento_archivio archivio_file archivio_".$dataRET['elenco_file_tipologia'][$xx]."\" style=\"background: url('".$file_anteprima."'); background-size: contain; background-repeat: no-repeat; background-position: center; position: relative; z-index: 5;\">";

				echo "<input type=\"hidden\" id=\"efile_".$xx."\" value=\"".$file."\" readonly=\"readonly\">";
				echo "<input type=\"hidden\" id=\"efileid_".$xx."\" value=\"".$dataRET['elenco_file_id'][$xx]."\" readonly=\"readonly\">";
				
				echo "</div>";
				echo "</td>";
				
				echo "<td>";
				echo "<div class=\"elemento_archivio_overlay_titolo\" style=\"width: 440px; overflow: hidden\">".$dataRET['elenco_file_descrizione'][$xx]."</div>";
				echo "</td>";
				
		//	echo "</div>";
			
			echo "</tr>";
			$xx++;
	endforeach;
	$totale_elementi=$xx;
	
	echo "</table>";
	echo "</div>";

endif;

echo "</div>";
echo "<div class=\"modal-footer\">";
echo "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Chiudi</button>";
echo "</div>";

echo "</div></div>";
echo "</div>";

?>
