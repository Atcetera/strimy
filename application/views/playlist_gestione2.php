<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

$privilegi=$this->strimy_model->account_privilegi($grupposelezionato);

$percorso_base=$this->config->item('real_path');

$percorso_users=$this->config->item('percorso_users');
$cartella_archivio=$percorso_base.$percorso_users.$stringa_casuale."/content";
$url_archivio="/strimy/users/".$stringa_casuale."/content";

// visualizza l'attuale playlist
$percorso_devices=$this->config->item('percorso_devices');
$cartella_device=$percorso_base.$percorso_devices.$device_id;
$file=$cartella_device."/playlist";


$dataDSP['id_dispositivo']=$device_id;
$dataDSP['id_account']=$grupposelezionato;
$dataDSP['giorno']=$giorno;

if (($giorno!=0) && ($giorno!=1) && ($giorno!=2) && ($giorno!=3) && ($giorno!=4) && ($giorno!=5) && ($giorno!=6) && ($giorno!=7))
	redirect('strimy/frontpage');

$dataPLY=$this->strimy_model->playlist($dataDSP);

$totale_sequenze=$dataPLY['totale_sequenze'];
$totale_elementi_sequenza=$dataPLY['totale_elementi_sequenza'];
$sequenza=$dataPLY['tipologia_sequenza'];
$id_sequenza=$dataPLY['id_sequenza'];
$titolo_sequenza=$dataPLY['titolo_sequenza'];
$attiva_sequenza=$dataPLY['attiva_sequenza'];
$sequenza_inizio=$dataPLY['sequenza_inizio'];
$sequenza_fine=$dataPLY['sequenza_fine'];
$sequenza_valore_1=$dataPLY['file_nome'];
$sequenza_valore_2=$dataPLY['file_durata'];
$elemento_id=$dataPLY['elemento_id'];
$file_id=$dataPLY['file_id'];
$file_anteprima=$dataPLY['file_anteprima'];
$file_descrizione=$dataPLY['file_descrizione'];

$sovraimpressione_messaggio=$dataPLY['sovraimpressione_messaggio'];
$sovraimpressione_posizione_messaggio=$dataPLY['sovraimpressione_posizione_messaggio'];

$stringa_casuale=$dataPLY['stringa_casuale'];
$orario_download=$dataPLY['orario_download'];
		


?>
<script language="Javascript">
	
function seleziona_div(index) {
	var sequenza_selezionata=$('#sequenza_selezionata').val();
	if (sequenza_selezionata!=index) {
		$(".sequenza_elementi").hide(50);
		$("div#sequenza_"+index).show(100);
		$(".contenitore_jingle").hide(10);
		$("span.intestazione_sequenza").show();
		$("span.intestazione_sequenza_modificabile").hide();	
	}
	$("span#intestazione_sequenza_"+index).hide();	
	$("span#intestazione_sequenza_modificabile_"+index).show();	
	var tipologia=$('#sequenza_tipologia_'+index).val();
	$(".archivio_file").hide(50);
	$(".archivio_"+tipologia).show(50);
	$('#sequenza_selezionata').val(index);
	$("div#contenitore_jingle_"+index).show(10);
}

function cambia_intestazione_sequenza(numero_sequenze) {
	etitolo=$('#etitolo_'+numero_sequenze).val();
	einizio=$('#einizio_'+numero_sequenze).val();
	efine=$('#efine_'+numero_sequenze).val();
	// alert(einizio);
	nuovo_span='<span id="intestazione_sequenza_'+numero_sequenze+'" class="intestazione_sequenza" style="display: none;">';
	nuovo_span=nuovo_span+'<span id="intestazione_sequenza_interna_'+numero_sequenze+'" class="intestazione_titolo_sequenza_interna">';
	nuovo_span=nuovo_span+etitolo+'</span><span class="orario_inizio">'+einizio+'</span>';
	if (efine!="")
		nuovo_span=nuovo_span+' --- '+efine;
	nuovo_span=nuovo_span+'</span>';
	$('span#intestazione_sequenza_'+numero_sequenze).replaceWith(nuovo_span);
	riordina();
}

function seleziona_sovraimpressione(casella, sequenza, elemento) {
	selezione=casella.options[casella.selectedIndex].value;
	$("#sovraimpressione_messaggio_"+sequenza+"_"+elemento).hide(10);
	$("#sovraimpressione_logo_"+sequenza+"_"+elemento).hide(10);
	$("#sovraimpressione_frame_"+sequenza+"_"+elemento).hide(10);
	if (selezione==1) {
		$("#sovraimpressione_messaggio_"+sequenza+"_"+elemento).show(20);
	}
	if (selezione==2) {
		$("#sovraimpressione_logo_"+sequenza+"_"+elemento).show(20);
	}
	if (selezione==3) {
		$("#sovraimpressione_frame_"+sequenza+"_"+elemento).show(20);
	}
}

function aggiungi(id_elemento, id_file, tipologia) {
	var sequenza_selezionata=$('#sequenza_selezionata').val();
	if (sequenza_selezionata==-1) {
		alert('Seleziona una sequenza');
		return false;
	}
	aggiungi_elemento(id_elemento, id_file, tipologia, sequenza_selezionata);
}

function aggiungi_elemento(id_elemento, id_file, tipologia, sequenza_selezionata) {

	var numero_elementi=$('#elementi_sequenza_'+sequenza_selezionata).val();
	var nuovo_div='<div id="elemento_'+sequenza_selezionata+'_'+numero_elementi+'" class="elemento_sequenza"></div>';
	nuovo_div=nuovo_div+'<div id="fine_sequenza_'+sequenza_selezionata+'" style="display: none;"></div>';

	$('div#fine_sequenza_'+sequenza_selezionata).replaceWith(nuovo_div);
	$('div#elemento_'+sequenza_selezionata+'_'+numero_elementi).load("<?php echo base_url(); ?>index.php/strimy/carica_elemento/"+id_elemento+"/"+id_file+"/"+tipologia+"/"+sequenza_selezionata+"/"+numero_elementi);
	
	numero_elementi=Math.round(numero_elementi)+1;
	$('#elementi_sequenza_'+sequenza_selezionata).val(numero_elementi);

	}

function aggiungi_sequenza(id, tipologia, titolo, orario_inizio, orario_fine, attiva) {
	var icona='';
	var suffisso='';
	var display_inizio='inizio (hh:mm)';
	var display_fine='fine (hh:mm)';
	var display_separatore=' --- ';
	var suffisso_display='';
	var div_pulsanti='';
	var numero_sequenze=$('#numero_sequenze').val();
	
	div_pulsanti='<div class="pulsante_destra" style=\"float: right\" onClick="cancella_sequenza(\''+numero_sequenze+'\')"><i class="fa fa-trash"></i></div>'
	div_pulsanti=div_pulsanti+'<div class="pulsante_centro" style=\"float: right\" onClick="seleziona_div(\''+numero_sequenze+'\')"><i class="fa fa-pencil"></i></div>'
	div_pulsanti=div_pulsanti+'<div class="pulsante_sinistra" id="pulsante_pausa_'+numero_sequenze+'" style=\"float: right\" onClick="attiva_sequenza(\''+numero_sequenze+'\')"><i class="fa fa-pause"></i></div>';
	div_pulsanti=div_pulsanti+'<div class="pulsante_sinistra" id="pulsante_play_'+numero_sequenze+'" style=\"float: right; display: none;\" onClick="attiva_sequenza(\''+numero_sequenze+'\')"><i class="fa fa-play"></i></div>';
	if (tipologia=="immagini")
		icona="fa fa-camera";
	if (tipologia=="video")
		icona="fa fa-video-camera";
	if (tipologia=="audio")
		icona="fa fa-volume-up";
	if (tipologia=="on") {
		icona="fa fa-plug";
		orario_fine="";
		suffisso=" readonly=\"readonly\"";
		suffisso_display=" style=\"display: none\"";
		display_inizio='orario accensione (hh:mm)';
		display_separatore='';
		}
	if (tipologia=="off") {
		icona="fa fa-power-off";
		orario_fine="";
		suffisso=" readonly=\"readonly\"";
		suffisso_display=" style=\"display: none\"";
		display_inizio='orario stand-by (hh:mm)';
		display_separatore='';
		}
	if ((tipologia=="radio") || (tipologia=="musictv")) {
		if (tipologia=="radio")
			icona="fa fa-wifi";
		if (tipologia=="musictv")
			icona="fa fa-video-camera";
		div_pulsanti='<div class="pulsante_destra" style=\"float: right\" onClick="cancella_sequenza(\''+numero_sequenze+'\')"><i class="fa fa-trash"></i></div>'
		div_pulsanti=div_pulsanti+'<div class="pulsante_centro" style=\"float: right\" onClick="seleziona_div(\''+numero_sequenze+'\')"><i class="fa fa-pencil"></i></div>'
	//	div_pulsanti=div_pulsanti+'<div class="pulsante_centro" style=\"float: right\" onClick="inserisci_jingle(\''+numero_sequenze+'\')"><i class="fa fa-microphone"></i></div>'
		div_pulsanti=div_pulsanti+'<div class="pulsante_centro" style=\"float: right\" onClick="random_sequenza(\''+numero_sequenze+'\', \''+tipologia+'\')"><i class="fa fa-magic"></i></div>'
		div_pulsanti=div_pulsanti+'<div class="pulsante_sinistra" id="pulsante_pausa_'+numero_sequenze+'" style=\"float: right\" onClick="attiva_sequenza(\''+numero_sequenze+'\')"><i class="fa fa-pause"></i></div>';
		div_pulsanti=div_pulsanti+'<div class="pulsante_sinistra" id="pulsante_play_'+numero_sequenze+'" style=\"float: right; display: none;\" onClick="attiva_sequenza(\''+numero_sequenze+'\')"><i class="fa fa-play"></i></div>';
	}
		
	var numero_sequenze=$('#numero_sequenze').val();
	var nuovo_div='<div id="sequenza_esterna_'+numero_sequenze+'" class="sequenza" style="position: relative;">';
	nuovo_div=nuovo_div+'<i class="'+icona+'" style="width: 20px;"></i>';
	nuovo_div=nuovo_div+'<span id="intestazione_sequenza_'+numero_sequenze+'" class="intestazione_sequenza">';
	nuovo_div=nuovo_div+'<span id="intestazione_sequenza_interna_'+numero_sequenze+'" class="intestazione_titolo_sequenza_interna"> '+titolo+'</span><span class="orario_inizio">'+orario_inizio+"</span>"+display_separatore+orario_fine;
	nuovo_div=nuovo_div+'</span>';
	nuovo_div=nuovo_div+'<span id="intestazione_sequenza_modificabile_'+numero_sequenze+'" class="intestazione_sequenza_modificabile" style="display: none;">';
	nuovo_div=nuovo_div+'<span'+suffisso_display+'> titolo<input type="text" id="etitolo_'+numero_sequenze+'" name="etitolo['+numero_sequenze+']" value="'+titolo+'" size="20"'+suffisso+' onBlur="cambia_intestazione_sequenza(\''+numero_sequenze+'\')"></span>';
	nuovo_div=nuovo_div+'<span> '+display_inizio+'<input type="text" id="einizio_'+numero_sequenze+'" name="einizio['+numero_sequenze+']" value="'+orario_inizio+'" maxlength="5" size="5" onBlur="cambia_intestazione_sequenza(\''+numero_sequenze+'\')"></span>';
	nuovo_div=nuovo_div+'<span'+suffisso_display+'> '+display_fine+'<input type="text" id="efine_'+numero_sequenze+'" name="efine['+numero_sequenze+']" value="'+orario_fine+'" maxlength="5" size="5" onBlur="cambia_intestazione_sequenza(\''+numero_sequenze+'\')"></span>';
	nuovo_div=nuovo_div+'</span>';
	
	nuovo_div=nuovo_div+'<div style="position: relative; float:right;">'; // il div che contiene i pulsanti e il selettore dei jingle
	nuovo_div=nuovo_div+div_pulsanti;
	nuovo_div=nuovo_div+'<div id="contenitore_jingle_'+numero_sequenze+'" class="contenitore_jingle"></div>';
	nuovo_div=nuovo_div+'</div>'
	
	nuovo_div=nuovo_div+'<br>';
	nuovo_div=nuovo_div+'<div id="sequenza_'+numero_sequenze+'" class="sequenza_elementi">';
	nuovo_div=nuovo_div+'<div id="fine_sequenza_'+numero_sequenze+'" class="elemento_sequenza"></div>';
	nuovo_div=nuovo_div+'<input type="hidden" id="elementi_sequenza_'+numero_sequenze+'" name="elementi_sequenza['+numero_sequenze+']" value="0">';
	nuovo_div=nuovo_div+'</div>';
	nuovo_div=nuovo_div+'</div>';
	nuovo_div=nuovo_div+'<input type="hidden" name="eidsequenza['+numero_sequenze+']" value="'+id+'">';
	nuovo_div=nuovo_div+'<input type="hidden" id="sequenza_tipologia_'+numero_sequenze+'" name="sequenza_tipologia['+numero_sequenze+']" value="'+tipologia+'">';
	nuovo_div=nuovo_div+'<input type="hidden" id="ecancella_sequenza_'+numero_sequenze+'" name="ecancella_sequenza['+numero_sequenze+']" value="0">';
	nuovo_div=nuovo_div+'<input type="hidden" id="eattiva_sequenza_'+numero_sequenze+'" name="eattiva_sequenza['+numero_sequenze+']" value="'+attiva+'">';
	nuovo_div=nuovo_div+'<div id="fine_sequenze" class="sequenza" style="display: none;"><span class="orario_inizio">999999999</span></div>';
		
	$('div#fine_sequenze').replaceWith(nuovo_div);

	if (attiva==0) {
		$("div#sequenza_esterna_"+numero_sequenze).addClass("sequenza_pausa");
		$("div#pulsante_pausa_"+numero_sequenze).hide();
		$("div#pulsante_play_"+numero_sequenze).show();
	}
	
	if ((tipologia=="radio") || (tipologia=="musictv")) {
		carica_radio(numero_sequenze, id, 0, tipologia);
	//	carica_jingle(numero_sequenze, id);	// funzione non più utilizzata
		}

	seleziona_div(numero_sequenze);
	numero_sequenze=Math.round(numero_sequenze)+1;
	$('#numero_sequenze').val(numero_sequenze);
	
}

function random_sequenza(sequenza, tipologia) {
	var elementi = prompt("Numero di brani da inserire", "150");
	if (elementi != null) {
		seleziona_div(sequenza);
		carica_radio(sequenza, 0, elementi, tipologia);
	}
}

function carica_radio(sequenza, id_sequenza, elementi, tipologia) {
	if (id_sequenza==0)
		$('div#sequenza_'+sequenza).load("<?php echo base_url(); ?>index.php/mp3/playlist_genera/"+sequenza+"/"+elementi+"/"+tipologia);
	else
		$('div#sequenza_'+sequenza).load("<?php echo base_url(); ?>index.php/mp3/playlist_carica/"+sequenza+"/"+id_sequenza);
}

function carica_jingle(sequenza, id) {
	// funzione non più utilizzata
	$('div#contenitore_jingle_'+sequenza).load("<?php echo base_url(); ?>index.php/mp3/playlist_carica_jingle/"+sequenza+"/"+id);
}

function cambia_categoria_jingle(casella, sequenza, indice) {
		var valore=casella.options[casella.selectedIndex].value;
		if (valore==0)
			$('#categoria_jingle_intervallo_'+sequenza+'_'+indice).hide();
		else
			$('#categoria_jingle_intervallo_'+sequenza+'_'+indice).show();
	}

function cancella_elemento(sequenza_selezionata, elemento) {
	var cancella=$('#ecancella_'+sequenza_selezionata+'_'+elemento).val();

	if (cancella==0) nuovacancella=1;
		else nuovacancella=0;
	$('#ecancella_'+sequenza_selezionata+'_'+elemento).val(nuovacancella);
	$('div#elemento_'+sequenza_selezionata+'_'+elemento).hide(50);
}

function cancella_sequenza(sequenza_selezionata) {
	var cancella=$('#ecancella_sequenza_'+sequenza_selezionata).val();
	if (cancella==0)
		if (!confirm('Cancello?')) {
			return false;
		}
	if (cancella==0) nuova=1;
		else nuova=0;
	$('#ecancella_sequenza_'+sequenza_selezionata).val(nuova);
	$('div#sequenza_esterna_'+sequenza_selezionata).hide(100);
}

function attiva_sequenza(sequenza_selezionata) {
	var attiva=$('#eattiva_sequenza_'+sequenza_selezionata).val();
	if (attiva==1) {
		nuova=0;
		$("div#sequenza_esterna_"+sequenza_selezionata).addClass("sequenza_pausa");
		$("div#pulsante_pausa_"+sequenza_selezionata).hide();
		$("div#pulsante_play_"+sequenza_selezionata).show();
		}
	if (attiva==0) {
		nuova=1;
		$("div#sequenza_esterna_"+sequenza_selezionata).removeClass("sequenza_pausa");
		$("div#pulsante_pausa_"+sequenza_selezionata).show();
		$("div#pulsante_play_"+sequenza_selezionata).hide();
	}
	
	$('#eattiva_sequenza_'+sequenza_selezionata).val(nuova);
	// $('div#sequenza_'+sequenza_selezionata).hide(100);
}


function checkform() {

	var numero_sequenze=document.playlist_form.numero_sequenze.value;
	var attiva_sequenza='';
	var inizio='';
	var fine='';
	var cancella_sequenza='';
	var xx=0;
	while (xx<numero_sequenze) {
		attiva_sequenza=attiva_sequenza+$('#eattiva_sequenza_'+xx).val();
		inizio=inizio+$('#einizio_'+xx).val();
		fine=fine+$('#efine_'+xx).val();
		cancella_sequenza=cancella_sequenza+$('#ecancella_sequenza_'+xx).val();
		if (xx<(numero_sequenze-1)) {
				attiva_sequenza=attiva_sequenza+"-";
				inizio=inizio+"-";
				fine=fine+"-";
				cancella_sequenza=cancella_sequenza+"-";
		}
		xx++;
	}
	
	var stringa = '';
	stringa=stringa+"edownload="+document.playlist_form.edownload.value;
	stringa=stringa+"&numero_sequenze="+numero_sequenze;
	stringa=stringa+"&eattiva_sequenza="+attiva_sequenza;
	stringa=stringa+"&einizio="+inizio;
	stringa=stringa+"&efine="+fine;
	stringa=stringa+"&ecancella_sequenza="+cancella_sequenza;
	
	$.ajax({

	  url: "<?php echo base_url(); ?>index.php/strimy/orario_playlist_check", 
	  data: stringa,
	  async: false,
	  dataType: 'html',
	  method: 'POST',
	  success: function(risultato) {
			if (risultato=="0") {
				document.playlist_form.submit();
			}
			else if (risultato=="1") {
				alert('ATTENZIONE: sovrapposizione orari');
				return false;
			}
			else {
				alert(risultato);
				return false;
			}
	  }
	});
	return false;

}



function riordina() {
	
	$('div#contenitore_sequenze>div.sequenza').tsort('span.orario_inizio');
		
	}

function mostra_aggiungi(elemento) {
		$("div.elemento_archivio_overlay").fadeOut();
		$("#elemento_archivio_overlay_"+elemento).fadeIn(100);
		
	}

function nascondi_aggiungi(elemento) {
		$("#elemento_archivio_overlay_"+elemento).fadeOut(10);
	}

function durata_elemento(operazione, sequenza, elemento) {
	var durata=$('#edurata_'+sequenza+'_'+elemento).val();
	if (isNaN(durata)) {
		durata=0;
		$('#edurata_'+sequenza+'_'+elemento).val(durata);	
	}
	if ((operazione=='diminuisci') && (durata==0))
		return false;
	if (operazione=='diminuisci')
		durata=Math.round(durata)-1;
	if (operazione=='aumenta')
		durata=Math.round(durata)+1;
	$('#edurata_'+sequenza+'_'+elemento).val(durata);	
	}

</script>
<?php



echo "<div id=\"playlist\" style=\"margin-bottom: 150px;\">";


// echo "<form id=\"playlist_form\">";
echo "<form id=\"playlist_form\" name=\"playlist_form\" method=\"post\" action=\"".base_url()."index.php/strimy/playlist_salva\">";


$this->db->from('dispositivi');
$this->db->where('id_dispositivo', $device_id);
$queryDIS=$this->db->get();
$rowDIS=$queryDIS->row();
echo "<br>Dispositivo in uso: <strong>".$device_id."</strong>";
if ($rowDIS->descrizione!="") echo ", ".$rowDIS->descrizione;
if ($rowDIS->id_gruppo!=0):
	$this->db->from('dispositivi_gruppi');
	$this->db->where('id', $rowDIS->id_gruppo);
	$queryDG=$this->db->get();
	$rowDG=$queryDG->row();
	echo " (".$rowDG->descrizione.")";
endif;
echo "<br>";

echo "<br>";


echo "<div class=\"btn-group\">";
echo "<button type=\"button\" class=\"btn btn-default fa fa-floppy-o\" onClick=\"checkform()\">salva</button>";

$xx=0;

if ($privilegi['radio']==1)
	$privilegi['magic_radio']=1;

foreach($privilegi as $privilegi_temp => $key):
	if ($privilegi[$privilegi_temp]==1):
		if ($privilegi_temp=="immagini"):
			echo "<button type=\"button\" class=\"btn btn-default fa fa-camera\" onClick=\"aggiungi_sequenza('0','".$privilegi_temp."','','00:00','00:05','1')\">Slideshow</button>";
		elseif ($privilegi_temp=="video"):
			echo "<button type=\"button\" class=\"btn btn-default fa fa-video-camera\" onClick=\"aggiungi_sequenza('0','".$privilegi_temp."','','00:00','00:05','1')\">Video</button>";
		elseif ($privilegi_temp=="audio"):
			echo "<button type=\"button\" class=\"btn btn-default fa fa-volume-up\" onClick=\"aggiungi_sequenza('0','".$privilegi_temp."','','00:00','00:05','1')\">Audio</button>";
		elseif ($privilegi_temp=="radio"):
			echo "<button type=\"button\" class=\"btn btn-default fa fa-wifi\" onClick=\"aggiungi_sequenza('0','".$privilegi_temp."','','00:00','00:05','1')\">Radio</button>";
		elseif ($privilegi_temp=="magic_radio"):
			echo "<button type=\"button\" class=\"btn btn-default fa fa-wifi\" onClick=\"openFancybox('".base_url()."index.php/mp3/radio_magic/".$device_id."');\">Magic radio</button>";
		endif;
		
		$xx++;
	endif;
endforeach;



echo "<button type=\"button\" class=\"btn btn-default fa fa-plug\" onClick=\"aggiungi_sequenza('0','on','accensione','00:00','00:05','1')\">on</button>";
echo "<button type=\"button\" class=\"btn btn-default fa fa-poweroff\" onClick=\"aggiungi_sequenza('0','on','stand-by','00:00','00:05','1')\">stand-by</button>";
echo "<button type=\"button\" class=\"btn btn-default fa fa-floppy-o\" onClick=\"checkform()\">salva</button>";


echo "</div>";

$giornoARR[0]="ogni giorno";
$giornoARR[1]="lunedì";
$giornoARR[2]="martedì";
$giornoARR[3]="mercoledì";
$giornoARR[4]="giovedì";
$giornoARR[5]="venerdì";
$giornoARR[6]="sabato";
$giornoARR[7]="domenica";

/*
$yy=0;
foreach ($giornoARR as $giorno_temp):
	echo "<a href=\"".base_url()."index.php/strimy/gestione_playlist/".$device_id."/".$yy."\">";
	if ($yy==$giorno)
		echo "<div class=\"tab_selezionata\">".$giorno_temp."</div>";
	else
		echo "<div class=\"tab_deselezionata\">".$giorno_temp."</div>";
	echo "</a>";
	$yy++;
endforeach;
*/

echo "<ul class=\"nav nav-tabs\">";
foreach ($giornoARR as $giorno_temp):
	if ($yy==$giorno)
		echo "<li class=\"active\">";
	else
		echo "<li>";
	echo "<a href=\"".base_url()."index.php/strimy/gestione_playlist/".$device_id."/".$yy."\">";
	echo $giorno_temp;
	echo "</a></li>";
	$yy++;
endforeach;
echo "</ul>";


// echo "</div>";

echo "<div id=\"linea\" style=\"width: 100%; border-bottom: 1px solid #F3F3F3; clear: both; margin-bottom: 10px;\"></div>";

echo "</div>";


echo "<div style=\"width: 90%; margin: 10px auto 10px auto;\">orario download (hh:mm)<input type=\"text\" name=\"edownload\" value=\"".$orario_download."\" size=\"5\"></div>";
// echo "<br><input type=\"button\" value=\"salva playlist\" onClick=\"checkform('modulo');\">";



$yy=0;
$zz=0;

echo "<input type=\"hidden\" name=\"egiorno_sequenza\" id=\"egiorno_sequenza\" value=\"".$giorno."\">";	// questo campo indica il numero totale di sequenze
echo "<input type=\"hidden\" id=\"numero_sequenze\" name=\"numero_sequenze\" value=\"0\">";	// questo campo indica il numero totale di sequenze
echo "<input type=\"hidden\" id=\"sequenza_selezionata\" value=\"-1\">";	// questo campo indica la sequenza attualmente selezizonata
echo "<div id=\"contenitore_sequenze\">";	// questo div contiene le sequenze
echo "<div id=\"fine_sequenze\" class=\"sequenza\" style=\"display: none;\"><span class=\"orario_inizio\">9999999</span></div>";	// questo tag serve solo per indicare dove andrà posizionato la nuova sequenza
echo "</div>";
echo "<script language=\"javascript\">";
while ($yy<$totale_sequenze) {
	
	?>
	
	aggiungi_sequenza('<?php echo $id_sequenza[$yy]; ?>','<?php echo $sequenza[$yy]; ?>','<?php echo $titolo_sequenza[$yy]; ?>','<?php echo $sequenza_inizio[$yy]; ?>','<?php echo $sequenza_fine[$yy]; ?>','<?php echo $attiva_sequenza[$yy]; ?>');
	
	<?php
	///////////////////////////////////////////////////////
	$xx=0;
	while ($xx<$totale_elementi_sequenza[$yy]) {
			$file=$sequenza_valore_1[$yy][$xx];
			$file_array=explode(".", $file);
			$elenco_file_estensione[$yy][$xx]=strtolower(end($file_array));
			if (($elenco_file_estensione[$yy][$xx]=="jpg") || ($elenco_file_estensione[$yy][$xx]=="png"))
					$elenco_file_tipologia[$yy][$xx]="immagini";
			if (($elenco_file_estensione[$yy][$xx]=="mp4") || ($elenco_file_estensione[$yy][$xx]=="mkv") || ($elenco_file_estensione[$yy][$xx]=="mov"))
					$elenco_file_tipologia[$yy][$xx]="video";
			if (($elenco_file_estensione[$yy][$xx]=="mp3") || ($elenco_file_estensione[$yy][$xx]=="wav"))
					$elenco_file_tipologia[$yy][$xx]="audio";
			
			$cancella_elemento=0;
			if (!file_exists($cartella_archivio."/".$file)):
				$cancella_elemento=1;				
			else:
		
			
			?>
		
			aggiungi_elemento('<?php echo $elemento_id[$yy][$xx]; ?>', '0', '<?php echo $elenco_file_tipologia[$yy][$xx]; ?>', '<?php echo $yy; ?>');
			<?php
			endif;

		$xx++;
		}
	///////////////////////////////////////////////////////

	$yy++;
	}


echo "seleziona_div('-1');";
echo "</script>";

echo "<div style=\"clear: both;\"></div>";

echo "<input type=\"hidden\" name=\"edevice_id\" value=\"".$device_id."\" readonly=\"readonly\">";
echo "<input type=\"hidden\" id=\"econferma\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
echo "</form>";

echo "</div>";



// visualizza l'archivio

echo "<div id=\"archivio\" class=\"owl-carousel owl-theme\">";

$dataLISTA['gruppo']=$grupposelezionato;
$dataRET=$this->strimy_model->lista_file_archivio($dataLISTA);


if ($dataRET['totale_file']==0):

	echo "Archivio vuoto";

else:
	
	// echo "<div style=\"clear: both\">";
	$xx=0;
	foreach ($dataRET['elenco_file'] as $file):
			$style="";
			if ($dataRET['elenco_file_tipologia'][$xx]=="immagini"):
				$file_anteprima=$url_archivio."/".$file;
			elseif ($dataRET['elenco_file_tipologia'][$xx]=="video"):
				$file_anteprima="/style/icone/archive_video.jpg";
			elseif ($dataRET['elenco_file_tipologia'][$xx]=="audio"):
				$file_anteprima="/style/icone/archive_music.jpg";
			endif;
			
			echo "<div class=\"item\">";
			
				
				echo "<div class=\"elemento_archivio archivio_file archivio_".$dataRET['elenco_file_tipologia'][$xx]."\" style=\"background: url('".$file_anteprima."'); background-size: 100% 100%; position: relative; z-index: 5;\" onmouseenter=\"mostra_aggiungi('".$xx."')\">";
				
				echo "<div id=\"elemento_archivio_overlay_".$xx."\" class=\"elemento_archivio_overlay\" onclick=\"aggiungi('0', '".$dataRET['elenco_file_id'][$xx]."', '".$dataRET['elenco_file_tipologia'][$xx]."');\" onmouseleave=\"nascondi_aggiungi('".$xx."')\"><i class=\"fa fa-plus\" style=\"color: white; font-size: 2em;\"></i>";
				
				echo "</div>";

				echo "<input type=\"hidden\" id=\"efile_".$xx."\" value=\"".$file."\" readonly=\"readonly\">";
				echo "<input type=\"hidden\" id=\"efileid_".$xx."\" value=\"".$dataRET['elenco_file_id'][$xx]."\" readonly=\"readonly\">";
				
				
				echo "</div>";
				echo "<div class=\"elemento_archivio_overlay_titolo\">".$dataRET['elenco_file_descrizione'][$xx]."</div>";
				
			echo "</div>";
			
			$xx++;
	endforeach;
	$totale_elementi=$xx;

endif;

echo "</div>";


echo "</div>";

?>
<script language="javascript">

$(document).ready(function() {
 
  var owl = $("#archivio");
 
  owl.owlCarousel({
     
      itemsCustom : [
        [0, 2],
        [450, 4],
        [600, 7],
        [700, 9],
        [1000, 10],
        [1200, 12],
        [1400, 13],
        [1600, 15]
      ],
      navigation : false,
      pagination: true,
      paginationNumbers: true
 
  });
 
});

</script>
