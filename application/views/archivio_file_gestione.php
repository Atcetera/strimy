<script language="Javascript">
	function cambia_cancellazione(casella, privilegio) {
		if (casella.checked)
			$("#ecancella").val('1');
		else
			$("#ecancella").val('0');
	}
	
	function check_cancella() {
		cancellazione=$("#ecancella").val();
		if (cancellazione==1) {
			if (confirm('Desideri cancellare il file?'))
				return true;
			else
				return false;
		}
		return true;
	}
	
	function cambia_tipologia(casella) {
		if (casella.value==0) {
			$("#tabella_brano").show(50);
			$("#tabella_categoria").hide(10);
		}
		if (casella.value==1) {
			$("#tabella_brano").hide(10);
			$("#tabella_categoria").show(50);
		}
		$("#ejingle").val(casella.value);
	}
	
	function file_selezionato() {
		$("#efile_selezionato").val(1);
	}
	
	function check_form() {
		
		$('#emessaggio_errore').hide();
		var errore=0;
		var messaggio="";
		
	//	alert($('#efile')[0].files[0].size);
		
		if ($("#efile_selezionato").val()==0) {

			messaggio="Non &egrave; stato selezionato alcun file";
			errore=1;
		}
		else {
		<?php if ($origine!="archivio_sistema"): ?>
			var size = $('#efile')[0].files[0].size;
			var dimensione_totale=$('#edimensione_totale').val();
			var limite_spazio=$('#elimite_spazio').val();
			var totale=Math.round(size)+Math.round(dimensione_totale);
			if (totale>limite_spazio) {
				errore=1;
				messaggio="La dimensione del file eccede lo spazio di archiviazione disponibile";
			}
		<?php endif; ?>
		}
		if (errore==1) {
			messaggio_errore(messaggio);
			return false;
		}
		else {
			messaggio="Attendi l'upload del file...";
			messaggio_conferma(messaggio);	
		}
	}
	
</script>

<?php

$gruppo=$this->flexi_auth->get_user_group_id();
$grupposelezionato=$this->strimy_model->accountselezionato();
$dataLISTA['gruppo']=$grupposelezionato;
$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
// $privilegi=$this->strimy_model->account_privilegi($grupposelezionato);

$percorso_base=$this->config->item('real_path');

$spazio_account=$this->users_model->spazio_account($grupposelezionato);

if ($id_file==0):
	$cnome_file="";
	$cdescrizione="";
else:
	$this->db->from($origine);
	$this->db->where('id', $id_file);
	$this->db->where('id_account', $grupposelezionato);
	$query=$this->db->get();
	$row=$query->row();
	$cnome_file=$row->nome_file;
	$cdescrizione=$row->descrizione;
	$valori=unserialize($row->valori);
endif;

echo "<form method=\"post\" class=\"form-horizontal\" action=\"".$this->config->item('base_url')."index.php/strimy/file_upload\" enctype=\"multipart/form-data\" onsubmit=\"return check_form()\">";


if ($id_file==0):
echo '<div id="form_efile" class="form-group">
    <label for="eform_file" class="col-sm-3 control-label">File da caricare</label>
    <div class="col-sm-8">
    
             <span class="btn btn-default btn-file fa fa-upload">
                Seleziona... (max '.$this->config->item('limite_upload').' mb)<input type="file" name="efile" id="efile" onchange="file_selezionato()" />
            </span>
      
    </div>
  </div>';

	echo "<input type=\"hidden\" name=\"ecategoria\" value=\"0\">";

	$etichetta="Conferma Upload";
	$file_selezionato=0;
else:

	if ($origine=="archivio_file")
		$directory_contenuto="content";
	elseif ($origine=="archivio_sistema")
		$directory_contenuto="../common";
	else
		$directory_contenuto="";

	$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);
	$thumbnail=$dataSTORAGE['full_path']."/users/".$stringa_casuale."/".$directory_contenuto."/".$valori['nome_file_thumb'];

	echo '<div id="form_efile" class="form-group">
    <label for="eform_file" class="col-sm-3 control-label">File</label>
    <div class="col-sm-8" style="overflow: hidden">
			<img src="'.$thumbnail.'"><br>'.$cnome_file.'			
    </div>
  </div>';


	$etichetta="Aggiorna dati";
	$file_selezionato=1;
endif;

echo '<div id="form_edescrizione" class="form-group">
    <label for="eform_descrizione" class="col-sm-3 control-label">Descrizione</label>
  
        <div class="col-sm-9">
      <input type="text" class="form-control" name="edescrizione" value="'.$cdescrizione.'" placeholder="se lasciata vuota, verr&agrave; utilizzato il nome del file">
	</div>

  </div>';

echo "<input type=\"hidden\" name=\"etipologia_file_redirect\" value=\"".$tipologia_file."\">";
echo "<input type=\"hidden\" name=\"eorigine\" value=\"".$origine."\">";
echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
echo "<input type=\"hidden\" name=\"eid_file\" value=\"".$id_file."\" readonly=\"readonly\">";

echo "<input type=\"hidden\" id=\"edimensione_totale\" value=\"".$spazio_account['dimensione_totale']."\">";
echo "<input type=\"hidden\" id=\"elimite_spazio\" value=\"".$spazio_account['limite_spazio']."\">";

echo "<input type=\"hidden\" id=\"efile_selezionato\" value=\"".$file_selezionato."\">";

echo "<div class=\"text-right\">";
echo "<span id=\"emessaggio_errore\" class=\"messaggio_errore\"></span>";
echo "<span id=\"emessaggio_conferma\" class=\"messaggio_conferma\"></span><br>";
echo "<input type=\"submit\" value=\"".$etichetta."\" class=\"btn btn-primary\">";
echo "</div>";
echo "</form>";

?>

