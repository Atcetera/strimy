<script language="Javascript">
 jQuery(document).ready(function () {
	jQuery('a#contatti').colorbox({width: "600px", closeButton: false, scrolling: false});
});

</script>


<div class="contenitore_paragrafo">
	
	<div class="paragrafo">
		<table>
			<tr>
			<td><img src="<?php echo base_url() ?>/style/images/monitor.png"></td>
			<td><div style="margin-left: 70px; font-size: 1.3em;"><span style="font-size: 1.5em; color: black; line-height: 1.7em;">Strimy</span><br>Trasforma il tuo monitor o TV in un digital signage professionale e controllalo da dove vuoi.</div>
			<div style="text-align: center; margin-left: auto; margin-right: auto;"> <a href="#" onClick="openFancybox('<?php echo base_url() ?>index.php/pub/contatti');"><input type="submit" value=" Tienimi informato " style="margin-top: 40px; font-size: 1.5em; font-weight: 300;"></a></div></td>
			</tr>
		</table>
		<div id="payoff">Il digital signage per tutti.</div>
	</div>
</div>

<div class="contenitore_paragrafo_bianco">
	<div class="paragrafo">
		<div class="titolo_paragrafo">Cos'è Strimy?</div>
		Strimy è un sistema di segnaletica digitale per vetrine, negozi, esercizi commerciali e luoghi pubblici.<br>
		Si adatta a qualunque monitor o tv e trasmette i tuoi contenuti in modo facile, efficiente ed 
		economico, lasciando a te il controllo completo del palinsesto.
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo">
		<table>
			<tr>
			<td><div class="icona_paragrafo"><i class="fa fa-gears"></i></div></td>
			<td><div class="titolo_paragrafo">Come funziona</div>
			Accedi al tuo pannello di controllo su www.strimy.it e personalizza la tua scaletta 
			con orari di accensione e spegnimento.<br> Carica foto, video, annunci personalizzati e notizie, oppure scegli stream di notizie o playlist di video e musica.<br>
			Strimy si aggiorna in automatico e riproduce i tuoi contenuti agli orari stabiliti, accendendo e spegnendo la TV quando serve!<br>
			E quando non ci sono contenuti programmati, Strimy mostra l'homescreen con orario, informazioni meteo e notizie utili.</td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo_bianco">
	<div class="paragrafo">
		<table>
			<tr>
			<td><div class="titolo_paragrafo">Dovunque, in ogni momento</div>
			Con Strimy non devi più essere in negozio o contattare il tuo tecnico per comunicare con i clienti. Devi chiudere per un imprevisto? Imposta l'avviso per comunicarlo nelle ore in cui non puoi esserci! Hai un nuovo prodotto in promozione giovedì? Hai diversi negozi o un'intera catena?<br>
			Non hai tempo di occupartene durante l'orario di apertura?<br>
			Nessun problema: puoi controllare le scalette di riproduzione di uno o cento schermi, dallo stesso pc, in qualsiasi momento, anche dal divano di casa!</td>
			<td><div class="icona_paragrafo_right"><i class="fa fa-laptop"></i></div></td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo">
		<table>
			<tr>
			<td><div class="icona_paragrafo"><i class="fa fa-check-square-o"></i></div></td>
			<td><div class="titolo_paragrafo">Pronto in 5 minuti</div>
			L'idea di base di Strimy è che tutto dev'essere semplice: non serve chiamare un tecnico né essere esperti di informatica. Anzi, abbiamo inventato Strimy proprio per chi è negato coi computer.<br>
			Il kit contiene già tutto il necessario: basta collegare i tre cavi alla tv, alla rete internet e a una presa di corrente.<br>
			Alla prima accensione, sullo schermo appare il codice di attivazione. Vai su www.strimy.it, registrati e inserisci il codice: il tuo Strimy è pronto per trasmettere!<br></td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo_bianco">
	<div class="paragrafo">
		<table>
			<tr>
			<td><div class="titolo_paragrafo">Piccolo, economico, ecologico</div>
			Strimy sfrutta la potenza e versatilità di <a href="http://www.raspberrypi.org">Raspberry Pi</a> per offrirti il digital signage più piccolo ed efficiente al mondo. È grande come una carta di credito: posizionalo dietro al monitor per risparmiare spazio e nasconderlo alla vista.<br>
			Strimy consuma 20 volte meno di un sistema tradizionale con computer.
			In un anno, un normale pc acceso 24 ore al giorno consuma 132,00 € di elettricità*: Strimy ne consuma 6,60!<br>
			Inoltre, Strimy accende e spegne la tv agli orari prestabiliti e, se la spegni manualmente, Strimy entra in modalità di basso consumo.<br>
			<span style="font-size: 0.8em; line-height: 1.1em; color: #999;"><br>*Considerando il consumo annuo di un normale pc (circa 880kw/h) a una tariffa energetica media nazionale (circa 0,15 € per Kw/h).</span></td>
			<td><div class="icona_paragrafo_right"><i class="fa fa-plug"></i></div></td>
			</tr>
		</table>
	</div>
</div>

<div class="contenitore_paragrafo">
	<div class="paragrafo_centrato">
		Strimy è un prodotto di <a href="http://www.atcetera.it">Atcetera</a> - p. iva 04582860260 - <a href="mailto:info@strimy.it">info@strimy.it</a> - <a href="<?php echo base_url() ?>index.php/pub/privacy">privacy</a>
	</div>

