<script language="Javascript">

function checkform() {

	var errore=0;
	var messaggio="";

	$(".form-group").removeClass('has-error');
    if ($("#city").val()=="") {
		$("#form_eparametro_city").addClass('has-error');
		errore=1;
	}
	
	if ($("#rss_url").val()=="") {
		$("#form_eparametro_rss_url").addClass('has-error');
		errore=1;
	}

	if ($("#edescrizione_dispositivo").val()=="") {
		$("#form_edescrizione_dispositivo").addClass('has-error');
		errore=1;
	}
	
	if (errore==1) {
		return false;
	}
	else {
		return true;
	}

}

</script>


<?php

$grupposelezionato=$this->strimy_model->accountselezionato();
$this->db->from('user_groups');
$this->db->where('ugrp_id', $grupposelezionato);
$query=$this->db->get();
$row=$query->row();
$stringa=$row->ugrp_stringa;

$percorso_base=$this->config->item('archivio_real_path');
$percorso_devices=$this->config->item('percorso_devices');

$cartella_base=$percorso_base.$percorso_devices.$device_id;

$nome_file="config";

$file=$cartella_base."/".$nome_file;


if ($operazione=="configurazione"):




	$parametro_id=Array();
	$parametro_nome=Array();
	$parametro_modificabile=Array();
	$parametro_visibile=Array();
	$parametro_default=Array();

	$parametro_id[]="name";
	$parametro_nome[]="nome dispositivo";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]=$device_id;

	$parametro_id[]="id_dispositivo";
	$parametro_nome[]="ID dispositivo";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]=$device_id;

	$parametro_id[]="user";
	$parametro_nome[]="Account";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]=$grupposelezionato."_".$stringa;

	$parametro_id[]="on";
	$parametro_nome[]="Orario di accensione";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="09:00";

	$parametro_id[]="off";
	$parametro_nome[]="Orario di spegnimento";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="23:00";

	$parametro_id[]="background";
	$parametro_nome[]="Immagine di sfondo";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="background_04.jpg";

	$parametro_id[]="musictv_background";
	$parametro_nome[]="Immagine di sfondo Music TV";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="";

	$parametro_id[]="city";
	$parametro_nome[]="Citt&agrave;";
	$parametro_modificabile[]=1;
	$parametro_visibile[]=1;
	$parametro_default[]="Milano";

	$parametro_id[]="rss_url";
	$parametro_nome[]="RSS news feed";
	$parametro_modificabile[]=1;
	$parametro_visibile[]=1;
	$parametro_default[]="http://feeds.feedburner.com/mtvitalia_news";

	$parametro_id[]="tempo_aggiornamento_meteo";
	$parametro_nome[]="Intervallo di aggiornamento meteo";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="600";

	$parametro_id[]="tempo_aggiornamento_rss";
	$parametro_nome[]="Intervallo di aggiornamento RSS";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="300";

	$parametro_id[]="transizione_rss";
	$parametro_nome[]="Durata visualizzazione news";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="5";

	$parametro_id[]="intervallo_aggiornamento_playlist";
	$parametro_nome[]="Sincronizzazione playlist 1";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="15";

	$parametro_id[]="intervallo_controllo_orari";
	$parametro_nome[]="Sincronizzazione playlist 2";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="1";

	$parametro_id[]="download_configurazioni";
	$parametro_nome[]="download_configurazioni";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="20";

	$parametro_id[]="colore_orologio";
	$parametro_nome[]="colore_orologio";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="255255255";

	$parametro_id[]="colore_dati_meteo";
	$parametro_nome[]="colore_dati_meteo";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="255255255";

	$parametro_id[]="colore_rss";
	$parametro_nome[]="colore_rss";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="255255255";

	$parametro_id[]="font_size";
	$parametro_nome[]="font_size";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="100";

	$parametro_id[]="spacing";
	$parametro_nome[]="spacing";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="1";

	$parametro_id[]="margin";
	$parametro_nome[]="margin";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="20.0";

	$parametro_id[]="background_color";
	$parametro_nome[]="background_color";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="000000000200";

	$parametro_id[]="audio_output";
	$parametro_nome[]="audio_output";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="2";

	$parametro_id[]="font_color";
	$parametro_nome[]="font_color";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="255255255255";

	/*
	$parametro_id[]="egruppo";
	$parametro_nome[]="egruppo";
	$parametro_modificabile[]=0;
	$parametro_visibile[]=0;
	$parametro_default[]="0";
	*/

	$this->db->from('dispositivi');
	$this->db->where('id_dispositivo', $device_id);
	$this->db->where('attivato', 1);
	$queryDIS=$this->db->get();

	if ($queryDIS->num_rows()==0): // non è stato trovato alcun file di configurazione: l'id è valido?

		echo "Id dispositivo <b>".$device_id."</b> non valido";


	else:

		$data_gruppi=$this->strimy_model->recupera_gruppi();

		$this->db->from('dispositivi');
		$this->db->where('id_dispositivo', $device_id);
		$this->db->where('id_account', $grupposelezionato);

		$query=$this->db->get();
		$row=$query->row();
	
		$this->db->from('dispositivi_gruppi');
		$this->db->where('id', $row->id_gruppo);
		$this->db->where('id_account', $grupposelezionato);
		$queryGRP=$this->db->get();
		if ($queryGRP->num_rows()==0)
			$id_gruppo=0;
		else
			$id_gruppo=$row->id_gruppo;

		echo "<form id=\"configurazione\" class=\"form-horizontal\" action=\"".$this->config->item('base_url')."index.php/strimy/configurazione_salva\" method=\"post\">";
		
	//	echo "<table class=\"table borderless\">";
		
		echo '<div id="form_edescrizione_dispositivo" class="form-group">
			<label for="edescrizione_dispositivo" class="col-sm-4 control-label">Descrizione dispositivo</label>
			<div class="col-sm-8">
			<input type="text" class="form-control" id="edescrizione_dispositivo" name="edescrizione_dispositivo" value="'.$row->descrizione.'">
			</div>
			</div>';
			
		echo '<div id="form_egruppo" class="form-group">
			<label for="e_egruppo" class="col-sm-4 control-label">Gruppo di appartenenza</label>';
		
		echo "<div class=\"col-md-8\">";
			
				echo "<select name=\"elayout_widget_".$indice."[]\" id=\"elayout_widget_".$indice."_".$zz."\" class=\"form-control playlist_select\">";
		
		// echo "<tr><td style=\"width: 150px;\">Descrizione dispositivo</td>";
		// echo "<td><input type=\"text\" name=\"edescrizione_dispositivo\" value=\"".$row->descrizione."\" size=\"30\"></td></tr>";
		// echo "<tr><td>Gruppo di appartenenza:</td>";
		// echo "<td><select name=\"egruppo\">";
		echo "<option value=\"0\"";
		if ($id_gruppo==0) echo " selected";
		echo ">(nessuno)</option>";
		$yy=0;
		foreach ($data_gruppi['gruppo_id'] as $gruppo_temp):
			echo "<option value=\"".$data_gruppi['gruppo_id'][$yy]."\"";
			if ($id_gruppo==$data_gruppi['gruppo_id'][$yy]) echo " selected";
			echo ">"." ".$data_gruppi['gruppo_descrizione'][$yy]."</option>";
			$yy++;
		endforeach;
		echo "</select>";
		// echo "</td></tr>";
		// echo "</table>";
		echo "</div></div>";
		
		echo "<p>Questi parametri vengono utilizzati per generare la homescreen e bypassano la configurazione delle playlist</p>";


		// lettura parametri di configurazione
		
		$parametro0=Array();
		$parametro1=Array();

		$xx=0;
		$this->db->from('dispositivi_configurazione');
		$this->db->where('id_dispositivo', $device_id);
		// $this->db->where('attivato', 1);
		$query=$this->db->get();
		if ($query->num_rows()>0):
			$row=$query->row();
			$parametri_configurazione=unserialize($row->parametri_configurazione);

			unset($parametri_configurazione['egruppo']);
			$totale_parametri=count($parametri_configurazione);
			$xx=0;
			foreach ($parametri_configurazione as $parametro0[$xx] => $parametro1[$xx]):
				$yy=array_search($parametro0[$xx], $parametro_id);
				// echo $parametro0[$xx];
						
				if (!$yy):	// il parametro non è definito a livello di configurazione e va aggiunto dinamicamente
				//	echo " no";
					$parametro_id[]=$parametro0[$xx];
					$parametro_nome[]=$parametro0[$xx];
					$parametro_visibile[]=0;
					$parametro_modificabile[]=0;
					$parametro_default[]="";
					
				endif;
				// echo "<br>";
				$xx++;

			endforeach;
		endif;

		$totale_righe=$xx;
		$totale_parametri=count($parametro_id);


		$xx=0;
		
		while ($xx<$totale_parametri) {
			
			$yy=array_search($parametro_id[$xx], $parametro0);

			if (!$yy):
				$cvalore=$parametro_default[$xx];
			else:
				$cvalore=$parametro1[$yy];
			endif;
			
			if (!isset($parametro_visibile[$xx]))
				$parametro_visibile[$xx]=1;
			
			echo '<div id="form_eparametro_'.$parametro_id[$xx].'" class="form-group"';
		
			if ($parametro_visibile[$xx]==0)
				echo " style=\"display: none;\"";
			echo '><label for="eid_dispositivo" class="col-sm-4 control-label">'.$parametro_nome[$xx].'</label>
			<div class="col-sm-8">';
			
			if ($parametro_id[$xx]=="user"):
				$cvalore=$parametro_default[$xx];
			endif;

			if ($parametro_id[$xx]=="musictv_background"):
				echo "<select name=\"".$parametro_id[$xx]."\" id=\"".$parametro_id[$xx]."\" class=\"form-control\">";
				$this->db->from('archivio_sistema');
				$this->db->where('id_account', $grupposelezionato);
				$this->db->order_by('nome_file', 'asc');
				$querySYS=$this->db->get();
				foreach ($querySYS->result() as $rowSYS):
					echo "<option value=\"".$rowSYS->nome_file."\"";
					if ($rowSYS->nome_file==$cvalore)
						echo " selected";
					echo ">".$rowSYS->nome_file;
					echo "</option>";
				endforeach;
				echo "</select>";
			else:
				echo "<input type=\"text\" class=\"form-control form-control\" id=\"".$parametro_id[$xx]."\" name=\"".$parametro_id[$xx]."\" value=\"".$cvalore."\"";
				if ($parametro_modificabile[$xx]==0)
					echo " readonly=\"readonly\"";
				echo ">";
			endif;
			echo "</div></div>";
			
			$xx++;
		}

		echo "<br>";

		echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
		echo "<input type=\"hidden\" name=\"edevice_id\" value=\"".$device_id."\" readonly=\"readonly\">";
		echo "<input type=\"hidden\" name=\"eaccount\" value=\"".$grupposelezionato."\" readonly=\"readonly\">";
		
		echo "<div class=\"text-right\">";
		echo "<input type=\"submit\" class=\"btn btn-primary\" onclick=\"return checkform();\" value=\"Aggiorna configurazione\">";
		echo "</div>";

		echo "</form>";

	endif;


elseif ($operazione=="comando_remoto"):

	$tipologia=$this->strimy_model->account_tipologia($grupposelezionato);
	$utente=$this->flexi_auth->get_user_id();
	$amministratore=$this->strimy_model->utente_tipologia($utente);

	?>
	<script language="Javascript">
		function checkform_comando_remoto() {
			
			var messaggio="";
			var valore=$('input:radio[name=ecomando]:checked').val();
			var device_id=$('#edevice_id').val();
			
			if (confirm("Attenzione! Il comando potrebbe generare comportamenti inaspettati e va eseguito sotto la propria responsabilità. Vuoi proseguire?")) {
				
				var stringa = '';
				stringa=stringa+"ecomando="+valore;
				stringa=stringa+"&edevice_id="+device_id;
				stringa=stringa+"&econferma=ok";
				
				$.ajax({

				  url: "<?php echo base_url(); ?>index.php/strimy/dispositivo_comando_remoto_conferma", 
				  data: stringa,
				  async: false,
				  dataType: 'html',
				  method: 'POST',
				  success: function(risultato) {
						if (risultato=="0") {
							if (valore==0) messaggio="comandi annullati";
								else messaggio="Comando inviato correttamente!";
							messaggio="<br>"+messaggio+"<br><br>";
							$('#form1').html(messaggio);
							$('#form0').hide(100);
							$('#form1').show(100);
						}
						else {
							alert('si è verificato un errore inaspettato:\n'+risultato);
						}
				  }
				});
				return false;
			}
			else
				return false;
		}
	</script>
	<?php

//	echo "Invia comando al dispositivo <b>".$device_id."</b>:<br><br>";
	echo "<div id=\"form0\">";
	echo "<form onsubmit=\"return checkform_comando_remoto();\">";
	
	echo "<div class=\"radio\">";
	echo '<label>
    <input type="radio" name="ecomando" value="1" checked="checked">
    <i class="fa fa-refresh"></i> riavvia dispositivo
	</label>
	</div>';

	if (($amministratore==1) && ($tipologia==0)):
		echo "<div class=\"radio top-buffer\">";
		echo '<label>
		<input type="radio" name="ecomando" value="2">
		<i class="fa fa-file-text-o"></i> invia report dettagliato
		</label>
		</div>';
	endif;

	echo "<div class=\"radio top-buffer\">";
	echo '<label>
    <input type="radio" name="ecomando" value="3">
    <i class="fa fa-download"></i> forza download dei contenuti
	</label>
	</div>';

	echo "<div class=\"radio top-buffer\">";
	echo '<label>
    <input type="radio" name="ecomando" value="4">
    <i class="fa fa-camera"></i> cattura screenshot
	</label>
	</div>';

	echo "<div class=\"radio top-buffer\">";
	echo '<label>
    <input type="radio" name="ecomando" value="0">
    <i class="fa fa-times"></i> annulla comandi pendenti
	</label>
	</div>';

	echo "<div class=\"text-right\">";
	echo "<input type=\"hidden\" name=\"econferma\" value=\"ok\" readonly=\"readonly\">";
	echo "<input type=\"hidden\" name=\"edevice_id\" id=\"edevice_id\" value=\"".$device_id."\" readonly=\"readonly\">";
	echo "<input type=\"submit\" value=\"Conferma\" class=\"btn btn-primary\">";
	echo "</div>";
	
	echo "</form>";
	echo "</div>";
	
	echo "<div id=\"form1\" style=\"display: none; text-align: center; width: 100% ; font-size: 1.2em;\">";
	echo "<br>Comando inviato correttamente!<br><br>";
	echo "</div>";

endif;


?>

