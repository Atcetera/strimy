<?php
class Stampa_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function parse_testo($testo) {

		$testo=str_replace("&euro;", chr(128), $testo);

		$testo=str_replace("à", "&agrave;", $testo);
		$testo=str_replace("è", "&egrave;", $testo);
		$testo=str_replace("é", "&eacute;", $testo);
		$testo=str_replace("ì", "&igrave;", $testo);
		$testo=str_replace("ò", "&ograve;", $testo);
		$testo=str_replace("ù", "&ugrave;", $testo);

		$testo = utf8_decode($testo);

		return $testo;
	
	}

	function stampa_fattura($data) {
		$grupposelezionato=$this->flexi_auth->get_user_group_id();
		// $utente=$this->Utenti_model->utenteselezionato();

	
		$this->db->from('paypal_pagamenti');
		$this->db->where('id', $data['id_fattura']);
		// if ($superamministratore==0)
			$this->db->where('id_account', $grupposelezionato);
		$this->db->where('pagato', 1);
		$queryFT=$this->db->get();
	
		if ($queryFT->num_rows()>0):
			define ('K_PATH_IMAGES', $this->config->item('real_path')."assets/img/");
			define ('PDF_HEADER_LOGO', "logo_strimy_black.png");
			define ('PDF_HEADER_LOGO_WIDTH', 50);
			define ('PDF_HEADER_STRING', $this->config->item('intestazione_2')."\n".$this->config->item('intestazione_3')."\ninfo@strimy.it - www.strimy.it - www.atcetera.it");

			$rowFT=$queryFT->row();
			tcpdf();
			$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			$obj_pdf->SetCreator(PDF_CREATOR);
			$title = $this->config->item('intestazione_1');
			$obj_pdf->SetTitle($title);
			$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
			$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$obj_pdf->setPrintHeader(true);  
			$obj_pdf->setPrintFooter(false); 
			$obj_pdf->SetDefaultMonospacedFont('helvetica');
			$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$obj_pdf->SetFont('helvetica', '', 10);
			$obj_pdf->setFontSubsetting(false);
			$obj_pdf->AddPage();
				// ob_start();
				
				$header="<html><body>";
			//	$header=$header."<img src=\"".$this->config->item('base_url')."/assets/img/icon.png\" style=\"width: 100px;\" />";

				$intestazione="";
				if ($rowFT->ana_nome_cognome!="")
					$intestazione=$intestazione.$rowFT->ana_nome_cognome."\r\n";
				if ($rowFT->ana_ragione_sociale!="")
					$intestazione=$intestazione.$rowFT->ana_ragione_sociale."\n";
				if ($rowFT->ana_indirizzo1!="")
					$intestazione=$intestazione.$rowFT->ana_indirizzo1."\n";
				if ($rowFT->ana_indirizzo2!="")
					$intestazione=$intestazione.$rowFT->ana_indirizzo2."\n";
				if ($rowFT->ana_codice_fiscale!="")
					$intestazione=$intestazione."c.f.: ".$rowFT->ana_codice_fiscale."\n";
				if ($rowFT->ana_partita_iva!="")
					$intestazione=$intestazione."p.iva: ".$rowFT->ana_partita_iva."\n";
				
				
				$contenuto="
				<div style=\"width: 100%; background-color: #ccc; font-weight: bold;\">
				Fattura n. ".$rowFT->codice."/S/".date('y', $rowFT->data_pagamento)." del ".date('d/m/y', $rowFT->data_pagamento)."
				</div>
				<br>
				<hr>
				<table class=\"table\" style=\"width: 100%; border-spacing: 3px;\">
				<tr>
					<td style=\"width: 60%\">".$rowFT->descrizione."</td>
					<td style=\"width: 15%\"></td>
					<td style=\"width: 5%\">€</td>
					<td style=\"width: 20%; text-align: right\">".$rowFT->importo_imponibile."</td>
				</tr>
				
				<tr>
					<td></td>
					<td>iva ".round($rowFT->percentuale_iva)."%</td>
					<td>€</td>
					<td style=\"text-align: right\">".$rowFT->importo_iva."</td>
				</tr>
				
				<tr>
					<td></td>
					<td>totale</td>
					<td>€</td>
					<td style=\"text-align: right; font-weight: bold;\">".$rowFT->importo_totale."</td>
				</tr>
				
				</table>
				
				<hr>";

			$transaction="transazione PayPal n. ".$rowFT->transaction_id." - saldato";

			$footer="</div></body></html>";
			
			$obj_pdf->writeHTML($header, true, false, true, false, '');
			$obj_pdf->Ln(20);
			$obj_pdf->MultiCell(180, 10, $intestazione, 0, 'L', 0, 1, '', '', false, 'M');
			$obj_pdf->Ln(40);
			$obj_pdf->writeHTML($contenuto, true, false, true, false, '');
			$obj_pdf->Ln(10);
			$obj_pdf->writeHTML($transaction, true, false, true, false, '');
			$obj_pdf->writeHTML($footer, true, false, true, false, '');
			$obj_pdf->Output('fattura.pdf', 'I');
			
		endif;
	}


}

?>
