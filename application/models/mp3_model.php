<?php

class Mp3_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function importa_dati_classifica($data=Array()) {
    
		$grupposelezionato=$this->strimy_model->accountselezionato();
    
		/*
		if (!isset($data['aggiorna']))
			$data['aggiorna']=0;
    
		$dataRES=Array();
		*/
		
		$this->db->from('archivio_radio_classifica');
		$this->db->where('id_account', $grupposelezionato);
		$this->db->order_by('punteggio', 'desc');
		$query=$this->db->get();
		
		// $xx=0;
		// $zz=0;
		$ultimo_aggiornamento=0;
		
		// azzera le statistiche presenti
		$dataRESET['punteggio']=0;
		$this->db->where('id_account', $grupposelezionato);
		$this->db->where('tipologia_file', 0);
		$this->db->update('archivio_radio', $dataRESET);
		
		foreach ($query->result() as $row):
			
			$this->db->from('archivio_radio');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('tipologia_file', 0);
			$this->db->like('artista', $row->artista);
			$this->db->like('titolo', $row->titolo);
			$queryCK=$this->db->get();
			
			$presente=0;
			$nome_file="";
			
			if ($queryCK->num_rows()>0):
				// il file esiste nell'archivio: aggiorna i dati
				
				$presente=1;
				$rowCK=$queryCK->row();
				$nome_file=$rowCK->nome_file;
				
				
				$dataPUNTEGGIO['punteggio']=$row->punteggio;
				$dataPUNTEGGIO['data_classifica']=$row->data_classifica;
				$this->db->where('id', $rowCK->id);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->update('archivio_radio', $dataPUNTEGGIO);
			
			else:
				// il file non esiste: inseriscilo in archivio
				$dataBRANO['punteggio']=$row->punteggio;
				$dataBRANO['data_classifica']=$row->data_classifica;
				$dataBRANO['artista']=$row->artista;
				$dataBRANO['titolo']=$row->titolo;
				$dataBRANO['tipologia_file']=0;
				$dataBRANO['id_account']=$grupposelezionato;
				$this->db->insert('archivio_radio', $dataBRANO);
		
			endif;
			
		endforeach;

		
		// dopo aver terminato l'importazione, elimina dall'archivio i brani senza punteggio e senza file MP3 associato
		$this->db->where('nome_file', '');
		$this->db->where('punteggio', 0);
		$this->db->where('tipologia_file', 0);
		$this->db->delete('archivio_radio');
		
	}

	
	function mp3_archivio_recupera($data=Array()) {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		if (!isset($data['tipologia_file']))
			$data['tipologia_file']="audio";
		
		if ($data['tipologia_file']=="audio")
			$tipo_file=0;
		elseif ($data['tipologia_file']=="video")
			$tipo_file=1;
    
		if (!isset($data['aggiorna']))
			$data['aggiorna']=0;
		if (!isset($data['esiste_file']))
			$data['esiste_file']=0;
		if (!isset($data['jingle']))
			$data['jingle']=1;
		if (!isset($data['ordine']))
			$data['ordine']="punteggio";
    
		$dataRES=Array();
    
    
		$this->db->from('archivio_radio');
		$this->db->where('id_account', $grupposelezionato);
		$this->db->where('tipologia_file', $tipo_file);
	
		if ($data['esiste_file']==1)
			$this->db->where('nome_file !=', '');
		if ($data['jingle']==0)
			$this->db->where('jingle', 0);
		$this->db->order_by('jingle', 'asc');
		if ($data['ordine']=="alfabetico"):
			$this->db->order_by('artista', 'asc');
			$this->db->order_by('titolo', 'asc');
		else:
			$this->db->order_by('punteggio', 'desc');
			$this->db->order_by('jingle_categoria', 'asc');
		endif;
		$query=$this->db->get();
		
		$xx=0;
		foreach ($query->result() as $row):
			if ($row->nome_file=="")
				$presente=0;
			else
				$presente=1;
				
			$punteggio=$row->punteggio;
			if (round($punteggio*1000)==0)
				$punteggio=0;
			
			$chiave_testo_temp=$this->config->item('chiave');
			$chiave_testo_temp=$chiave_testo_temp[$row->chiave];
			$chiave_testo_temp=explode(" ", $chiave_testo_temp);
			
			$dataRES['id_file'][]=$row->id;
			$dataRES['jingle'][]=$row->jingle;
			$dataRES['jingle_categoria'][]=$row->jingle_categoria;
			$dataRES['titolo'][]=$row->titolo;
			$dataRES['artista'][]=$row->artista;
			$dataRES['punteggio'][]=$punteggio;
			$dataRES['rotazione'][]=$row->rotazione;
			$dataRES['bpm'][]=$row->bpm;
			$dataRES['chiave'][]=$row->chiave;
			$dataRES['chiave_testo'][]=$chiave_testo_temp[0];
			$dataRES['power'][]=$row->power;
			$dataRES['disattivo'][]=$row->disattivo;
		//	$dataRES['anno'][]="20".$row->anno;
			$dataRES['data_classifica'][]=date('d/m/y', $row->data_classifica);
			$dataRES['data_classifica_raw'][]=$row->data_classifica;
			$dataRES['data_inserimento'][]=$row->data_inserimento;
			$dataRES['data_modifica'][]=$row->data_modifica;
			$dataRES['file'][]=$row->nome_file;
			$dataRES['genere_1'][]=$row->genere_1;
			$dataRES['genere_2'][]=$row->genere_2;
			$dataRES['genere_3'][]=$row->genere_3;
			$dataRES['genere_4'][]=$row->genere_4;
			$dataRES['tipologia_file'][]=$row->tipologia_file;
			$dataRES['presente'][]=$presente;
			$xx++;
		endforeach;
		$numero_risultati=$xx;
		
		$dataRES['numero_risultati']=$numero_risultati;
		return $dataRES;
		
	}
	
	function gestione_file_mp3_cancella($data){
		$grupposelezionato=$data['id_account'];
		$id_file=$data['id_file'];

		$percorso_base=$this->config->item('real_path');
		$percorso_users=$this->config->item('percorso_users');

		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
		$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/radio";
		
		$this->db->from('archivio_radio');
		$this->db->where('id', $id_file);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		if ($query->num_rows()>0):
			$row=$query->row();
			$nome_file=$row->nome_file;
	
			$dataFUP['file_da_cancellare']=$row->nome_file;
			$dataFUP['cartella']="radio";
			$dataFUP['grupposelezionato']=$grupposelezionato;
			$this->strimy_model->file_cancella($dataFUP);
			
			$dataBRN['nome_file']="";
			$this->db->where('id', $id_file);
			$this->db->update('archivio_radio', $dataBRN);
			
			$this->db->where('id_file', $id_file);
			// $this->db->where('id_account', $grupposelezionato);
			$this->db->delete('playlist_elementi');
			
		endif;
		
	}
	
	
	function mp3_archivio_inserisci($data) {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$nome_file=$data['nome_file'];
		
		$filetemp=explode("-", $nome_file);
		if (!isset($filetemp[0])) $filetemp[0]="";
		if (!isset($filetemp[1])) $filetemp[1]="";
			
		$artista=$filetemp[0];
		$titolo="";
		
		if ($filetemp[1]!=""):
			
			$filetemp1=explode(".", $filetemp[1]);
			array_pop($filetemp1);
			$ff=0;
			foreach ($filetemp1 as $filetemp1_temp):
				if ($ff>0)
					$titolo=$titolo.".";
				$titolo=$titolo.$filetemp1_temp;
				$ff++;
			endforeach;
			
		endif;
		
		$file_nomefile=trim(preg_replace("/[^a-z0-9\.]/", "_", strtolower($nome_file)));
		
		$this->db->from('archivio_radio');
		$this->db->where('nome_file', $nome_file);
		$query=$this->db->get();
		if ($query->num_rows()==0):	// il file non è presente in archivio: inseriscilo
			$dataFILE['nome_file']=$nome_file;
			$dataFILE['artista']=$artista;
			$dataFILE['titolo']=$titolo;
			$dataFILE['id_account']=$grupposelezionato;
			$this->db->insert('archivio_mp3', $dataFILE);
			
			$id_file=$this->db->insert_id();
		endif;

		return $id_file;
		
	}	

	
	function playlist_casuale($dataIMP) {

		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$limite_brani=$dataIMP['limite_brani'];
		
		$peso[0]=5;
		$peso[1]=2;
		$peso[2]=1;
		
		$this->db->select('archivio_radio.punteggio as punteggio, archivio_radio.nome_file as nome_file, archivio_radio.id as id, archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.chiave as chiave, archivio_radio.power as power');
		$this->db->from('archivio_radio');
		// $this->db->join('archivio_mp3', 'archivio_mp3.id=archivio_mp3_classifica.id_archivio_mp3');
		$this->db->where('archivio_radio.id_account', $grupposelezionato);
		$this->db->where('archivio_radio.nome_file !=', '');
		$this->db->where('archivio_radio.jingle', 0);
		$this->db->where('archivio_radio.disattivo', 0);
		$this->db->order_by('archivio_radio.punteggio', 'desc');
		$query=$this->db->get();
		$xx=0;
		$zz=0;
		
		$max_peso=0;
		$step_totali=count($peso);
		$totali=$query->num_rows();
		$step=round($totali/$step_totali);
		
		$step_attuale=1;
		
		foreach ($query->result() as $row):
			
			$check=$step_attuale*$step;
			
			if ($xx==$check):	// ho raggiunto uno step
				$totale_brani_step[$step_attuale]=$zz;
				$zz=0;
				if ($max_peso<$peso[($step_attuale-1)]):
					$max_peso=$peso[($step_attuale-1)];
				endif;
				$step_attuale++;
			endif;

			
			$brano_id_temp[$step_attuale][$zz]=$row->id;
			$brano_artista_temp[$row->id]=$row->artista;
			$brano_titolo_temp[$row->id]=$row->titolo;
			$brano_punteggio_temp[$row->id]=$row->punteggio;
			$brano_nome_file_temp[$row->id]=$row->nome_file;
			$brano_chiave_temp[$row->id]=$row->chiave;
			$brano_power_temp[$row->id]=$row->power;
			
			$zz++;
			$xx++;
		endforeach;
		
		$totale_brani=$xx;
		
		
		if ($totale_brani>0):
		
			$xx=1;
			while ($xx<($step_totali+1)) {
				if (!isset($brano_id_temp[$xx]))
					$brano_id_temp[$xx]=Array();
				shuffle($brano_id_temp[$xx]);
				$xx++;
			}
			
			$brani_definitivi=Array();

			$totale_brani=count($brani_definitivi);
			
			$xx=1;
			while ($xx<($step_totali+1)) {
				$yy=$step_totali-$xx+1;
				$peso_step=$peso[($yy-1)];
				$zz=0;
				
				$totale_brani_step=count($brano_id_temp[$yy]);
				
				$array_step=ceil(count($brani_definitivi)/$peso_step);

	//			echo "Ciclo: ".$yy." Peso: ".$peso_step." Numero elementi playlist attuale: ".count($brani_definitivi)." Intervallo: ".$array_step." ";

				$jj=0;
				$brani_temp[$jj]=$brani_definitivi;
				while ($jj<$peso_step) {
					$limit_inf=$jj*$array_step;
					if ( ($limit_inf+$array_step+1)>count($brani_definitivi) )
						$brani_temp[$jj]=array_slice($brani_definitivi, $limit_inf);
					else
						$brani_temp[$jj]=array_slice($brani_definitivi, $limit_inf, $array_step);
						
				//	echo "<br>Elementi nell'intervallo ".$jj.": ".count($brani_temp[$jj])." (inizio da ".$limit_inf.")";
					$jj++;
				}
					
				$jj=0;
				$brani_temporanei=Array();
				while ($jj<$peso_step) {
					$brani_temp[$jj]=array_merge($brani_temp[$jj], $brano_id_temp[$yy]);
					$brani_temporanei=array_merge($brani_temporanei, $brani_temp[$jj]);
					$jj++;
				}

				$brani_definitivi=$brani_temporanei;
			
				$xx++;
			}
			

			$totale_brani=count($brani_definitivi);
			$brani_definitivi_2=array();

			// divide la playlist totale in un numero di sottogruppi pare al numero di peso massimo*2, e rimescola i rispettivi elementi
			$totale_gruppi=$max_peso*2;
			$numero_elementi_gruppo=ceil(count($brani_definitivi)/$totale_gruppi);
			// echo $totale_brani." ".$numero_elementi_gruppo." ".$totale_gruppi."<br>";
			
			$totale_brani=count($brani_definitivi);
			$xx=0;
			
			while ($xx<$totale_gruppi) {
				$limite_inf=$xx*$numero_elementi_gruppo;
				if ((count($brani_definitivi_2)+$numero_elementi_gruppo)>$brani_definitivi)
					$brani_gruppo[$xx]=array_slice($brani_definitivi, $limite_inf);
				else
					$brani_gruppo[$xx]=array_slice($brani_definitivi, $limite_inf, $numero_elementi_gruppo);
					
				shuffle($brani_gruppo[$xx]);
				
				$brani_definitivi_2=array_merge($brani_definitivi_2, $brani_gruppo[$xx]);
				$xx++;
			}
			
			$brano_id_temp=$brani_definitivi_2;
			
			$totale_brani=count($brano_id_temp);
			
			if (($limite_brani==0) || ($limite_brani>$totale_brani))
				$limite_brani=$totale_brani;
			
			$xx=0;
			while ($xx<$limite_brani) {
				$idtemp=$brano_id_temp[$xx];
				// echo ($xx+1).". ".$brano_artista[$idtemp]." ".$brano_titolo[$idtemp]." (".$brano_punteggio[$idtemp].")";

				$brano_id[$xx]=$brano_id_temp[$xx];
				$brano_artista[$xx]=$brano_artista_temp[$idtemp];
				$brano_titolo[$xx]=$brano_titolo_temp[$idtemp];
				$brano_punteggio[$xx]=$brano_punteggio_temp[$idtemp];
				$brano_nome_file[$xx]=$brano_nome_file_temp[$idtemp];
				$brano_chiave[$xx]=$brano_chiave_temp[$idtemp];
				$brano_power[$xx]=$brano_power_temp[$idtemp];
				
				$xx++;
			}
		
		else:
			$brano_id=Array();
			$brano_artista=Array();
			$brano_titolo=Array();
			$brano_punteggio=Array();
			$brano_nome_file=Array();
			$brano_chiave=Array();
			$brano_power=Array();
		
		endif;
	
		$dataRET['brano_id']=$brano_id;
		$dataRET['brano_artista']=$brano_artista;
		$dataRET['brano_titolo']=$brano_titolo;
		$dataRET['brano_punteggio']=$brano_punteggio;
		$dataRET['brano_nome_file']=$brano_nome_file;
		
		return $dataRET;
				
	}

	function inserisci_jingle($data) {  // inserisce i jingle nella playlist data, secondo l'intervallo specificato
	
		$file_id=$data['id_file'];
		$elemento_file=$data['file'];
	//	$descrizione=$data['descrizione_file'];
		$jingle_categoria=$data['jingle_categoria'];
		$intervallo=$data['jingle_intervallo'];
		
	//	$grupposelezionato=$this->strimy_model->accountselezionato();
		$this->db->from('archivio_radio');
	//	$this->db->where('id_account', $grupposelezionato);
		$this->db->where('jingle_categoria', $jingle_categoria);
		$this->db->where('disattivo', 0);
		$this->db->where('jingle', 1);
		$query=$this->db->get();
		
		$xx=0;
		foreach ($query->result() as $row):
			$idtemp=$row->id;
			$jingle_id[$xx]=$idtemp;
			$jingle_nome_file[$idtemp]=$row->nome_file;
			$xx++;
		endforeach;

		$numero_jingle=count($jingle_id);
		$numero_elementi=count($file_id);
		
		$jingle_id1=Array();
		
		$intervallo_temp=0;
		$xx=0;
		$yy=0;
		while ($xx<$numero_elementi) {
			if (($numero_jingle>0) && ($xx>0) && (($xx/$intervallo)==round($xx/$intervallo)) ):
				// viene creata una copia dell'array $jingle_id, con nome $jingle_id1
				// da cui verranno estratti casualmente i jingle da inserire nella playlist;
				// quando tutti i jingle sono stati utilizzati, e quindi l'array è vuoto, viene creata una nuova copia;
				// questa routine permette di riprodurre tutti i jingle almeno una volta, senza ripetizioni
				if (count($jingle_id1)==0):
					$jingle_id1=$jingle_id;
					// echo "ocio!!<br>";
				endif;
				// inserisce un jingle nella posizione corrente
				$jingle_rand = array_rand($jingle_id1, 1);
				$jingle_rand=$jingle_id1[$jingle_rand];
				unset($jingle_id1[array_search($jingle_rand, $jingle_id1)]);
				$nuovo_file_id[$yy]=$jingle_rand;
				$nuovo_elemento_file[$yy]=$jingle_nome_file[$jingle_rand];
				$yy++;
			endif;

			$nuovo_file_id[$yy]=$file_id[$xx];
			$nuovo_elemento_file[$yy]=$elemento_file[$xx];
			$xx++;
			$yy++;
		}
		
		$numero_elementi=count($nuovo_file_id);
		
		
		$dataRET['id_file']=$nuovo_file_id;
		$dataRET['file']=$nuovo_elemento_file;
		$dataRET['numero_elementi']=$numero_elementi;
		
		return $dataRET;
		

	}



	/*
	function radio_magic_genera_old($dataIMP) {
		
		if (!isset($dataIMP['limite_brani']))
			$dataIMP['limite_brani']=0;
		if (!isset($dataIMP['sequenza']))
			$dataIMP['sequenza']=0;
		if (!isset($dataIMP['giorni']))
			$dataIMP['giorni']=1;
		if (!isset($dataIMP['criterio_ordinamento']))
			$dataIMP['criterio_ordinamento']="power";  // power/bpm

		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$limite_brani=$dataIMP['limite_brani'];
		
		$peso[0]=4;
		$peso[1]=2;
		$peso[2]=1;
		$peso[3]=0.5;
		$sequenza=$dataIMP['sequenza'];
		$giorni=$dataIMP['giorni'];
		$criterio_ordinamento=$dataIMP['criterio_ordinamento']; 
		
		
		$step_totali=count($peso);
		
		$xx=0;
		while ($xx<$step_totali) {
			$peso[$xx]=$peso[$xx]*$giorni;
			$xx++;
		}
		
		
		// recupera i brani in classifica
		$this->db->select('archivio_radio.punteggio as punteggio, archivio_radio.nome_file as nome_file, archivio_radio.id as id, archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.bpm as bpm, archivio_radio.power as power, archivio_radio.chiave as chiave');
		$this->db->from('archivio_radio');
		$this->db->where('archivio_radio.id_account', $grupposelezionato);
		$this->db->where('archivio_radio.nome_file !=', '');
		$this->db->where('archivio_radio.jingle', 0);
		$this->db->where('archivio_radio.disattivo', 0);
		$this->db->where('archivio_radio.punteggio !=', 0);
		if (isset($dataIMP['power']))
			$this->db->where_in('archivio_radio.power', $dataIMP['power']);
		$this->db->order_by('archivio_radio.punteggio', 'desc');
		$query=$this->db->get();
		$xx=0;
		$zz=0;
		
		$max_peso=0;
		$totali=$query->num_rows();
		$step=ceil($totali/($step_totali-1));
		//$step_round=round($totali/($step_totali-1));
		
		$step_attuale=1;
		
		foreach ($query->result() as $row):
			
		//	echo $step_attuale."<br>";
			
			$check=$step_attuale*$step;
			
			if ($xx==$check):	// ho raggiunto uno step
				$totale_brani_step[$step_attuale]=$zz;
				$zz=0;
				if ($max_peso<$peso[($step_attuale-1)]):
					$max_peso=$peso[($step_attuale-1)];
				endif;
				$step_attuale++;
			endif;
			
			$brano_id_temp[$step_attuale][$zz]=$row->id;
			$brano_artista_temp[$row->id]=$row->artista;
			$brano_titolo_temp[$row->id]=$row->titolo;
			$brano_bpm_temp[$row->id]=$row->bpm;
			$brano_power_temp[$row->id]=$row->power;
			$brano_chiave_temp[$row->id]=$row->chiave;
			$brano_punteggio_temp[$row->id]=$row->punteggio;
			$brano_nome_file_temp[$row->id]=$row->nome_file;
			
			if (!isset($peso[($step_attuale-1)])) $peso[($step_attuale-1)]=0;
			$brano_peso[$row->id]=$peso[($step_attuale-1)];
			
			$zz++;
			$xx++;
		endforeach;
		
		$totale_brani=$xx;

		$step_attuale++;
		$zz=0;
		// recupera i brani esterni alla classifica (quelli con punteggio pari a 0)
		$this->db->select('archivio_radio.punteggio as punteggio, archivio_radio.nome_file as nome_file, archivio_radio.id as id, archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.bpm as bpm, archivio_radio.power as power, archivio_radio.chiave as chiave');
		$this->db->from('archivio_radio');
		$this->db->where('archivio_radio.id_account', $grupposelezionato);
		$this->db->where('archivio_radio.nome_file !=', '');
		$this->db->where('archivio_radio.jingle', 0);
		$this->db->where('archivio_radio.disattivo', 0);
		$this->db->where('archivio_radio.punteggio', 0);
		if (isset($dataIMP['power']))
			$this->db->where_in('archivio_radio.power', $dataIMP['power']);
		$this->db->order_by('archivio_radio.punteggio', 'random');
		$this->db->limit($step);
		$query2=$this->db->get();
		foreach ($query2->result() as $row2):
			$brano_id_temp[$step_attuale][$zz]=$row2->id;
			$brano_artista_temp[$row2->id]=$row2->artista;
			$brano_titolo_temp[$row2->id]=$row2->titolo;
			$brano_bpm_temp[$row2->id]=$row2->bpm;
			$brano_power_temp[$row2->id]=$row2->power;
			$brano_chiave_temp[$row2->id]=$row2->chiave;
			$brano_punteggio_temp[$row2->id]=$row2->punteggio;
			$brano_nome_file_temp[$row2->id]=$row2->nome_file;
			$brano_peso[$row2->id]=$peso[($step_attuale-1)];
			$zz++;
			$xx++;
		endforeach;		
		$totale_brani=$totale_brani+$xx;
		
		

		// procede ad elaborare i brani

		if ($totale_brani>0):
		
			$xx=1;
			while ($xx<($step_totali+1)) {
				if (!isset($brano_id_temp[$xx]))
					$brano_id_temp[$xx]=Array();
				shuffle($brano_id_temp[$xx]);
				$xx++;
			}
			
			$brani_definitivi=Array();

			$totale_brani=count($brani_definitivi);
			
			$xx=1;
			while ($xx<($step_totali+1)) {
				$yy=$step_totali-$xx+1;
				$peso_step=$peso[($yy-1)];
				$zz=0;
				
				$totale_brani_step=count($brano_id_temp[$yy]);
				
				if ($peso_step>1)
					$array_step=round(count($brani_definitivi)/$peso_step);
				else
					$array_step=ceil(count($brani_definitivi)/$peso_step);

				$jj=0;
				$brani_temp[$jj]=$brani_definitivi;
				while ($jj<$peso_step) {
					$limit_inf=$jj*$array_step;
					if ( ($limit_inf+$array_step+1)>count($brani_definitivi) )
						$brani_temp[$jj]=array_slice($brani_definitivi, $limit_inf);
					else
						$brani_temp[$jj]=array_slice($brani_definitivi, $limit_inf, $array_step);

					$jj++;
				}
					
				$jj=0;
				$brani_temporanei=Array();
				while ($jj<$peso_step) {
					$brani_temp[$jj]=array_merge($brani_temp[$jj], $brano_id_temp[$yy]);
					$brani_temporanei=array_merge($brani_temporanei, $brani_temp[$jj]);
					$jj++;
				}

				$brani_definitivi=$brani_temporanei;
			
				$xx++;
			}
			

			$totale_brani=count($brani_definitivi);
			$brani_definitivi_2=array();

			// divide la playlist totale in un numero di sottogruppi pare al numero di peso massimo*2, e rimescola i rispettivi elementi
			$totale_gruppi=$max_peso*2;
			$numero_elementi_gruppo=ceil(count($brani_definitivi)/$totale_gruppi);
			// echo $totale_brani." ".$numero_elementi_gruppo." ".$totale_gruppi."<br>";
			
			$totale_brani=count($brani_definitivi);
			$xx=0;
			
			while ($xx<$totale_gruppi) {
				$limite_inf=$xx*$numero_elementi_gruppo;
				if ((count($brani_definitivi_2)+$numero_elementi_gruppo)>$brani_definitivi)
					$brani_gruppo[$xx]=array_slice($brani_definitivi, $limite_inf);
				else
					$brani_gruppo[$xx]=array_slice($brani_definitivi, $limite_inf, $numero_elementi_gruppo);
					
				shuffle($brani_gruppo[$xx]);
				
				$brani_definitivi_2=array_merge($brani_definitivi_2, $brani_gruppo[$xx]);
				$xx++;
			}
			
			$brano_id_temp=$brani_definitivi_2;
			

			if ($sequenza>0):
				// divisione playlist per segmenti e riordino secondo il parametro indicato
				unset($brani_definitivi);
				unset($brani_definitivi_2);	
				$brani_definitivi=Array();
				$xx=0;
				$zz=0;
				$step_sequenza=1;
				while ($xx<$totale_brani) {
					$brani_definitivi_2[$zz]=$brano_id_temp[$xx];
					
					if ($criterio_ordinamento=="power")
						$criterio[$zz]=$brano_power_temp[$brano_id_temp[$xx]];
					elseif ($criterio_ordinamento=="bpm")
						$criterio[$zz]=$brano_bpm_temp[$brano_id_temp[$xx]];
						
					if (($zz==($sequenza-1)) || ($xx==($totale_brani-1))):
						if (($step_sequenza/2)==round($step_sequenza/2))
							array_multisort($criterio, SORT_DESC, $brani_definitivi_2);
						else
							array_multisort($criterio, SORT_ASC, $brani_definitivi_2);
						$brani_definitivi=array_merge($brani_definitivi, $brani_definitivi_2);
						unset($brani_definitivi_2);
						unset($criterio);
						$zz=-1;
						$step_sequenza++;
					endif;
					$xx++;
					$zz++;
				}
				$brano_id_temp=$brani_definitivi;
			endif;

			$totale_brani=count($brano_id_temp);

			
			
			if (($limite_brani==0) || ($limite_brani>$totale_brani))
				$limite_brani=$totale_brani;
			
			$xx=0;
			while ($xx<$limite_brani) {
				$idtemp=$brano_id_temp[$xx];

				$brano_id[$xx]=$brano_id_temp[$xx];
				$brano_artista[$xx]=$brano_artista_temp[$idtemp];
				$brano_titolo[$xx]=$brano_titolo_temp[$idtemp];
				$brano_bpm[$xx]=$brano_bpm_temp[$idtemp];
				$brano_power[$xx]=$brano_power_temp[$idtemp];
				$brano_chiave[$xx]=$brano_chiave_temp[$idtemp];
				$brano_punteggio[$xx]=$brano_punteggio_temp[$idtemp];
				$brano_nome_file[$xx]=$brano_nome_file_temp[$idtemp];
				$brano_id_elemento[$xx]=0;
				$valore_vuoto[$xx]=0;

			//	echo ($xx+1).". ".$brano_artista[$idtemp]." ".$brano_titolo[$idtemp]." (".$brano_punteggio[$idtemp]."). ".$brano_peso[$idtemp]." <b>".$brano_power[$idtemp]."</b><br>";
				
				$xx++;
			}
		
		else:
			$brano_id=Array();
			$brano_artista=Array();
			$brano_titolo=Array();
			$brano_punteggio=Array();
			$brano_nome_file=Array();
			$brano_bpm=Array();
			$brano_chiave=Array();
			$brano_power=Array();
			$brano_id_elemento=Array();
			$valore_vuoto=Array();
		
		endif;
	
		$dataRET['brano_id']=$brano_id;
		$dataRET['brano_artista']=$brano_artista;
		$dataRET['brano_titolo']=$brano_titolo;
		$dataRET['brano_punteggio']=$brano_punteggio;
		$dataRET['brano_power']=$brano_power;
		$dataRET['brano_bpm']=$brano_bpm;
		$dataRET['brano_chiave']=$brano_chiave;
		$dataRET['brano_nome_file']=$brano_nome_file;
		$dataRET['brano_id_elemento']=$brano_id_elemento;
		$dataRET['valore_vuoto']=$valore_vuoto;
		
		return $dataRET;
		
	}

	*/
	
	function mp3_archivio_check($dataIMP) {
		
		if (!isset($dataIMP['tipologia_file'])) $dataIMP['tipologia_file']=0;
		if (!isset($dataIMP['origine'])) $dataIMP['origine']=1;
		if (!isset($dataIMP['test'])) $dataIMP['test']=0;
		if (!isset($dataIMP['tipo_richiesta'])) $dataIMP['tipo_richiesta']="";

		$grupposelezionato=$this->strimy_model->accountselezionato();

		$numero_brani=Array();
		$xx=0;
		foreach ($dataIMP['numero_brani'] as $numero_brani_temp):
			if ($xx>0)
				$numero_brani_temp=$numero_brani_temp+$numero_brani[($xx-1)];
			$numero_brani[]=$numero_brani_temp;
			$xx++;
		endforeach;
		$dataIMP['numero_brani']=$numero_brani;

		$dataRET=Array();
		$this->db->select('archivio_radio.punteggio as punteggio, archivio_radio.nome_file as nome_file, archivio_radio.id as id, archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.bpm as bpm, archivio_radio.power as power, archivio_radio.chiave as chiave, archivio_radio.durata as durata, archivio_radio.genere_1 as genere_1, archivio_radio.genere_2 as genere_2, archivio_radio.genere_3 as genere_3, archivio_radio.genere_4 as genere_4, archivio_radio.durata as durata');
		$this->db->from('archivio_radio');
		$this->db->where('archivio_radio.id_account', $grupposelezionato);
		$this->db->where('archivio_radio.nome_file !=', '');
		$this->db->where('archivio_radio.jingle', 0);
		$this->db->where('archivio_radio.disattivo', 0);
		$this->db->where('archivio_radio.tipologia_file', $dataIMP['tipologia_file']);
		if ($dataIMP['origine']==1)	// prende solo i brani in classifica
			$this->db->where('archivio_radio.punteggio !=', 0);
		if ($dataIMP['origine']==2) // prende solo i brani fuori classifica
			$this->db->where('archivio_radio.punteggio', 0);
		if ( (isset($dataIMP['power'])) && (count($dataIMP['power'])>0) )
			$this->db->where_in('archivio_radio.power', $dataIMP['power']);
		if ( (isset($dataIMP['genere_1'])) && (count($dataIMP['genere_1'])>0) )
			$this->db->where_in('archivio_radio.genere_1', $dataIMP['genere_1']);
		if ( (isset($dataIMP['genere_2'])) && (count($dataIMP['genere_2'])>0) )
			$this->db->where_in('archivio_radio.genere_2', $dataIMP['genere_2']);
		if ( (isset($dataIMP['genere_3'])) && (count($dataIMP['genere_3'])>0) )
			$this->db->where_in('archivio_radio.genere_3', $dataIMP['genere_3']);
		if ( (isset($dataIMP['genere_4'])) && (count($dataIMP['genere_4'])>0) )
			$this->db->where_in('archivio_radio.genere_4', $dataIMP['genere_4']);
		$this->db->order_by('archivio_radio.punteggio', 'desc');
		$this->db->order_by('archivio_radio.id', 'desc');
		$query=$this->db->get();
		
		if ($dataIMP['tipo_richiesta']=="conteggio_brani"):
			$totale_brani=$query->num_rows();
			$durata=0;
			if ($dataIMP['origine']==2)
				$moltiplicatore=1;
			else
				$moltiplicatore=$dataIMP['ripetizioni_brani'][0];
			$xx=0;
			$peso_attuale=0;
			$nomefile="";
			foreach ($query->result() as $row):
				if ( ($xx==($dataIMP['numero_brani'][$peso_attuale]-1)) && ($dataIMP['origine']!=2) ):
					if (isset($dataIMP['numero_brani'][($peso_attuale+1)]))
						$peso_attuale++;
					$moltiplicatore=$dataIMP['ripetizioni_brani'][$peso_attuale];
				endif;
				// if ($row->punteggio==0) $moltiplicatore=1;
				$durata=$durata+(round($row->durata))*$moltiplicatore;
				$nomefile=$nomefile."-".$row->nome_file;
				$xx++;
			endforeach;
			$dataDUR['durata']=$durata;
			$dataDUR['cumulativo']=1;
			$dataFOR=$this->mp3_model->durata_formattata($dataDUR);
			$durata_formattata=$dataFOR['durata_formattata'];
			if ($durata<3601)
				$durata_formattata="00:".$durata_formattata;

			if ($dataIMP['test']==1)
				$durata_formattata=$durata_formattata." ".$nomefile;

			$dataRET['totale_brani']=$totale_brani;
			$dataRET['durata']=$durata;
			$dataRET['durata_formattata']=$durata_formattata;
		endif;
		
		if ($dataIMP['test']==1):
			echo "Totale brani estratti: ".$query->num_rows()."<br>";
		endif;
		
		$dataRET['query']=$query;

		return $dataRET;
		
	}


	function radio_magic_genera($dataIMP) {
		
		if (!isset($dataIMP['limite_brani']))
			$dataIMP['limite_brani']=0;
		if (!isset($dataIMP['sequenza']))
			$dataIMP['sequenza']=0;
		if (!isset($dataIMP['giorni']))
			$dataIMP['giorni']=1;
		if (!isset($dataIMP['criterio_ordinamento']))
			$dataIMP['criterio_ordinamento']="power";  // power/bpm
		if (!isset($dataIMP['tipologia_file']))
			$dataIMP['tipologia_file']=0;	// 0: audio; 1: video
		if (!isset($dataIMP['test']))
			$dataIMP['test']=0;

		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$limite_brani=$dataIMP['limite_brani'];

		
		if ($dataIMP['test']==1):
			echo "<b> sequenza ";
			if ($dataIMP['tipologia_file']==0)
				echo "audio";
			elseif ($dataIMP['tipologia_file']==1)
				echo "video";
			echo "</b><br><br>";
		endif;
		
		// indica il "peso" dei brani in classifica
		// es. se step[0]=20 e $peso[0]=5
		// e step[1]=50 e $peso[1]=3
		// significa che le prime 20 canzoni in classifica saranno riprodotte 5 volte al giorno
		// mentre le successive 30 (dalla 21 alla 50) saranno riprodotte 3 volte al giorno
		// un valore di step nullo ($step[]=0) indica che non c'è limite, ed è accettato solo come ultimo indice dell'array
		$kk=0;
		foreach ($dataIMP['numero_brani'] as $temp):
			if ($dataIMP['numero_brani'][$kk]==0):
				$dataIMP['numero_brani'][$kk]=1;
				$dataIMP['ripetizioni_brani'][$kk]=0;
			endif;

			$step[$kk]=$dataIMP['numero_brani'][$kk];
			if ($kk>0) $step[$kk]=$step[$kk]+$step[($kk-1)]	;
			$peso[$kk]=$dataIMP['ripetizioni_brani'][$kk];
			
			$kk++;
		endforeach;

		// se si vogliono considerare solo i brani fuori classifica, il criterio del peso e delle ripetizioni non è più valido
		// bisogna azzerare gli array $step e $peso e considerare un intervallo unico
		if ($dataIMP['origine']==2):
			unset($step);
			unset($peso);
			$step[0]=0;
			$peso[0]=1;
		endif;

		// questa variabile indica il numero totale dei brani che dovranno essere ricavati dal database, sulla base degli intervalli impostati
		$totale_minimo_brani=$step[(count($step)-1)];
		
		$sequenza=$dataIMP['sequenza'];
		$giorni=$dataIMP['giorni'];
		$criterio_ordinamento=$dataIMP['criterio_ordinamento']; 
		
		$step_totali=count($peso);
		
		
		$brano0_id_temp=Array();	// questo array conterrà l'id di tutti i brani non inseriti in classifica (a punteggio 0)
		
		$xx=0;
		while ($xx<$step_totali) {
			$peso[$xx]=$peso[$xx]*$giorni;
			$xx++;
		}
		
		$dataQUERY=$this->mp3_archivio_check($dataIMP);
		$query=$dataQUERY['query'];
		$xx=0;
		$zz=0;
		
		$max_peso=1;
		$check=0;
		
		if ($query->num_rows()>0):
		
			foreach ($query->result() as $row):
			
				$step_attuale=$step[$check];
				if (round($row->punteggio)>0):
					$brano_id_temp[$check][$zz]=$row->id; 	// questo array contiene gli id dei brani CON punteggio
					$brano_peso[$check][]=$row->id;	// viene creato un duplicato dell'array con tutti i brani appartenenti al gruppo, da cui andare a individuare i brani da eliminare in caso di durata eccessiva
				else:
					$brano_id_temp[$check][$zz]=0;
					$brano0_id_temp[]=$row->id;	// questo array contiene gli id dei brani SENZA punteggio
				endif;
				$brano_artista_temp[$row->id]=$row->artista;
				$brano_titolo_temp[$row->id]=$row->titolo;
				$brano_bpm_temp[$row->id]=$row->bpm;
				$brano_power_temp[$row->id]=$row->power;
				$brano_chiave_temp[$row->id]=$row->chiave;
				$brano_punteggio_temp[$row->id]=$row->punteggio;
				$brano_nome_file_temp[$row->id]=$row->nome_file;
				$brano_durata_temp[$row->id]=$row->durata;
				$brano_ripetizioni_temp[$row->id]=$peso[$check];
				$brano_peso_temp[$row->id]=$check;
				$brano_durata_temp[$row->id]=$row->durata;
				
				
				if ($dataIMP['test']==1):
					echo "Peso ".$check.": ";
					echo $brano_artista_temp[$row->id]." ".$brano_titolo_temp[$row->id]."<br>";
				endif;
				
				if ($max_peso<$peso[$check])
						$max_peso=$peso[$check];
				
				if ( ($xx==($step_attuale-1)) && (isset($step[($check+1)])) && (($xx<($totale_minimo_brani-1)) || ($totale_minimo_brani==0)) ):
					$check++;
					if ($dataIMP['test']==1):
						echo "<br>";
					endif;
				endif;
				
				$zz++;
				$xx++;
				
				// se viene raggiunto il totale dei brani impostati in fase di generazione esce dal ciclo
				if ($xx==$totale_minimo_brani)
					break;
				
			endforeach;

			if ($dataIMP['test']==1):
				echo "<br><br>".$this->db->last_query();
				echo "<br><br>";
				echo "Totale brani: ".$xx."<br>";
				echo "da classifica: ".($xx-count($brano0_id_temp))."<br>";
				echo "fuori classifica: ".count($brano0_id_temp)."<br>";
				echo "brani impostati: ".$limite_brani."<br>";
				echo "<br><br>";
			endif;
			
			if ($totale_minimo_brani==0)
				$totale_minimo_brani=$xx;
			
			// se il numero di brani in archivio non arriva al limite minimo specificato da $totale_minimo_brani,
			// vengono create delle posizioni vuote che andranno poi riempite dai brani non in classifica
			while (($xx<$totale_minimo_brani) && ($dataIMP['origine']!=1)) {
				$brano_id_temp[$check][$zz]=0;
				$zz++;
				$xx++;
			}
			
			
			$totale_brani=$xx;
			$step_totali=$check+1;

			// divide a metà i singoli sottogruppi di brani (aggregati per peso), partendo dall'ultimo gruppo,
			// e inserisce al suo interno i brani del gruppo precedente, partendo dal penultimo, e procede così
			// fino ad arrivare al gruppo 0; in questo modo viene garantita una distribuzione uniforme dei brani
		
		else:
			$totale_brani=0;
		
		endif;

		if ($totale_brani>0):
		
			// mescola i brani all'interno dei singoli gruppi
			$xx=0;
			while ($xx<$step_totali) {
				if (!isset($brano_id_temp[$xx]))
					$brano_id_temp[$xx]=Array();
				shuffle($brano_id_temp[$xx]);
				$xx++;
			}
			
			$brani_definitivi=Array();

			$totale_brani=count($brani_definitivi);
			
			$xx=0;
			while ($xx<$step_totali) {
				$yy=$step_totali-$xx-1;
				$peso_step=$peso[$yy];
				
				if ($peso_step>0):
				
					$totale_brani_step=count($brano_id_temp[$yy]);
					
					if ($peso_step>1)
						$array_step=round(count($brani_definitivi)/$peso_step);
					else
						$array_step=ceil(count($brani_definitivi)/$peso_step);

					$jj=0;
					$brani_temp[$jj]=$brani_definitivi;
					while ($jj<$peso_step) {
						$limit_inf=$jj*$array_step;
						if ( ($limit_inf+$array_step+1)>count($brani_definitivi) )
							$brani_temp[$jj]=array_slice($brani_definitivi, $limit_inf);
						else
							$brani_temp[$jj]=array_slice($brani_definitivi, $limit_inf, $array_step);

						$jj++;
					}
						
					$jj=0;
					$brani_temporanei=Array();
					while ($jj<$peso_step) {
						$brani_temp[$jj]=array_merge($brani_temp[$jj], $brano_id_temp[$yy]);
						$brani_temporanei=array_merge($brani_temporanei, $brani_temp[$jj]);
						$jj++;
					}

					$brani_definitivi=$brani_temporanei;
				
				endif;
			
				$xx++;
				
				
			}
			

			$totale_brani=count($brani_definitivi);
			$brano0_id_temp1=Array();



			// echo "Totale ora: ".$totale_brani."<br>";


			// va a riempire le posizioni vuote con i brani non presenti in classifica, estratti in maniera casuale dall'array $brano0_id_temp
			$xx=0;
			$zz=0;
			while ($xx<$totale_brani) {
				if ($brani_definitivi[$xx]==0): // questa posizione è vuota
					if (count($brano0_id_temp1)==0)
						$brano0_id_temp1=$brano0_id_temp;	// crea un duplicato dell'array $brano0_id_temp da cui estrarre casualmente i brani, fino ad esaurimento; quando l'array è vuoto viene creato un nuovo duplicato
					$brano0_temporaneo = array_rand($brano0_id_temp1, 1);
					$brani_definitivi[$xx]=$brano0_id_temp1[$brano0_temporaneo];
					unset($brano0_id_temp1[$brano0_temporaneo]);
					$zz++;
				endif;
				$xx++;
			}
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Le routine a seguire servono a calcolare la durata complessiva della playlist generata, per evitare che sia superiore alla fascia oraria
			// decisa dall'utente; non è possibile infatti eliminare semplicemente i brani in eccesso perché ciò causerebbe una loro errata distribuzione;
			// i brani vanno invece eliminati in maniera uniforme lungo tutta la durata della playlist, fino a rientrare nei limiti di durata desiderata.
			//
			// Non è possibile invece ottenere il risultato contrario, ovvero l'estensione di una sequenza troppo corta, perché questo è conseguenza
			// di filtri troppo restrittivi in fase di generazione da parte dell'utente
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			$brani_definitivi_2=array();

			// la playlist totale è generata; se il tempo totale dei brani è superiore all'intervallo di esecuzione dovranno essere eliminati,
			// uno alla volta, i brani in eccesso, partendo da quelli con punteggio pari a 0, fino a rientrare nell'intervallo.
			// in questo modo viene garantito il numero delle riproduzioni dei brani con punteggio più elevato a discapito degli altri,
			// che servono solo da riempitivo
			
			$durata_intervallo=$dataIMP['fine_sequenza']-$dataIMP['inizio_sequenza'];
			$durata_totale=0;
			$brani_senza_punteggio=Array();
			$xx=0;
			if ($dataIMP['test']==1)
				echo "<br><br>Playlist preliminare, da cui andranno eliminati i brani in eccesso:<br>";
			foreach ($brani_definitivi as $brano_temp):
				$durata_totale=$durata_totale+$brano_durata_temp[$brano_temp];
				if ($brano_punteggio_temp[$brano_temp]==0)
					$brani_senza_punteggio[]=$xx;
				if ($dataIMP['test']==1)
					echo ($xx+1)." ".$brano_temp." ".$brano_titolo_temp[$brano_temp]." ".$brano_peso_temp[$brano_temp]." ".$brano_punteggio_temp[$brano_temp]."<br>";
				$xx++;
			endforeach;
			
			// elimina i brani in eccesso, partendo da quelli con punteggio 0
			
			shuffle($brani_senza_punteggio);
			
			while (($durata_totale>$durata_intervallo) && (count($brani_senza_punteggio)>0)) {
				
					$brano0_temporaneo=$brani_senza_punteggio[(count($brani_senza_punteggio)-1)];
					unset($brani_senza_punteggio[(count($brani_senza_punteggio)-1)]);
					$id_brano_temporaneo=$brani_definitivi[$brano0_temporaneo];
					
					// $array_duplicati=array_keys($brani_definitivi, $id_brano_temporaneo);
					
					if (($durata_totale-$brano_durata_temp[$id_brano_temporaneo])>$durata_intervallo):
						unset($brani_definitivi[$brano0_temporaneo]);
						$durata_totale=$durata_totale-$brano_durata_temp[$id_brano_temporaneo];
					
						if ($dataIMP['test']==1):
							echo "durata totale: ".$durata_totale." su ".$durata_intervallo." ".count($brani_senza_punteggio)." su ".count($brani_definitivi)." ".$brano0_temporaneo." ".$id_brano_temporaneo."<br>";
						endif;
						
					else:
						if ($dataIMP['test']==1):
							echo "Forzo l'uscita dal ciclo<br>";
						endif;
					
						break;
					endif;
			}

			if ($dataIMP['test']==1)
				echo "Brani a questo punto: ".count($brani_definitivi)."!<br>";



			// duplica gli array dei brani suddivisi per peso, rimescolandoli
			// da questi array verranno estratti, uno alla volta e in modo casuale, gli id dei brani da eliminare
			$xx=0;
			while ($xx<(count($peso))) {
				$brano0_peso[$xx]=Array();
				$yy=0;
				while($yy<$peso[$xx]) {
					shuffle($brano_peso[$xx]);
					$brano0_peso[$xx]=array_merge($brano0_peso[$xx], $brano_peso[$xx]);
					if ($dataIMP['test']==1):
					//	echo $xx." ".$peso[$xx]." peso totale: ".count($brano0_peso[$xx])."<br>";
					endif;
					$yy++;
				}
				

				$xx++;
			}


			// se ci sono ancora brani in eccesso, li elimina cominciando da quelli con punteggio più basso
			$xx=0;
			$ultimo_peso=count($peso)-1;
			$id_brano_temporaneo=0;
			$brano_durata_temp[$id_brano_temporaneo]=0;
			while ( (($durata_totale-$brano_durata_temp[$id_brano_temporaneo])>$durata_intervallo) && ($dataIMP['origine']!=2) ) {
				
				$brani_totali_temp=0;
				foreach ($brano0_peso as $brano0_peso_temp):
					$brani_totali_temp=$brani_totali_temp+count($brano0_peso_temp);
				endforeach;
				
				if ($dataIMP['test']==1)
					echo $brani_totali_temp."<br>";
				
				if ($brani_totali_temp==0):
					if ($dataIMP['test']==1):
						echo "Forzo l'uscita dal ciclo<br>";
					endif;
					break;
				endif;
				
				if (count($brano0_peso[$ultimo_peso])>0):

					$elementi_temp=count($brano0_peso[$ultimo_peso])-1;
					$id_brano_temporaneo = $brano0_peso[$ultimo_peso][$elementi_temp];
					unset($brano0_peso[$ultimo_peso][$elementi_temp]);
					
					$array_duplicati=array_keys($brani_definitivi, $id_brano_temporaneo);

					$brano_temporaneo = array_rand($array_duplicati, 1);
					$id_brano_temporaneo_duplicato=$array_duplicati[$brano_temporaneo];


					if ( ($durata_totale-$brano_durata_temp[$id_brano_temporaneo])>$durata_intervallo):
						$durata_totale=$durata_totale-$brano_durata_temp[$id_brano_temporaneo];
						unset($brani_definitivi[$id_brano_temporaneo_duplicato]);				
					endif;
			
					$errore=0;

				endif;
				
				$ultimo_peso=$ultimo_peso-1;
				if ($ultimo_peso<0)
					$ultimo_peso=count($peso)-1;

				$xx++;

			}



			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// termine delle routine di eliminazione dei brani in eccesso
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			


			// divide la playlist totale in un numero di sottogruppi pari al numero di peso massimo*2, e rimescola i rispettivi elementi
			$totale_gruppi=$max_peso*2;
			$numero_elementi_gruppo=ceil(count($brani_definitivi)/$totale_gruppi);
			
			$totale_brani=count($brani_definitivi);
			$xx=0;
			

			
			while ($xx<$totale_gruppi) {
				$limite_inf=$xx*$numero_elementi_gruppo;
				if ((count($brani_definitivi_2)+$numero_elementi_gruppo)>$brani_definitivi)
					$brani_gruppo[$xx]=array_slice($brani_definitivi, $limite_inf);
				else
					$brani_gruppo[$xx]=array_slice($brani_definitivi, $limite_inf, $numero_elementi_gruppo);
					
				shuffle($brani_gruppo[$xx]);
				
				$brani_definitivi_2=array_merge($brani_definitivi_2, $brani_gruppo[$xx]);
				$xx++;
			}
			
			$brano_id_temp=$brani_definitivi_2;
			

			if ($sequenza>0):
				// divisione playlist per segmenti e ordinamento secondo il parametro indicato (power, bpm...)
				unset($brani_definitivi);
				unset($brani_definitivi_2);	
				$brani_definitivi=Array();
				$xx=0;
				$zz=0;
				$step_sequenza=1;
				while ($xx<$totale_brani) {
					$brani_definitivi_2[$zz]=$brano_id_temp[$xx];
					
					if ($criterio_ordinamento=="power")
						$criterio[$zz]=$brano_power_temp[$brano_id_temp[$xx]];
					elseif ($criterio_ordinamento=="bpm")
						$criterio[$zz]=$brano_bpm_temp[$brano_id_temp[$xx]];
						
					if (($zz==($sequenza-1)) || ($xx==($totale_brani-1))):
						if (($step_sequenza/2)==round($step_sequenza/2))
							array_multisort($criterio, SORT_DESC, $brani_definitivi_2);
						else
							array_multisort($criterio, SORT_ASC, $brani_definitivi_2);
						$brani_definitivi=array_merge($brani_definitivi, $brani_definitivi_2);
						unset($brani_definitivi_2);
						unset($criterio);
						$zz=-1;
						$step_sequenza++;
					endif;
					$xx++;
					$zz++;
				}
				$brano_id_temp=$brani_definitivi;
			endif;

			$totale_brani=count($brano_id_temp);
			
			if (($limite_brani==0) || ($limite_brani>$totale_brani))
				$limite_brani=$totale_brani;
			
			$xx=0;
			$zz=0;
			
			$brano_id_temp_definitivi=Array();
			
			$durata_totale=0;
			while ($xx<$limite_brani) {
				$idtemp=$brano_id_temp[$xx];

				// verifica che non ci sia una coincidenza di brani nel range delle 5 posizioni successive
				$presente=0;
				$yy=1;
				while ($yy<6) {
					if ( (isset($brano_id_temp[($xx+$yy)])) && ($brano_id_temp[$xx]==$brano_id_temp[($xx+$yy)]) ):
						if (!isset($coincidenze)) 
							$coincidenze=0;
						$coincidenze++;
						$presente=1;
					endif;
					$yy++;
				}

				if ($presente==0):
					$brani_id_temp_definitivi[$zz]=$idtemp;
					$zz++;
				endif;

				$durata_totale=$durata_totale+$brano_durata_temp[$brano_temp];
				if ($dataIMP['test']==1):
					if ($presente==1) echo "*** doppione eliminato *** ";
					echo ($xx+1).". (".$idtemp.") ".$brano_artista_temp[$idtemp]." ".$brano_titolo_temp[$idtemp]." (Punteggio: ".$brano_punteggio_temp[$idtemp]."). Power: ".$brano_power_temp[$idtemp]." <b> Durata: ".$brano_durata_temp[$idtemp]." Ripetizioni giornaliere: ".$brano_ripetizioni_temp[$idtemp]."</b><br>";
				endif;
				
				$xx++;
			}
		
		
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Come ultimo passaggio, serve verificare che la durata complessiva della playlist generata non sia inferiore a quello della fascia oraria
			// decisa dall'utente; in caso contrario i brani andranno aggiunti in coda uno alla volta, a partire dal primo, fino al raggiungimento dell'orario
			// desiderato
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			$xx=0;
			// $durata_intervallo=$dataIMP['fine_sequenza']-$dataIMP['inizio_sequenza'];
			$durata_totale=0;
			foreach ($brani_id_temp_definitivi as $brano_temp):
				$durata_totale=$durata_totale+$brano_durata_temp[$brano_temp];
				$xx++;
			endforeach;

			$zz=$xx;
			$xx=0;
			$brani_definitivi_2=$brani_id_temp_definitivi;
			$brano_ripetuto_temp=Array();	// questo array specifica se il brano è ripetuto o meno
			if ($dataIMP['test']==1)
				echo "(".$durata_totale." su ".$durata_intervallo.")<br>";
			while ( ($durata_totale<$durata_intervallo) && (count($brani_definitivi_2)>0) ) {
				$brani_id_temp_definitivi[$zz]=$brani_definitivi_2[$xx];
				$brano_ripetuto_temp[$zz]=1;
				$durata_totale=$durata_totale+$brano_durata_temp[$brani_definitivi_2[$xx]];
				if ($dataIMP['test']==1)
					echo "Aggiungo: ".$brani_definitivi_2[$xx]." - ".$durata_totale." su ".$durata_intervallo."<br>";
				$xx++;
				$zz++;
				if ($xx>(count($brani_definitivi_2)-1))
					$xx=0;
			}
		//	echo "ok";
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			

			// genera i valori da restituire
			$zz=0;
			foreach ($brani_id_temp_definitivi as $idtemp):
			
				if (!isset($brano_ripetuto_temp[$zz])) $brano_ripetuto_temp[$zz]=0;
			
				$brano_id[$zz]=$idtemp;
				$brano_artista[$zz]=$brano_artista_temp[$idtemp];
				$brano_titolo[$zz]=$brano_titolo_temp[$idtemp];
				$brano_bpm[$zz]=$brano_bpm_temp[$idtemp];
				$brano_power[$zz]=$brano_power_temp[$idtemp];
				$brano_chiave[$zz]=$brano_chiave_temp[$idtemp];
				$brano_punteggio[$zz]=$brano_punteggio_temp[$idtemp];
				$brano_nome_file[$zz]=$brano_nome_file_temp[$idtemp];
				$brano_ripetizioni[$zz]=$brano_ripetizioni_temp[$idtemp];
				$brano_durata[$zz]=$brano_durata_temp[$idtemp];
				$brano_peso[$zz]=$brano_peso_temp[$idtemp];
				$brano_ripetuto[$zz]=$brano_ripetuto_temp[$zz];
				$brano_id_elemento[$zz]=0;
				$valore_vuoto[$zz]=0;
				
				if (!isset($totale_brani_peso[$brano_peso[$zz]]))
					$totale_brani_peso[$brano_peso[$zz]]=0;
				$totale_brani_peso[$brano_peso[$zz]]++;
					
				$zz++;
			endforeach;

		
		
		$zz=0;
		$messaggio="Totale brani:";
		foreach ($totale_brani_peso as $totale_brani_peso_temp):
			// $yy=array_search($brano_peso, $zz);
			$totale_brani_peso_temp_desc=$totale_brani_peso[$zz];
			if ($totale_brani_peso_temp_desc=="") $totale_brani_peso_temp_desc=0;
			$messaggio=$messaggio." peso ".($zz+1)." (".$totale_brani_peso_temp_desc.");";
			$zz++;
		endforeach;
		
		
		if (!isset($coincidenze))
			$coincidenze=0;
		$messaggio=$messaggio." numero doppioni: ".$coincidenze;
		
		else:
			$brano_id=Array();
			$brano_artista=Array();
			$brano_titolo=Array();
			$brano_punteggio=Array();
			$brano_nome_file=Array();
			$brano_bpm=Array();
			$brano_chiave=Array();
			$brano_power=Array();
			$brano_durata=Array();
			$brano_id_elemento=Array();
			$brano_ripetuto=Array();
			$valore_vuoto=Array();
		
		endif;
	
		$dataRET['brano_id']=$brano_id;
		$dataRET['brano_artista']=$brano_artista;
		$dataRET['brano_titolo']=$brano_titolo;
		$dataRET['brano_punteggio']=$brano_punteggio;
		$dataRET['brano_power']=$brano_power;
		$dataRET['brano_bpm']=$brano_bpm;
		$dataRET['brano_chiave']=$brano_chiave;
		$dataRET['brano_nome_file']=$brano_nome_file;
		$dataRET['brano_id_elemento']=$brano_id_elemento;
		$dataRET['brano_ripetuto']=$brano_ripetuto;
		$dataRET['brano_durata']=$brano_durata;
		$dataRET['valore_vuoto']=$valore_vuoto;
		
		if ($dataIMP['test']==1):
			echo "<br><br>";
			echo $messaggio;
			echo "<br><br>----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------<br><br><br><br>";
		endif;
		
		return $dataRET;
		
	}

	function durata_secondi($data) {
		$durata=$data['durata'];
		$durata=explode(":", $durata);
		$durata_secondi=($durata[0]*3600)+($durata[1]*60)+($durata[2]);
		$dataRET['durata_secondi']=$durata_secondi;
		return $dataRET;
	}

	function durata_formattata($data) {
		if (!isset($data['cumulativo']))
			$data['cumulativo']=0;	// se =0, specifica che dopo le 23 il conteggio delle ore deve ripartire da 0, altrimenti viene cumulato
		
		$durata=round($data['durata']);
		$ore=0;
		$minuti=0;
		$secondi=0;
		while ($durata>3599) {
			$ore++;
			$durata=$durata-3600;
		}
		while ($durata>59) {
			$minuti++;
			$durata=$durata-60;
		}
		$secondi=round($durata);
		if ($secondi<1) $secondi=0;
		
		if ($minuti<10) $minuti="0".$minuti;
		if ($secondi<10) $secondi="0".$secondi;
		
		if ($ore>23):
			if ($data['cumulativo']==0):
				$ore="00:";
			else:
				$ore=$ore.":";
			endif;
		elseif ($ore>0):
			if ($ore<10):
				$ore="0".$ore.":";
			else:
				$ore=$ore.":";
			endif;
		else:
			$ore="";
		endif;
		$dataRET['durata_formattata']=$ore.$minuti.":".$secondi;
		$dataRET['durata_formattata_breve']=$ore.$minuti;
		$dataRET['durata_formattata_secondi']=$minuti."'".$secondi.'"';
		return $dataRET;
	}


}

?>
