<?php

class Users_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function account_privilegi($gruppo) {
		
		$widget=$this->config->item('widget');
		
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $gruppo);
		$query=$this->db->get();
		$row=$query->row();
		$privilegi=unserialize($row->ugrp_privilegi);
		
		foreach ($widget as $widget_temp):
			if (!isset($privilegi[$widget_temp])) $privilegi[$widget_temp]=1;
		endforeach;
		
		/*
		if (!isset($privilegi['immagini'])) $privilegi['immagini']=1;
		if (!isset($privilegi['video'])) $privilegi['video']=1;
		if (!isset($privilegi['audio'])) $privilegi['audio']=1;
		if (!isset($privilegi['radio'])) $privilegi['radio']=1;
		*/
		
		return $privilegi;
	}
    
    function aggiungi_account($dataACC, $dataAGG=0)
    {
		
		$widget=$this->config->item("widget");
		
		if (!isset($dataACC['description']))
			$dataACC['description']="";
		if (!isset($dataACC['gruppo']))
			$dataACC['gruppo']=1;
		if (!isset($dataACC['tipologia']))
			$dataACC['tipologia']=2;
		if (!isset($dataACC['storage']))
			$dataACC['storage']="";
		if (!isset($dataACC['bucket']))
			$dataACC['bucket']="";
		if (!isset($dataACC['url']))
			$dataACC['url']="";
		if (!isset($dataACC['email']))
			$dataACC['email']="";
		
		if (!isset($dataACC['crediti']))
			$dataACC['crediti']=0;
		
		if (!isset($dataACC['privilegi']))
			$dataACC['privilegi']=Array();
		$privilegi=$dataACC['privilegi'];
		
		foreach ($widget as $widget_temp):
			if (!isset($privilegi[$widget_temp]))
				$privilegi[$widget_temp]=0;
		endforeach;
		
		/*
		if (!isset($privilegi['immagini']))
			$privilegi['immagini']=0;
		if (!isset($privilegi['video']))
			$privilegi['video']=0;
		if (!isset($privilegi['audio']))
			$privilegi['audio']=0;
		if (!isset($privilegi['radio']))
			$privilegi['radio']=0;
		*/
		
		if (!isset($dataACC['activation']))
			$dataACC['activation']=0;
		if (!isset($dataACC['request_activation']))
			$dataACC['request_activation']=time();
		
		$privilegi_serialized=serialize($privilegi);
		
		$stringa_casuale="";
		$lunghezza_stringa = 15;
		$caratteri_stringa = "0123456789abcdefghijklmnopqrstuvwxyz";
			for ($p = 0; $p < $lunghezza_stringa; $p++) {
				$stringa_casuale .= $caratteri_stringa[mt_rand(0, (strlen($caratteri_stringa))-1)];
			}
				
		$is_admin = FALSE;
		$custom_data = array(
			'ugrp_stringa' => $stringa_casuale,
			'ugrp_tipologia' => $dataACC['tipologia'],
			'ugrp_gruppo_riferimento' => $dataACC['gruppo'],
			'ugrp_privilegi' => $privilegi_serialized,
			'ugrp_storage' => $dataACC['storage'],
			'ugrp_bucket' => $dataACC['bucket'],
			'ugrp_url' => $dataACC['url'],
			'ugrp_email' => $dataACC['email'],
			'ugrp_activation' => $dataACC['active'],
			'ugrp_request_activation' => $dataACC['request_activation'],
			'crediti' => $dataACC['crediti']
		);
		$id_nuovo_account=$this->flexi_auth->insert_group($dataACC['name'], $dataACC['description'], $is_admin, $custom_data);
		
		if ($dataAGG!=0):
			$dataAGG['id_account']=$id_nuovo_account;
			$this->db->insert('user_groups_dettagli', $dataAGG);
		endif;
		
		$dataACCOUNT['stringa_casuale']=$stringa_casuale;
		$dataACCOUNT['id_nuovo_account']=$id_nuovo_account;
		return $dataACCOUNT;
	}
	
	function crea_directory($data) {
		$percorso_base=$this->config->item('archivio_real_path');
		$percorso_users=$this->config->item('percorso_users');
		$cartella_account=$percorso_base.$percorso_users."user_".$data['id_nuovo_account']."_".$data['stringa_casuale'];
		mkdir($cartella_account, 0775);
		chmod($cartella_account, 0775);
		$cartella_content=$cartella_account."/content";
		mkdir($cartella_content, 0775);
		chmod($cartella_content, 0775);
		$cartella_content=$cartella_account."/radio";
		mkdir($cartella_content, 0775);
		chmod($cartella_content, 0775);
		$cartella_content=$cartella_account."/system";
		mkdir($cartella_content, 0775);
		chmod($cartella_content, 0775);	
	}

	function elimina_directory($directory) {
		
		if(is_dir($directory)){
			$files = glob( $directory . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
				foreach($files as $file)
				{
					$this->elimina_directory($file);
				}
  
				rmdir($directory);
		}
		elseif(is_file($directory)) {
				unlink($directory);  
		}
		
	}

	function crediti_residui($grupposelezionato) {
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $grupposelezionato);
		$queryCRE=$this->db->get();
		$rowCRE=$queryCRE->row();
		return $rowCRE->crediti;
	}

	function spazio_account($grupposelezionato) {
		$dataRET['limite_spazio']=$this->config->item('limite_spazio')*1024;
		$this->db->select_sum('dimensioni_file');
		$this->db->from('archivio_file');
		$this->db->where('id_account', $grupposelezionato);
		$queryCRE=$this->db->get();
		$rowCRE=$queryCRE->row();
		$dataRET['dimensione_totale']=$rowCRE->dimensioni_file;
		return $dataRET;
	}

	function elenca_dettagli_account($grupposelezionato) {
		$this->db->from('user_groups_dettagli');
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		$row=$query->row();
		echo "<table>";
		echo "<tr><td>Nome e cognome *</td><td><input type=\"text\" name=\"nome_cognome\" id=\"nome_cognome\" value=\"".$row->nome_cognome."\"></td></tr>";
		echo "<tr><td>Ragione sociale</td><td><input type=\"text\" name=\"ragione_sociale\" id=\"ragione_sociale\" value=\"".$row->ragione_sociale."\"></td></tr>";
		echo "<tr><td>Indirizzo</td><td><input type=\"text\" name=\"indirizzo\" id=\"indirizzo\" value=\"".$row->indirizzo."\"></td></tr>";
		echo "<tr><td>Citt&agrave;</td><td><input type=\"text\" name=\"citta\" id=\"citta\" value=\"".$row->citta."\"></td></tr>";
		echo "<tr><td>C.A.P.</td><td><input type=\"text\" name=\"cap\" id=\"cap\" value=\"".$row->cap."\"></td></tr>";
		echo "<tr><td>Provincia</td><td><input type=\"text\" name=\"provincia\" id=\"provincia\" value=\"".$row->provincia."\"></td></tr>";
		echo "<tr><td>Partita IVA</td><td><input type=\"text\" name=\"partita_iva\" id=\"partita_iva\" value=\"".$row->partita_iva."\"></td></tr>";
		echo "<tr><td>Codice fiscale</td><td><input type=\"text\" name=\"codice_fiscale\" id=\"codice_fiscale\" value=\"".$row->codice_fiscale."\"></td></tr>";
		echo "<tr><td>E-mail *</td><td><input type=\"text\" name=\"email\" id=\"email\" value=\"".$row->email."\"></td></tr>";
		echo "<tr><td></td><td><small>Attenzione: &egrave; l'indirizzo principale dell'account, utilizzato per l'invio<br>di tutte le comunicazioni tecniche e amministrative</td></tr>";
		echo "</table>";
	}
	
	
	function select_lista_account($data) {
		
		$grupposelezionato=$this->flexi_auth->get_user_group_id();
		$gruppo=$data['gruppo_selezionato'];

		$this->db->from('user_groups');
		$this->db->order_by('ugrp_name', 'asc');
		$query=$this->db->get();
	
		$xx=0;
		foreach ($query->result() as $row):
			echo "<option value=\"".$row->ugrp_id."\"";
			if ($row->ugrp_id==$gruppo)
				echo " selected";
			echo ">";
			echo $row->ugrp_id.". ".$row->ugrp_name;
			if ($row->ugrp_id==$grupposelezionato)
				echo " (tu)";
			echo "</option>";
			$xx++;
		endforeach;
	
	}
	
}
?>
