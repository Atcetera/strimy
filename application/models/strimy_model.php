<?php

class Strimy_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function accountselezionato() {
		// $gruppo=get_cookie('accountselezionato');
		
		$gruppo=$this->flexi_auth->get_user_group_id();
		
		if ( (!isset($gruppo)) || ($gruppo=="") || ($gruppo==0) ):
			redirect('users/logout');
			die();
		endif;
		
		return $gruppo;
	}
    
    function stringa_casuale($data) {
		if (!isset($data['gruppo']))
			$data['gruppo']=$this->accountselezionato();
		$this->db->select('ugrp_stringa');
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $data['gruppo']);
		$queryGRP=$this->db->get();
		$rowGRP=$queryGRP->row();
		$stringa_casuale="user_".$data['gruppo']."_".$rowGRP->ugrp_stringa;
		return $stringa_casuale;
	}
	
	function stringa_casuale_genera($lunghezza_stringa=15) {
		$stringa_casuale="";
		$caratteri_stringa = "0123456789abcdefghijklmnopqrstuvwxyz";
		for ($p = 0; $p < $lunghezza_stringa; $p++) {
			$stringa_casuale .= $caratteri_stringa[mt_rand(0, (strlen($caratteri_stringa))-1)];
		}	
		return $stringa_casuale;
	}
	
	function recupera_storage($grupposelezionato=0) {
		$data=Array();
		if ($grupposelezionato==0)
			$grupposelezionato=$this->accountselezionato();
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $grupposelezionato);
		$queryGRP=$this->db->get();
		$rowGRP=$queryGRP->row();
		$data['storage']=$rowGRP->ugrp_storage;
		$data['bucket']=$rowGRP->ugrp_bucket;
		$data['url']=$rowGRP->ugrp_url;
		$data['app_url']=$this->config->item('base_url');
		if ($rowGRP->ugrp_storage==0):
			$data['full_path']=$this->config->item('archivio_base_url'); // ."strimy";
			$data['real_path']=$this->config->item('archivio_real_path').$this->config->item('percorso_users');
		else:
			$data['full_path']=$rowGRP->ugrp_url;
			$data['real_path']=$rowGRP->ugrp_url;
		endif;
		return $data;
	}
    
    function select_lista_account($data) {
		if (!isset($data['reload'])) $data['reload']=0;	// se 1 specifica di fare il refresh in caso di cambiamento di account
		$gruppo=$this->flexi_auth->get_user_group_id();
		$tipologia=$this->account_tipologia($gruppo);
		$tipologie=$this->config->item("tipologie");
		
		if ($tipologia==2):	// si tratta di un account cliente; non è possibile selezionare altri account
			echo "<input type=\"hidden\" name=\"egruppo_selezionato\" value=\"".$gruppo."\">";
			
		else:	// è un account di tipo admin o rivenditore; è possibile selezionare altri account
		
			if (!isset($data['gruppo_selezionato']))
				$data['gruppo_selezionato']=$gruppo;
			
			
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $gruppo);
			$query=$this->db->get();
			$row=$query->row();
			$account_id[]=$row->ugrp_id;
			$account_nome[]=$row->ugrp_name;
			$account_tipologia[]=$row->ugrp_tipologia;
			
			$this->db->from('user_groups');
			$this->db->where('ugrp_id !=', $gruppo);
			if ($tipologia==1)
				$this->db->where('ugrp_gruppo_riferimento', $gruppo);
			$this->db->order_by('ugrp_name', 'asc');
			$query2=$this->db->get();
			foreach ($query2->result() as $row2):
				$account_id[]=$row2->ugrp_id;
				$account_nome[]=$row2->ugrp_name;
				$account_tipologia[]=$row2->ugrp_tipologia;
			endforeach;
			
			echo "<select name=\"account_selezionato\"";
			if ($data['reload']==1)
				echo " onChange=\"cambia_account(this);\"";
			echo ">";
			$xx=0;
			foreach ($account_id as $temp):
				echo "<option value=\"".$account_id[$xx]."\"";
				if ($account_id[$xx]==$data['gruppo_selezionato'])
					echo " selected";
				echo ">";
				echo $account_nome[$xx];
				if ($account_id[$xx]==$gruppo):
					echo " (tu)";
				else:
					echo " (".$tipologie[$account_tipologia[$xx]].")";
				endif;
				echo "</option>";
				$xx++;
			endforeach;
			echo "</select>";
			if ($data['reload']==1)
				echo "<input type=\"hidden\" id=\"account_attualmente_selezionato\" value=\"".$data['gruppo_selezionato']."\">";
			
		endif;
		
	}

	function dispositivo_elimina($device_id) {
		
		$percorso_base=$this->config->item('archivio_real_path');
		$percorso_devices=$this->config->item('percorso_devices');
	
		// queste funzioni interessano solo la magic radio
		// e sono deprecate nella rel2
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('playlist');
		
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('playlist_sequenze');
		
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('playlist_elementi');
	
		// elimina il dispositivo
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('dispositivi');

		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('dispositivi_configurazione');
		
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('layout_indice');
		
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('layout_playlist');
		
		$this->db->where('id_dispositivo', $device_id);
		$this->db->delete('layout_playlist_widget');
		
		// elimina ricorsivamente la directory del dispositivo
		$dir=$percorso_base.$percorso_devices.$device_id;

		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir."/".$object) == "dir") 
					rmdir($dir."/".$object); 
					else unlink ($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	
	}

	function utente_tipologia($utente) {
		$this->db->from('user_accounts');
		$this->db->where('uacc_id', $utente);
		$query=$this->db->get();
		$row=$query->row();
		return $row->uacc_amministratore;
	}
    
    function account_tipologia($gruppo) {
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $gruppo);
		$query=$this->db->get();
		$row=$query->row();
		return $row->ugrp_tipologia;
	}
/*
    function account_privilegi($gruppo) {
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $gruppo);
		$query=$this->db->get();
		$row=$query->row();
		$privilegi=unserialize($row->ugrp_privilegi);
		
		if (!isset($privilegi['immagini'])) $privilegi['immagini']=1;
		if (!isset($privilegi['video'])) $privilegi['video']=1;
		if (!isset($privilegi['audio'])) $privilegi['audio']=1;
		if (!isset($privilegi['radio'])) $privilegi['radio']=1;
		return $privilegi;
	}
*/   
    function recupera_gruppi($data=Array()) {
		if (!isset($data['id_dispositivo'])) $data['id_dispositivo']=0;
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$gruppo_id=Array();
		$gruppo_descrizione=Array();
		
		$this->db->select('dispositivi_gruppi.id as id, dispositivi_gruppi.descrizione as descrizione');
		$this->db->from('dispositivi_gruppi');
		$this->db->where('dispositivi_gruppi.id_account', $grupposelezionato);
		if ($data['id_dispositivo']!=0):
			$this->db->join('dispositivi', 'dispositivi.id_gruppo=dispositivi_gruppi.id');
			$this->db->where('dispositivi.id_dispositivo', $data['id_dispositivo']);
		endif;
		$this->db->order_by('dispositivi_gruppi.descrizione', 'asc');
		$query=$this->db->get();
		foreach ($query->result() as $row):
			$gruppo_id[]=$row->id;
			$gruppo_descrizione[]=$row->descrizione;
		endforeach;
		$dataRET['gruppo_id']=$gruppo_id;
		$dataRET['gruppo_descrizione']=$gruppo_descrizione;
		return $dataRET;
	}
    
    function recupera_gruppi_dispositivi() {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$id_gruppo[0]=0;
		$desc_gruppo[0]="";
		$this->db->from('dispositivi_gruppi');
		$this->db->where('id_account', $grupposelezionato);
		$this->db->order_by('descrizione', 'asc');
		$queryGRP=$this->db->get();
		foreach ($queryGRP->result() as $rowGRP):
			$id_gruppo[]=$rowGRP->id;
			$desc_gruppo[]=$rowGRP->descrizione;
		endforeach;
		$dataRET['id_gruppo']=$id_gruppo;
		$dataRET['desc_gruppo']=$desc_gruppo;
		return $dataRET;
	}

	function lista_releases($data) {
		$dataRET=Array();		
		$directory=$this->config->item('percorso_devices')."/".$data['id_dispositivo']."/releases";
		$files_ret = $this->lista_file_generici($this->config->item('archivio_real_path').$directory);
		if (!isset($files_ret[0])):
			$directory=$this->config->item('percorso_releases');
			$files_ret = $this->lista_file_generici($this->config->item('archivio_real_path').$directory);
		endif;
		
		if (isset($files_ret[0])):
			$dataRET['file']=$files_ret[0];
			$dataRET['url']=$this->config->item('archivio_base_url').$directory;
		endif;
		return $dataRET;
	}

	function lista_file_generici($directory) {
		$files = scandir($directory);
		$files_ret=Array();
		if ($files):
			foreach ($files as $files_temp):
				if (($files_temp!=".") && ($files_temp!=".."))
					$files_ret[]=$files_temp;
			endforeach;
		endif;
		return $files_ret;
	}

	function lista_file_contenuti($data) {
		
		$layout_widget=Array();
		$layout_widget[]=0;
		$layout_widget[]=1;
		$layout_widget[]=2;

		$dataRET['file']=Array();
		$dataRET['percorso']=Array();
		
		$this->db->select('playlist_generica_elementi.tipologia_file as tipologia_file, playlist_generica_elementi.id_file as id_file, layout_playlist_widget.tipologia as tipologia');
		$this->db->from('playlist_generica_elementi');
		$this->db->join('layout_playlist_widget', 'layout_playlist_widget.id_playlist=playlist_generica_elementi.id_playlist');
		$this->db->where('layout_playlist_widget.id_dispositivo', $data['id_dispositivo']);
		$this->db->where('layout_playlist_widget.giorno', $data['giorno']);
		$this->db->where_in('layout_playlist_widget.tipologia', $layout_widget);
		$this->db->order_by('layout_playlist_widget.tipologia', 'asc');
		$query=$this->db->get();
		foreach ($query->result() as $row):
			if (!isset($file[$row->tipologia_file]))
				$file[$row->tipologia_file]=Array();
			$file[$row->tipologia_file][]=$row->id_file;
		endforeach;
		
		$tabella_db="";
		$xx=0;
		
		// elenca i contenuti del giorno negli archivi "content" e "radio"
		foreach ($layout_widget as $tipologia):
		
			if (count($file[$tipologia]) > 0):
		
				if ($tipologia==0):
					$tabella_db="archivio_radio";
					$percorso="radio";
				elseif (($tipologia==1) || ($tipologia==2)):
					$tabella_db="archivio_file";
					$percorso="content";
				endif;
					
				$this->db->select($tabella_db.'.nome_file as nome_file');
				$this->db->from($tabella_db);
				$this->db->where_in($tabella_db.'.id', $file[$tipologia]);
				$this->db->order_by('nome_file', 'asc');
				$queryCONT=$this->db->get();
				foreach ($queryCONT->result() as $rowCONT):
					$dataRET['file'][]=$rowCONT->nome_file;
					$dataRET['percorso'][]=$percorso;
				endforeach;
			
			endif;
			
		endforeach;

		// echo "ok";

		// elenca i contenuti nell'archivio "system"; sono permanenti, comuni a tutti gli account e indipendenti dalla playlist
		$this->db->select('archivio_sistema.nome_file as nome_file');
		$this->db->from('archivio_sistema');
		// $this->db->where('id_account', $data['gruppo']);
		$this->db->order_by('nome_file', 'asc');
		$queryCONT=$this->db->get();
		foreach ($queryCONT->result() as $rowCONT):
			$dataRET['file'][]=$rowCONT->nome_file;
			$dataRET['percorso'][]="../common";
		endforeach;

		return $dataRET;
		
	}

  
 
    function lista_file_archivio($data)	// vecchia procedura di recupero dell'elenco dei file
    {
		
		if (!isset($data['directory_contenuto']))
			$data['directory_contenuto']="content";
			
		if ($data['directory_contenuto']=="content")
			$tabella_db="archivio_file";
		elseif ($data['directory_contenuto']=="radio")
			$tabella_db="archivio_radio";
		elseif ($data['directory_contenuto']=="../common")
			$tabella_db="archivio_sistema";
		
		
		error_reporting(E_ERROR);
	
		if (!isset($data['id_file'])) $data['id_file']=0;
		
		$gruppo=$data['gruppo'];
		$stringa_casuale=$this->stringa_casuale($data);
		
	//	$dataSTORAGE=$this->strimy_model->recupera_storage($gruppo);
		
	//	$dataSTORAGE['real_path'].$stringa_casuale
		$percorso_base=$this->config->item('archivio_real_path');
		
		$percorso_users=$this->config->item('percorso_users');
		$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/".$data['directory_contenuto'];
		
        $xx=0;
		$elenco_file=Array();
		$elenco_file_thumb=Array();
		$elenco_file_estensione=Array();
		$elenco_file_tipo=Array();
		$elenco_file_tipologia=Array();
		$elenco_file_id=Array();
		$elenco_file_data_upload=Array();
		$this->db->from($tabella_db);
		if ($data['amministratore']==0)
			$this->db->where('id_account', $gruppo);
		if ($data['id_file']!=0)
			$this->db->where('id', $data['id_file']);
		if ( (isset($data['tipologia_file'])) && ($data['tipologia_file']!=-1) )
			$this->db->where('tipologia_file', $data['tipologia_file']);
		$this->db->order_by('id', 'asc');
		$queryFILE=$this->db->get();
		foreach ($queryFILE->result() as $rowFILE):
			$file=$rowFILE->nome_file;
			$valori=unserialize($rowFILE->valori);
			$percorso_file=$cartella_base."/".$file;
			// if(($file!="") && (file_exists($percorso_file))){
			if($file!="") {
					$file_array=explode(".", $file);
					$elenco_file_estensione[$xx]=strtolower(end($file_array));
					$elenco_file_tipologia[$xx]="";
					if (($elenco_file_estensione[$xx]=="jpg") || ($elenco_file_estensione[$xx]=="png")):
						$elenco_file_tipologia[$xx]="immagini";
					//	$elenco_file_anteprima[$xx]=$url_archivio."/".$file;
					elseif (($elenco_file_estensione[$xx]=="mp4") || ($elenco_file_estensione[$xx]=="mkv") || ($elenco_file_estensione[$xx]=="mov") || ($elenco_file_estensione[$xx]=="avi")):
						$elenco_file_tipologia[$xx]="video";
					elseif (($elenco_file_estensione[$xx]=="mp3") || ($elenco_file_estensione[$xx]=="wav")):
						$elenco_file_tipologia[$xx]="audio";
					//	$elenco_file_anteprima[$xx]="/style/icone/archive_video.jpg";
					endif;
					$elenco_file[$xx]=$file;
					$elenco_file_id[$xx]=$rowFILE->id;
					if (trim($rowFILE->descrizione!=""))
						$elenco_file_descrizione[$xx]=$rowFILE->descrizione;
					else
						$elenco_file_descrizione[$xx]=$file;
					
					$elenco_file_tipo[$xx]=$rowFILE->tipologia_file;
					$elenco_file_data_upload[$xx]=$rowFILE->data_upload;
					$elenco_file_thumb[$xx]=$valori['nome_file_thumb'];
			$xx++;
			}
		endforeach;
		
		$totale_file=$xx;
	//	array_multisort($elenco_file, SORT_ASC, $elenco_file_estensione, $elenco_file_id, $elenco_file_tipologia, $elenco_file_descrizione);
		
		$dataRET['totale_file']=$totale_file;
		$dataRET['elenco_file']=$elenco_file;
		$dataRET['elenco_file_thumb']=$elenco_file_thumb;
		$dataRET['elenco_file_estensione']=$elenco_file_estensione;
		$dataRET['elenco_file_tipologia']=$elenco_file_tipologia;
		$dataRET['elenco_file_tipo']=$elenco_file_tipo;
		$dataRET['elenco_file_descrizione']=$elenco_file_descrizione;
		$dataRET['elenco_file_id']=$elenco_file_id;
		$dataRET['elenco_file_data_upload']=$elenco_file_data_upload;
		$dataRET['totale_file']=$totale_file;
		
		return $dataRET;

    }

	/*
    function lista_file_archivio_new($data)	// vecchia procedura di recupero dell'elenco dei file
    {
		
		if (!isset($data['directory_contenuto']))
			$data['directory_contenuto']="content";
			
		if ($data['directory_contenuto']=="content")
			$tabella_db="archivio_file";
		elseif ($data['directory_contenuto']=="radio")
			$tabella_db="archivio_radio";
		elseif ($data['directory_contenuto']=="system")
			$tabella_db="archivio_sistema";
		
		
		error_reporting(E_ERROR);
	
		
	
		if (!isset($data['id_file'])) $data['id_file']=0;
		
		$giorno=$data['giorno'];
		$gruppo=$data['gruppo'];
		$device_id=$data['device_id'];
		$stringa_casuale=$this->stringa_casuale($data);
		
		$percorso_base=$this->config->item('real_path');
		$percorso_users=$this->config->item('percorso_users');
		$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/".$data['directory_contenuto'];
		
		
        $xx=0;
		$elenco_file=Array();
		$elenco_file_estensione=Array();
		$elenco_file_tipologia=Array();
		$elenco_file_id=Array();
		
		
		if ($data['directory_contenuto']=="system"):
			$this->db->from($tabella_db);
			$this->db->where('id_account', $gruppo);
		
		else:
			$this->db->select($tabella_db.'.nome_file as nome_file');
			$this->db->from($tabella_db);
			$this->db->join('playlist_elementi', 'playlist_elementi.id_file='.$tabella_db.'.id');
			$this->db->join('playlist', 'playlist.id=playlist_elementi.id_playlist');
			$this->db->where('playlist.id_dispositivo', $device_id);
			$this->db->where('playlist.giorno', $giorno);

		endif;

		if ($data['id_file']!=0)
			$this->db->where($tabella_db.'.id', $data['id_file']);
		$this->db->order_by($tabella_db.'.id', 'asc');
		$this->db->group_by($tabella_db.'.id');

		$queryFILE=$this->db->get();
		foreach ($queryFILE->result() as $rowFILE):
			$file=$rowFILE->nome_file;
			$percorso_file=$cartella_base."/".$file;
			
			// if(($file!="") && (file_exists($percorso_file))){
			if($file!=""){
					$file_array=explode(".", $file);
					$elenco_file_estensione[$xx]=strtolower(end($file_array));
					$elenco_file_tipologia[$xx]="";
					if (($elenco_file_estensione[$xx]=="jpg") || ($elenco_file_estensione[$xx]=="png")):
						$elenco_file_tipologia[$xx]="immagini";
					//	$elenco_file_anteprima[$xx]=$url_archivio."/".$file;
					elseif (($elenco_file_estensione[$xx]=="mp4") || ($elenco_file_estensione[$xx]=="mkv") || ($elenco_file_estensione[$xx]=="mov")):
						$elenco_file_tipologia[$xx]="video";
					elseif (($elenco_file_estensione[$xx]=="mp3") || ($elenco_file_estensione[$xx]=="wav")):
						$elenco_file_tipologia[$xx]="audio";
					//	$elenco_file_anteprima[$xx]="/style/icone/archive_video.jpg";
					endif;
					$elenco_file[$xx]=$file;
					$elenco_file_id[$xx]=$rowFILE->id;
					if (trim($rowFILE->descrizione!=""))
						$elenco_file_descrizione[$xx]=$rowFILE->descrizione;
					else
						$elenco_file_descrizione[$xx]=$file;
			$xx++;
			}
		endforeach;
		
		$totale_file=$xx;
	//	array_multisort($elenco_file, SORT_ASC, $elenco_file_estensione, $elenco_file_id, $elenco_file_tipologia, $elenco_file_descrizione);
		
		$dataRET['totale_file']=$totale_file;
		$dataRET['elenco_file']=$elenco_file;
		$dataRET['elenco_file_estensione']=$elenco_file_estensione;
		$dataRET['elenco_file_tipologia']=$elenco_file_tipologia;
		$dataRET['elenco_file_descrizione']=$elenco_file_descrizione;
		$dataRET['elenco_file_id']=$elenco_file_id;
		$dataRET['totale_file']=$totale_file;
		
		return $dataRET;

    }
*/

	function invia_mail($data=Array()) {
		$indirizzo_destinazione="info@atcetera.it";
		$header_addizionali="FROM:strimy<non-rispondere@strimy.it>";

		if (!isset($data['messaggio'])) $data['messaggio']="";
		if (!isset($data['oggetto'])) $data['oggetto']="";

		mail ($indirizzo_destinazione, $data['oggetto'], $data['messaggio'], $header_addizionali);
	}


	function orario_check($orario) {  // controlla che l'orario sia formattato correttamente
		$orario_temp=explode(":",$orario);
		if ( (!isset($orario_temp[0])) || (!is_numeric($orario_temp[0])) ) $orario_temp[0]=0;
		if ( (!isset($orario_temp[1])) || (!is_numeric($orario_temp[1])) ) $orario_temp[1]=0;
		$orario_finale_temp=mktime($orario_temp[0], $orario_temp[1], 0, 1, 1, date('y'));
		$orario_finale=date('H:i', $orario_finale_temp);
		$data['orario_timestamp']=$orario_finale_temp;
		$data['orario_formattato']=$orario_finale;
		return $data;
	}

	function playlist_system($data) {  // questa funzione elenca i contenuti comuni a tutti gli utenti, e crea un layout generico "homescreen"
		
		$id_dispositivo=$data['id_dispositivo'];
		
		$this->db->from('dispositivi_configurazione');
		$this->db->where('id_dispositivo', $id_dispositivo);
		$queryDIS=$this->db->get();
		if ($queryDIS->num_rows()>0):
			$rowDIS=$queryDIS->row();
			$parametri_configurazione=unserialize($rowDIS->parametri_configurazione);
			$valori[1]['city']=$parametri_configurazione['city'];
			$valori[2]['url']=$parametri_configurazione['rss_url'];
			$valori[2]['background_color']="33-33-33-0.5";  // colore di default (in formato RGBA)
		else:
			$valori[1]['city']="";
			$valori[2]['url']="";
		endif;
		
		$layout=$this->config->item('layout');
		$layout_descrizione=$this->config->item('layout_shortcode');
		$layout_widget=$this->config->item('layout_widget');
		$widget=$this->config->item('widget');
		$homescreen_slide_duration=$this->config->item('homescreen_slide_duration');
		$dataDES['layout']=Array();
		
		$widget_temp=array_search("homescreen", $layout_descrizione);
		
		$dataLAY['layout_type']=$layout_descrizione[$widget_temp];
		$dataLAY['time_start']=0;
		$dataLAY['time_stop']=0;
		
		
		
//		echo "(".$widget_temp.")";
		
		$tipologia_layout_widget=$layout_widget[$widget_temp];

		$xx=0;
		$totale_widget=count($tipologia_layout_widget);

		while ($xx<$totale_widget) {
		
			$tipologia_playlist=$tipologia_layout_widget[$xx];
			$tipologia_playlist=$widget[$tipologia_playlist];


			$dataPLY=Array();
			$dataPLY['content']=Array();
			if ( ($tipologia_playlist==0) || ($tipologia_playlist==1) || ($tipologia_playlist==2) ):
				$this->db->from('archivio_sistema');
				$this->db->where('tipologia_file', 2);	// recupera solo le immagini, necessarie per gli sfondi
				$querySYS=$this->db->get();
				foreach ($querySYS->result() as $rowSYS):
					$valori_unserialized=unserialize($rowSYS->valori);
					$dataPLYtemp['file_name']=$rowSYS->nome_file;
					$dataPLYtemp['duration']=$homescreen_slide_duration;
					$dataPLYtemp['image_size']=$valori_unserialized['dimensioni'];
					$dataPLYtemp['crop_mask']="0;0;".$valori_unserialized['dimensioni'];
					$dataPLYtemp['crop_ratio']="1";
					$dataPLY['content'][]=$dataPLYtemp;
				endforeach;
				shuffle($dataPLY['content']);
			else:
				// la playlist non ha alcun contenuto (es. è solo un widget news)
				// quindi è sufficiente visualzzare i parametri
				$dataPLY['content'][]=$valori[$xx];
			endif;
				
					
			$dataWID['order']=$xx;
			$dataWID['widget_type']=$tipologia_layout_widget[$xx];
			$dataWID['content']=$dataPLY['content'];
					
				
			$dataLAY['widget'][]=$dataWID;	
			$xx++;
		}
		
		$dataDES['layout'][]=$dataLAY;
	
		return $dataDES;
	}

	function playlist($dataQUERY) {  // questa funzione elenca i layout secondo i parametri passati dalla query
		
		$queryLAY=$dataQUERY['query'];
		
		$layout=$this->config->item('layout');
		$layout_descrizione=$this->config->item('layout_shortcode');
		$layout_widget=$this->config->item('layout_widget');
		$widget=$this->config->item('widget');
		$widget_slideshow_shortcode=$this->config->item('widget_slideshow_shortcode');
		$dataDES['layout']=Array();
		foreach ($queryLAY->result() as $rowLAY):
			
			$orario_inizio=$rowLAY->orario_inizio;
			$orario_fine=$rowLAY->orario_fine;
			
			$dataLAY=Array();
			$dataLAY['layout_type']=$layout_descrizione[$rowLAY->tipologia];
			$dataLAY['time_start']=($orario_inizio-$dataQUERY['differenza_settimana'])*1;
			$dataLAY['time_stop']=($orario_fine)*1;
			
			$tipologia_layout_widget=$layout_widget[$rowLAY->tipologia];
			
			$xx=0;
			$totale_widget=count($tipologia_layout_widget);
		
			while ($xx<$totale_widget) {
			
				$dataWID=Array();
				$this->db->select('playlist_generica_indice.id as id, playlist_generica_indice.valori as valori, layout_playlist_widget.tipologia as tipologia, layout_playlist_widget.tipologia_file as tipologia_file');
				$this->db->from('playlist_generica_indice');
				$this->db->join('layout_playlist_widget', 'layout_playlist_widget.id_playlist=playlist_generica_indice.id');
				$this->db->where('layout_playlist_widget.id_layout', $rowLAY->id);
				$this->db->where('layout_playlist_widget.ordine', $xx);
				$this->db->order_by('layout_playlist_widget.id_layout', 'asc');
				$queryWID=$this->db->get();
				if ($queryWID->num_rows()==0):
					$id_playlist=0;
					$tipologia_playlist=-1;
				//	$widget_type="";
				else:
					$rowWID=$queryWID->row();
					$id_playlist=$rowWID->id;
					$tipologia_playlist=$rowWID->tipologia;
				//	$widget_type=$layout_widget[$rowLAY->tipologia][$xx];
				endif;
				
				$dataPLY=Array();
				$dataPLY['content']=Array();
				if ($id_playlist!=0):
					if ( ($tipologia_playlist==0) || ($tipologia_playlist==1) || ($tipologia_playlist==2) ):
						// la playlist ha un contenuto (es. radio, slideshow)
						// quindi è necessario caricarlo dal database
						$dataPLYOR['id_playlist']=$id_playlist;
						$dataPLYOR['tipologia_playlist']=$tipologia_playlist;
						$dataPLY=$this->playlist_generica($dataPLYOR);
					elseif ($tipologia_playlist>0):
						// la playlist non ha alcun contenuto (es. è solo un widget news)
						// quindi è sufficiente visualzzare i parametri
						$dataPLY['content'][]=unserialize($rowWID->valori);
					endif;
				endif;

				// se si tratta di una playlist Slideshow, il widget dovrà essere adattato secondo la tipologia dei file:
				// se si tratta di video, tipologia_widget dovrà essere VideoFrame
				// se si tratta di immagini, tipologia_widget dovrà essere Slideshow
				// e così via
				$layout_widget_temp=array_search($tipologia_playlist, $widget);
				if ($tipologia_playlist==2):
					$layout_widget_temp=array_search($rowWID->tipologia_file, $widget_slideshow_shortcode);
				endif;
				
				$dataWID['order']=$xx;
				// $dataWID['tipologia_playlist']=$rowLAY->tipologia;
				$dataWID['widget_type']=$layout_widget_temp;
				$dataWID['content']=$dataPLY['content'];
				
			
				$dataLAY['widget'][]=$dataWID;	
				$xx++;
			}
			
			$dataDES['layout'][]=$dataLAY;
			
		endforeach;	
	
		return $dataDES;
	}

	function playlist_generica($data) {  // questa funzione elenca i contenuti dei layout dati
		$dataWID=Array();
		$dataWID['content']=Array();
		
		if (($data['tipologia_playlist']==0) || ($data['tipologia_playlist']==1)):
		
			$this->db->select('archivio_radio.nome_file as nome_file, archivio_radio.artista as artista, archivio_radio.titolo as titolo, playlist_generica_elementi.valori as valori');
			$this->db->from('archivio_radio');
			$this->db->join('playlist_generica_elementi', 'playlist_generica_elementi.id_file=archivio_radio.id');
			$this->db->where('playlist_generica_elementi.id_playlist', $data['id_playlist']);
			$this->db->order_by('playlist_generica_elementi.ordine', 'asc');
			$queryPLY=$this->db->get();
			foreach ($queryPLY->result() as $rowPLY):
				$dataPLYtemp=Array();
				$dataVAL=Array();
				if ($rowPLY->valori!="")
					$dataVAL=unserialize($rowPLY->valori);
				$dataPLYtemp['file_name']=$rowPLY->nome_file;
				$dataPLYtemp['artist']=$rowPLY->artista;
				$dataPLYtemp['title']=$rowPLY->titolo;
				$dataPLY=$dataPLYtemp+$dataVAL;
				$dataWID['content'][]=$dataPLY;
			endforeach;

		elseif ($data['tipologia_playlist']==2):
			$this->db->select('archivio_file.nome_file, playlist_generica_elementi.valori as valori');
			$this->db->from('archivio_file');
			$this->db->join('playlist_generica_elementi', 'playlist_generica_elementi.id_file=archivio_file.id');
			$this->db->where('playlist_generica_elementi.id_playlist', $data['id_playlist']);
			$this->db->order_by('playlist_generica_elementi.ordine', 'asc');
			$queryPLY=$this->db->get();
			foreach ($queryPLY->result() as $rowPLY):
				$dataPLYtemp=Array();
				$dataVAL=Array();
				if ($rowPLY->valori!="")
					$dataVAL=unserialize($rowPLY->valori);
				$dataPLYtemp['file_name']=$rowPLY->nome_file;
				$dataPLY=$dataPLYtemp+$dataVAL;
				$dataWID['content'][]=$dataPLY;
			endforeach;

		elseif ($data['tipologia_playlist']==50):

		endif;

		return $dataWID;
	}

	function playlist_generica_cancella($data) {
		
		$this->db->from('playlist_generica_indice');
		$this->db->where('id', $data['id_playlist']);
		$this->db->where('id_account', $data['id_account']);
		$query=$this->db->get();
		if ($query->num_rows()>0):
		
			$this->db->where('id', $data['id_playlist']);
			$this->db->where('id_account', $data['id_account']);
			$this->db->delete('playlist_generica_indice');

			$this->db->where('id_playlist', $data['id_playlist']);
			$this->db->where('id_account', $data['id_account']);
			$this->db->delete('playlist_generica_elementi');
			
			$this->db->where('id_playlist', $data['id_playlist']);
			$this->db->delete('layout_playlist_widget');
			
		endif;
	}

	/*
	function playlist_old($data) {
	
		error_reporting(E_ERROR);
	
		$sequenza_valore_1=Array();
		$sequenza_valore_2=Array();
		$file_id=Array();
	
		if (!isset($data['giorno']))
			$data['giorno']=0;
	
		$device_id=$data['id_dispositivo'];
		$giorno=$data['giorno'];
		$yy=0;
		
		// echo $device_id."<br><br>";
		
		$this->db->from('playlist');
		$this->db->where('id_dispositivo', $device_id);
		$this->db->where('giorno', $giorno);
		if (isset($data['id_account']))
			$this->db->where('id_account', $data['id_account']);
		$queryPL=$this->db->get();
		if ($queryPL->num_rows()==0):
			// non esiste alcuna playlist definita
			$stringa_casuale="0";
			$orario_download_inizio="00:00";
			$orario_download_fine="00:01";
		else:
			$rowPL=$queryPL->row();
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $rowPL->id_account);
			$queryGRP=$this->db->get();
			$rowGRP=$queryGRP->row();
		
			$stringa_casuale=$rowPL->id_account."_".$rowGRP->ugrp_stringa;
		
			$url_archivio="/strimy/users/user_".$stringa_casuale."/content";
			// esiste una playlist
			$rowPL=$queryPL->row();
			$id_playlist=$rowPL->id;
			$this->db->from('playlist_sequenze');
			$this->db->where('id_playlist', $id_playlist);
			$this->db->order_by('inizio', 'asc');
			$this->db->order_by('posizione', 'asc');
			$queryPLS=$this->db->get();
			foreach ($queryPLS->result() as $rowPLS):
			
				if ($rowPLS->tipologia=="download"):
					$orario_download_inizio=date('H:i', $rowPLS->inizio);
					$orario_download_fine=date('H:i', ($rowPLS->inizio+60));
				elseif ($rowPLS->tipologia!="username"):
					$id_sequenza[$yy]=$rowPLS->id;
					$sequenza[$yy]=$rowPLS->tipologia;
					$titolo_sequenza[$yy]=$rowPLS->testo;
					$attiva_sequenza[$yy]=$rowPLS->attiva;
					$sequenza_inizio[$yy]=date('H:i', $rowPLS->inizio);
					$sequenza_fine[$yy]=date('H:i', $rowPLS->fine);

					$this->db->select('playlist_elementi.id as id, playlist_elementi.durata as durata, playlist_elementi.id_file as id_file, playlist_elementi.file as file, playlist_elementi.durata as durata, playlist_elementi.sovraimpressione as sovraimpressione, playlist_elementi.sovraimpressione_messaggio as sovraimpressione_messaggio, playlist_elementi.sovraimpressione_posizione_messaggio as sovraimpressione_posizione_messaggio, playlist_elementi.descrizione_file as descrizione_file, playlist_elementi.jingle as jingle, archivio_radio.jingle as jingle_archivio, archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.anno_pubblicazione as anno_pubblicazione, archivio_radio.durata as durata_brano');
					$this->db->from('playlist_elementi');
					$this->db->join('archivio_radio', 'archivio_radio.id=playlist_elementi.id_file');
					$this->db->where('playlist_elementi.id_playlist', $id_playlist);
					$this->db->where('playlist_elementi.id_sequenza', $rowPLS->id);
					$this->db->order_by('playlist_elementi.posizione', 'asc');
					$queryPLE=$this->db->get();
					
					$xx=0;
					foreach ($queryPLE->result() as $rowPLE):
						$elemento_id[$yy][$xx]=$rowPLE->id;
						$sequenza_valore_1[$yy][$xx]=$rowPLE->file;
						$sequenza_valore_2[$yy][$xx]=$rowPLE->durata;
						$sovraimpressione[$yy][$xx]=$rowPLE->sovraimpressione;
						$sovraimpressione_messaggio[$yy][$xx]=$rowPLE->sovraimpressione_messaggio;
						$sovraimpressione_posizione_messaggio[$yy][$xx]=$rowPLE->sovraimpressione_posizione_messaggio;
						$file_id[$yy][$xx]=$rowPLE->id_file;
						$file_descrizione[$yy][$xx]=$rowPLE->descrizione_file;
						$file_jingle[$yy][$xx]=$rowPLE->jingle;
						$file_jingle_archivio[$yy][$xx]=$rowPLE->jingle_archivio;
						$file_artista[$yy][$xx]=$rowPLE->artista;
						$file_titolo[$yy][$xx]=$rowPLE->titolo;
						$file_durata[$yy][$xx]=$rowPLE->durata_brano;
						$file_anno_pubblicazione[$yy][$xx]=$rowPLE->anno_pubblicazione;
						
						if ($sequenza[$yy]=="immagini"):
							$file_anteprima[$yy][$xx]=$url_archivio."/".$rowPLE->file;
						else:
							$file_anteprima[$yy][$xx]="/style/icone/archive_video.jpg";
						endif;
						$xx++;
					endforeach;
					$totale_elementi_sequenza[$yy]=$xx;

					if (($sequenza[$yy]=="radio") || (($sequenza[$yy]=="musictv"))):
						// è una sequenza radio in cui bisogna inserire i jingle
						$this->db->from('playlist_sequenze_jingle');
						$this->db->where('id_playlist', $id_playlist);
						$this->db->where('id_sequenza', $rowPLS->id);
						$queryJIN=$this->db->get();
						foreach ($queryJIN->result() as $rowJIN):
						
							$dataJIN['file']=$sequenza_valore_1[$yy];
							$dataJIN['id_file']=$elemento_id[$yy];
							$dataJIN['jingle_categoria']=$rowJIN->jingle_categoria;
							$dataJIN['jingle_intervallo']=$rowJIN->jingle_intervallo;
							$dataELERET=$this->mp3_model->inserisci_jingle($dataJIN);
							$sequenza_valore_1[$yy]=$dataELERET['file'];
							$elemento_id[$yy]=$dataELERET['id_file'];
							$totale_elementi_sequenza[$yy]=$dataELERET['numero_elementi'];
						
						endforeach;
						
					endif;
				
					$yy++;
				
				endif;
				
			endforeach;
			
		endif;
		$totale_sequenze=$yy;		
		
		$yy=0;
		$zz=4;
		$riga[0]="<username>;".$stringa_casuale.";0";
		$riga[1]="</username>;0;0";
		$riga[2]="<download>;".$orario_download_inizio.";".$orario_download_fine;
		$riga[3]="</download>;0;0";
		while ($yy<$totale_sequenze) {
			
			if ($attiva_sequenza[$yy]==1):
			
				$mostra_sequenza[$yy]=0;
				$tipologia=$sequenza[$yy];
				$tag_apertura="";
				$tag_chiusura="";
				if ($tipologia=="video"):
					$tag_apertura="<video>";
					$tag_chiusura="</video>";
				elseif ($tipologia=="immagini"):
					$tag_apertura="<slideshow>";
					$tag_chiusura="</slideshow>";
				elseif ($tipologia=="audio"):
					$tag_apertura="<music>";
					$tag_chiusura="</music>";
				elseif ($tipologia=="radio"):
					$tag_apertura="<radio>";
					$tag_chiusura="</radio>";
				elseif ($tipologia=="musictv"):
					$tag_apertura="<musictv>";
					$tag_chiusura="</musictv>";
				elseif ($tipologia=="on"):
					$tag_apertura="<on>";
					$tag_chiusura="</on>";
					$mostra_sequenza[$yy]=1;
				elseif ($tipologia=="off"):
					$tag_apertura="<off>";
					$tag_chiusura="</off>";
					$mostra_sequenza[$yy]=1;
				else: // se la tipologia è sconosciuta, ignora la sequenza
					$totale_elementi_sequenza[$yy]=0;
				endif;
				$xx=0;
				$ff=0;

				if ($mostra_sequenza[$yy]==1) $numero_elementi=1;	// include la sequenza anche se è vuota (es. sequenze "ON" o "OFF")
				else
					$numero_elementi=$totale_elementi_sequenza[$yy];

				while ($xx<($numero_elementi)) {
					if ($xx==0): // è il primo elemento della sequenza
						$riga[$zz]=$tag_apertura.";".$sequenza_inizio[$yy].";".$sequenza_fine[$yy];
						
						$dataDUR['durata']=$sequenza_inizio[$yy];
						$dataDURET=$this->mp3_model->durata_secondi($dataDUR);
						$inizio_previsto=$dataDURET['durata_secondi'];
						$zz++;
					endif;
					if ($totale_elementi_sequenza[$yy]>0):	// elenca gli elementi solo se sono effettivamente presenti
						if ( (!isset($elemento_durata[$yy][$xx])) || (!is_numeric($elemento_durata[$yy][$xx])) ) $elemento_durata[$yy][$xx]=0;
						
						// $elemento_iniziale è il primo elemento di ogni array di riga; di default è un numero e non viene utilizzato, altrimenti può indicare un messaggio, un'overlay e così via
						$elemento_iniziale=$ff+1;
						
						
						if (!isset($sequenza_valore_2[$yy][$xx])) $sequenza_valore_2[$yy][$xx]=0;
						// se si tratta di una sequenza radio, il primo elemento della riga è l'orario previsto di riproduzione
						if (($tag_apertura=="<radio>") || ($tag_apertura=="<musictv>")):
							$dataDUR['durata']=$inizio_previsto;
							$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
							$elemento_iniziale=$dataDURET['durata_formattata'];
							$inizio_previsto=$inizio_previsto+$file_durata[$yy][$xx];

							$file_artista[$yy][$xx]=trim($file_artista[$yy][$xx]);
							$file_titolo[$yy][$xx]=trim($file_titolo[$yy][$xx]);

							// se il file è catalogato come jingle, ma è specificato il nome dell'artista e il titolo,
							// allora viene interpretato come file normale
							if ( ($file_jingle_archivio[$yy][$xx]==1) && ($file_artista[$yy][$xx]!="") && ($file_titolo[$yy][$xx]!="") ):
								$file_jingle_archivio[$yy][$xx]=0;
								if ($file_anno_pubblicazione[$yy][$xx]!=0):
									$file_titolo[$yy][$xx]=$file_titolo[$yy][$xx]." (".$file_anno_pubblicazione[$yy][$xx].")";
								endif;
							endif;
							
							if ($file_jingle_archivio[$yy][$xx]==0):
								$sequenza_valore_2[$yy][$xx]="0#".$file_artista[$yy][$xx]."#".$file_titolo[$yy][$xx];
							else:
								$sequenza_valore_2[$yy][$xx]="1#-#-";
							endif;

						endif;
						
						if ($sovraimpressione[$yy][$xx]==1):	// c'è un messaggio
							$elemento_iniziale="%".$sovraimpressione_posizione_messaggio[$yy][$xx]."_".$sovraimpressione_messaggio[$yy][$xx];
						endif;
						$riga[$zz]=$elemento_iniziale.";".$sequenza_valore_1[$yy][$xx].";".$sequenza_valore_2[$yy][$xx];
						$zz++;
						$ff++;
					endif;
					if ($xx==($numero_elementi-1)): // è l'ultimo elemento della sequenza
						$riga[$zz]=$tag_chiusura.";_;1";
						$zz++;
					endif;
					$xx++;
				}
				endif;
			$yy++;
		}

		$totale_righe=count($riga);

		$dataRET['totale_sequenze']=$totale_sequenze;
		$dataRET['totale_elementi_sequenza']=$totale_elementi_sequenza;
		$dataRET['id_sequenza']=$id_sequenza;
		$dataRET['tipologia_sequenza']=$sequenza;
		$dataRET['titolo_sequenza']=$titolo_sequenza;
		$dataRET['attiva_sequenza']=$attiva_sequenza;
		$dataRET['sequenza_inizio']=$sequenza_inizio;
		$dataRET['sequenza_fine']=$sequenza_fine;
		$dataRET['elemento_id']=$elemento_id;
		$dataRET['file_nome']=$sequenza_valore_1;
		$dataRET['file_durata']=$sequenza_valore_2;
		$dataRET['file_id']=$file_id;
		$dataRET['file_descrizione']=$file_descrizione;
		
		$dataRET['sovraimpressione']=$sovraimpressione;
		$dataRET['sovraimpressione_messaggio']=$sovraimpressione_messaggio;
		$dataRET['sovraimpressione_posizione_messaggio']=$sovraimpressione_posizione_messaggio;
		
		$dataRET['file_anteprima']=$file_anteprima;
		$dataRET['stringa_casuale']=$stringa_casuale;
		$dataRET['orario_download']=$orario_download_inizio;
		$dataRET['riga']=$riga;
		$dataRET['totale_righe']=$totale_righe;
		
		return $dataRET;
	
	}
	*/
	
	function playlist_salva($data) {

		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		if ($grupposelezionato==0) die('logged out');
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $grupposelezionato);
		$queryUT=$this->db->get();
		$rowUT=$queryUT->row();
		$stringa_utente=$grupposelezionato."_".$rowUT->ugrp_stringa;

		// verifica che non ci siano doppioni della playlist associati ad account sbagliato
		$this->db->from('playlist');
		$this->db->where('id_dispositivo', $data['device_id']);
		$this->db->where('id_account !=', $data['grupposelezionato']);
		$queryPC=$this->db->get();
		foreach ($queryPC->result() as $rowPC):
			$this->db->where('id', $rowPC->id);
			$this->db->delete('playlist');
			$this->db->where('id_playlist', $rowPC->id);
			$this->db->delete('playlist_sequenze');
			$this->db->where('id_playlist', $rowPC->id);
			$this->db->delete('playlist_elementi');
		endforeach;

		$this->db->from('playlist');
		$this->db->where('id_dispositivo', $data['device_id']);
		$this->db->where('giorno', $data['giorno']);
		// $this->db->where('id_account', $grupposelezionato);
		$queryPL=$this->db->get();
		if ($queryPL->num_rows()==1):
			$nuova_playlist=0;
			$rowPL=$queryPL->row();
			$id_playlist=$rowPL->id;
		else:
			$nuova_playlist=1;
			$dataPL['id_dispositivo']=$data['device_id'];
			$dataPL['id_account']=$data['grupposelezionato'];
			$dataPL['giorno']=$data['giorno'];
			$this->db->insert('playlist', $dataPL);
			$id_playlist=$this->db->insert_id();
		endif;
	
		
		// riordina le sequenze secondo l'orario di inizio			
		if ($data['numero_sequenze']>0)
			array_multisort($data['inizio_timestamp'], SORT_ASC, $data['fine_timestamp'], $data['titolo_sequenza'], $data['attiva_sequenza'], $data['reset_sequenza'], $data['inizio_sequenza'], $data['fine_sequenza'], $data['id_sequenza'], $data['sequenza_tipologia'], $data['sequenza_jingle_categoria'], $data['sequenza_jingle_intervallo'], $data['elementi_sequenza'], $data['id_elemento'], $data['elemento_durata'], $data['elemento_file'], $data['elemento_file_id'], $data['elemento_cancella'], $data['sovraimpressione'], $data['sovraimpressione_messaggio'], $data['sovraimpressione_posizione_messaggio']);

		
		
		/////////////////////////////////////////
		// aggiorna il database
		/////////////////////////////////////////

		// se ci sono delle sequenze cancellate, procede alla loro eliminazione
		$yy=0;
		while ($yy<$data['numero_sequenze_iniziali']) {
			if ($data['sequenza_cancella'][$yy]==1):
				$this->db->where('id', $data['id_sequenza_temp'][$yy]);
				$this->db->where('id_playlist', $id_playlist);
				$this->db->delete('playlist_sequenze');
					
				$this->db->where('id_sequenza', $data['id_sequenza_temp'][$yy]);
				$this->db->where('id_playlist', $id_playlist);
				$this->db->delete('playlist_elementi');
			endif;
			$yy++;
		}
		
		
		$dataSEQ['id_playlist']=$id_playlist;
		$dataSEQ['tipologia']="username";
		$dataSEQ['testo']=$stringa_utente;
		$dataSEQ['id_dispositivo']=$data['device_id'];
		$dataSEQ['giorno']=$data['giorno'];
		$dataSEQ['attiva']=1;
		$this->db->from('playlist_sequenze');
		$this->db->where('id_playlist', $id_playlist);
		$this->db->where('tipologia', 'username');
		$this->db->where('giorno', $data['giorno']);
		$queryCK=$this->db->get();
		if ($queryCK->num_rows()==0):
		else:
			$this->db->where('id_playlist', $id_playlist);
			$this->db->where('tipologia', 'username');
			$this->db->where('giorno', $data['giorno']);
			$this->db->update('playlist_sequenze', $dataSEQ);
		endif;
		unset($dataSEQ);
		
		$dataSEQ['id_playlist']=$id_playlist;
		$dataSEQ['tipologia']="download";
		$dataSEQ['inizio']=$data['download_timestamp'];
		$dataSEQ['id_dispositivo']=$data['device_id'];
		$dataSEQ['giorno']=$data['giorno'];
		$dataSEQ['attiva']=1;
		$this->db->from('playlist_sequenze');
		$this->db->where('id_playlist', $id_playlist);
		$this->db->where('tipologia', 'download');
		$this->db->where('giorno', $data['giorno']);
		$queryCK=$this->db->get();
		if ($queryCK->num_rows()==0):
			$this->db->insert('playlist_sequenze', $dataSEQ);
		else:
			$this->db->where('id_playlist', $id_playlist);
			$this->db->where('tipologia', 'download');
			$this->db->where('giorno', $data['giorno']);
			$this->db->update('playlist_sequenze', $dataSEQ);
		endif;
		unset($dataSEQ);
		
		$yy=0;
		while ($yy<$data['numero_sequenze']) {
			
			$dataSEQ['id_playlist']=$id_playlist;
			$dataSEQ['id_dispositivo']=$data['device_id'];
			$dataSEQ['giorno']=$data['giorno'];
			$dataSEQ['tipologia']=$data['sequenza_tipologia'][$yy];
			$dataSEQ['inizio']=$data['inizio_timestamp'][$yy];
			$dataSEQ['fine']=$data['fine_timestamp'][$yy];
			$dataSEQ['testo']=$data['titolo_sequenza'][$yy];
			$dataSEQ['attiva']=$data['attiva_sequenza'][$yy];
			// $dataSEQ['jingle_categoria']=$data['sequenza_jingle_categoria'][$yy];
			// $dataSEQ['jingle_intervallo']=$data['sequenza_jingle_intervallo'][$yy];
			///////////////////////////////
			
			
			$dataSEQ['id_playlist']=$id_playlist;
			$dataSEQ['id_dispositivo']=$data['device_id'];
			$dataSEQ['giorno']=$data['giorno'];
			$dataSEQ['tipologia']=$data['sequenza_tipologia'][$yy];
			$dataSEQ['inizio']=$data['inizio_timestamp'][$yy];
			$dataSEQ['fine']=$data['fine_timestamp'][$yy];
			$dataSEQ['testo']=$data['titolo_sequenza'][$yy];
			$dataSEQ['attiva']=$data['attiva_sequenza'][$yy];
			// $dataSEQ['jingle_categoria']=$data['sequenza_jingle_categoria'][$yy];
			// $dataSEQ['jingle_intervallo']=$data['sequenza_jingle_intervallo'][$yy];
			$dataSEQ['posizione']=$yy;
			if ($data['id_sequenza'][$yy]==0):
				$this->db->insert('playlist_sequenze', $dataSEQ);
				$data['id_sequenza'][$yy]=$this->db->insert_id();
			else:
				$this->db->where('id', $data['id_sequenza'][$yy]);
				$this->db->where('id_playlist', $id_playlist);
				$this->db->update('playlist_sequenze', $dataSEQ);
			endif;
			unset($dataSEQ);

			// inserisce i jingle associati alla sequenza
			$ff=0;
			foreach ($data['sequenza_jingle_categoria'][$yy] as $jingle_categoria_temp):
			//	echo "Jingle ".$ff." ";
			//	echo $data['sequenza_jingle_id'][$yy][$ff]." ".$data['sequenza_jingle_categoria'][$yy][$ff]." ".$data['sequenza_jingle_intervallo'][$yy][$ff]." ";
			
				$dataJIN['id_playlist']=$id_playlist;
				$dataJIN['id_dispositivo']=$data['device_id'];
				$dataJIN['giorno']=$data['giorno'];
				$dataJIN['id_sequenza']=$data['id_sequenza'][$yy];
				$dataJIN['jingle_categoria']=$data['sequenza_jingle_categoria'][$yy][$ff];
				$dataJIN['jingle_intervallo']=$data['sequenza_jingle_intervallo'][$yy][$ff];
				if ($data['sequenza_jingle_id'][$yy][$ff]==0):
					$this->db->insert('playlist_sequenze_jingle', $dataJIN);
				elseif ($data['sequenza_jingle_id'][$yy][$ff]!=0):
					$this->db->where('id', $data['sequenza_jingle_id'][$yy][$ff]);
					$this->db->update('playlist_sequenze_jingle', $dataJIN);
				endif;

				$ff++;
			endforeach;
			
			// procede con l'inserimento dei singoli elementi
			if ($data['reset_sequenza'][$yy]==1):
				// la sequenza è stata resettata (es. una playlist musicale è stata generata da zero)
				// quindi tutti i suoi elementi, anziché essere aggiornati, saranno eliminati e salvati di nuovo
				$this->db->where('id_sequenza', $data['id_sequenza'][$yy]);
				$this->db->delete('playlist_elementi');
			endif;
			
			$numero_elementi=$data['elementi_sequenza'][$yy];
			$xx=0;
			while ($xx<($numero_elementi)) {
				
				if (!isset($data['elemento_cancella'][$yy][$xx]))
					$data['elemento_cancella'][$yy][$xx]=1;
				// echo $elemento_cancella[$yy][$xx]."<br>";
				
				if ($data['elemento_cancella'][$yy][$xx]==0):
				
					if (!isset($data['jingle'][$yy][$xx]))
						$data['jingle'][$yy][$xx]=0;
				
					$dataELE['id_playlist']=$id_playlist;
					$dataELE['id_sequenza']=$data['id_sequenza'][$yy];
					$dataELE['file']=$data['elemento_file'][$yy][$xx];
					$dataELE['id_file']=$data['elemento_file_id'][$yy][$xx];
					$dataELE['jingle']=$data['jingle'][$yy][$xx];
					$dataELE['descrizione_file']=$data['elemento_file'][$yy][$xx];
					$dataELE['id_dispositivo']=$data['device_id'];
					$dataELE['giorno']=$data['giorno'];
					if (!isset($data['sovraimpressione'][$yy][$xx])) $data['sovraimpressione'][$yy][$xx]=0;
					if ($data['sovraimpressione'][$yy][$xx]==1):
						$dataELE['sovraimpressione']=$data['sovraimpressione'][$yy][$xx];
						$dataELE['sovraimpressione_messaggio']=$data['sovraimpressione_messaggio'][$yy][$xx];
						$dataELE['sovraimpressione_posizione_messaggio']=$data['sovraimpressione_posizione_messaggio'][$yy][$xx];
					endif;
					if (!isset($data['elemento_durata'][$yy][$xx])) $data['elemento_durata'][$yy][$xx]=0;
					$dataELE['durata']=$data['elemento_durata'][$yy][$xx];
					$dataELE['tipologia']=$data['sequenza_tipologia'][$yy];
					$dataELE['posizione']=$xx;
					
					if ($data['id_elemento'][$yy][$xx]==0):
						// echo $xx."<br>";
						$this->db->insert('playlist_elementi', $dataELE);
					else:
						$this->db->where('id', $data['id_elemento'][$yy][$xx]);
						$this->db->where('id_playlist', $id_playlist);
						$this->db->update('playlist_elementi', $dataELE);
					endif;
					unset($dataELE);
					
				else:	// cancella l'elemento della sequenza
					if (!isset($data['id_elemento'][$yy][$xx])) $data['id_elemento'][$yy][$xx]=0;
					$this->db->where('id', $data['id_elemento'][$yy][$xx]);
					$this->db->where('id_playlist', $id_playlist);
					$this->db->delete('playlist_elementi');
				endif;
				
				$xx++;
			}
			
			$yy++;
		}

		// playlist salvata correttamente
			
	}
	
	
	function check_dispositivo($data) {
		
		$file=$data['file'];
		
		$orario_attuale=time();

		$errore=0;
		$intervallo_secondi=0;
		$status_raw="";
		$messaggio="";
		
		if (!file_exists($file)):
			// il file non esiste
			$errore=1;
			$messaggio="Dispositivo non connesso o in attesa di attivazione";
		else:

			$xx=0;
			$fp = fopen($file, 'r+');
			// fseek($fp, 10, SEEK_SET); //Mi posiziono al 10° carattere
			while(!feof($fp))
			{
				$riga_temp=fgets($fp);
				$riga[$xx]=explode(";", $riga_temp);
				$xx++;
			}
			fclose($fp);
			$intervallo_secondi=$orario_attuale-$riga[0][0];
			if (!isset($riga[0][1])) $riga[0][1]="";
			$status=$riga[0][1];

			$status_raw=explode("_", $status);
			$status_raw=trim($status_raw[0]);

			$intervallo=$intervallo_secondi;

			if ($intervallo>120):
				if ($intervallo>86400)
					$intervallo="più di 1 giorno fa";
				elseif ($intervallo>3600)
					$intervallo="più di 1 ora fa";
				elseif ($intervallo>900)
					$intervallo="più di 15 minuti fa";
				else
					$intervallo=$intervallo." secondi fa";
				$messaggio="ATTENZIONE: dispositivo non attivo? (ultimo check: ".$intervallo.")";
				
			else:
				$messaggio="stato: ".$status;
			endif;
		endif;	
		
		$dataRESULT['messaggio']=$messaggio;
		$dataRESULT['intervallo']=$intervallo_secondi;
		$dataRESULT['status_raw']=$status_raw;
	
		return $dataRESULT;
		
	}
	
	function descrizione_dispositivo($id_dispositivo) {
		if ($id_dispositivo<0):
			$this->db->from('dispositivi_gruppi');
			$this->db->where('id', ($id_dispositivo*-1));
			$query=$this->db->get();
			$row=$query->row();
			$descrizione="gruppo ".$row->descrizione;
		else:
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $id_dispositivo);
			$query=$this->db->get();
			$row=$query->row();
			if (trim($row->descrizione=="")):
				$descrizione=$row->id_dispositivo;
			else:
				$descrizione=$row->descrizione;
			endif;
		endif;
		return $descrizione;
	}
	
	function file_upload($data) {
		// funzione di caricamento del file
		// a seconda delle impostazioni dell'account, caricherà il file nello storage locale o nel bucket remoto
		// nella directory specificata
		
		error_reporting(E_ALL);
		
		if (!isset($data['grupposelezionato']))
			$grupposelezionato=$this->accountselezionato();	
		else
			$grupposelezionato=$data['grupposelezionato'];

		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
		
		$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);

		if ($dataSTORAGE['storage']==0):
			// il parametro "copia" indica l'origine del file
			// se è un upload diretto viene usato il metodo move_uploaded_file
			// se il file viene elaborato in un passaggio intermedio (es. resize di un'immagine) viene usato il metodo copy e il file viene poi eliminato
			if (!isset($data['copia']))
				$data['copia']=0;
			$file=$dataSTORAGE['real_path'].$stringa_casuale."/".$data['cartella']."/".$data['nome_del_file'];

			// echo $data['file_da_caricare']." ---- ".$file."<br>";
			
			if ($data['copia']==0):
				// sposta il file caricato dalla directory temporanea a quella definitiva
				move_uploaded_file(realpath($data['file_da_caricare']), $file);
			else:
				chmod(realpath($data['file_da_caricare']), 0777);
				copy(realpath($data['file_da_caricare']), $file);
				unlink(realpath($data['file_da_caricare']));
			endif;
			chmod(realpath($file), 0777);
		elseif ($dataSTORAGE['storage']==1):
			$this->load->library('s3');
			$this->s3->putObjectFile($data['file_da_caricare'], $dataSTORAGE['bucket'], "users/".$stringa_casuale."/".$data['cartella']."/".$data['nome_del_file']);
		elseif ($dataSTORAGE['storage']==2):
			$this->load->library('google');
			$this->google->upload_file($data['file_da_caricare'], $dataSTORAGE['bucket'], "users/".$stringa_casuale."/".$data['cartella']."/".$data['nome_del_file']);
		endif;	
		
	}

	function file_cancella($data) {
		// funzione di cancellazione del file
		// a seconda delle impostazioni dell'account, eliminerà il file dallo storage locale o dal bucket remoto
		
		if (!isset($data['grupposelezionato']))
			$grupposelezionato=$this->accountselezionato();	
		else
			$grupposelezionato=$data['grupposelezionato'];

		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
		
		$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);

		if ($dataSTORAGE['storage']==0):
			$file=$dataSTORAGE['real_path'].$stringa_casuale."/".$data['cartella']."/".$data['file_da_cancellare'];
			if (file_exists($file)):
				unlink($file);
			endif;
		elseif ($dataSTORAGE['storage']==1):
			$this->load->library('s3');
			$this->s3->deleteObject($dataSTORAGE['bucket'], "users/".$stringa_casuale."/".$data['cartella']."/".$data['file_da_cancellare']);
		elseif ($dataSTORAGE['storage']==2):
			$this->load->library('google');
			$this->google->delete_file($dataSTORAGE['bucket'], "users/".$stringa_casuale."/".$data['cartella']."/".$data['file_da_cancellare']);
		endif;
		
	}

	function aggiorna_layout_indice_dispositivo($data) {
		// questa funzione aggiorna i dati nella tabella layout_indice, che, per ogni dispositivo,
		// riporta l'orario in cui è stata apportata l'ultima modifica diretta o indiretta
		// all'array del palinsesto
		$dataIND=Array();
		if (isset($data['ultima_modifica']))
			$dataIND['ultima_modifica']=$data['ultima_modifica'];
		if (isset($data['ultima_modifica_manuale']))
			$dataIND['ultima_modifica_manuale']=$data['ultima_modifica_manuale'];
		if (isset($data['programmazione_personalizzata']))
				$dataIND['programmazione_personalizzata']=$data['programmazione_personalizzata'];
			
		$this->db->from('layout_indice');
		$this->db->where('id_dispositivo', $data['id_dispositivo']);
		$queryIND=$this->db->get();
		if ($queryIND->num_rows()==0):
			$dataIND['id_dispositivo']=$data['id_dispositivo'];
			$this->db->insert('layout_indice', $dataIND);
		else:
			$this->db->where('id_dispositivo', $data['id_dispositivo']);
			$this->db->update('layout_indice', $dataIND);
		endif;	
	}

	function aggiorna_dispositivi_associati($data) {
		// Quando viene eliminato un file o modificata/eliminata una playlist, è necessario verificare quali siano i dispositivi interessati
		// (ovvero quelli in cui il file o la playlist erano in programmazione) e aggiornare il relativo timestamp layout_indice.ultima_modifica
		if (!isset($data['id_account']))
			$data['id_account']=$this->accountselezionato();

		$timestamp=time();
		if ( (isset($data['id_file'])) || (isset($data['id_playlist'])) ):
		
			if (isset($data['id_file'])):
				$this->db->select('playlist_generica_elementi.id_playlist, layout_playlist_widget.id_dispositivo');
				$this->db->from('playlist_generica_elementi');
				$this->db->where('playlist_generica_elementi.id_file', $data['id_file']);
				$this->db->where('playlist_generica_elementi.id_account', $data['id_account']);
				$this->db->join('layout_playlist_widget', 'layout_playlist_widget.id_playlist=playlist_generica_elementi.id_playlist');
				$this->db->group_by('layout_playlist_widget.id_dispositivo');
			elseif (isset($data['id_playlist'])):
				$this->db->select('id_dispositivo');
				$this->db->from('layout_playlist_widget');
				$this->db->where('id_playlist', $data['id_playlist']);
				$this->db->group_by('id_dispositivo');
			endif;
			$query=$this->db->get();
			foreach ($query->result() as $row):
				$dataUMO['ultima_modifica']=$timestamp;
				$this->db->where('id_dispositivo', $row->id_dispositivo);
				$this->db->update('layout_indice', $dataUMO); 
			endforeach;
		endif;
	}

	// converte un colore da formato HEX a RGB
	function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
		  $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		  $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		  $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
		  $r = hexdec(substr($hex,0,2));
		  $g = hexdec(substr($hex,2,2));
		  $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = $r."-".$g."-".$b."-1";
	   return $rgb; // returns a string with the rgb values
	}

	// converte un colore da formato RGB a HEX
	function rgb2hex($rgb) {
		$rgb=explode("-", $rgb);
		$hex = "#";
		$hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
		$hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
		$hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

		return $hex; // returns the hex value including the number sign (#)
	}

}

?>
