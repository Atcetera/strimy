<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
set_include_path(APPPATH . 'third_party/' . PATH_SEPARATOR . get_include_path());
require_once APPPATH . 'third_party/Google/autoload.php';

define('GAPI_CLIENT_ID', '365306327730-9ef0l108l0lbjlc7bmqgp76kriipij45.apps.googleusercontent.com');
define('GAPI_EMAIL_ADDRESS', '365306327730-9ef0l108l0lbjlc7bmqgp76kriipij45@developer.gserviceaccount.com');
define('GAPI_API_KEY', 'AIzaSyANwSEVvp6-S9kebR_FNjEdYD4kPnUK4zo');
define('GAPI_PROJECT_ID', 'strimy-995');
define('KEY_FILE_LOCATION', APPPATH.'third_party/google_passphrase.p12');


class Google extends Google_Client {

    function __construct($params = array()) {
        parent::__construct();
    }


	public function google_connect() {

		$google_cert = new Google_Auth_AssertionCredentials(
			GAPI_EMAIL_ADDRESS,
			Google_Service_Storage::DEVSTORAGE_FULL_CONTROL,
			file_get_contents(KEY_FILE_LOCATION)
		);
		 
		$google_client = new Google_Client();
		$google_client->setAssertionCredentials($google_cert);
		$google_client->setDeveloperKey(GAPI_API_KEY);
		 
		$service = new Google_Service_Storage($google_client);

		$data['service']=$service;
		$data['client']=$google_client;
		return $data;

		}


	public function list_buckets() {
		$service=$this->google_connect();
		$list = $service->buckets->listBuckets(GAPI_PROJECT_ID);
		print_r($list);
		}


	public function upload_file($local_file, $bucket, $remote_file) {

		error_reporting(E_ALL);

		$dataRET=$this->google_connect();
		$client=$dataRET['client'];
		$service=$dataRET['service'];
		
		$obj = new Google_Service_Storage_StorageObject();

		$obj->setName($remote_file);
		$obj->setBucket($bucket);

		$mimetype = mime_content_type($local_file);

		$chunkSizeBytes = 1 * 1024 * 1024;
		$client->setDefer(true);
		$status = false;

		$filetoupload = array('name' => $remote_file, 'uploadType' => 'resumable');

		$request = $service->objects->insert($bucket,$obj,$filetoupload);

		$media = new Google_Http_MediaFileUpload($client, $request, $mimetype, null, true, $chunkSizeBytes);
		$media->setFileSize(filesize($local_file));
		$handle = fopen($local_file, "rb");

		while (!$status && !feof($handle)) {
			$chunk = fread($handle, $chunkSizeBytes);
			$status = $media->nextChunk($chunk);
		}

		$result = false;
		if($status != false) {
			$result = $status;
		}

		fclose($handle);
		$client->setDefer(false);
		
	}


	public function delete_file($bucket, $remote_file) {
		
		error_reporting(E_ALL);

		$dataRET=$this->google_connect();
		$client=$dataRET['client'];
		$service=$dataRET['service'];

		$request = $service->objects->delete($bucket,$remote_file);

		$client->setDefer(false);
		
	}


	/*
	public function upload_file_back($local_file, $bucket, $remote_file) {

		error_reporting(E_ALL);

		$dataRET=$this->google_connect();
		$client=$dataRET['client'];
		$service=$dataRET['service'];

		$obj = new Google_Service_Storage_StorageObject();
		$obj->setName($remote_file);
		$obj->setContentType('media');
		$obj->setSize(filesize($local_file));

		$dataFILE['data']=file_get_contents($local_file);
		$dataFILE['uploadType']="media";

		try {
		$service->objects->insert(
			$bucket,
			$obj,
			$dataFILE
			);
		}
		catch (Exception $e) {
			echo $e;
		}
	
	}
	*/


}
