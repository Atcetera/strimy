<?php
use Ratchet\Server\IoServer;
// use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\IpBlackList;

require 'websocket-server/vendor/autoload.php';


$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = 'N30l1ft';
$db['default']['database'] = 'strimy';

$connessione = mysql_connect($db['default']['hostname'],$db['default']['username'],$db['default']['password']);
mysql_select_db($db['default']['database'], $connessione);

// $connected_clients_id=Array();
$connected_clients_dispositivi=Array();


class Chat implements MessageComponentInterface {
	protected $clients;

	public function __construct() {
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn) {
		// Store the new connection to send messages to later

		global $connected_clients_dispositivi;

		$id_dispositivo = $conn->WebSocket->request->getQuery();

		$errore=0;

		// verifica che il dispositivo sia tra quelli attivati
		$query=mysql_query("SELECT * FROM dispositivi WHERE id_dispositivo='".$id_dispositivo."' AND attivato=1");
		if (($id_dispositivo=="") || (mysql_num_rows($query)==0))
			$errore=1;

		// verifica che il dispositivo non sia già connesso
		if (in_array($id_dispositivo, $connected_clients_dispositivi))
			$errore=1;

		if ($errore==0):
			$this->clients->attach($conn);
			echo "New connection! ({$conn->resourceId})\n";
			$connected_clients_dispositivi[$conn->resourceId]=$id_dispositivo;
		else:
			echo "Connessione non autorizzata ({$conn->resourceId})\n";
			$conn->close();
		endif;
	   }

	public function onMessage(ConnectionInterface $from, $msg) {
		$numRecv = count($this->clients) - 1;
		echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
		    , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

		foreach ($this->clients as $client) {
		    if ($from !== $client) {
		        // The sender is not the receiver, send to each client connected
		        $client->send($msg);
		    }
		}
	}

	public function onClose(ConnectionInterface $conn) {
		// The connection is closed, remove it, as we can no longer send it messages
		$this->clients->detach($conn);

		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}

	function get_device_id($id_dispositivo) {
		global $connected_clients_dispositivi;
		if (in_array($id_dispositivo, $connected_clients_dispositivi)):
			$id=array_search($id_dispositivo, $connected_clients_dispositivi);
			$id=$id[0];
		else:
			$id=-1;
		endif;
		return $id;
	}

	public function test() {
		echo "ciao";
	}

}


echo "Avvio server...\n";

$server = IoServer::factory(
// new HttpServer(
    new WsServer(
    	    new Chat()
    ),
// ),
9300
);

$server->run();



