<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/

// questi valori numerici identificano, per convenzione, la tipologia del file all'interno dell'applicazione
// 0: file audio (mp3)
// 1: file video (mpg, mp4...)
// 2: immagini (jpg, png...)

// configurazione path archivio interno
$config['archivio_base_url'] = 'http://storage.strimy.it/';
$config['archivio_real_path'] = '/opt/Atcetera/clienti/storage.strimy.it/htdocs/';
// configurazione directory
$config['percorso_devices'] = 'devices/';
$config['percorso_users'] = 'users/';
$config['percorso_releases'] = 'releases/';

// valori di default
$config['default_crediti']=15;
$config['default_storage']=5000*1024*1024;	// Mbyte
$config['default_privilegi'][2]=1;		// Slideshow
$config['default_privilegi'][50]=1;		// News
$config['default_privilegi'][51]=1;		// Weather
$config['default_privilegi'][52]=1;		// Logo

/// configurazione opzioni varie
$config['s3']=0;	// 0= usa hosting interno (default); 1= usa Amazon S3; 2= usa Google Cloud Storage
$config['log_dispositivi']=0;	// se =1 tiene traccia continua degli status dei dispositvi durante il giorno (utile per il debugging); ATTENZIONE: può rallentare il server e saturare il database in poco tempo

$config['nome_prodotto']=$nome_prodotto;

// limite dello spazio di archiviazione per singolo utente (in Kbyte)
$config['limite_spazio']=5000*1024;
// massima dimensione del singolo upload (in Mbyte)
$config['limite_upload']=120;

// configurazione PayPal per fatturazione
$config['paypal_account']="info@atcetera.it"; // account PayPal reale usato per ricevere pagamenti
$config['paypal_sandbox_account']="admin@civicoweb.it"; // account PayPal sandbox usato per ricevere pagamenti di test
$config['paypal_sandbox']=1; // 0: url Paypal (produzione); 1: url sandbox (test)
$config['iva']=22; // percentuale iva
$config['intestazione_1']="Atcetera sas di Matteo D'Agord e Luca Civiero & c.";
$config['intestazione_2']="Via Francia, 37 - 31033 Castelfranco Veneto TV";
$config['intestazione_3']="c.f. e p.iva: 04582860260";

$config['tipologie'][0]="admin";	
$config['tipologie'][1]="rivenditore";
$config['tipologie'][2]="cliente";

$config['genere'][0]="--";
$config['genere'][1]="rock";
$config['genere'][2]="blues";
$config['genere'][3]="pop";
$config['genere'][4]="dance";
$config['genere'][5]="lounge";
$config['genere'][6]="chillout";
$config['genere'][7]="house";

$config['chiave'][0]="--";
$config['chiave'][1]="C";
$config['chiave'][2]="D";
$config['chiave'][3]="E";
$config['chiave'][4]="F";
$config['chiave'][5]="G";
$config['chiave'][6]="A";
$config['chiave'][7]="B";

$config['power'][0]="--";
$config['power'][1]="1";
$config['power'][2]="2";
$config['power'][3]="3";
$config['power'][4]="4";
$config['power'][5]="5";

// sottotipologia file (slideshow)
$config['slideshow_tipologia'][0]="audio";
$config['slideshow_tipologia'][1]="video";
$config['slideshow_tipologia'][2]="immagini";

// gli shortcode relativi ai diversi tipi di Slideshow (audio, video, immagini)
$config['widget_slideshow_shortcode']['AudioFrame']=0;
$config['widget_slideshow_shortcode']['VideoFrame']=1;
$config['widget_slideshow_shortcode']['Slideshow']=2;

// layout predefiniti
$config['layout'][0]="Music TV";
$config['layout_shortcode'][0]="musictv";
$config['layout_widget'][0][0]="VideoFrame";
$config['layout_widget'][0][1]="News";
$config['layout_widget'][0][2]="Logo";
$config['layout_widget'][0][3]="Weather";

$config['layout'][1]="Radio in store";
$config['layout_shortcode'][1]="radioinstore";
$config['layout_widget'][1][0]="Radio";

$config['layout'][2]="Gallery fullscreen";
$config['layout_shortcode'][2]="gallery";
$config['layout_image'][2]="gallery.jpg";
$config['layout_widget'][2][0]="Slideshow";

$config['layout'][3]="Double gallery";
$config['layout_shortcode'][3]="double_gallery";
$config['layout_image'][3]="double_gallery.jpg";
$config['layout_widget'][3][0]="Slideshow";
$config['layout_widget'][3][1]="Slideshow";
$config['layout_widget'][3][2]="News";

$config['layout'][4]="Gallery";
$config['layout_shortcode'][4]="gallery_news";
$config['layout_image'][4]="gallery_news.jpg";
$config['layout_widget'][4][0]="Slideshow";
$config['layout_widget'][4][1]="News";

$config['layout'][5]="Gallery & Audio";
$config['layout_shortcode'][5]="gallery_news_audio";
$config['layout_image'][5]="gallery_news_audio.jpg";
$config['layout_widget'][5][0]="Slideshow";
$config['layout_widget'][5][1]="News";
$config['layout_widget'][5][2]="Slideshow";

$config['layout'][10000]="Homescreen";
$config['layout_visibility'][10000]=0;	// il layout è nascosto; serve per riprodurre contenuti comuni a tutti gli utenti
$config['layout_shortcode'][10000]="homescreen";
$config['layout_widget'][10000][0]="Slideshow";
$config['layout_widget'][10000][1]="Weather";
$config['layout_widget'][10000][2]="News";



// widget predefiniti
$config['widget']['Radio']=0;
$config['widget']['VideoFrame']=1;
$config['widget']['Slideshow']=2;
$config['widget']['News']=50;
$config['widget']['Weather']=51;
// $config['widget']['Logo']=52;

// widget descrizione (descrizione "umana" che apparirà a video)
$config['widget_descrizione']['Radio']="Radio in store";
$config['widget_descrizione']['VideoFrame']="Music TV";
$config['widget_descrizione']['Slideshow']="Carosello";
$config['widget_descrizione']['News']="News";
$config['widget_descrizione']['Weather']="Meteo";
$config['widget_descrizione']['Logo']="Logo";

// widget icona (apparirà a video)
$config['widget_icona']['Radio']="fa-music";
$config['widget_icona']['VideoFrame']="fa-video-camera";
$config['widget_icona']['Slideshow']="fa-photo";
$config['widget_icona']['News']="fa-newspaper-o";
$config['widget_icona']['Weather']="fa-sun-o";
$config['widget_icona']['Logo']="fa-star";

// aspect ratio predefiniti
$config['widget_aspect_ratio'][0]=0;
$config['widget_aspect_ratio_descrizione'][0]="libero";
$config['widget_aspect_ratio'][1]=16/9;
$config['widget_aspect_ratio_descrizione'][1]="16/9 (fullscreen)";
$config['widget_aspect_ratio'][2]=96/99;
$config['widget_aspect_ratio_descrizione'][2]="8/9 (con news)";
$config['widget_aspect_ratio'][3]=1920/990;
$config['widget_aspect_ratio_descrizione'][3]="16/9 (con news)";

// durata delle slide nella homescreen (in secondi)
$config['homescreen_slide_duration']=120;

