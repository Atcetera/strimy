<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mp3 extends CI_Controller {

	public function scan() {

		$percorso_base=$this->config->item('real_path');

		$percorso_users=$this->config->item('percorso_users');
		$cartella_archivio=$percorso_base.$percorso_users."comune/content";
		$url_archivio="/strimy/users/comune/content";
	
	
		echo "Elenco dei file contenuti in: ".$cartella_archivio."<br><br>";
		
		$xx=0;
		if ($handle = opendir($cartella_archivio)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != ".."):
					$file[$xx]=$entry;
					$xx++;
				endif;
			}
			closedir($handle);
		}

		$totale_file=$xx;

		$xx=0;
		while ($xx<$totale_file) {
			$dataINSERISCI['nome_file']=$file[$xx];
			$this->mp3_model->mp3_archivio_inserisci($dataINSERISCI);
		}

		unset($dataFILE);
	
		$xx=0;
		while ($xx<$totale_file) {
			$file_nomefile=$file[$xx];
			$file_nomefile=trim(preg_replace("/[^a-z0-9\.]/", "_", strtolower($file_nomefile)));
			echo $file[$xx]." ".$file_nomefile."<br>";
			if ($file[$xx]!=$file_nomefile):
				rename ($cartella_archivio."/".$file[$xx], $cartella_archivio."/".$file_nomefile);
				$dataFILE['nome_file']=$file_nomefile;
				$this->db->where('nome_file', $file[$xx]);
				$this->db->update('archivio_mp3', $dataFILE);
			endif;
			$xx++;
		}

	}

	public function gestione_file_mp3($operazione="", $id_file=0, $tipologia_file="audio") {
		$data['id_file']=$id_file;
		$data['operazione']=$operazione;
		$data['tipologia_file']=$tipologia_file;
		if ($operazione=="gestisci_brano"):
			$data['titolo']="Aggiungi/modifica brano";
		elseif ($operazione=="carica_classifica"):
			$data['titolo']="Carica classifica";
		elseif ($operazione=="gestisci_categorie"):
			$data['titolo']="Gestisci categorie jingle";
		endif;
		$this->load->view('site_header_short', $data);
		$this->load->view('radio_file_gestione', $data);
		$this->load->view('site_footer_short');
	}

	public function aggiorna_classifica() {
		
		// questi parametri indicano la posizione delle colonne ARTISTA, TITOLO, PUNTEGGIO e DATA
		// all'interno del file classifica da importare
		$campo_artista=4;
		$campo_titolo=5;
		$campo_punteggio=8;
		$campo_data=11;
		
		$errore=0;
		
		$percorso_base=$this->config->item('real_path');

		$percorso_users=$this->config->item('percorso_users');
		// $cartella_archivio=$percorso_base.$percorso_users."comune/dati";
		// $url_archivio="/strimy/users/comune/dati";
		// $file=$cartella_archivio."/".$nome_file;
		
		$percorso_users=$this->config->item('percorso_users');
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

		$cartella_base=$percorso_base.$percorso_users.$stringa_casuale;
		// $file=$cartella_base."/".$nome_file;
		
		
		if ( ($this->input->post("econferma")=="ok") && (isset($_FILES["efile"]["name"])) && ($_FILES["efile"]["name"]!="") ):

			$file_nomefile=$_FILES["efile"]["name"];
			$file_tipofile=$_FILES["efile"]["type"];
			$file_dimensione=($_FILES["efile"]["size"] / 1024);
			$file_temporaneo=$_FILES["efile"]["tmp_name"];

			$file=$_FILES["efile"]["tmp_name"];

		//	$file_nomefile=preg_replace("/[^a-z0-9\.]/", "_", strtolower($file_nomefile));

		//	echo $file_tipofile;

		
			if (($file_tipofile=="text/plain") || ($file_tipofile=="text/txt") || ($file_tipofile=="text/csv")):
				
			//	move_uploaded_file($file_temporaneo, $file);
			//	chmod($file, 0777);
			
				$errore=0;
			
			else:
			
				echo "formato errato";
			
				$errore=1;

			endif;

		else:

			$errore=1;


		endif;

		// se si sono verificati errori interrompe l'operazione, altrimenti prosegue

		if ($errore==1):

			die('<br>errore');
		
			$dataRES['risultato']=1; // file non caricato
			$dataRES['messaggio']="file non specificato o errato";
		
			$this->load->view('site_header');
			$this->load->view('gestione_musica', $dataRES);
			$this->load->view('site_footer');
		
		else:
		
			// prosegue con l'importazione

			$xx=-2;
			if (file_exists($file)):
				$fh = fopen($file, "r") or die("can't open file");
					while (!feof($fh)) {
						$riga = fgets($fh);
						
						if ($xx==-1): // è la riga di intestazione
							$intestazione=explode("|", $riga);
						endif;
						
						if (($xx>-1) && ($riga!="")):
							
							$yy=0;
							foreach ($intestazione as $intestazione_temp):
								if ($yy==0)
									$inizio=0;
								else
									$inizio=$inizio+$fine+1;
								$fine=strlen($intestazione[$yy]);
								$valori_riga[$xx][$yy]=substr($riga, $inizio, $fine);
								$valori_riga[$xx][$yy]=trim(str_replace("|", "", $valori_riga[$xx][$yy]));
								
			//					echo $valori_riga[$xx][$yy]." ";
								
								$yy++;
							endforeach;
						endif;
						
						$xx++;
				//		echo "<br>";
					}
				fclose($fh);
			else:
				echo "<b>".$file."</b> inesistente!<br>";
			endif;

			$totale_righe=$xx;
		
		//	die();
			
			$xx=0;
			$zz=0;
		
			$max_records=$this->input->post('emax_records');
			
			while (($xx<$totale_righe) && ($zz<$max_records)) {
				
				$yy=0;
				if (!isset($valori_riga[$xx])) $valori_riga[$xx]="";
				if (is_array($valori_riga[$xx])):
					
						foreach ($valori_riga[$xx] as $valori_riga_temp):
							
							$valori_riga_temp=trim($valori_riga_temp);
							
							if ($yy==$campo_data):  // è la data; filtra il record
								$datatemp=explode(" ",trim($valori_riga[$xx][$yy]));
								$datatemp_data=explode("/", $datatemp[0]);
								$datatemp_ora=explode(".", $datatemp[1]);
								$orario=mktime($datatemp_ora[0],$datatemp_ora[1],$datatemp_ora[2],$datatemp_data[1],$datatemp_data[0],$datatemp_data[2]);
								$record[$zz]=$valori_riga[$xx];
								$data_classifica[$zz]=$orario;
								$artista[$zz]=$valori_riga[$xx][$campo_artista];
								$titolo[$zz]=$valori_riga[$xx][$campo_titolo];
								$punteggio[$zz]=str_replace(",",".", $valori_riga[$xx][$campo_punteggio]);

								$zz++;
							endif;
							
							$yy++;
						endforeach;

				endif;
				$xx++;
			}
		
			$totale_record=$zz;
			
			$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('archivio_radio_classifica');
		
			$ultimo_aggiornamento=time();
		
			$xx=0;
			while ($xx<$totale_record) {
			
				$dataREC['artista']=$artista[$xx];
				$dataREC['titolo']=$titolo[$xx];
				$dataREC['punteggio']=$punteggio[$xx];
				$dataREC['data_classifica']=$data_classifica[$xx];
				$dataREC['id_account']=$grupposelezionato;
				$dataREC['ultimo_aggiornamento']=$ultimo_aggiornamento;
				$this->db->insert('archivio_radio_classifica', $dataREC);
				
				$xx++;
			}
		
		//	$dataAGGIORNA['aggiorna']=1;
			$this->mp3_model->importa_dati_classifica();
			
			$dataRES['risultato']=2; // file caricato correttamente
			$dataRES['messaggio']="Totale ".$totale_record." record importati su ".$totale_righe." analizzati";
		
			$this->load->view('site_header');
			$this->load->view('radio_gestione', $dataRES);
			$this->load->view('site_footer');	
		
		endif;
		
	}


public function aggiorna_categoria() {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$id=$this->input->post('eid_categoria');
		$descrizione=$this->input->post('edescrizione_categoria');
		$tipologia_file=$this->input->post('etipologia_file');
		$id_gruppo_dispositivo=$this->input->post('eid_gruppo_dispositivo');
		$dataCAT['descrizione']=$descrizione;
		$dataCAT['tipologia_file']=$tipologia_file;
		$dataCAT['id_gruppo_dispositivo']=$id_gruppo_dispositivo;
		if ($id==0):
			$dataCAT['id_account']=$grupposelezionato;
			$this->db->insert('archivio_radio_jingle_categorie', $dataCAT);
		else:
			$this->db->where('id', $id);
			$this->db->update('archivio_radio_jingle_categorie', $dataCAT);
		endif;
		if ($tipologia_file==0)
			$suffisso="audio";
		elseif ($tipologia_file==0)
			$suffisso="video";
		redirect('mp3/gestione_radio/'.$suffisso);
}


function elimina_categoria($id) {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$this->db->where('id', $id);
		$this->db->where('id_account', $grupposelezionato);
		$this->db->delete('archivio_radio_jingle_categorie');
		redirect('mp3/gestione_radio');
}


public function gestione_file_mp3_salva() {
		
		$errore=0;
		
		$percorso_base=$this->config->item('real_path');

		$percorso_users=$this->config->item('percorso_users');
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

		$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/radio";

		$id_file=$this->input->post('eid_file');
		
		if ($this->input->post('etipologia_file')==0)
			$stringa_confronto="audio";
		elseif ($this->input->post('etipologia_file')==1)
			$stringa_confronto="video";
		
		/*
		if ( ($this->input->post('ejingle')==0) && (($this->input->post('eartista')=="") || ($this->input->post('etitolo')=="")) ):
			$errore=1;
		endif;
		*/
		// die($errore);
		
		if ( ($errore==0) && ($this->input->post("econferma")=="ok") ):

			if ( ($this->input->post("ecancella")==1) || ( (isset($_FILES["efile"]["name"])) && ($_FILES["efile"]["name"]!="") )):
				// il file deve essere cancellato e/o sostituito
				$dataFC['id_file']=$id_file;
				$dataFC['id_account']=$grupposelezionato;
				$this->mp3_model->gestione_file_mp3_cancella($dataFC);
			endif;


			if ( (isset($_FILES["efile"]["name"])) && ($_FILES["efile"]["name"]!="") ):

				$file_nomefile=$_FILES["efile"]["name"];
				$file_tipofile=$_FILES["efile"]["type"];
				$file_dimensione=($_FILES["efile"]["size"] / 1024);
				$file_temporaneo=$_FILES["efile"]["tmp_name"];

				if ($this->input->post('etipologia_file')==0):
					$estensione_file="mp3";
				elseif ($this->input->post('etipologia_file')==1):
					$stringa_confronto="video";
					$estensione_file=explode(".", $file_nomefile);
					$estensione_file=$estensione_file[(count($estensione_file)-1)];
				endif;

				require_once($percorso_base.'htdocs/php-lib/getid3/getid3/getid3.php');
				$getID3 = new getID3;
				$ThisFileInfo = $getID3->analyze($file_temporaneo);
				$lunghezza=$ThisFileInfo['playtime_string']; // playtime in minutes:seconds, formatted string
				$lunghezza_secondi=$ThisFileInfo['playtime_seconds']; // playtime in seconds
				
				// die("test ".$file_tipofile." ".$this->input->post('etipologia_file'));
				
				if ( (strpos($file_tipofile,$stringa_confronto)) !== false):
					
					if ($id_file!=0):
						$prefisso_file=$id_file;
					else:
					//	$this->db->where('id_account', $grupposelezionato);
						$this->db->from('archivio_radio');
						$this->db->order_by('id', 'desc');
						$queryCNT=$this->db->get();
						if ($queryCNT->num_rows()==0):
							$prefisso_file=1;
						else:
							$rowCNT=$queryCNT->row();
							$prefisso_file=$rowCNT->id;
							$prefisso_file++;
						endif;
					endif;
					
					$nomefile=$prefisso_file."_".preg_replace("/[^a-z0-9\.]/", "_", strtolower($file_nomefile));
					$num_char=strlen($nomefile);
					if ($num_char>150)
						$nomefile=substr($nomefile, 0, 145).".".$estensione_file;

					$dataFUP['file_da_caricare']=$file_temporaneo;
					$dataFUP['nome_del_file']=$nomefile;
					$dataFUP['cartella']="radio";
					$dataFUP['grupposelezionato']=$grupposelezionato;
					$this->strimy_model->file_upload($dataFUP);

					$dataBR['nome_file']=$nomefile;
					$dataBR['durata']=$lunghezza_secondi;

				endif;
				
				// die($nomefile);
				
			endif;

			$dataBR['artista']=$this->input->post('eartista');
			$dataBR['titolo']=$this->input->post('etitolo');
			$dataBR['anno_pubblicazione']=$this->input->post('eanno_pubblicazione');
			$dataBR['genere_1']=$this->input->post('egenere_0');
			$dataBR['genere_2']=$this->input->post('egenere_1');
			$dataBR['genere_3']=$this->input->post('egenere_2');
			$dataBR['genere_4']=$this->input->post('egenere_3');
			$dataBR['bpm']=$this->input->post('ebpm');
			$dataBR['chiave']=$this->input->post('echiave');
			$dataBR['power']=$this->input->post('epower');
			$dataBR['punteggio']=$this->input->post('epunteggio');
			$dataBR['jingle']=$this->input->post('ejingle');
			$dataBR['jingle_categoria']=$this->input->post('ejingle_categoria');
			$dataBR['tipologia_file']=$this->input->post('etipologia_file');

			if ($id_file==0):
				$dataBR['id_account']=$grupposelezionato;
				$dataBR['data_inserimento']=time();
				$this->db->insert('archivio_radio', $dataBR);
				// $id_file=$this->db->insert_id();
			else:
				$dataBR['data_modifica']=time();
				$this->db->where('id', $id_file);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->update('archivio_radio', $dataBR);
			endif;
		
		endif;
		
		
		if ($errore==1):
			$dataRES['risultato']=1; // niente da comunicare
			$dataRES['messaggio']="Errore in fase di caricamento";
			$dataRES['tipologia_file']=$stringa_confronto;
			$this->load->view('site_header');
			$this->load->view('radio_gestione', $dataRES);
			$this->load->view('site_footer');
		else:
			redirect('mp3/gestione_radio/'.$stringa_confronto);
		endif;
		
	}


public function gestione_file_mp3_cancella($id_file, $tipologia_file) {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$this->db->from('archivio_radio');
		$this->db->where('id', $id_file);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		if ($query->num_rows()>0):
			$dataFC['id_file']=$id_file;
			$dataFC['id_account']=$grupposelezionato;
			$this->mp3_model->gestione_file_mp3_cancella($dataFC);
			$this->db->where('id', $id_file);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('archivio_radio');
		endif;
		
		redirect('mp3/gestione_radio/'.$tipologia_file);
		
	}


public function gestione_file_mp3_attiva() {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$elementi=$_REQUEST;
		$id_file=$elementi['eid_file'];
		$disattiva=$elementi['edisattiva'];
		
		$dataFC['disattivo']=$disattiva;
		$this->db->where('id', $id_file);
		$this->db->where('id_account', $grupposelezionato);
		$this->db->update('archivio_radio', $dataFC);
		
	}

/*
public function upload_jingle() {
		
		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

		$percorso_base=$this->config->item('real_path');
		$percorso_users=$this->config->item('percorso_users');
		$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/radio";
		
		$errore=0;
		
		// $percorso_base=$this->config->item('real_path');
		// $percorso_users=$this->config->item('percorso_users');
		// $cartella_archivio=$percorso_base.$percorso_users."comune/content";
		// $url_archivio="/strimy/users/comune/content";
		// $file=$cartella_archivio."/".$nome_file;
		
		
		if ( ($this->input->post("econferma")=="ok") && (isset($_FILES["efile"]["name"])) && ($_FILES["efile"]["name"]!="") ):

			$file_nomefile=$_FILES["efile"]["name"];
			$file_tipofile=$_FILES["efile"]["type"];
			$file_dimensione=($_FILES["efile"]["size"] / 1024);
			$file_temporaneo=$_FILES["efile"]["tmp_name"];
			
			if ($file_tipofile=="audio/mp3"):
				
				if ($this->input->post('ecategoria_jingle')==0):
					$dataDESCAT['descrizione']=$this->input->post('edescrizione_categoria');
					$dataDESCAT['id_account']=$grupposelezionato;
					$this->db->insert('archivio_mp3_jingle_categorie', $dataDESCAT);
					$id_categoria=$this->db->insert_id();
				else:
					$id_categoria=$this->input->post('ecategoria_jingle');
				endif;
				
				$file_nomefile=preg_replace("/[^a-z0-9\.]/", "_", strtolower($file_nomefile));

				$dataFL['nome_file']=$file_nomefile;
				$dataFL['id_account']=$grupposelezionato;
				$dataFL['jingle']=1;
				$dataFL['jingle_categoria']=$id_categoria;

				$this->db->insert('archivio_mp3', $dataFL);
				$id_file=$this->db->insert_id();
				$file_nomefile=$id_file."_jingle_".$file_nomefile;

				move_uploaded_file($file_temporaneo, $cartella_base."/".$file_nomefile);
				chmod($cartella_base."/".$file_nomefile, 0777);
				
				$dataDB['nome_file']=$file_nomefile;
				$this->db->where('id', $id_file);
				$this->db->update('archivio_mp3', $dataDB);

				redirect('mp3/gestione_musica');
				
			else:
			
				$errore=1;

			endif;

		else:

			$errore=1;


		endif;

		if ($errore==1):
			$dataRES['risultato']=1; // niente da comunicare
			$dataRES['messaggio']="Errore in fase di caricamento";
			$this->load->view('site_header');
			$this->load->view('gestione_musica', $dataRES);
			$this->load->view('site_footer');
		else:
			redirect('mp3/gestione_musica');
		endif;
		
	}
*/


	function gestione_radio($tipologia_file="audio") {
		
		$dataRES['risultato']=0; // niente da comunicare
		$dataRES['tipologia_file']=$tipologia_file;
		
		$this->load->view('site_header');
		$this->load->view('radio_gestione', $dataRES);
		$this->load->view('site_footer');	
	}


	function radio_magic($id_dispositivo) {
		$data['id_dispositivo']=$id_dispositivo;
		$this->load->view('radio_magic', $data);
	}

/*
	function radio_magic_new($id_gruppo_dispositivo, $operazione="", $variabile=1) {
		// questa funzione richiama la nuova magic radio, in via di sperimentazione

		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		if ($operazione=="aggiorna_ordine_brani"):
			$elementi=$_REQUEST;
			$eid=$elementi['eid'];
			$eid=explode(',', $eid);
			$xx=0;
			foreach($eid as $eid_temp):
				$dataORD['ordine']=($xx+1);
				$this->db->where('id', $eid_temp);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->update('playlist_temporanea', $dataORD);
				echo $this->db->last_query();
				$xx++;
			endforeach;
		
		elseif ($operazione=="elimina_brano"):
			$this->db->where('id', $variabile);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('playlist_temporanea');
		
		elseif ($operazione=="numero_brani"):
			
			$elementi=$_REQUEST;
			$dataIMP['tipologia_file']=$elementi['etipologia_file'];
			$dataIMP['origine']=$elementi['eorigine'];
			$dataIMP['power']=explode("-", $elementi['epower']);
			$dataIMP['genere_1']=explode("-", $elementi['egenere_1']);
			$dataIMP['genere_2']=explode("-", $elementi['egenere_2']);
			$dataIMP['genere_3']=explode("-", $elementi['egenere_3']);
			$dataIMP['genere_4']=explode("-", $elementi['egenere_4']);
			
			$dataIMP['numero_brani']=explode("-", $elementi['enumero_brani']);
			$dataIMP['ripetizioni_brani']=explode("-", $elementi['eripetizioni_brani']);
			
			$dataIMP['tipo_richiesta']="conteggio_brani";
			// questa funzione restituisce il numero di brani presenti in archivio, secondo i parametri indicati
			$dataRET=$this->mp3_model->mp3_archivio_check($dataIMP);
			echo $dataRET['totale_brani'].";".$dataRET['durata_formattata'];
		
		else:
		
			if ($operazione==""):
				$this->db->from('playlist_temporanea_indice');
				$this->db->where('id_account', $grupposelezionato);
				$this->db->where('id_gruppo_dispositivo', $id_gruppo_dispositivo);
				$query=$this->db->get();
				if ($query->num_rows()>0)
					$operazione="revisione";
				else
					$operazione="creazione";
			endif;
			
			
			$data['id_gruppo_dispositivo']=$id_gruppo_dispositivo;
			$data['operazione']=$operazione;
			$data['giorno']=$variabile;
			$this->load->view('site_header');
			$this->load->view('radio_magic_new', $data);
			$this->load->view('site_footer');
		endif;
	}
*/
	function radio_magic_new($id_dispositivo, $operazione="", $variabile=1) {
		// questa funzione richiama la nuova magic radio, in via di sperimentazione

		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		if ($operazione=="aggiorna_ordine_brani"):
			$elementi=$_REQUEST;
			$eid=$elementi['eid'];
			$eid=explode(',', $eid);
			$xx=0;
			foreach($eid as $eid_temp):
				$dataORD['ordine']=($xx+1);
				$this->db->where('id', $eid_temp);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->update('playlist_temporanea', $dataORD);
				echo $this->db->last_query();
				$xx++;
			endforeach;
		
		elseif ($operazione=="elimina_brano"):
			$this->db->where('id', $variabile);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('playlist_temporanea');
		
		elseif ($operazione=="numero_brani"):
			
			$elementi=$_REQUEST;
			$dataIMP['tipologia_file']=$elementi['etipologia_file'];
			$dataIMP['origine']=$elementi['eorigine'];
			$dataIMP['power']=explode("-", $elementi['epower']);
			$dataIMP['genere_1']=explode("-", $elementi['egenere_1']);
			$dataIMP['genere_2']=explode("-", $elementi['egenere_2']);
			$dataIMP['genere_3']=explode("-", $elementi['egenere_3']);
			$dataIMP['genere_4']=explode("-", $elementi['egenere_4']);
			
			$dataIMP['numero_brani']=explode("-", $elementi['enumero_brani']);
			$dataIMP['ripetizioni_brani']=explode("-", $elementi['eripetizioni_brani']);
			
			$dataIMP['tipo_richiesta']="conteggio_brani";
			// questa funzione restituisce il numero di brani presenti in archivio, secondo i parametri indicati
			$dataRET=$this->mp3_model->mp3_archivio_check($dataIMP);
			echo $dataRET['totale_brani'].";".$dataRET['durata_formattata'];
		
		else:
		
			if ($operazione==""):
				$this->db->from('playlist_temporanea_indice');
				$this->db->where('id_account', $grupposelezionato);
				$this->db->where('id_dispositivo', $id_dispositivo);
				$query=$this->db->get();
				if ($query->num_rows()>0)
					$operazione="revisione";
				else
					$operazione="creazione";
			endif;
			
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->where('id_account', $grupposelezionato);
			$queryIDG=$this->db->get();
			if ($queryIDG->num_rows()==0):
				$id_gruppo_dispositivo=0;
			else:
				$rowIDG=$queryIDG->row();
				$id_gruppo_dispositivo=$rowIDG->id_gruppo;
			endif;
			
			$data['id_dispositivo']=$id_dispositivo;
			$data['id_gruppo_dispositivo']=$id_gruppo_dispositivo;
			$data['operazione']=$operazione;
			$data['giorno']=$variabile;
			$this->load->view('site_header');
			$this->load->view('radio_magic_new', $data);
			$this->load->view('site_footer');
		endif;
	}


	function radio_magic_new_gestisci($id_playlist=0, $tipologia_playlist=0, $operazione="") {
		
			$grupposelezionato=$this->strimy_model->accountselezionato();
			$data['id_playlist']=$id_playlist;
			
			if ($operazione==""):
				$this->db->from('magic_temporanea_indice');
				$this->db->where('id', $id_playlist);
				$this->db->where('id_account', $grupposelezionato);
				$query=$this->db->get();
				if (($id_playlist==0) || ($query->num_rows()==0)):
					$operazione="creazione";
				else:
					$operazione="revisione";
				endif;
			endif;
			$data['operazione']=$operazione;
			$data['tipologia_playlist']=$tipologia_playlist;
			$this->load->view('site_header');
			$this->load->view('radio_magic_new', $data);
			$this->load->view('site_footer');	
	}

	function radio_magic_new_gestisci_cancella($id_playlist=0) {
		
			$grupposelezionato=$this->strimy_model->accountselezionato();
			
			$this->db->from('magic_temporanea_indice');
			$this->db->where('id', $id_playlist);
			$this->db->where('id_account', $grupposelezionato);
			$query=$this->db->get();
			if ($query->num_rows()>0):
				$row=$query->row();
				$id_playlist_definitiva=$row->rif_id_playlist_definitiva;
				
				$this->db->where('id', $id_playlist);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->delete('magic_temporanea_indice');
				
				$this->db->where('id', $id_playlist);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->delete('magic_temporanea_elementi');
				
				$this->db->where('id', $id_playlist);
				$this->db->where('id_account', $grupposelezionato);
				$this->db->delete('magic_temporanea_sequenze');
				
				if ($id_playlist_definitiva!=0):
					$dataDEL['id_playlist']=$id_playlist_definitiva;
					$dataDEL['id_account']=$grupposelezionato;
					$this->strimy_model->playlist_generica_cancella($dataDEL);
					
					/*
					$this->db->where('id', $id_playlist_definitiva);
					$this->db->where('id_account', $grupposelezionato);
					$this->db->delete('playlist_generica_indice');

					$this->db->where('id_playlist', $id_playlist_definitiva);
					$this->db->where('id_account', $grupposelezionato);
					$this->db->delete('playlist_generica_elementi');
					*/
				endif;
				
			endif;
			
			redirect('strimy/gestione_playlist');
	}



	function radio_magic_new_aggiungi_sequenza($id=0, $tipologia_file_default=0, $indice, $orario_iniziale) {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$power=$this->config->item('power');
		$genere=$this->config->item('genere');
				
		if ($id==0):
			/*
			$dataDUR['durata']=$orario_iniziale.":00";
			$dataDURET=$this->mp3_model->durata_secondi($dataDUR);
			$tempo_attuale=$dataDURET['durata_secondi']+3600;
			$dataDUR['durata']=$tempo_attuale;
			$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
			$orario_finale=explode(":", $dataDURET['durata_formattata']);
			$orario_finale=$orario_finale[0].":".$orario_finale[1];
			$orario_iniziale_temp1=explode(":", $orario_iniziale);
			
			if ($orario_iniziale_temp1[0]=="00")
				$orario_finale="00:00";
			*/
			
			$cdurata_formattata="01:00";
			$csequenza=5;
			$ccriterio_ordinamento="power";
			$corigine=0;
			$cnumero_brani[0]=10;
			$cnumero_brani[1]=20;
			$cnumero_brani[2]=30;
			$cnumero_brani[3]=100;
			$cnumero_ripetizioni[0]=4;
			$cnumero_ripetizioni[1]=3;
			$cnumero_ripetizioni[2]=2;
			$cnumero_ripetizioni[3]=1;
			$ctipologia_file=$tipologia_file_default;
			
			$xx=0;
			foreach ($power as $temp):
				$cpower[$xx]=$xx;
				$xx++;
			endforeach;
			
			$zz=0;
			while ($zz<4) {
				$xx=0;
				foreach ($genere as $temp):
					$cgenere[$zz][$xx]=$xx;
					$xx++;
				endforeach;
				$zz++;
			}
			
			
		else:
			$this->db->from('magic_temporanea_sequenze');
			$this->db->where('id', $id);
			$this->db->where('id_account', $grupposelezionato);
			$queryOPT=$this->db->get();
			$rowOPT=$queryOPT->row();
			$valori=unserialize($rowOPT->valori);
			
			$orario_iniziale=$valori['orario_inizio_formattato'];
			$orario_finale=$valori['orario_fine_formattato'];
			$csequenza=$valori['sequenza'];
			$ccriterio_ordinamento=$valori['criterio_ordinamento'];
			$corigine=$valori['origine'];
			$cnumero_brani=$valori['numero_brani'];
			$cnumero_ripetizioni=$valori['ripetizioni_brani'];
			$cpower=$valori['power'];
			$cgenere[0]=$valori['genere_1'];
			$cgenere[1]=$valori['genere_2'];
			$cgenere[2]=$valori['genere_3'];
			$cgenere[3]=$valori['genere_4'];
			if (!isset($valori['tipologia_file'])) $valori['tipologia_file']=0;
			$ctipologia_file=$valori['tipologia_file'];
			
			$dataDUR['durata']=$rowOPT->durata;
			$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
			$cdurata_formattata=$dataDURET['durata_formattata_breve'];
			
		endif;

		$dataIMP['origine']=$corigine;
		$dataIMP['power']=$cpower;
		$dataIMP['genere_1']=$cgenere[0];
		$dataIMP['genere_2']=$cgenere[1];
		$dataIMP['genere_3']=$cgenere[2];
		$dataIMP['genere_4']=$cgenere[3];
		$dataIMP['numero_brani']=$cnumero_brani;
		$dataIMP['ripetizioni_brani']=$cnumero_ripetizioni;
		$dataIMP['tipologia_file']=$ctipologia_file;
		$dataIMP['tipo_richiesta']="conteggio_brani";
		$dataRET=$this->mp3_model->mp3_archivio_check($dataIMP);
		

		echo "<div style=\"position: relative; float: left; border-top: 1px solid #CCC; width: 100%; margin-top: 20px; margin-left: 80px;\">";	// div contenitore

			echo "<div style=\"position: relative; width: 100%; background-color: #DDD; padding: 5px; height: 30px;\">";	// div titolo
			
				echo "<span style=\"position: relative; float: right; right: 80px;\">";
				echo "<a href=\"javascript:rimuovi_sequenza('".$indice."');\"><i class=\"fa fa-trash\"></i></a>";
				echo "</span>";
				
				echo "<span style=\"float: left; width: 25px;\">";
				if ($ctipologia_file==0)
					echo "<i class=\"fa fa-music\" style=\"width: 20px;\"></i>";
				elseif ($ctipologia_file==1)
					echo "<i class=\"fa fa-video-camera\" style=\"width: 20px;\"></i>";
				echo "</span>";
				
				echo "<span style=\"float: left; width: 120px; margin-top: -1px\">";
				echo "<strong>Sequenza n.<span id=\"span_numero_sequenza_".$indice."\">".($indice+1)."</span></strong>";
				echo "</span>";
				
				
				echo "<span style=\"float: left; width: 400px;\">";
				echo "<span id=\"totale_brani_".$indice."\" style=\"font-size: 0.9em;\">Brani selezionati: ".$dataRET['totale_brani']." - Durata totale: ".$dataRET['durata_formattata'];"</span>";
				echo "</span>";
			
			echo "</div>";	// fine div titolo

		
		echo "<div style=\"position: relative; float: left; padding-top: 10px;\">";	// div sinistra

			echo "<input type=\"hidden\" id=\"eattiva_".$indice."\" name=\"eattiva[]\" value=\"1\">";
			echo "<input type=\"hidden\" name=\"eid[]\" value=\"".$id."\">";
		
			echo "<input type=\"hidden\" id=\"etipologia_file_".$indice."\" name=\"etipologia_file[]\" value=\"".$ctipologia_file."\">";
			
			echo "Durata (hh:mm) <input type=\"text\" id=\"edurata_".$indice."\" name=\"edurata[]\" value=\"".$cdurata_formattata."\" maxlength=\"5\" size=\"5\">";
		//	echo "Orario inizio (hh:mm) <input type=\"text\" id=\"einizio_".$indice."\" name=\"einizio[]\" value=\"".$orario_iniziale."\" maxlength=\"5\" size=\"5\">";
		//	echo " orario fine (hh:mm) <input type=\"text\" id=\"efine_".$indice."\" name=\"efine[]\" value=\"".$orario_finale."\" maxlength=\"5\" size=\"5\" onblur=\"aggiorna_orari();\">";
			echo "<br><br>";
			
			echo "Ordinamento:<br>";
			echo "Criterio ";
			echo "<select name=\"ecriterio_ordinamento[]\">";
			echo "<option value=\"power\"";
				if ($ccriterio_ordinamento=="power") echo " selected";
			echo ">Emozione</option>";
			echo "<option value=\"bpm\"";
				if ($ccriterio_ordinamento=="bpm") echo " selected";
			echo ">B.p.m.</option>";
			echo "</select>";
			
			echo " n.brani <input type=\"text\" name=\"esequenza[]\" value=\"".$csequenza."\" size=\"2\">";
			echo "<br><small>Questo parametro permette di ordinare gruppi di brani in ordine crescente o descrescente,<br>secondo il criterio e il numero indicati";
			echo "</small>";

		echo "</div>";	// fine div sinistra


		echo "<div style=\"position: relative; float: left; margin-left: 20px; padding-top: 10px;\">";	// div destra
		
			echo "<strong>Filtri opzionali:</strong><br>";
			
			$origine_opt[0]="tutti";
			$origine_opt[1]="da classifica";
			$origine_opt[2]="fuori classifica";
			
			$width0="150";
			$width="40";
			echo "<span class=\"descrizione_filtro\" style=\"float: left; width: 130px;\">Origine brani:</span>";
			echo "<select id=\"eorigine_".$indice."\" name=\"eorigine[]\" onchange=\"cambia_origine(this, '".$indice."')\">";
			$xx=0;
			foreach ($origine_opt as $temp):
				echo "<option value=\"".$xx."\"";
				if ($xx==$corigine)
					echo " selected";
				echo ">".$temp."</option>";
				$xx++;
			endforeach;
			echo "</select>";

			echo "<br>";
			echo "<div id=\"div_rotazione_".$indice."\" style=\"margin-top: 10px; font-size: 0.8em; clear: both;";
			
				if ($corigine=="2") echo " display: none;";
				echo ">";
				echo "<span style=\"float: left; clear: both;\"><span class=\"descrizione_filtro\" style=\"margin-top: 5px; float: left; width: 150px;\">n. brani ad alta rotazione</span><input type=\"text\" id=\"enumero_brani_0_".$indice."\" name=\"enumero_brani_0[]\" value=\"".$cnumero_brani[0]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"> ripetizioni giornaliere <input type=\"text\" id=\"eripetizioni_brani_0_".$indice."\" name=\"eripetizioni_brani_0[]\" value=\"".$cnumero_ripetizioni[0]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"></span>";
				echo "<span style=\"float: left; clear: both;\"><span class=\"descrizione_filtro\" style=\"margin-top: 5px; float: left; width: 150px;\">n. brani a media rotazione</span><input type=\"text\" id=\"enumero_brani_1_".$indice."\" name=\"enumero_brani_1[]\" value=\"".$cnumero_brani[1]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"> ripetizioni giornaliere <input type=\"text\" id=\"eripetizioni_brani_1_".$indice."\" name=\"eripetizioni_brani_1[]\" value=\"".$cnumero_ripetizioni[1]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"></span>";
				echo "<span style=\"float: left; clear: both;\"><span class=\"descrizione_filtro\" style=\"margin-top: 5px; float: left; width: 150px;\">n. brani a bassa rotazione</span><input type=\"text\" id=\"enumero_brani_2_".$indice."\" name=\"enumero_brani_2[]\" value=\"".$cnumero_brani[2]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"> ripetizioni giornaliere <input type=\"text\" id=\"eripetizioni_brani_2_".$indice."\" name=\"eripetizioni_brani_2[]\" value=\"".$cnumero_ripetizioni[2]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"></span>";
				echo "<span style=\"float: left; clear: both;\"><span class=\"descrizione_filtro\" style=\"float: left; width: 150px;\">n. brani a bassissima rotazione</span><input type=\"text\" id=\"enumero_brani_3_".$indice."\" name=\"enumero_brani_3[]\" value=\"".$cnumero_brani[3]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"> ripetizioni giornaliere <input type=\"text\" id=\"eripetizioni_brani_3_".$indice."\" name=\"eripetizioni_brani_3[]\" value=\"".$cnumero_ripetizioni[3]."\" style=\"width: ".$width."px;\" onblur=\"totale_brani('".$indice."');\"></span>";

			echo "</div>";
		

			echo "<div style=\"float: left; font-size: 0.8em; margin-top: 20px;\">";
			
				echo "<span class=\"descrizione_filtro\" style=\"float: left; width: 130px;\">Emozione:</span>";

				$xx=0;
				foreach ($power as $temp):
				
					if (in_array($xx, $cpower)):
						$potere_selezionato=1;
					else:
						$potere_selezionato=0;
					endif;

					echo "<input type=\"checkbox\"";
					if ($potere_selezionato==1)
						echo " checked=\"checked\"";
					echo "style=\"margin-left: 10px;\" onclick=\"cambia('epower', '".$indice."', '".$xx."');\">";
					echo "<input type=\"hidden\" id=\"epower_".$indice."_".$xx."\" name=\"epower_".$indice."[".$xx."]\" class=\"power_".$indice."\" value=\"".$potere_selezionato."\">";
					if ($xx==0) echo "non spec.";
						else echo $temp;
					echo " ";
					$xx++;
				endforeach;	

				echo "<br>";
				
				$zz=0;
				while ($zz<4) {
					echo "<span class=\"descrizione_filtro\" style=\"float: left; width: 130px;\">";
					if ($zz==0)
						echo "Genere:";
					else
						echo "Sottogenere ".$zz.":";
					echo "</span>";
					$xx=0;
					foreach ($genere as $temp):
					//	if ($id==0):	// se è una sequenza nuova il genere è selezionato di default, altrimenti viene preso dal database
					//		$genere_selezionato=1;
					//	else:
							if (in_array($xx, $cgenere[$zz])):
								$genere_selezionato=1;
							else:
								$genere_selezionato=0;
							endif;
					//	endif;
					
						echo "<input type=\"checkbox\"";
							if ($genere_selezionato==1)
						echo " checked=\"checked\"";
						echo " style=\"margin-left: 10px;\" onclick=\"cambia('egenere_".$zz."', '".$indice."', '".$xx."');\">";
						echo "<input type=\"hidden\" id=\"egenere_".$zz."_".$indice."_".$xx."\" name=\"egenere_".$zz."_".$indice."[".$xx."]\" class=\"genere_".$zz."_".$indice."\" value=\"".$genere_selezionato."\">";
						if ($xx==0) echo "non spec.";
							else echo $temp;
						echo " ";
						$xx++;
					endforeach;	
					echo "<br>";
					$zz++;
				}
				echo "</div>";
		
			echo "</div>";	// fine div destra
		
		echo "</div>";	// fine div contenitore
		
		?>
		<script language="Javascript">
			aggiorna_orari();
		</script>
		<?php
		
	}


	function radio_magic_salva_new() {
		
		error_reporting(E_ERROR);

		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$operazione=$this->input->post('eoperazione');
		
		if ($this->input->post("econferma")!="ok")
			die();
		
		if ($operazione=="crea"):  // questa funzione crea l'anteprima della playlist, che potrà essere successivamente modificata
		
			// variabile per il debugging
			// se =0 valore di produzione: non mostrerà alcun output
			// se =1 valore di test: visualizzerà i dati dei brani e gli eventuali errori
			// se =2 valore di test: visualizzerà solo gli echo del controller ma non quelli del model
			$test=0;
		
			$eid_playlist=$this->input->post('eid_playlist');
			$eid=$this->input->post('eid');
			$descrizione_playlist=$this->input->post('edescrizione_playlist');
			$attiva=$this->input->post('eattiva');
			$giorno_oggi=$this->input->post('egiorno');
			$tipologia_playlist=$this->input->post('etipologia_playlist');
			// $salva_impostazioni=$this->input->post('esalva_impostazioni');
			
			$op_sequenza=$this->input->post('esequenza');
			$op_durata=$this->input->post('edurata');
			$op_criterio_ordinamento=$this->input->post('ecriterio_ordinamento');
			$op_tipologia_file=$this->input->post('etipologia_file');
			$op_origine=$this->input->post('eorigine');
//			$op_giorni=7;
			
			$op_numero_brani[0]=$this->input->post('enumero_brani_0');
			$op_numero_brani[1]=$this->input->post('enumero_brani_1');
			$op_numero_brani[2]=$this->input->post('enumero_brani_2');
			$op_numero_brani[3]=$this->input->post('enumero_brani_3');
			$op_ripetizioni_brani[0]=$this->input->post('eripetizioni_brani_0');
			$op_ripetizioni_brani[1]=$this->input->post('eripetizioni_brani_1');
			$op_ripetizioni_brani[2]=$this->input->post('eripetizioni_brani_2');
			$op_ripetizioni_brani[3]=$this->input->post('eripetizioni_brani_3');
			
			
			$numero_sequenze=$this->input->post('enumero_sequenze');
			
			// prosegue con la creazione della playlist temporanea per il primo dei dispositivi selezionati;
			// successivamente duplicherà i contenuti per tutti gli altri dispositivi: questo riduce i tempi di elaborazione
			
			if ($eid_playlist==0):
				$dataPLAY['id_account']=$grupposelezionato;
				$dataPLAY['descrizione']=$descrizione_playlist;
				$dataPLAY['tipologia']=$tipologia_playlist;
				$this->db->insert('magic_temporanea_indice', $dataPLAY);
				$eid_playlist=$this->db->insert_id();
			else:
				$this->db->where('id_account', $grupposelezionato);
				$this->db->where('id_playlist', $eid_playlist);
				$this->db->delete('magic_temporanea_sequenze');
				$this->db->where('id_account', $grupposelezionato);
				$this->db->where('id_playlist', $eid_playlist);
				$this->db->delete('magic_temporanea_elementi');
			endif;

			
			$zz=0;
			$zz1=0;
			
			while ($zz<$numero_sequenze) {
				// verifica che le opzioni siano state inserite correttamente
				// prepara gli array per la successiva generazione dei singoli intervalli playlist
				// memorizza le opzioni nel database per poi riprenderle in fase di nuova creazione
			
				if ($attiva[$zz]==1):
			
					$dataOP[$zz1]['sequenza']=$op_sequenza[$zz];
					$dataOP[$zz1]['criterio_ordinamento']=$op_criterio_ordinamento[$zz];
					$dataOP[$zz1]['tipologia_file']=$op_tipologia_file[$zz];
					$dataOP[$zz1]['origine']=$op_origine[$zz];
					// $dataOP[$zz1]['giorni']=1;
					$dataOP[$zz1]['orario_inizio_formattato']=$op_inizio_sequenza[$zz];
					$dataOP[$zz1]['orario_fine_formattato']=$op_fine_sequenza[$zz];

					$power=$this->input->post('epower_'.$zz);
					$genere[0]=$this->input->post('egenere_0_'.$zz);
					$genere[1]=$this->input->post('egenere_1_'.$zz);
					$genere[2]=$this->input->post('egenere_2_'.$zz);
					$genere[3]=$this->input->post('egenere_3_'.$zz);

					$dataOP[$zz1]['numero_brani']=Array();
					$dataOP[$zz1]['ripetizioni_brani']=Array();

					$kk=0;
					while ($kk<4) {
						$dataOP[$zz1]['numero_brani'][$kk]=$op_numero_brani[$kk][$zz];
						$dataOP[$zz1]['ripetizioni_brani'][$kk]=$op_ripetizioni_brani[$kk][$zz];
						
						$kk++;
					}
					
					
					$tipologia_file[$zz1]=$op_tipologia_file[$zz];
					
					$dataDUR['durata']=$op_durata[$zz1];
					$dataDURET=$this->mp3_model->durata_secondi($dataDUR);
					$durata_secondi=$dataDURET['durata_secondi'];
					
					if (!isset($durata_secondi_precedente))
						$durata_secondi_precedente=0;
					
					$dataOP[$zz1]['inizio_sequenza']=$durata_secondi_precedente;
					$dataOP[$zz1]['fine_sequenza']=$durata_secondi+$durata_secondi_precedente;

					$durata_secondi_precedente=$durata_secondi+$durata_secondi_precedente;

					$dataOP[$zz1]['power']=Array();
					$xx=0;
					foreach ($power as $power_temp):
							if ($power_temp==1):
								$dataOP[$zz1]['power'][]=$xx;
							endif;
							$xx++;
					endforeach;

					$kk=1;
					while($kk<5) {
						$genere_temporaneo=$genere[($kk-1)];
						$dataOP[$zz1]['genere_'.$kk]=Array();
						$xx=0;
						foreach ($genere_temporaneo as $genere_temp):
								if ($genere_temp==1):
									$dataOP[$zz1]['genere_'.$kk][]=$xx;
								endif;
								$xx++;
						endforeach;
						$kk++;
					}
					
					
					// salva le opzioni per ogni singola sequenza oraria, per poi riproporle in fase di generazione
					$dataOPSER['valori']=serialize($dataOP[$zz1]);
					$dataOPSER['ordine']=$zz1;
					$dataOPSER['id_playlist']=$eid_playlist;
					$dataOPSER['id_account']=$grupposelezionato;
					$dataOPSER['durata']=$durata_secondi;
					$dataOPSER['tipologia_file']=$tipologia_file[$zz1];
					
					$this->db->insert('magic_temporanea_sequenze', $dataOPSER);
					$id_sequenza_inserita[$zz1]=$this->db->insert_id();

					$zz1++;
				endif;

			$zz++;
			}

			$numero_sequenze=$zz1;
		
			$giorno_attuale=1;

					$sequenza_definitiva[$giorno_attuale]=Array();
					
					$zz=0;
					$kk=1;
					while ($zz<$numero_sequenze) {

						$dataOP[$zz]['test']=$test;
						$dataRET0=$this->mp3_model->radio_magic_genera($dataOP[$zz]);
						$id_sequenza_temp=$id_sequenza_inserita[$zz];
						
						$ff=0;
						
						if ($zz==0):
							// la variabile $fine_prevista_secondi_accumulata[$giorno_attuale] serve a verificare che un brano, a causa dell'accumulo dei tempi,
							// non venga riprodotto oltre l'orario di fine di una sequenza
							$fine_prevista_secondi_accumulata[$giorno_attuale]=$dataOP[$zz]['inizio_sequenza'];
							$dataDUR['durata']=$fine_prevista_secondi_accumulata[$giorno_attuale];
							$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
						endif;

						if ($test>0)
							echo "(inizio previsto: ".$fine_prevista_secondi_accumulata[$giorno_attuale]." - fine prevista: ".$dataOP[$zz]['fine_sequenza'].")<br>";
						
						
						// echo "<b>Sequenza dalle ".$inizio_sequenza[$zz]." alle ".$fine_sequenza[$zz]."</b>:<br>giorno ".($giorno_attuale)."<br>";
						foreach ($dataRET0['brano_id'] as $brano_id_temp):
							
							// verifica che il brano, in considerazione dell'accumulo dei tempi, possa essere riprodotto entro l'orario di termine della playlist;
							// in caso contrario lo ignora
						
						
							
						if ($fine_prevista_secondi_accumulata[$giorno_attuale]<$dataOP[$zz]['fine_sequenza']+1):

								$dataELEMENTO['id_account']=$grupposelezionato;
								$dataELEMENTO['id_file']=$brano_id_temp;
								$dataELEMENTO['ordine']=$kk;
								$dataELEMENTO['ripetizione']=$dataRET0['brano_ripetuto'][$ff];
								$dataELEMENTO['sequenza']=$zz;
								$dataELEMENTO['id_sequenza']=$id_sequenza_temp;
								$dataELEMENTO['id_playlist']=$eid_playlist;
								$this->db->insert('magic_temporanea_elementi', $dataELEMENTO);

								$fine_prevista_secondi_accumulata[$giorno_attuale]=$fine_prevista_secondi_accumulata[$giorno_attuale]+$dataRET0['brano_durata'][$ff];

								$kk++;
						
						else:
							if ($test>0)
								echo "*** ";
							
						endif;
							
							if ($test>0)
								echo "inizio previsto: ".$fine_prevista_secondi_accumulata[$giorno_attuale]." ";
							
							
							
							if ($test>0):
							
								$dataDUR['durata']=$fine_prevista_secondi_accumulata[$giorno_attuale];
								$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
								// $fine_sequenza_secondi=$dataDURET['durata_formattata'];
							
								echo ($ff+1).". ".$dataRET0['brano_artista'][$ff]." - ".$dataRET0['brano_titolo'][$ff]." (".$dataRET0['brano_punteggio'][$ff].") ".$durata_brano;
								echo " - fine prevista: ".$fine_prevista_secondi_accumulata[$giorno_attuale];
								echo "<br>";
								
							endif;
							
							$ff++;
						endforeach;
						if ($test>0)
							echo "--<br>";

					$zz++;
					}
					
			$url="mp3/radio_magic_new_gestisci/".$eid_playlist."/".$tipologia_playlist;
			if ($test>0)
				echo "<br>[ <a href=\"/index.php/".$url."\">prosegui</a> ]";
			else
				redirect($url);
		
		
		
		elseif ($operazione=="compila"):

			$eid=$this->input->post('eid');
			$eid_playlist_temporanea=$this->input->post('eid_playlist_temporanea');
			$tipologia_playlist=$this->input->post('etipologia_playlist');
			$dataPL['descrizione']=$this->input->post('edescrizione_playlist');
			$dataPL['tipologia']=$tipologia_playlist;
			
			if ($eid==0):
				$dataPL['id_account']=$grupposelezionato;
				$this->db->insert('playlist_generica_indice', $dataPL);
				$eid=$this->db->insert_id();
				$dataPL2['rif_id_playlist_definitiva']=$eid;
			else:
				$this->db->where('id', $eid);
				$this->db->update('playlist_generica_indice', $dataPL);
			endif;

			$dataPL2['descrizione']=$dataPL['descrizione'];
			$this->db->where('id', $eid_playlist_temporanea);
			$this->db->update('magic_temporanea_indice', $dataPL2);

			// elimina eventuali doppioni generati per errore
			$this->db->where('id !=', $eid);
			$this->db->where('tipologia', $tipologia_playlist);
			$this->db->where('descrizione', $dataPL['descrizione']);
			$this->db->delete('playlist_generica_indice');

			$xx=0;
			$zz=0;
			$id_file=Array();
			$tipologia_file=Array();

			$this->db->from('magic_temporanea_sequenze');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('id_playlist', $eid_playlist_temporanea);
			$this->db->order_by('ordine', 'asc');
			$querySEQ=$this->db->get();
				
			foreach ($querySEQ->result() as $rowSEQ):	// ciclo delle sequenze
			
				$this->db->from('magic_temporanea_elementi');
				$this->db->where('id_playlist', $eid_playlist_temporanea);
				$this->db->where('id_sequenza', $rowSEQ->id);
				$this->db->order_by('ordine', 'asc');
				$queryELE=$this->db->get();
				foreach ($queryELE->result() as $rowELE):
					$id_file[$zz]=$rowELE->id_file;
					$tipologia_file[$zz]=$rowSEQ->tipologia_file;
					$zz++;
				endforeach;
			
				$xx++;
			endforeach;
			$totale_file=$zz;
			
			
			$this->db->where('id_playlist', $eid);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('playlist_generica_elementi');

			$zz=0;
			while ($zz<$totale_file) {
				$dataFILE['id_playlist']=$eid;
				$dataFILE['id_account']=$grupposelezionato;
				$dataFILE['id_file']=$id_file[$zz];
				$dataFILE['tipologia_file']=$tipologia_file[$zz];
				$dataFILE['ordine']=$zz;
				$this->db->insert('playlist_generica_elementi', $dataFILE);
				$zz++;
			}
			
			redirect('strimy/gestione_playlist');
			
		endif;
		
	}


	public function radio_magic_new_inserisci_brano() {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$id_brano=$this->input->post('eid_brano');
		$brano_posizione=(int)$this->input->post('ebrano_posizione');
		$id_dispositivo=$this->input->post('eid_dispositivo');
		$giorno_oggi=$this->input->post('egiorno');

		if ($brano_posizione>0):
	
			$this->db->from('archivio_radio');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('id', $id_brano);
			$queryBRANO=$this->db->get();
			$rowBRANO=$queryBRANO->row();

			$this->db->from('playlist_temporanea');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->where('giorno', $giorno_oggi);
			$this->db->order_by('ordine', 'asc');
			$querySEQ=$this->db->get();
			
			if ($querySEQ->num_rows()>0):
			
				$dataNUOVO=Array();
				$ff=0;	// questa variabile riporta l'ordine del file nel corso della playlist giornaliera e serve per calcolare il punto di inserimento del nuovo file
				foreach ($querySEQ->result() as $rowSEQ):
					
					$dataNUOVO['id'][]=$rowSEQ->id;
					$dataNUOVO['id_file'][]=$rowSEQ->id_file;
					$dataNUOVO['sequenza'][]=$rowSEQ->sequenza;
					$dataNUOVO['id_sequenza'][]=$rowSEQ->id_sequenza;
					$dataNUOVO['ordine'][]=($ff+1);
					$dataNUOVO['jingle_categoria'][]=$rowSEQ->jingle_categoria;
					$ff++;
									
					if ($ff==$brano_posizione):

						$dataNUOVO['id'][]=0;
						$dataNUOVO['id_file'][]=$id_brano;
						$dataNUOVO['sequenza'][]=$rowSEQ->sequenza;
						$dataNUOVO['id_sequenza'][]=$rowSEQ->id_sequenza;
						$dataNUOVO['ordine'][]=($ff+1);
						$dataNUOVO['jingle_categoria'][]=0;
						
						$ff++;
					endif;

				endforeach;
				
				$totale_brani=$ff;
				$xx=0;
				while ($xx<$totale_brani) {
					$dataINSERT['id_file']=$dataNUOVO['id_file'][$xx];
					$dataINSERT['ordine']=$dataNUOVO['ordine'][$xx];
					$dataINSERT['sequenza']=$dataNUOVO['sequenza'][$xx];
					$dataINSERT['id_sequenza']=$dataNUOVO['id_sequenza'][$xx];
					$dataINSERT['jingle_categoria']=$dataNUOVO['jingle_categoria'][$xx];
					$dataINSERT['giorno']=$giorno_oggi;
					$dataINSERT['id_dispositivo']=$id_dispositivo;
					$dataINSERT['id_account']=$grupposelezionato;
					
					if ($dataNUOVO['id'][$xx]!=0):
						$this->db->where('id', $dataNUOVO['id'][$xx]);
						$this->db->update('playlist_temporanea', $dataINSERT);
						
					else:
						$this->db->insert('playlist_temporanea', $dataINSERT);
					
					endif;
					
					$xx++;
				}
		
			else:
				
				// se la playlist è vuota, inserisce il brano all'inizio

				$dataINSERT['id_file']=$id_brano;
				$dataINSERT['ordine']=1;
				$dataINSERT['sequenza']=0;
				$dataINSERT['jingle_categoria']=0;
				$dataINSERT['giorno']=$giorno_oggi;
				$dataINSERT['id_dispositivo']=$id_dispositivo;
				$dataINSERT['id_account']=$grupposelezionato;
				$this->db->insert('playlist_temporanea', $dataINSERT);
				
			endif;
		
		endif;

		redirect("mp3/radio_magic_new/".$id_dispositivo."/revisione/".$giorno_oggi);
		
	}




	public function radio_magic_new_cancella_jingle($id_dispositivo=0, $jingle_categoria=0, $giorno_oggi, $giorno) {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$this->db->where('id_account', $grupposelezionato);
		$this->db->where('id_dispositivo', $id_dispositivo);
		$this->db->where('jingle_categoria', $jingle_categoria);
		if ($giorno!=0)
			$this->db->where('giorno', $giorno);
		$this->db->delete('playlist_temporanea');
		redirect("mp3/radio_magic_new/".$id_dispositivo."/revisione/".$giorno_oggi);
	}

	public function radio_magic_new_inserisci_jingle() {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$sequenza=$this->input->post('esequenza');
		$jingle_selezionato=$this->input->post('ejingle_selezionato');
		$jingle_categoria=$this->input->post('ejingle_categoria');
		$jingle_intervallo=$this->input->post('ejingle_intervallo');
		$id_dispositivo=$this->input->post('eid_dispositivo');
		$giorno_oggi=$this->input->post('egiorno');
		$settimana=$this->input->post('esettimana');
		
		$categoria=Array();
		if ( (isset($jingle_selezionato)) && (count($jingle_selezionato)>0) && (is_array($jingle_selezionato))):
			foreach ($jingle_selezionato as $jingle_temp):
					$categoria[]=$jingle_temp;
			endforeach;
			
			$categoria0=$categoria;
		
		//	$indiceJIN=Array();
			$jingle_categorie=Array();
			$this->db->from('archivio_radio');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('jingle', 1);
			$this->db->where_in('jingle_categoria', $categoria);
			$queryJIN=$this->db->get();
			foreach ($queryJIN->result() as $rowJIN):
				// $indiceJIN[]=$rowJIN->id;
				// $categoriaJIN[$rowJIN->id]=$rowJIN->jingle_categoria;
				$jingle_categorie[$rowJIN->jingle_categoria][]=$rowJIN->id;
			//	echo $rowJIN->id;
			endforeach;
			$jingle_categorie0=$jingle_categorie;

			$this->db->from('playlist_temporanea');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->order_by('giorno', 'desc');
			$queryTOT=$this->db->get();
			
			$xx=0;
			
			if ($queryTOT->num_rows()>0):
			$rowTOT=$queryTOT->row();
			$giorni_totali=$rowTOT->giorno;
			
			$giorno=1;
			// $indicetemp_JIN=Array();
			while ($giorno<($giorni_totali+1)) {
				
				$this->db->from('playlist_temporanea');
				$this->db->where('id_account', $grupposelezionato);
				$this->db->where('id_dispositivo', $id_dispositivo);
				$this->db->where('giorno', $giorno);
				$this->db->order_by('ordine', 'asc');
				$querySEQ=$this->db->get();
				
				$indiceFILE=Array();
				
				$ff=1;	// questa variabile riporta l'ordine del file nel corso della playlist giornaliera
				$kk=1;	// questa variabile indica il posizionamento del brano, e serve per calcolare il punto di inserimento del jingle
				foreach ($querySEQ->result() as $rowSEQ):
					
					$dataNUOVO['id'][]=$rowSEQ->id;
					$dataNUOVO['id_file'][]=$rowSEQ->id_file;
					$dataNUOVO['sequenza'][]=$rowSEQ->sequenza;
					$dataNUOVO['id_sequenza'][]=$rowSEQ->id_sequenza;
					$dataNUOVO['ordine'][]=$ff;
					$dataNUOVO['jingle_categoria'][]=$rowSEQ->jingle_categoria;
					$dataNUOVO['id_account'][]=$grupposelezionato;
					$dataNUOVO['id_dispositivo'][]=$id_dispositivo;
					$dataNUOVO['giorno'][]=$giorno;
					$ff++;
					
					
					if (($kk>0) && (round($kk/($jingle_intervallo)))==($kk/($jingle_intervallo)) && ( ($sequenza==-1) || ($sequenza==$rowSEQ->sequenza) ) && ( (($settimana) || ($giorno==$giorno_oggi)) ) ):
					
						// seleziona una categoria di jingle tra quelle selezionate, per garantire una distribuzione uniforme
						if (count($categoria0)==0) $categoria0=$categoria;
						$categoria_rand = $categoria0[(count($categoria0)-1)];
						unset($categoria0[(count($categoria0)-1)]);

						// estrare un jingle tra quelli della categoria selezionata, fino a esaurimento; poi ricomincia dall'inizio
						// in questo modo verranno eseguiti, a rotazione, tutti i jingle senza ripetizioni
						if (count($jingle_categorie0[$categoria_rand])==0) $jingle_categorie0[$categoria_rand]=$jingle_categorie[$categoria_rand];
						$jingle_rand = array_rand($jingle_categorie0[$categoria_rand], 1);
						$jingle_rand=$jingle_categorie0[$categoria_rand][$jingle_rand];
						unset($jingle_categorie0[$categoria_rand][array_search($jingle_rand, $jingle_categorie0[$categoria_rand])]);

						$dataNUOVO['id'][]=0;
						$dataNUOVO['id_file'][]=$jingle_rand;
						$dataNUOVO['sequenza'][]=$rowSEQ->sequenza;
						$dataNUOVO['id_sequenza'][]=$rowSEQ->id_sequenza;
						$dataNUOVO['ordine'][]=$ff;
						$dataNUOVO['jingle_categoria'][]=$categoria_rand;
						$dataNUOVO['id_account'][]=$grupposelezionato;
						$dataNUOVO['id_dispositivo'][]=$id_dispositivo;
						$dataNUOVO['giorno'][]=$giorno;
						
						$ff++;
						$xx++;
					endif;
					
					$kk++;
					$xx++;
				endforeach;
				
				$giorno++;
			}


			endif;
			
/*
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('id_gruppo_dispositivo', $id_gruppo_dispositivo);
			$this->db->delete('playlist_temporanea');
*/			
			$totale_brani=$xx;
			$xx=0;
			while ($xx<$totale_brani) {
				$dataINSERT['id_file']=$dataNUOVO['id_file'][$xx];
				$dataINSERT['ordine']=$dataNUOVO['ordine'][$xx];
				$dataINSERT['sequenza']=$dataNUOVO['sequenza'][$xx];
				$dataINSERT['id_sequenza']=$dataNUOVO['id_sequenza'][$xx];
				$dataINSERT['jingle_categoria']=$dataNUOVO['jingle_categoria'][$xx];
				$dataINSERT['giorno']=$dataNUOVO['giorno'][$xx];
				$dataINSERT['id_dispositivo']=$id_dispositivo;
				$dataINSERT['id_account']=$grupposelezionato;
				
				if ($dataNUOVO['id'][$xx]!=0):
					$this->db->where('id', $dataNUOVO['id'][$xx]);
					$this->db->update('playlist_temporanea', $dataINSERT);
					
				else:
					$this->db->insert('playlist_temporanea', $dataINSERT);
				
				endif;
				
				$xx++;
			}
		
		endif;
		

		redirect("mp3/radio_magic_new/".$id_dispositivo."/revisione/".$giorno_oggi);
		
	}


	public function playlist_singolo_elemento($data) {
			
		echo '<div id="elemento_'.$data['sequenza_selezionata'].'_'.$data['numero_elementi'].'" class="elemento_sequenza" style="height: 20px;">';
	
		$brano_power=$this->config->item('power');
		$brano_chiave=$this->config->item('chiave');
	
		echo "<span class=\"elemento_radio\" style=\"width: 25px;\">";
		echo "<small>".($data['numero_elementi']+1).".</small>";
		echo "</span>";
		echo "<span class=\"elemento_radio\" style=\"width: 300px;\">";
		echo $data['artista'];
		echo "</span>";
		echo "<span class=\"elemento_radio\" style=\"width: 300px;\">";
		echo $data['titolo'];
		echo "</span>";
		echo "<span class=\"elemento_radio\" style=\"width: auto;\">";
		echo "<small>";
		echo "punteggio: ".$data['punteggio']."; ";
		echo "emozione: ".$brano_power[$data['power']]."; ";
		echo "bpm: ".$data['bpm']."; ";
		echo "chiave: ".$brano_chiave[$data['chiave']];
		echo "</small>";
		echo "</span>";
		
		echo '<input type="hidden" name="eidelemento['.$data['sequenza_selezionata'].']['.$data['numero_elementi'].']" value="'.$data['id_elemento'].'" readonly="readonly">';
		echo '<input type="hidden" name="efile['.$data['sequenza_selezionata'].']['.$data['numero_elementi'].']" value="'.$data['nome_file'].'" readonly="readonly">';
		echo '<input type="hidden" name="efileid['.$data['sequenza_selezionata'].']['.$data['numero_elementi'].']" value="'.$data['file_id'].'" readonly="readonly">';
		echo '<input type="hidden" id="ecancella_'.$data['sequenza_selezionata'].'_'.$data['numero_elementi'].'" name="ecancella['.$data['sequenza_selezionata'].']['.$data['numero_elementi'].']" value="0">';

		echo '</div>';		
	
	}

	
	
	public function playlist_genera($sequenza_selezionata, $limite_brani=0, $tipologia="") {
		
		error_reporting(E_ALL);
		
		// echo $tipologia;
		
		$data=Array();
		$data['limite_brani']=150;
		$data['origine']=0;
		$data['numero_brani'][0]=150;
		$data['ripetizioni_brani'][0]=1;
		$data['inizio_sequenza']=0;
		$data['fine_sequenza']=86400;
		$data['test']=0;
		if ($tipologia=="musictv")
			$data['tipologia_file']=0;
		if ($tipologia=="musictv")
			$data['tipologia_file']=1;
		$dataRET=$this->mp3_model->radio_magic_genera($data);

		$numero_elementi=0;
		foreach ($dataRET['brano_id'] as $idtemp):

			/*
			if (!isset($dataRET['brano_power'][$numero_elementi])) $dataRET['brano_power'][$numero_elementi]=Array();
			if (!isset($dataRET['brano_bpm'][$numero_elementi])) $dataRET['brano_bpm'][$numero_elementi]=Array();
			if (!isset($dataRET['brano_chiave'][$numero_elementi])) $dataRET['brano_chiave'][$numero_elementi]=Array();
			*/
		
			$dataELE['sequenza_selezionata']=$sequenza_selezionata;
			$dataELE['numero_elementi']=$numero_elementi;
			$dataELE['id_elemento']=0;
			$dataELE['file_id']=$dataRET['brano_id'][$numero_elementi];
			$dataELE['nome_file']=$dataRET['brano_nome_file'][$numero_elementi];
			$dataELE['artista']=$dataRET['brano_artista'][$numero_elementi];
			$dataELE['titolo']=$dataRET['brano_titolo'][$numero_elementi];
			$dataELE['punteggio']=$dataRET['brano_punteggio'][$numero_elementi];
			$dataELE['power']=$dataRET['brano_power'][$numero_elementi];
			$dataELE['bpm']=$dataRET['brano_bpm'][$numero_elementi];
			$dataELE['chiave']=$dataRET['brano_chiave'][$numero_elementi];
			$this->playlist_singolo_elemento($dataELE);
			
			/*
			echo '<div id="elemento_'.$sequenza_selezionata.'_'.$numero_elementi.'" class="elemento_sequenza" style="height: 20px;">';
		
			echo ($numero_elementi+1)." ";
			echo $dataRET['brano_artista'][$numero_elementi]." ";
			echo $dataRET['brano_titolo'][$numero_elementi]." ";
			echo $dataRET['brano_punteggio'][$numero_elementi]." ";
			
			echo '<input type="hidden" name="eidelemento['.$sequenza_selezionata.']['.$numero_elementi.']" value="0" readonly="readonly">';
			echo '<input type="hidden" name="efile['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$dataRET['brano_nome_file'][$numero_elementi].'" readonly="readonly">';
			echo '<input type="hidden" name="efileid['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$idtemp.'" readonly="readonly">';
			echo '<input type="hidden" id="ecancella_'.$sequenza_selezionata.'_'.$numero_elementi.'" name="ecancella['.$sequenza_selezionata.']['.$numero_elementi.']" value="0">';

			echo '</div>';
			*/

			$numero_elementi++;


		endforeach;
		
		echo '<div id="fine_sequenza_'.$sequenza_selezionata.'" class="elemento_sequenza" style="display: none;"></div>';
		echo '<input type="hidden" id="elementi_sequenza_'.$sequenza_selezionata.'" name="elementi_sequenza['.$sequenza_selezionata.']" value="'.$numero_elementi.'">';
		echo '<input type="hidden" name="reset_sequenza['.$sequenza_selezionata.']" value="1">';
		echo '</div>';
		
	}



	public function playlist_carica($sequenza_selezionata, $id_sequenza) {
		
		error_reporting(E_ALL);

		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$this->db->select('archivio_radio.artista as artista, archivio_radio.titolo as titolo, archivio_radio.punteggio as punteggio, archivio_radio.bpm as bpm, archivio_radio.power as power, archivio_radio.chiave as chiave');
		$this->db->select('archivio_radio.nome_file as nome_file, archivio_radio.id as file_id, playlist_elementi.id as id_elemento');
		$this->db->from('archivio_radio');
		// $this->db->join('archivio_mp3_classifica', 'archivio_mp3_classifica.id_archivio_mp3=archivio_mp3.id');
		$this->db->join('playlist_elementi', 'playlist_elementi.id_file=archivio_radio.id');
		$this->db->where('archivio_radio.id_account', $grupposelezionato);
		$this->db->where('archivio_radio.disattivo', 0);
		$this->db->where('playlist_elementi.id_sequenza', $id_sequenza);
		$this->db->order_by('playlist_elementi.posizione', 'asc');
		$query=$this->db->get();

		$numero_elementi=0;
		foreach ($query->result() as $row):
			$dataELE['sequenza_selezionata']=$sequenza_selezionata;
			$dataELE['numero_elementi']=$numero_elementi;
			$dataELE['id_elemento']=$row->id_elemento;
			$dataELE['file_id']=$row->file_id;
			$dataELE['nome_file']=$row->nome_file;
			$dataELE['artista']=$row->artista;
			$dataELE['titolo']=$row->titolo;
			$dataELE['punteggio']=$row->punteggio;
			$dataELE['power']=$row->power;
			$dataELE['bpm']=$row->bpm;
			$dataELE['chiave']=$row->chiave;
			$this->playlist_singolo_elemento($dataELE);
			
			/*
			echo '<div id="elemento_'.$sequenza_selezionata.'_'.$numero_elementi.'" class="elemento_sequenza" style="height: 20px;">';
		
			$idtemp=$row->file_id;
		
			echo ($numero_elementi+1)." ";
			echo $row->artista." ";
			echo $row->titolo." ";
			echo $row->punteggio." ";
			
			echo '<input type="hidden" name="eidelemento['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$row->id_elemento.'" readonly="readonly">';
			echo '<input type="hidden" name="efile['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$row->nome_file.'" readonly="readonly">';
			echo '<input type="hidden" name="efileid['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$idtemp.'" readonly="readonly">';
			echo '<input type="hidden" id="ecancella_'.$sequenza_selezionata.'_'.$numero_elementi.'" name="ecancella['.$sequenza_selezionata.']['.$numero_elementi.']" value="0">';

			echo '</div>';
			*/
			$numero_elementi++;


		endforeach;
		
		echo '<div id="fine_sequenza_'.$sequenza_selezionata.'" class="elemento_sequenza" style="display: none;"></div>';
		echo '<input type="hidden" id="elementi_sequenza_'.$sequenza_selezionata.'" name="elementi_sequenza['.$sequenza_selezionata.']" value="'.$numero_elementi.'">';
		echo '</div>';
		
	}



	public function playlist_carica_jingle($sequenza_selezionata, $id_sequenza=0) {		// funzione non più utilizzata ma mantenuta per retrocompatibilità
		
		$max_jingle=3;
		$jingle_categoria=Array();
		$jingle_intervallo=Array();
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$this->db->from('playlist_sequenze_jingle');
		$this->db->where('id_sequenza', $id_sequenza);
		$this->db->order_by('id', 'asc');
		$querySEQ=$this->db->get();
		$xx=0;
		foreach ($querySEQ->result() as $rowSEQ):
			$jingle_id[$xx]=$rowSEQ->id;
			$jingle_categoria[$xx]=$rowSEQ->jingle_categoria;
			$jingle_intervallo[$xx]=$rowSEQ->jingle_intervallo;
			$xx++;
		endforeach;
		
		while ($xx<$max_jingle) {
			$jingle_id[$xx]=0;
			$jingle_categoria[$xx]=0;
			$jingle_intervallo[$xx]=10;
			$xx++;
		}


		$categoria_elementi[]=0;
		$categoria_descrizione[]="nessuno";
		$categoria_id[]=0;
		$this->db->from('archivio_radio_jingle_categorie');
		$this->db->where('id_account', $grupposelezionato);
		$this->db->order_by('descrizione', 'asc');
		$queryJIN=$this->db->get();
		foreach ($queryJIN->result() as $rowJIN):
			$this->db->from('archivio_radio');
			$this->db->where('id_account', $grupposelezionato);
			$this->db->where('jingle', 1);
			$this->db->where('disattivo', 0);
			$this->db->where('jingle_categoria', $rowJIN->id);
			$queryCNT=$this->db->get();
			$categoria_elementi[]=$queryCNT->num_rows();
			$categoria_descrizione[]=$rowJIN->descrizione;
			$categoria_id[]=$rowJIN->id;
		endforeach;

		$xx=0;
		echo "<br>";
		while ($xx<$max_jingle) {
		
			
			echo "<br><br>";
			echo "jingle <select name=\"esequenza_jingle_categoria[".$sequenza_selezionata."][".$xx."]\" onchange=\"cambia_categoria_jingle(this, '".$sequenza_selezionata."','".$xx."');\">";
			$yy=0;
			foreach ($categoria_id as $categoria_temp):
				echo "<option value=\"".$categoria_temp."\"";
				if ($jingle_categoria[$xx]==$categoria_temp) echo " selected";
				echo ">".$categoria_descrizione[$yy];
				if ($categoria_temp!=0) echo " (".$categoria_elementi[$yy].")";
				echo "</option>";
				$yy++;		
			endforeach;
			echo "</select>";
			echo "<br>";
			
			echo "<span id=\"categoria_jingle_intervallo_".$sequenza_selezionata."_".$xx."\"";
			if ($jingle_categoria[$xx]==0)
				echo " style=\"display: none\"";
			echo ">";
			echo "intervallo: <input type=\"text\" name=\"esequenza_jingle_intervallo[".$sequenza_selezionata."][".$xx."]\" size=\"5\" value=\"".$jingle_intervallo[$xx]."\">";
			echo "</span>";
			echo "<input type=\"hidden\" name=\"esequenza_jingle_id[".$sequenza_selezionata."][".$xx."]\" value=\"".$jingle_id[$xx]."\">";
			$xx++;
		
		}
		
	}




	public function sincronizza() {
		/*
		$this->db->from('archivio_mp3');
		$this->db->order_by('id', 'asc');
		$query=$this->db->get();
		foreach ($query->result() as $row):
			echo $row->artista." ".$row->titolo." <b>";
			$this->db->from('archivio_mp3_classifica');
			$this->db->where('id_archivio_mp3', $row->id);
			$query2=$this->db->get();
			if ($query2->num_rows()>0):
				echo "presente";
			else:
				$this->db->from('archivio_mp3_classifica');
				$this->db->like('artista', $row->artista);
				$this->db->like('titolo', $row->titolo);
				$query3=$this->db->get();
				if ($query3->num_rows()>0):
					$row3=$query3->row();
					$dataUP['id_archivio_mp3']=$row->id;
					$this->db->where('id', $row3->id);
					$this->db->update('archivio_mp3_classifica', $dataUP);
					echo "aggiornato!!!";
				else:
					echo "assente";
				endif;
			endif;
			
			echo "</b>";
			echo "<br>";
		endforeach;
		*/
		
		/*
		$dataUP['id_account']=1;
		$this->db->update('archivio_mp3', $dataUP);
		$this->db->update('archivio_mp3_classifica', $dataUP);
		*/
		
	}
	
	/*
	public function calcola_durata() {
		
		$percorso_base=$this->config->item('real_path');
		$percorso_users=$this->config->item('percorso_users');
		
		$this->db->from('archivio_radio');
		$this->db->where('nome_file !=', '');
		$this->db->where('durata', '');
		$query=$this->db->get();

		$xx=0;

		
		echo $percorso_base."<br>";
				
		require_once($percorso_base.'htdocs/php-lib/getid3/getid3/getid3.php');
		
		foreach ($query->result() as $row):
			
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $row->id_account);
			$query2=$this->db->get();
			$row2=$query2->row();
			$stringa_casuale=$row2->ugrp_stringa;
			$cartella_base=$percorso_base.$percorso_users."user_".$row->id_account."_".$stringa_casuale."/radio";
			$file=$cartella_base."/".$row->nome_file;

			echo $file."<br>";
			
			$idtemp=$row->id;

			if (file_exists($file)):
				echo ($xx+1).". ".$row->nome_file;
				$getID3 = new getID3;
				$ThisFileInfo = $getID3->analyze($file);
				
				
				
				$lunghezza=$ThisFileInfo['playtime_string']; // playtime in minutes:seconds, formatted string
				$lunghezza_secondi=$ThisFileInfo['playtime_seconds']; // playtime in seconds
				
				if (is_numeric($lunghezza_secondi)) echo " numero (".$idtemp.") ";

				echo " ".$lunghezza." ".$lunghezza_secondi;

				$dataSEC['durata']=$lunghezza_secondi;
				$this->db->where('id', $idtemp);
				$this->db->update('archivio_radio', $dataSEC);

				echo "<br>";
				$xx++;
			endif;
		endforeach;
		
	}
	*/
	
	public function player($id=0) {
		$data['id']=$id;
		$data['titolo']="Player";
		$this->load->view('site_header_short', $data);
		$this->load->view('player', $data);
		$this->load->view('site_footer_short');
	}

}
