<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function cambia_account($grupposelezionato, $pagina)
	{
		// questa funzione permette di cambiare l'account di lavoro attualmente selezionato
		// lo memorizza in un cookie ed effettua il refresh della pagina
		$cookie = array(
		'name'   => 'accountselezionato',
		'value'  => $grupposelezionato,
		'expire' => '28800'
		);
		$this->input->set_cookie($cookie);
		$pagina=str_replace("---", "/", $pagina);
		redirect($pagina);
	}

	public function indice_account()
	{
		$data['operazione']="indice_account";
		$this->load->view('site_header');
		$this->load->view('utenti_account_gestione', $data);
		$this->load->view('site_footer');	
	}

	public function indice_utenti()
	{
		$data['operazione']="indice_user";
		$this->load->view('site_header');
		$this->load->view('utenti_account_gestione', $data);
		$this->load->view('site_footer');	
	}

	public function edita_account($id=0)
	{
		
		if ($id==0)
			$data['titolo']="Crea nuovo account";
		else
			$data['titolo']="Modifica account";
		$data['operazione']="edita_account";
		$data['id_account']=$id;
		$this->load->view('site_header_short', $data);
		$this->load->view('utenti_account_gestione', $data);
		$this->load->view('site_footer_short');	
	}

	public function elimina_account($id_account=0)
	{
		$procedi=0;
		$gruppo=$this->flexi_auth->get_user_group_id();
		$tipologia=$this->strimy_model->account_tipologia($gruppo);
		if ($tipologia==0):	
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $id_account);
			$query=$this->db->get();
			if ($query->num_rows()>0):
				$procedi=1;	 // se l'account richiedente è un amministratore, può procedere alla cancellazione
			endif;
		elseif ($tipologia==1):
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $id_account);
			$this->db->where('ugrp_gruppo_riferimento', $gruppo);
			$query=$this->db->get();
			if ($query->num_rows()>0):
				$procedi=1;	// se l'account richiedente è un rivenditore, e il gruppo specificato è un suo cliente, può procedere alla cancellazione
			endif;
		endif;
		
		if ($procedi==1):

			// elimina la cartella dell'account e relativi contenuti
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $id_account);
			$querySTC=$this->db->get();
			$rowSTC=$querySTC->row();
			$stringa_casuale=$rowSTC->ugrp_stringa;

			$percorso_base=$this->config->item('archivio_real_path');
			$percorso_users=$this->config->item('percorso_users');
			$cartella_account=$percorso_base.$percorso_users."user_".$id_account."_".$stringa_casuale;
			$this->users_model->elimina_directory($cartella_account);

			// elimina tutti i dispositvi associati all'account
			$this->db->from('dispositivi');
			$this->db->where('id_account', $id_account);
			$queryDIS=$this->db->get();
			foreach ($queryDIS->result() as $rowDIS):
				$this->strimy_model->dispositivo_elimina($rowDIS->id_dispositivo);
			endforeach;

			// elimina l'account
			$this->flexi_auth->delete_group($id_account);
			
			// elimina gli utenti appartenenti all'account
			$this->db->where('uacc_group_fk', $id_account);
			$this->db->delete('user_accounts');
			
			// elimina tutte le entry associate
			$this->db->where('id_account', $id_account);
			$this->db->where('archivio_file', $id_account);
			
			$this->db->where('id_account', $id_account);
			$this->db->where('archivio_sistema', $id_account);
			
			$this->db->where('id_account', $id_account);
			$this->db->where('archivio_radio', $id_account);
			
			$this->db->where('id_account', $id_account);
			$this->db->where('archivio_radio_classifica', $id_account);
			
			$this->db->where('id_account', $id_account);
			$this->db->where('archivio_radio_jingles', $id_account);
			
		endif;
		
		redirect('users/indice_account');
		
	}

	public function check_user() {
		$risultato=0;
		$user=$this->input->post('username');
		$email=$this->input->post('email');

		// controlla che l'username non sia già registrato
		$this->db->from('user_accounts');
		$this->db->where('uacc_username', $user);
		$query=$this->db->get();
		if ($query->num_rows()>0) {
			$risultato=1;
		}

		if ($risultato==0):
			// controlla che l'email non sia associata a un utente esistente
			$this->db->from('user_accounts');
			$this->db->where('uacc_email', $email);
			$query=$this->db->get();
			if ($query->num_rows()>0) {
				$risultato=2;
			}

			// controlla che l'email non sia associata a un account esistente
			$this->db->from('user_groups');
			$this->db->where('ugrp_email', $email);
			$query=$this->db->get();
			if ($query->num_rows()>0) {
				$risultato=2;
			}
		endif;
	
		echo $risultato;
		
	}

	public function edita_user($id=0)
	{
		if ($id==0)
			$data['titolo']="Aggiungi nuovo utente";
		else
			$data['titolo']="Modifica utente";
		$data['operazione']="edita_user";
		$data['id_user']=$id;
		$this->load->view('site_header_short', $data);
		$this->load->view('utenti_account_gestione', $data);
		$this->load->view('site_footer_short');	
	}

	public function elimina_user($id_utente=0)
	{
		$procedi=0;
		$gruppo=$this->flexi_auth->get_user_group_id();
		$tipologia=$this->strimy_model->account_tipologia($gruppo);
		$utente=$this->flexi_auth->get_user_id();
		$amministratore=$this->strimy_model->utente_tipologia($utente);

		$this->db->from('user_accounts');
		$this->db->where('uacc_id', $id_utente);
		$queryUT=$this->db->get();
		$rowUT=$queryUT->row();
		$gruppo_utente=$rowUT->uacc_group_fk;
		
		if (($tipologia==0) && ($amministratore==1)):
			// se l'account richiedente è un amministratore, e l'utente richiedente è amministratore può procedere alla cancellazione
			$procedi=1;

		elseif (($gruppo==$gruppo_utente) && ($amministratore==1)):
			// se l'utente richiedente è amministratore, e appartierene allo stesso gruppo, può procedere alla cancellazione 
			$procedi=1;

		elseif (($tipologia==1) && ($amministratore==1)):
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $gruppo_utente);
			$this->db->where('ugrp_gruppo_riferimento', $gruppo);
			$query=$this->db->get();
			if ($query->num_rows()>0):
				$procedi=1;	// se l'account richiedente è un rivenditore, e il gruppo specificato è un suo cliente, può procedere alla cancellazione
			endif;
		endif;
		
		if ($procedi==1):
			$this->flexi_auth->delete_user($id_utente);
		endif;
		
		redirect('users/indice_utenti');
		
	}


	public function salva_utente()
	{
		$utente=$this->flexi_auth->get_user_id();
		$utente_amministratore=$this->strimy_model->utente_tipologia($utente);
		
		$username=$this->input->post('eusername');
		$password=$this->input->post('epassword');
		$email=$this->input->post('eemail');
		$group_id=$this->input->post('account_selezionato');
		$amministratore=$this->input->post('eamministratore');
		$user_id=$this->input->post('eid_account');
		$activate = TRUE;
		
		if ($this->input->post('econferma')=="ok"):
		
			if ($user_id==0):	// inserimento di un nuovo utente
				$user_data = array();
				
				if ($utente_amministratore==1)	// check di sicurezza: se l'utente che ha modificato è amministratore, può cambiare lo status degli altri utenti
					$user_data['uacc_amministratore']=$amministratore;
				
				$this->flexi_auth->insert_user($email, $username, $password, $user_data, $group_id, $activate);
				
			else:	// modifica di un utente esistente
				
				$user_data = array(
					'uacc_email' => $email,
					'uacc_group_fk' => $group_id
				);
				
				if ($utente_amministratore==1)	// check di sicurezza: se l'utente che ha modificato è amministratore, può cambiare lo status degli altri utenti
					$user_data['uacc_amministratore']=$amministratore;
				if ($password!="")
					$user_data['uacc_password']=$password;
				
				$this->flexi_auth->update_user($user_id, $user_data);
			endif;
		
		endif;
		
		redirect('users/indice_utenti');
		
	}

	public function salva_account()
	{
		
		$gruppo=$this->flexi_auth->get_user_group_id();
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $gruppo);
		$query=$this->db->get();
		$row=$query->row();

		$tipologia=$row->ugrp_tipologia; 
		
		if (($this->input->post('econferma')=="ok") && ($tipologia!=2)):

			$id_account=$this->input->post('eid_account');
			$name = $this->input->post('enome');
			$description = $this->input->post('edescrizione');
			$email = $this->input->post('eemail');
			$tipologia = $this->input->post('etipologia');
			$storage = $this->input->post('estorage');
			$bucket = $this->input->post('ebucket');
			$url = $this->input->post('eurl');
			$crediti = $this->input->post('ecrediti');
			$crediti_scala = $this->input->post('ecrediti_scala');

			if ($crediti_scala)
				$crediti_scala=1;
			else
				$crediti_scala=0;

			if ($storage==0):
				$bucket_amazon="";
				$url="";
			endif;


			/*
			$privilegi['immagini']=$this->input->post('eimmagini');
			$privilegi['video']=$this->input->post('evideo');
			$privilegi['audio']=$this->input->post('eaudio');
			$privilegi['radio']=$this->input->post('eradio');
			*/
			
			$privilegi=$this->input->post('eprivilegi');
			
/*			
			$privilegi_serialized=serialize($privilegi);
*/			
			if ($id_account==0):	// inserimento di un nuovo account
			
				$dataACC['privilegi']=$privilegi;
				$dataACC['name']=$name;
				$dataACC['description']=$description;
				$dataACC['email']=$email;
				$dataACC['tipologia']=$tipologia;
				$dataACC['storage']=$storage;
				$dataACC['bucket']=$bucket;
				$dataACC['url']=$url;
				$dataACC['active']=1;
				$dataACC['crediti']=$crediti;
				$dataACC['crediti_scala']=$crediti_scala;

				$dataACCOUNT=$this->users_model->aggiungi_account($dataACC);
				
				$id_nuovo_account=$dataACCOUNT['id_nuovo_account'];
				$stringa_casuale=$dataACCOUNT['stringa_casuale'];
				
				// crea la relativa directory archivio
				$this->users_model->crea_directory($dataACCOUNT);

				// crea il record nella tabella user_groups_dettagli
				$dataANG['id_account']=$id_nuovo_account;
				$this->db->insert('user_groups_dettagli', $dataANG);
			
			else:	// modifica di un account esistente
		
				$privilegi_serialized=serialize($privilegi);
				
				$slash=1;
				while ($slash==1) {
					if (substr($url, -1)=="/"):
						$url=rtrim($url, "/");
					else:
						$slash=0;
					endif;
				}
		
				$group_data = array(
					'ugrp_name' => $name,
					'ugrp_desc' => $description,
					'ugrp_email' => $email,
					'ugrp_tipologia' => $tipologia,
					'ugrp_privilegi' => $privilegi_serialized,
					'ugrp_storage' => $storage,
					'ugrp_bucket' => $bucket,
					'ugrp_url' => $url,
					'crediti' => $crediti,
					'crediti_scala' => $crediti_scala
				);

				$this->db->where('ugrp_id', $id_account);
				$this->db->update('user_groups', $group_data);

				// verifica che sia presente il record nella tabella user_groups_dettagli
				$this->db->from('user_groups_dettagli');
				$this->db->where('id_account', $id_account);
				$queryANG=$this->db->get();
				if ($queryANG->num_rows()==0):
					$dataANG['id_account']=$id_account;
					$this->db->insert('user_groups_dettagli', $dataANG);
				endif;

			endif;
			
		endif;
		redirect('users/indice_account');
	}

	public function login()
	{
		$data=Array();
		$this->load->view('site_header');
		$this->load->view('login', $data);
		$this->load->view('site_footer');
	}

	public function login_check()
	{
		
		$identity = $this->input->post('euser');
		$password = $this->input->post('epassword');
		$remember_user = true;
		$risultato=$this->flexi_auth->login($identity, $password, $remember_user);
		
		if ($risultato):
			$gruppo=$this->flexi_auth->get_user_group_id();
			$cookie = array(
				'name'   => 'accountselezionato',
				'value'  => $gruppo,
				'expire' => '28800'
				);
			$this->input->set_cookie($cookie);
			redirect('strimy/frontpage');
		else:
			redirect('users/login');
		endif;
			
	}

	public function logout()
	{
		error_reporting(E_ERROR);
		$cookie = array(
			'name'   => 'accountselezionato',
			'value'  => 0
		);
		$this->input->set_cookie($cookie);
		$this->flexi_auth->logout(FALSE);
		redirect('users/login');
	}

	public function spazio_account() {
		$grupposelezionato=$this->flexi_auth->get_user_group_id();
		$spazio_account=$this->users_model->spazio_account($grupposelezionato);
		
		$occupazione=round(100*$spazio_account['dimensione_totale']/$spazio_account['limite_spazio']);
		
	
		echo '<div class="progress" style="margin: 0;">';
			echo '<div class="progress-bar text-center" role="progressbar" aria-valuenow="'.$occupazione.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$occupazione.'%; color: white">';
			echo ' <span>'.(round($spazio_account['dimensione_totale']/1024/1024*10)/10).'/'.round($spazio_account['limite_spazio']/1024/1024).'Mb</span>';
			echo '</div>';
		echo '</div>';
	}

	public function gestione_account() {
		$data['operazione']="pagamento";
		$this->load->view('site_header');
		$this->load->view('account_gestione', $data);
		$this->load->view('site_footer');
	}
	
	public function gestione_account_pagamento($operazione="pagamento") {
		$data['operazione']=$operazione;
		if ($operazione=="conferma")
			$this->load->view('site_header');
		$this->load->view('account_gestione_pagamento', $data);
		if ($operazione=="conferma")
			$this->load->view('site_footer');
	}
	
	public function gestione_account_dati() {
		$data['titolo']="Dati anagrafici";
		$data['operazione']="gestione";
		$this->load->view('site_header_short', $data);
		$this->load->view('account_gestione', $data);
		$this->load->view('site_footer_short');
		// $gruppo=$this->flexi_auth->get_user_group_id();
		// $this->users_model->elenca_dettagli_account($gruppo);
	}
	
	public function gestione_account_dati_salva() {
		$dataFORM=$this->input->post();
		$grupposelezionato=$this->flexi_auth->get_user_group_id();
		$tipoform=$dataFORM['etipo_form'];
		unset($dataFORM['etipo_form']);

		$this->db->from('user_groups_dettagli');
		$this->db->where('id_account', $grupposelezionato);
		$queryACC=$this->db->get();
		if ($queryACC->num_rows()==0):
			$dataFORM['id_account']=$grupposelezionato;
			$this->db->insert('user_groups_dettagli', $dataFORM);
		else:
			$this->db->where('id_account', $grupposelezionato);
			$this->db->update('user_groups_dettagli', $dataFORM);
		endif;
		
		if ($tipoform=="pagamento"):
			$data['operazione']="conferma";
			$this->load->view('account_gestione_pagamento', $data);
		endif;
		
	}
	
	function stampa_fattura($id_fattura=0)
	{
		$this->load->helper('pdf_helper');
		$this->load->model('Stampa_model');
		$data['id_fattura']=$id_fattura;
		$this->Stampa_model->stampa_fattura($data);
	}
	

}

