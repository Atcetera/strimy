<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pub extends CI_Controller {

	// questo controller gestisce l'accesso ai contenuti pubblici, ovvero quelli visibili a tutti i visitatori
	public function index() {
		if ($this->config->item('base_url')=="http://www.westrim.it/"): // è il portale di Westrim
			$page="index_westrim";
		else: // è il portale di Strimy
			$page="index";
		endif;
		
		$this->load->view('site_header');
		$this->load->view($page);
		$this->load->view('site_footer');
	}
/*
	public function registrati() {
		$data['operazione']="registrazione";
		$this->load->view('site_header', $data);
		$this->load->view('registrazione', $data);
		$this->load->view('site_footer');
	}
*/

	public function registrazione() {
		
		if ($_POST['conferma']=="ok"):

			$stringa_casuale=$this->strimy_model->stringa_casuale_genera(20);
			
			$dataACC['username']=$_POST['username'];
			$dataACC['email']=$_POST['email'];
			$dataACC['stringa_casuale']=$stringa_casuale;
			$dataACC['data_richiesta']=time();
			
			$this->db->insert('user_groups_attivazione', $dataACC);
			$id_account=$this->db->insert_id();
			
			$messaggio="";
			$messaggio=$messaggio."Attiva il tuo account cliccando qui:\n";
			$messaggio=$messaggio.base_url()."index.php/pub/registrazione_conferma/".$id_account."/".$stringa_casuale;

			$headers = "From: Strimy <no-reply@strimy.it>\r\n";
			$headers .= "X-Mailer: PHP/" . phpversion();
			
			mail($_POST['email'],"Registrazione Strimy",$messaggio,$headers);
			
		endif;
	}

	public function registrazione_controllo() {
		
		$errore_user=0;
		$errore_mail=0;
		
		if ($_POST['conferma']=="ok"):
			
			$username=$_POST['username'];
			$email=$_POST['email'];

			// verifica che non sia già presente un utente con lo stesso nome
			$this->db->from('user_accounts');
			$this->db->where('uacc_username', $username);
			$query=$this->db->get();
			if ($query->num_rows()>0)
				$errore_user=1;
				
			// verifica che non sia già presente un account con lo stesso nome
			$this->db->from('user_groups');
			$this->db->where('ugrp_name', $username);
			$query=$this->db->get();
			if ($query->num_rows()>0)
				$errore_user=1;

			// verifica che non sia già presente un utente con lo stesso indirizzo email
			$this->db->from('user_accounts');
			$this->db->where('uacc_email', $email);
			$query=$this->db->get();
			if ($query->num_rows()>0)
				$errore_mail=1;

			// verifica che non sia già presente un account con lo stesso indirizzo email
			$this->db->from('user_groups');
			$this->db->where('ugrp_email', $username);
			$query=$this->db->get();
			if ($query->num_rows()>0)
				$errore_mail=1;

			$errore_totale=$errore_user."-".$errore_mail;

			if ($errore_totale=="0-0"):
				$this->db->where('username', $username);
				$this->db->or_where('email', $email);
				$this->db->delete('user_groups_attivazione');
			endif;

			echo $errore_totale;

		endif;
	}

	public function registrazione_conferma($id=0, $stringa_casuale="") {
		
		$login=$this->flexi_auth->is_logged_in();

		if ($login)
			redirect("strimy/frontpage");
		
		$this->db->from('user_groups_attivazione');
		$this->db->where('id', $id);
		$this->db->where('stringa_casuale', $stringa_casuale);
		$query=$this->db->get();
		if ($query->num_rows()>0):
			$row=$query->row();

			$dataACC['privilegi']=$this->config->item('default_privilegi');
			$dataACC['name']=$row->username;
			$dataACC['description']="";
			$dataACC['email']=$row->email;
			$dataACC['tipologia']=2;
			$dataACC['storage']=0;
			$dataACC['bucket']="";
			$dataACC['url']="";
			$dataACC['active']=1;
			$dataACC['limite_storage']=$this->config->item('default_storage');
			$dataACC['crediti']=$this->config->item('default_crediti');
			$dataACC['crediti_scala']=1;
			$dataACC['data_attivazione']=time();

			$dataACCOUNT=$this->users_model->aggiungi_account($dataACC);
				
			$id_nuovo_account=$dataACCOUNT['id_nuovo_account'];
			$stringa_casuale=$dataACCOUNT['stringa_casuale'];
				
			// crea la relativa directory archivio
			$this->users_model->crea_directory($dataACCOUNT);

			// crea il nuovo utente
			$password=$this->strimy_model->stringa_casuale_genera(8);
			$user_data['uacc_amministratore']=1;	
			$this->flexi_auth->insert_user($row->email, $row->username, $password, $user_data, $id_nuovo_account, TRUE);

			// invia la mail con le credenziali
			$messaggio="";
			$messaggio=$messaggio."Congratulazioni! Il tuo account è attivo.\n\n";
			$messaggio=$messaggio."Queste sono le credenziali di accesso:\n";
			$messaggio=$messaggio."username: ".$row->username."\n";
			$messaggio=$messaggio."password: ".$password."\n\n";
			$messaggio=$messaggio."Ti abbiamo regalato ".$this->config->item('default_crediti')." crediti per usare da subito Strimy!\n\n";
			$messaggio=$messaggio."Accedi e configura subito il tuo primo dispositivo. Ti consigliamo anche di cambiare la password.\n\n";
			$headers = "From: Strimy <no-reply@strimy.it>\r\n";
			$headers .= "X-Mailer: PHP/" . phpversion();
			mail($row->email,"Account Strimy attivato!",$messaggio,$headers);
			
			// invia la notifica ad Atcetera
			$messaggio="";
			$messaggio=$messaggio."Si è iscritto un nuovo utente: ".$row->username." (".$row->email.")\n\n";
			$headers = "From: Strimy <no-reply@strimy.it>\r\n";
			$headers .= "X-Mailer: PHP/" . phpversion();
			mail($this->config->item('paypal_account'),"[strimy] Nuovo utente Strimy",$messaggio,$headers);

			// elimina l'utente dalla coda di attivazione
			$this->db->where('stringa_casuale', $row->stringa_casuale);
			$this->db->delete('user_groups_attivazione');

			$dataMESS['messaggio']="<div class=\"alert alert-success text-center\" role=\"alert\">Congratulazioni: il tuo account &egrave; attivo!<br>Abbiamo inviato le credenziali di accesso all'indirizzo <strong>".$row->email."</strong></div>";
		
		else:
			$dataMESS['messaggio']="<div class=\"alert alert-danger text-center\" role=\"alert\"><strong>Attenzione</strong>: link di attivazione errato o scaduto</div>";
		endif;
		
		$this->load->view('site_header');
		$this->load->view('login', $dataMESS);
		$this->load->view('site_footer');
	}

	public function demo() {
		$this->load->view('site_header');
		$this->load->view('demo');
		$this->load->view('site_footer');		
	}

	public function privacy() {
		$this->load->view('site_header');
		$this->load->view('privacy');
		$this->load->view('site_footer');		
	}

	public function contatti() {
	//	$this->load->view('site_header');
		$this->load->view('contatti');
	//	$this->load->view('site_footer');		
	}

	public function contatti_invia() {
	
		if ($this->input->post('econferma')=="ok"):
	
			$nome=$this->input->post('enome');
			$azienda=$this->input->post('eazienda');
			$email=$this->input->post('email');
			$telefono=$this->input->post('etelefono');
			$messaggio=$this->input->post('emessaggio');
			
			$destinatario="info@atcetera.it";
			$headers="From: Strimy webform <non-rispondere@strimy.it>";
			$oggetto="nuovo messaggio Strimy";
		
			$testo="Nuovo contatto dalla form di www.strimy.it. I dati raccolti sono:\n";
			$testo=$testo."\nNome: ".$nome;
			$testo=$testo."\nAzienda: ".$azienda;
			$testo=$testo."\nTelefono: ".$telefono;
			$testo=$testo."\nEmail: ".$email;
			$testo=$testo."\nMessaggio:\n".$messaggio;
			$testo=$testo."\n\n";


			if (mail($destinatario, $oggetto, $testo, $headers))
				echo "0";
			else
				echo "1";

			/*
			$headers="From: Strimy <non-rispondere@strimy.it>";
			$oggetto_conferma="Grazie per averci contattato!";
			$testo_conferma="Il tuo messaggio e' stato ricevuto e provvederemo a ricontattarti quanto prima.\nGrazie!";
			$testo_conferma=$testo_conferma."\n\nlo staff di Strimy - Atcetera";
		
			mail($email, $oggetto_conferma, $testo_conferma, $headers);
			*/
		
		endif;
		
	}

}

