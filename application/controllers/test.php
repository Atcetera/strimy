<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

/*
	function reset() {
		$this->db->where('id_dispositivo', '0');
		$this->db->delete('playlist_sequenze');
	
		$this->db->where('id_dispositivo', '0');
		$this->db->delete('playlist_elementi');
	}

	public function tester() {
		$this->db->from('playlist_elementi');
		$this->db->order_by('nome_file');
		$this->db->where('nome_file !=', '');
		$query=$this->db->get();
		foreach ($query->result() as  $row):
			$parts=explode(".", $row->nome_file);
			$np=count($parts);
			if ($parts[($np-1)]!="mp3")
			echo $row->id." ".$row->nome_file."<br>";
		
		endforeach;
	
	}


	public function jingle() {
		$this->db->from('playlist_sequenze');
		$this->db->where('tipologia', 'radio');
		$this->db->where('jingle_categoria !=', '0');
		$this->db->order_by('id_dispositivo', 'asc');
		$this->db->order_by('giorno', 'asc');
		$query=$this->db->get();
		foreach ($query->result() as $row):
			echo $row->id." ".$row->id_dispositivo." ".$row->id_playlist." ".$row->giorno."<br>";
			
			$dataJIN['id_sequenza']=$row->id;
			$dataJIN['id_dispositivo']=$row->id_dispositivo;
			$dataJIN['id_playlist']=$row->id_playlist;
			$dataJIN['giorno']=$row->giorno;
			$dataJIN['jingle_categoria']=$row->jingle_categoria;
			$dataJIN['jingle_intervallo']=$row->jingle_intervallo;
			$this->db->insert('playlist_sequenze_jingle', $dataJIN);
			unset($dataJIN);
			
		endforeach;
	
		
	}
*/



	// questa funzione serve a upgradare il database,
	// o a modificare ricorsivamente la struttura delle directory
	// se richiesto dopo importanti cambiamenti o upgrade dell'applicazione
	public function upgrade() {
		$ok=0;	// 0: blocca lo script;	1: esegue lo script (a proprio rischio e pericolo
		
		if ($ok==0)
			die('procedura non ammessa');

		/////////////////////////////////////////////////////////////////////////////////////////
		// fix della tabella dispositivi_configurazione per adeguarla alla nuova funzione
		$fields=$this->db->list_fields('dispositivi_configurazione');
		foreach ($fields as $temp):
			echo $temp."<br>";
			$field[]=$temp;
		endforeach;

		$this->db->from('dispositivi_configurazione');
		$query=$this->db->get();
		
		foreach ($query->result_array() as $row):
			
			foreach ($field as $temp):
				echo $row[$temp]."<br>";
			endforeach;
			
			$data['id_dispositivo']=$row['name'];
			unset($row['parametri_configurazione']);
			$data['parametri_configurazione']=serialize($row);
			$this->db->where('name', $row['name']);
			$this->db->update('dispositivi_configurazione', $data);
			
			echo "<br>";
		endforeach;
		
		
		echo "<br><strong>Fix 1: completato</strong><br><br>";
		
		
		/////////////////////////////////////////////////////////////////////////////////////////
		// calcola la durata dei brani e la salva nel database
		$percorso_base=$this->config->item('real_path');
		$percorso_users=$this->config->item('percorso_users');
		
		$this->db->from('archivio_radio');
		$this->db->where('nome_file !=', '');
		$this->db->where('durata', '');
		$query=$this->db->get();

		$xx=0;

		
		echo $percorso_base."<br>";
				
		require_once($percorso_base.'htdocs/php-lib/getid3/getid3/getid3.php');
		
		foreach ($query->result() as $row):
			
			$this->db->from('user_groups');
			$this->db->where('ugrp_id', $row->id_account);
			$query2=$this->db->get();
			$row2=$query2->row();
			$stringa_casuale=$row2->ugrp_stringa;
			$cartella_base=$percorso_base.$percorso_users."user_".$row->id_account."_".$stringa_casuale."/radio";
			$file=$cartella_base."/".$row->nome_file;

			echo $file."<br>";
			
			$idtemp=$row->id;

			if (file_exists($file)):
				echo ($xx+1).". ".$row->nome_file;
				$getID3 = new getID3;
				$ThisFileInfo = $getID3->analyze($file);
				
				
				
				$lunghezza=$ThisFileInfo['playtime_string']; // playtime in minutes:seconds, formatted string
				$lunghezza_secondi=$ThisFileInfo['playtime_seconds']; // playtime in seconds
				
				if (is_numeric($lunghezza_secondi)) echo " numero (".$idtemp.") ";

				echo " ".$lunghezza." ".$lunghezza_secondi;

				$dataSEC['durata']=$lunghezza_secondi;
				$this->db->where('id', $idtemp);
				$this->db->update('archivio_radio', $dataSEC);

				echo "<br>";
				$xx++;
			endif;
		endforeach;
		
		echo "<br><strong>Fix 2: completato</strong><br><br>";
		
	}

}

