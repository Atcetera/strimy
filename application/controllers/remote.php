<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remote extends CI_Controller {

/*
	function reset() {
		$this->db->where('id_dispositivo', '0');
		$this->db->delete('playlist_sequenze');
	
		$this->db->where('id_dispositivo', '0');
		$this->db->delete('playlist_elementi');
	}
*/

	public function login() {
		// header('Access-Control-Allow-Origin: *'); 
		// header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		$callback ='check_login';
		$identity=$_GET['username'];
		$password=$_GET['password'];
		$remember_user = true;
	 
		$arr =array();
		$risultato=$this->flexi_auth->login($identity, $password, $remember_user);
		if ($risultato):
			$valore=$this->flexi_auth->get_user_group_id();
		else:
			$valore=0;
		endif;
		echo $valore;
	//	$arr['login']=$valore;
	//	echo $callback.'(' . json_encode($arr) . ')';
		
	}


	public function elenca_dispositivi($username="", $account="") {

		$percorso_base=$this->config->item('real_path');
		$percorso_devices=$this->config->item('percorso_devices');
		$percorso_users=$this->config->item('percorso_users');

	//	$cartella_base=$percorso_base.$percorso_devices.$device_id;
	
		$nome_file="check.txt";
//		$file=$cartella_base."/".$nome_file;
		$orario=time();
		$data=date('d/m/y', $orario);
		
		$orario_attuale=time();
		
		$id_gruppo[0]=0;
		$desc_gruppo[0]="";
		$this->db->from('dispositivi_gruppi');
		$this->db->where('id_account', $account);
		$this->db->order_by('descrizione', 'asc');
		$queryGRP=$this->db->get();
		foreach ($queryGRP->result() as $rowGRP):
			$id_gruppo[]=$rowGRP->id;
			$desc_gruppo[]=$rowGRP->descrizione;
		endforeach;

		
		echo "ultimo check: ".date('d/m/y H:i:s', $orario_attuale);
		echo "<br>";
		
		$yy=0;
		foreach ($id_gruppo as $id_gruppo_temp):
			
			
			$this->db->from('dispositivi');
			$this->db->where('id_account', $account);
			$this->db->where('id_gruppo', $id_gruppo_temp);
			$this->db->where('attivato', 1);
			$this->db->order_by('id', 'asc');
			// $this->db->order_by('id_gruppo', 'asc');
			$query=$this->db->get();
			
			if ($id_gruppo[$yy]!=0)
				echo "<span style=\"font-size: 2em; font-weight: bold;\">".$desc_gruppo[$yy]."</span>";
			
			
			echo "<ul data-role=\"listview\" data-inset=\"true\">";
		   
			foreach ($query->result() as $row):

				$cartella_base=$percorso_base.$percorso_devices.$row->id_dispositivo;

				$file=$cartella_base."/".$nome_file;
				$data=date('d/m/y', $orario);

				$dispositivo_funzionante=0;
				$errore=0;
				if (!file_exists($file)):
					// il file non esiste
					$errore=1;
					$messaggio="Dispositivo non connesso o in attesa di attivazione";
				else:
					$xx=0;
					$fp = fopen($file, 'r+');
					// fseek($fp, 10, SEEK_SET); //Mi posiziono al 10° carattere
					while(!feof($fp))
					{
						$riga_temp=fgets($fp);
						$riga[$xx]=explode(";", $riga_temp);
						$xx++;
					}
					fclose($fp);
					$intervallo=$orario_attuale-$riga[0][0];
					$status=$riga[0][1];

					if ($intervallo>120):
						if ($intervallo>86400)
							$intervallo="più di 1 giorno fa";
						elseif ($intervallo>3600)
							$intervallo="più di 1 ora fa";
						elseif ($intervallo>900)
							$intervallo="più di 15 minuti fa";
						else
							$intervallo=$intervallo." secondi fa";
						$messaggio="spento o non connesso (da ".$intervallo.")";
					else:
						$dispositivo_funzionante=1;
						$messaggio="stato: ".$status;
					endif;
				endif;

				echo "<li>";
				if ($dispositivo_funzionante==0)
					echo "<div>";
				if ($dispositivo_funzionante==1)
					echo "<div style=\"border-right: 8px solid #00FF00;\">";
				echo "<span onclick=\"dettagli_dispositivo('".$row->id_dispositivo."')\">";
				echo "<strong>".$row->id_dispositivo;
				if ($row->comando_remoto!=0)
					echo " <span style=\"color: #900; font-size: 0.9em;\">(".$row->comando_remoto.")</span>";
				echo "</strong><br>";
				echo $row->descrizione;
				echo "<br>".$messaggio;
				echo "</span>";
				echo "</div>";

				echo "<div id=\"dettagli_".$row->id_dispositivo."\" class=\"dettagli_dispositivo\" style=\"display: none; padding-top: 10px; font-size: 0.8em\">";
				echo "seleziona comando:<br>";
				echo "<button onclick=\"checkform_comando_remoto('1', '".$row->id_dispositivo."');\">riavvia dispositivo</button>";
				echo "<button onclick=\"checkform_comando_remoto('2', '".$row->id_dispositivo."');\">invia report dettagliato</button>";
				echo "<button onclick=\"checkform_comando_remoto('3', '".$row->id_dispositivo."');\">forza download dei contenuti</button>";
				echo "<button onclick=\"checkform_comando_remoto('0', '".$row->id_dispositivo."');\">annulla comandi pendenti</button>";
				echo "<br>";
				echo "<button onclick=\"dettagli_dispositivo_chiudi()\">(chiudi)</button>";
				echo "<br>";
				echo "</div>";

				echo "</li>";

				
			endforeach;
			
			echo "</ul>";
			echo "<br>";
			
		$yy++;
		endforeach;
		
		
		$this->db->from('dispositivi');
		$this->db->where('attivato', 0);
		$this->db->order_by('data_preattivazione', 'asc');
		$query2=$this->db->get();
		
		if ($query2->num_rows()>0):
			echo "dispositivi in attesa di attivazione:";
			echo "<br>";
			echo "<ul data-role=\"listview\" data-inset=\"true\">";
			foreach ($query2->result() as $row2):
				echo "<li>";
				echo "<strong>".$row2->id_dispositivo."</strong>";
				echo "<br>";
				echo "in attesa dal ".date('d/m/y H:i:s', $row2->data_preattivazione);
				echo "<br>";
				echo "<span id=\"".$row2->id_dispositivo."\">";
				echo "<button onclick=\"attiva_dispositivo('".$row2->id_dispositivo."', '".$account."');\">";
				echo "attiva";
				echo "</button>";
				echo "</span>";
			endforeach;
			echo "</ul>";
		endif;
		
		echo "<input type=\"hidden\" id=\"edita\" value=\"0\">";
		
		?>
		<script language="Javascript">
			function attiva_dispositivo(id_dispositivo, id_account) {
				var localita="";
				var descrizione="";
				while (localita == "") {
					localita=window.prompt("Localita:","");
					if (localita==null)
						return false;
				}
				while (descrizione == "") {
					descrizione=window.prompt("Descrizione:",localita);
					if (descrizione==null)
						return false;
				}

				var parametri="eaccount="+id_account;
				parametri=parametri+"&eid_dispositivo="+id_dispositivo;
				parametri=parametri+"&elocalita="+localita;
				parametri=parametri+"&edescrizione="+descrizione;
				parametri=parametri+"&econferma=ok";
			
				// alert(parametri);
			
				$.ajax({
				url: "<?php echo base_url() ?>index.php/strimy/dispositivo_attivazione", 
				data: parametri,
				async: false,
				dataType: 'html',
				method: 'POST',
				success: function(risultato) {
					if (risultato==0)
						("span#"+id_dispositivo).html("Attendi...");
					}
				});
		
				return false;

			}
	
	
			function dettagli_dispositivo(id_dispositivo) {
				$("div.dettagli_dispositivo").hide(20);
				$("div#dettagli_"+id_dispositivo).show(100);
				$("#edita").val(1);
			}

			function dettagli_dispositivo_chiudi() {
				$("div.dettagli_dispositivo").hide(50);
				$("#edita").val(0);
			}
			
			function checkform_comando_remoto(valore, id_dispositivo) {
			
				var messaggio="";
				var messaggio_valore="";
					
				var stringa = '';
				stringa=stringa+"ecomando="+valore;
				stringa=stringa+"&edevice_id="+id_dispositivo;
				stringa=stringa+"&econferma=ok";

				if (valore==1)
					messaggio_valore="riavvio dispositivo";
				if (valore==2)
					messaggio_valore="report dettagliato";
				if (valore==3)
					messaggio_valore="download forzato";
				if (valore==0)
					messaggio_valore="annullamento comandi";
			

				if (confirm("Comando: "+messaggio_valore+".\nConfermi l'operazione?")) {
				
					$.ajax({

					  url: "<?php echo base_url(); ?>index.php/strimy/dispositivo_comando_remoto_conferma", 
					  data: stringa,
					  async: false,
					  dataType: 'html',
					  method: 'POST',
					  success: function(risultato) {
							if (risultato=="0") {
							
								if (valore==0) messaggio="comandi annullati";
									else messaggio="Comando inviato correttamente!";
								alert(messaggio);
								dettagli_dispositivo_chiudi();
								}
							else {
								alert('si è verificato un errore inaspettato:\n'+risultato);
								}
							}
						
						});
					
					return false;
				}

			}
			
	</script>
		
		<?php
		
	}

	public function log() {

		$percorso_base=$this->config->item('real_path');
		$cartella_base=$percorso_base."htdocs/strimy/logs";
		$nome_file="songs.rpt";
		$test=0;

		$errore=0;		
		if (!isset($_FILES["efile"]["name"])) $_FILES["efile"]["name"]="";
		
		if ($_FILES["efile"]["name"]==$nome_file):
			$file_temporaneo=$_FILES["efile"]["tmp_name"];
		elseif ($test==1):
			$file_temporaneo=$cartella_base."/".$nome_file;
		else:
			$errore=1;
		endif;

		// se si sono verificati errori interrompe l'operazione, altrimenti prosegue

		if ($errore==1):

			die('file: '.$_FILES["efile"]["name"].'; file errato o assente');
		
		else:
			$errore=0;
		
			if (file_exists($file_temporaneo)):
				$fh = fopen($file_temporaneo, "r") or die("can't open file");
					$xx=0;
					while (!feof($fh)) {
						$riga = fgets($fh);
						if ($riga!=""):

							$riga_temp=explode(";", $riga);
							$id_dispositivo=$riga_temp[0];
							$data=$riga_temp[1];
							$ora=$riga_temp[2];
							$file=$riga_temp[3];
							
							if (isset($riga_temp[4]))
								$tipo_file=$riga_temp[4];
							else
								$tipo_file=0;
							
							$ora_temp=explode(":", $ora);
							$data_temp=explode("/", $data);
							$orario=mktime($ora_temp[0],$ora_temp[1],$ora_temp[2],$data_temp[1],$data_temp[0],$data_temp[2]);
							
							if ($xx==0):
								$this->db->from('dispositivi');
								$this->db->where('id_dispositivo', $id_dispositivo);
								$this->db->where('attivato', 1);
								$queryDIS=$this->db->get();
								if ($queryDIS->num_rows()==0):
									die('dispositivo non presente');
								endif;
							endif;
							
							// verifica che la voce di log non sia già stata inserita nel database, per evitare record doppi
							/*
							$this->db->from('logs');
							$this->db->where('id_dispositivo', $id_dispositivo);
							$this->db->where('orario', $orario);
							$this->db->where('nome_file', $file);
							$this->db->where('tipologia_file', $tipo_file);
							$queryLOG=$this->db->get();
							*/
							// if ($queryLOG->num_rows()==0):
							
								// recupera l'id del file, e di conseguenza artista e titolo
								$id_temp=explode("_", $file);
								$id_temp=$id_temp[0];
								$this->db->from('archivio_radio');
								$this->db->where('id', $id_temp);
								$queryIDF=$this->db->get();
								if ($queryIDF->num_rows()>0):
									$rowIDF=$queryIDF->row();
									$dataIMP['artista']=$rowIDF->artista;
									$dataIMP['titolo']=$rowIDF->titolo;
								endif;
							
								$dataIMP['id_file']=$id_temp;
								$dataIMP['id_dispositivo']=$id_dispositivo;
								$dataIMP['orario']=$orario;
								$dataIMP['nome_file']=$file;
								$dataIMP['tipologia_file']=$tipo_file;
								$this->db->insert('logs', $dataIMP);
							// endif;
							unset ($dataIMP);
							
							$xx++;
						endif;
					}
				fclose($fh);
				$totale_righe=$xx;

			else:
			
				die('file errato o assente');

			endif;


		// operazione terminata
		endif;
		echo "importazione terminata";
		
	}


	public function leggi_log() {
		/*
			$this->db->from('logs');
			$query=$this->db->get();
			foreach ($query->result() as $row):
				echo $row->nome_file." ";
				echo $row->id_dispositivo." ";
				echo "<b>".date("d/m/Y H:i:s", $row->orario)."</b>";
				echo "<br>";
			endforeach;
		*/
	}

	public function check_dispositivi() {

		$timeout_secondi=300;	// intervallo di tempo, in secondi, oltre il quale un dispositivo è considerato offline

		$percorso_base=$this->config->item('archivio_real_path');
		$percorso_devices=$this->config->item('percorso_devices');
		$percorso_users=$this->config->item('percorso_users');
		$nome_file="check.txt";

		$this->db->from('dispositivi');
		$this->db->where('attivato', 1);
		$query=$this->db->get();
		
		foreach ($query->result() as $row):
			
			$cartella_base=$percorso_base.$percorso_devices.$row->id_dispositivo;
			$file=$cartella_base."/".$nome_file;
			$data_check['file']=$file;

			$data_risultato=$this->strimy_model->check_dispositivo($data_check);

			$ora_attuale_raw=time();
			$data_attuale=date("d/m/Y", $ora_attuale_raw);
			$ora_attuale=date("H:i:s", $ora_attuale_raw);

			$messaggio_offline=0;
			$messaggio_online=0;

			if (($data_risultato['intervallo']>$timeout_secondi) || ($data_risultato['status_raw']=="non-funzionante")):  // il dispositivo risulta offline o non funzionante; verifica se è già stato inviato un messaggio di avvertimento
				$this->db->from('log_messaggi');
				$this->db->where('id_dispositivo', $row->id_dispositivo);
				$this->db->where('offline_inviato', 1);
				$queryCOFF=$this->db->get();
				if ($queryCOFF->num_rows()==0):
					$messaggio_offline=1;
				endif;

			else:	// il dispositivo risulta online; verifica se era stato inviato un messaggio di offline
			
				$this->db->from('log_messaggi');
				$this->db->where('id_dispositivo', $row->id_dispositivo);
				$this->db->where('offline_inviato', 1);
				$queryCOFF=$this->db->get();
				if ($queryCOFF->num_rows()>0):
					$messaggio_online=1;
				endif;
			
			endif;

			if ($messaggio_offline==1):
				echo $row->id_dispositivo." ".$data_risultato['intervallo']." <b>non funzionante</b><br>";
				
				$messaggio="Il dispositivo ".$row->id_dispositivo." ha smesso di funzionare alle ".$ora_attuale." del ".$data_attuale."\n\n";
				$messaggio=$messaggio."status: ".$data_risultato['status_raw']."\n";
				$messaggio=$messaggio."dettagli dispositivo\n- descrizione: ".$row->descrizione."\n- attivazione: ".date('d/m/Y', $row->data_attivazione);
				$data_mail['messaggio']=$messaggio;
				$data_mail['oggetto']="[strimy alert] dispositivo ".$row->id_dispositivo." non funzionante!";
				$this->strimy_model->invia_mail($data_mail);
				
				$dataINS['offline_inviato']=1;
				$dataINS['id_dispositivo']=$row->id_dispositivo;
				$this->db->insert('log_messaggi', $dataINS);
				
			endif;
		
			if ($messaggio_online==1):
				echo $row->id_dispositivo." ".$data_risultato['intervallo']." <b>funzionante</b><br>";
				
				$messaggio="Il dispositivo ".$row->id_dispositivo." ha ripreso a funzionare alle ".$ora_attuale." del ".$data_attuale."\n\n";
				$messaggio=$messaggio."dettagli dispositivo\n- descrizione: ".$row->descrizione."\n- attivazione: ".date('d/m/Y', $row->data_attivazione);
				$data_mail['messaggio']=$messaggio;
				$data_mail['oggetto']="[strimy] dispositivo ".$row->id_dispositivo." funzionante";
				$this->strimy_model->invia_mail($data_mail);
				
				$this->db->where('id_dispositivo', $row->id_dispositivo);
				$this->db->where('offline_inviato', 1);
				$this->db->delete('log_messaggi');
				
			endif;
		
		
		endforeach;
		
		echo "check terminato";
		
	}

	public function leggi_log_playlist($id_dispositivo="") {
		if ($id_dispositivo!=""):
			$this->db->from('logs');
			$this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->order_by('orario', 'desc');
			$query=$this->db->get();
			
			foreach ($query->result() as $row):
				echo $row->id_dispositivo." -- ".date('d/m/y H:i:s', $row->orario)." -- ".$row->nome_file;
				echo "<br>";
			endforeach;
		endif;

	}


	public function leggi_log_dispositivi($id_dispositivo="", $orario=0, $risoluzione=15) {
		if ($id_dispositivo!=""):
		
			?>
			
			<style>
				
				.barra {
					float: left;
					position: relative;
					top: 20px;
					border-right: 1px solid #ccc;
					height: 100px;
					opacity: 0.6;
				}

				.barra:hover {
					opacity: 1;
				}
				
				.barra_stato {
					float: left;
					position: absolute; 
					top: -165px;
					left: -10px;
					font-size: 0.9em;
					height: 20px;
					z-index: 20;
					display: none;
					border: 1px solid #666;
					color: #666;
					padding: 5px;
					background-color: #eee;
					width: 200px;
					height: 50px;
				}
				
				.datepicker{
					width: 150px;
				}
				
			</style>
			
			<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>js/jquery-1.7.min.js"></script>
			<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>js/jquery.tinysort.min.js"></script>
			<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
			
			<script language="javascript">
				$(function() {
					$(".datepicker").datepicker();
				});

				$.datepicker.setDefaults({
					dateFormat: "dd/mm/y",
					dayNames: [ "Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato" ],
					dayNamesMin: [ "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" ],
					firstDay: 1,
					changeMonth: true,
					changeYear: true,
					monthNames: [ "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" ],
					monthNamesShort: [ "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" ],
					yearRange: '<?php echo (date('Y'))-100; ?>:<?php echo date('Y'); ?>',
					defaultDate: new Date(<?php echo (date('Y'))-20; ?>, 00, 01)
				});
				
				function mostrainfo(indice) {
					$('#barra_stato_'+indice).show();
				}
				
				function nascondiinfo(indice) {
					$('#barra_stato_'+indice).hide();
				}
				function aggiorna() {
					var risoluzione=$('#erisoluzione').val();
					var giorno=$('#egiorno').val();
					var dispositivo=$('#edispositivo').val();
					giorno=giorno.replace("/", "-");
					giorno=giorno.replace("/", "-");
					$('#contenitore_grafico').css('opacity', '0.3');
					var url="<?php echo $this->config->item('base_url'); ?>index.php/remote/leggi_log_dispositivi/"+dispositivo+"/"+giorno+"/"+risoluzione;
					window.location.href=url;
				}
			</script>
			<?php
			
			$stato[]="offline";
			$stato[]="non-funzionante";
			$stato[]="home";
			$stato[]="radio";
			$stato[]="musictv";
			
			$colore['offline']="#eee";
			$colore['non-funzionante']="#111";
			$colore['home']="#ffc";
			$colore['radio']="#fcc";
			$colore['musictv']="#fcf";
			
			$step_risoluzione[]="5";
			$step_risoluzione[]="10";
			$step_risoluzione[]="15";
			$step_risoluzione[]="20";
			$step_risoluzione[]="30";
			$step_risoluzione[]="60";
			$step_risoluzione[]="120";
			
			if ($orario==0):
				$orario=time();
				$orario_temp=mktime(0,0,0,date('m', $orario), date('d', $orario), date('y', $orario));
			else:
				$orario_temp=explode("-", $orario);
				$orario_temp=mktime(0,0,0,$orario_temp[1], $orario_temp[0], $orario_temp[2]);
				$orario=$orario_temp;
			endif;
			
			$orario_oggi=$orario_temp;
			
			if ($risoluzione<5)
				$risoluzione=5;
			if ($risoluzione>120)
				$risoluzione=120;
			
			$orario_domani=$orario_temp+(24*60*60);
			$step=$risoluzione*60; // secondi di step
			$giorno=date('d/m/y', $orario);

			$total_width=1200;
			$step_width=round($total_width/(24*60*60/$step));
			if ($step_width<2)
				$step_width=2;

			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $id_dispositivo);
			$queryDIS=$this->db->get();
			$rowDIS=$queryDIS->row();
			echo "Dispositivo: <strong>".$id_dispositivo."</strong><br>";
			echo "Descrizione: <strong>".$rowDIS->descrizione."</strong><br>";

		//	echo "Giornata del ".date('d/m/y', $orario)."<br>";
			echo "<div style=\"position: relative; margin-top: 20px; z-index: 200; font-size: 0.9em\">";
			echo "data<input type=\"text\" id=\"egiorno\" value=\"".$giorno."\" class=\"datepicker\" style=\"width: 100px;\" readonly=\"readonly\">";
			echo " risoluzione";
			echo "<select id=\"erisoluzione\">";
			foreach ($step_risoluzione as $risoluzione_temp):
				echo "<option value=\"".$risoluzione_temp."\"";
				if ($risoluzione_temp==$risoluzione)
					echo " selected";
				echo ">".$risoluzione_temp;
				echo "</option>";
			endforeach;
			echo "</select>min";
			echo "<input type=\"hidden\" id=\"edispositivo\" value=\"".$id_dispositivo."\">";
			echo " <input type=\"submit\" value=\"aggiorna\" onclick=\"aggiorna();\">";
			echo "</div>";

			if ($orario_domani>time())
				$orario_domani=time();

			$stato_dispositivo=Array();
			$orario_server=Array();
			$orario_dispositivo=Array();
			$orario_playlist=Array();
			$totale_conteggi=0;
			while ($orario_temp<$orario_domani) {
				// if ($orario_temp>time())
				//	break;
				if (!isset($orario_check[$orario_temp])):
					$orario_check[$orario_temp]=1;
					$orario_server[]=$orario_temp;
					$this->db->from('log_dispositivi');
					$this->db->where('id_dispositivo', $id_dispositivo);
					$this->db->where('orario_server >', ($orario_temp-1));
					$this->db->where('orario_server <', ($orario_temp+$step));
					$query=$this->db->get();
					if ($query->num_rows()>0):
						$row=$query->row();
						$stato_dispositivo_temp=$row->stato_dispositivo;
						if ($stato_dispositivo_temp=="non")
							$stato_dispositivo_temp="non-funzionante";
						$stato_dispositivo[]=$stato_dispositivo_temp;
						$orario_dispositivo[]=$row->orario_dispositivo;
						$orario_playlist[]=$row->orario_playlist;
						
					else:
						$stato_dispositivo_temp="offline";
						$stato_dispositivo[]=$stato_dispositivo_temp;
						$orario_dispositivo[]=0;
						$orario_playlist[]=0;
					endif;
					if (!isset($totale_stato[$stato_dispositivo_temp]))
						$totale_stato[$stato_dispositivo_temp]=0;
					$totale_stato[$stato_dispositivo_temp]++;
					$totale_conteggi++;
				endif;
				$orario_temp=$orario_temp+$step;
			}

			
			echo "<div id=\"contenitore_grafico\">";
			
			echo "<div style=\"width: ".($total_width+120)."px; padding: 50px 10px 10px 10px; float: left; position: relative;\">";
			echo "<div style=\"width: ".($total_width+80)."px; height: 120px; border-bottom: 1px solid black; border-left: 1px solid black; float: left; position: relative;\">";
			$xx=0;
			foreach ($orario_server as $orario_temp):

				if ((isset($stato_dispositivo[$xx])) && ($stato_dispositivo[$xx]!="")):

					if ($orario_temp<time()):
						echo "<div id=\"barra_".$xx."\" class=\"barra\" style=\"width: ".($step_width-1)."px; background-color: ".$colore[$stato_dispositivo[$xx]].";\" onmouseover=\"mostrainfo('".$xx."');\" onmouseout=\"nascondiinfo('".$xx."');\">";
						echo "</div>";
					endif;
				
				endif;
				
				$xx++;
			endforeach;
			echo "</div>";


			// barra di stato
			
			echo "<div style=\"width: ".($total_width+80)."px; height: 20px; float: left; border-left: 1px solid black; position: relative;\">";
			$xx=0;
			foreach ($orario_server as $orario_temp):

				if ((isset($stato_dispositivo[$xx])) && ($stato_dispositivo[$xx]!="")):

					
					echo "<div id=\"barra_ora_".$xx."\" style=\"float: left; position: relative; top: 0px; color: #999; width: ".($step_width-1)."px; font-size: 0.8em; height: 20px; z-index: 10;";
					
					$ora=date("H", $orario_temp);
					if (!isset($ora_check[$ora])):
						$ora_check[$ora]=1;
						if ($ora>0)
							echo "border-left: 1px solid #ccc;";
						echo "\">".$ora;
					else:
						echo "margin-left: 1px;\">&nbsp;";
					endif;
					
					// echo "</div>";
					
					echo "<div id=\"barra_stato_".$xx."\" class=\"barra_stato\">";
					echo "Orario del server: ".date('H:i:s', $orario_temp)."<br>";
					if ($stato_dispositivo[$xx]!="offline"):
						echo "Orario del dispositivo: ".date('H:i:s', $orario_dispositivo[$xx])."<br>";
						echo "Orario della playlist: ".date('H:i:s', $orario_playlist[$xx])."<br>";
					endif;
					echo "Stato del dispositivo: <strong>".$stato_dispositivo[$xx]."</strong>";
					echo "</div>";
				
					echo "</div>";
				
				endif;
				
				$xx++;
			endforeach;
			
			echo "</div>";
			echo "</div>";

			echo "<div style=\"clear: both; margin-bottom: 20px;\"></div>";
			foreach ($stato as $stato_temp):
				echo "<div style=\"float: left; position: relative; width: 160px; margin-right: 30px; color: #666; font-size: 0.8em;\">";
				echo "<div style=\"float: left; position: relative; width: 30px; height: 30px; margin-right: 5px; border: 1px solid #333; background-color: ".$colore[$stato_temp]."; opacity: 0.8;\"></div>";
				echo "<span style=\"float: left; position: relative; top: 2px;\">".$stato_temp." ";
				if (!isset($totale_stato[$stato_temp]))
					$totale_stato[$stato_temp]=0;
				
				if ($totale_conteggi==0)
					$tempo_parziale=0;
				else
					$tempo_parziale=round($totale_stato[$stato_temp]*(($orario_domani-$orario_oggi)/60)/$totale_conteggi);
				
				$minuti=0;
				$ore=0;
				while ($tempo_parziale>59) {
					$tempo_parziale=$tempo_parziale-60;
					$ore++;
				}
				$minuti=$tempo_parziale;
				
				if ($totale_conteggi==0)
					$percentuale="0";
				else
					$percentuale=round($totale_stato[$stato_temp]/$totale_conteggi*1000)/10;
				
				echo $percentuale."%";
				echo "<br>".$ore."h ".$minuti."m";
				echo "</span>";
				echo "</div>";
			endforeach;

			echo "</div>";

		endif;

	}

}

