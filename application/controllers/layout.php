<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Layout extends CI_Controller {

	public function programmazione($id_gruppo=0)
	{
		$data['id_gruppo']=$id_gruppo;
		$this->load->view('site_header');
		$this->load->view('programmazione', $data);
		$this->load->view('site_footer');
	}

	public function indice($id_dispositivo=0, $giorno=-1) {

		if ($giorno==-1): // se il giorno non è specificato e va ricavato dal clock di sistema
			$giorno=date('w');
			if ($giorno==0) $giorno=7;  // se è domenica il sistema restituisce 0, ma va gestito come 7
		endif;

		$grupposelezionato=$this->strimy_model->accountselezionato();
		if ($id_dispositivo>0):
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->where('id_account', $grupposelezionato);
		else:
			$this->db->from('dispositivi_gruppi');
			$this->db->where('id', ($id_dispositivo*-1));
			$this->db->where('id_account', $grupposelezionato);
		endif;
		$queryDIS=$this->db->get();
		
		$data['id_dispositivo']=$id_dispositivo;
		$data['giorno']=$giorno;
		$this->load->view('site_header');
		if ($queryDIS->num_rows()>0)
			$this->load->view('layout_indice', $data);
		$this->load->view('site_footer');
		
	}

	function aggiungi_layout($id=0, $tipologia=0, $indice, $orario_iniziale) {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$layout=$this->config->item('layout');
		$layout_widget=$this->config->item('layout_widget');
		$layout_image=$this->config->item('layout_image');
		$widget=$this->config->item('widget');
		$widget_descrizione=$this->config->item('widget_descrizione');
	
		$layout_oltre_mezzanotte=0;				
		if ($id==0):
			$dataDUR['durata']=$orario_iniziale.":00";
			$dataDURET=$this->mp3_model->durata_secondi($dataDUR);
			$tempo_attuale=$dataDURET['durata_secondi']+3600;
			$dataDUR['durata']=$tempo_attuale;
			$dataDURET=$this->mp3_model->durata_formattata($dataDUR);
			$orario_finale=explode(":", $dataDURET['durata_formattata']);
			$orario_finale=$orario_finale[0].":".$orario_finale[1];
			$orario_iniziale_temp1=explode(":", $orario_iniziale);
			
			if ($orario_iniziale_temp1[0]=="00"):
				$orario_finale="00:00";
			endif;
			
		else:
			$this->db->from('layout_playlist');
			$this->db->where('id', $id);
			$this->db->where('id_account', $grupposelezionato);
			$queryOPT=$this->db->get();
			$rowOPT=$queryOPT->row();
			// $valori=unserialize($rowOPT->valori);

			date_default_timezone_set('UTC');
			$orario_iniziale=date('H:i', $rowOPT->orario_inizio);
			$orario_finale=date('H:i', $rowOPT->orario_fine);
			
			$differenza=( date('H', $rowOPT->orario_fine)*3600+date('i', $rowOPT->orario_fine)*60 ) - ( date('H', $rowOPT->orario_inizio)*3600+date('i', $rowOPT->orario_inizio)*60 );
			
			if ($differenza<1):
				$layout_oltre_mezzanotte=1;
			endif;
						
		endif;

		echo "<div class=\"panel panel-default\">";
		// echo "<div class=\"panel-body\" style=\"height: 100px\">";

		// echo "<div style=\"position: relative; width: 90%; float: left; border-top: 1px solid #CCC; margin-top: 20px; margin-left: 20px;\">";	// div contenitore

		echo "<div class=\"panel-heading\">";
			echo "<h3 class=\"panel-title pull-left\"  style=\" line-height: normal; padding-top: 4px;\"><span id=\"span_numero_layout_".$indice."\">".($indice+1)."</span>. ".$layout[$tipologia]."</h3>";
			
				echo "<span id=\"giorno_successivo_".$indice."\" class=\"avviso_orario\"";
				if ($layout_oltre_mezzanotte==0):
					echo " style=\"display: none;\">";
				else:
					echo ">";
					?>
					<script language="Javascript">
					// l'orario supera mezzanotte e va gestito separatamente per evitare sovrapposizioni in fase di salvataggio
						$('#check_giorno_successivo').val(1);
					</script>
					<?php
				endif;
				echo "<i class=\"fa fa-exclamation-triangle\"></i> la riproduzione terminer&agrave; alle ore <span id=\"giorno_successivo_interno_".$indice."\">".$orario_finale."</span> del giorno successivo";
				echo "</span>";
			
			echo"<button class=\"btn btn-default pull-right fa fa-trash\" onclick=\"return rimuovi_layout('".$indice."');\"></button>";
			echo "<div class=\"clearfix\"></div>";
		echo "</div>";

		echo "<input type=\"hidden\" name=\"etipologia[]\" value=\"".$tipologia."\">";
		echo "<input type=\"hidden\" id=\"eattiva_".$indice."\" name=\"eattiva[]\" value=\"1\">";
		echo "<input type=\"hidden\" name=\"eid[]\" value=\"".$id."\">";

		echo "<div class=\"panel-body\">";
    	
			echo "<table class=\"table table-condensed table-hover\">";

			echo "<tr><td style=\"width: 200px;\">";
			if (isset($layout_image[$tipologia])):
				echo "<img src=\"".$this->config->item('base_url')."assets/layout/".$layout_image[$tipologia]."\" style=\"width: 180px;\">";
			endif;
			echo "</td>";

			echo "<td>";
			$zz=0;
			foreach ($layout_widget[$tipologia] as $layout_widget_temp):
				$this->db->from('layout_playlist_widget');
				$this->db->where('id_layout', $id);
				$this->db->where('ordine', $zz);
				$queryWID=$this->db->get();
				if ($queryWID->num_rows()>0):
					$rowWID=$queryWID->row();
					$id_playlist=$rowWID->id_playlist;
				else:
					$id_playlist=0;
				endif;
				$tipologia_playlist=$widget[$layout_widget_temp];
				
				// echo "<span style=\"float: left; width: 90px;\">";
				echo "<div class=\"row\" style=\"margin-bottom: 3px;\">";
				echo "<div class=\"col-md-1 text-left\" style=\"max-width: 20px; padding-top: 4px;\">";
				echo "<span class=\"label label-info\">".($zz+1)."</span> "; // ". ".$layout_widget_temp;
				echo "</div>";
				
				echo "<div class=\"col-md-9\">";
			
				echo "<select name=\"elayout_widget_".$indice."[]\" id=\"elayout_widget_".$indice."_".$zz."\" class=\"form-control playlist_select\">";
				echo "<option value=\"0\"";
				if ($id_playlist==0)
					echo " selected";
				echo ">- ".$widget_descrizione[$layout_widget_temp]." -</option>";
				$this->db->from('playlist_generica_indice');
				$this->db->where('id_account', $grupposelezionato);
				$this->db->where('tipologia', $tipologia_playlist);	// decommentare questa riga per limitare la scelta di una playlist della tipologia prevista dal widget
				$this->db->order_by('tipologia', 'asc');
				$this->db->order_by('tipologia_file', 'asc');
				$this->db->order_by('descrizione', 'asc');
				$queryPLY=$this->db->get();
				foreach ($queryPLY->result() as $rowPLY):
					echo "<option value=\"".$rowPLY->id."\"";
					if ($rowPLY->id==$id_playlist)
						echo " selected";
					echo ">";
					echo $rowPLY->descrizione;
					
					$descrizione_playlist_temp=$widget_descrizione[array_search($rowPLY->tipologia, $widget)];
					
					echo " (".$descrizione_playlist_temp;
					if ($rowPLY->tipologia==2):
						if ($rowPLY->tipologia_file==0):
							echo " audio";
						elseif ($rowPLY->tipologia_file==1):
							echo " video";
						elseif ($rowPLY->tipologia_file==2):
							echo " immagini";
						endif;
					
					endif;
					
					echo ")";
					
					echo "</option>";
				endforeach;
				echo "</select>";
				
				// echo "<input type=\"text\" name=\"layout_widget_tipologia_file_".$indice."[]\" value=\"".$rowPLY->tipologia_file."\">";
				
				echo "</div>";
				echo "</div>";
				// echo "<br>";
				$zz++;
			endforeach;
			echo "<input type=\"hidden\" name=\"elayout_widget_totali[]\" value=\"".$zz."\">";

				
				echo "</td>";
				
				echo "<td style=\"width: 200px;\">";
				
					echo "<table style=\"table table-condensed\">";
					echo "<tr><td style=\"width: 20px;\"><i class=\"fa fa-clock-o\"></i></td>";
					echo "<td>";
					echo "<input type=\"time\" id=\"einizio_".$indice."\" name=\"einizio[]\" value=\"".$orario_iniziale."\" onblur=\"controlla_orari('".$indice."')\" maxlength=\"5\" size=\"5\"></td>";
					echo "<td style=\"padding-left: 5px;\">inizio</td>";
					echo "</tr>";
					
					echo "<tr><td></td><td style=\"text-align: center\">|</td><td></td></tr>";
					
					echo "<tr><td><i class=\"fa fa-clock-o\"></i></td>";
					echo "<td>";
					echo "<input type=\"time\" id=\"efine_".$indice."\" name=\"efine[]\" value=\"".$orario_finale."\" onblur=\"controlla_orari('".$indice."')\" maxlength=\"5\" size=\"5\" onblur=\"aggiorna_orari();\"></td>";
					echo "<td style=\"padding-left: 5px;\">fine</td>";
					
					echo "</tr>";
					echo "</table>";
				

				echo "</table>";
				
				echo "</td>";
				
				echo "</tr></table>";


		echo "</div>";
		echo "</div>";
		
//		echo "<div id=\"div_anteprima_".$indice."\" style=\"float: left; padding-left: 20px;\">";	// il div contenente l'anteprima della playlist
		
//		echo "</div>";

		echo "</div>";		

		echo "</div>";	// fine div contenitore
		
		?>
		<script language="Javascript">
			aggiorna_orari();
		</script>
		<?php
		
	}
/*
	function orario() {
		$time=1441806992;
		$step=86400;
		$xx=0;
		while ($xx < 60) {
			echo date('d/m/y - H:i:s', $time)." << ".$time."<br>";
			$time=$time+$step;
			$xx++;
		}
		
	}
*/


/*
function anteprima_playlist($id_playlist, $tipologia, $indice, $ordine) {
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
		
		if ($tipologia==0):
			$tabella_db="archivio_radio";
			$directory_contenuto="radio";
		else:
			$tabella_db="archivio_file";
			$directory_contenuto="content";
		endif;

		$url_base=$dataSTORAGE['full_path']."/users/".$stringa_casuale."/".$directory_contenuto;
		
		$this->db->select('playlist_generica_elementi.id_file as id_file, '.$tabella_db.'.nome_file as nome_file');
		$this->db->from('playlist_generica_elementi');
		$this->db->join($tabella_db, $tabella_db.'.id=playlist_generica_elementi.id_file');
		$this->db->order_by('playlist_generica_elementi.ordine', 'asc');
		$this->db->where($tabella_db.'.id_account', $grupposelezionato);
		$this->db->where('playlist_generica_elementi.id_playlist', $id_playlist);
		$queryDES=$this->db->get();
		$zz=0;
		foreach ($queryDES->result() as $rowDES):
			if ($tipologia==2): // è un file immagine
				$file_anteprima=$url_base."/".$rowDES->nome_file;
				$nomediv="div_anteprima_immagine_".$indice."_".$ordine."_".$zz;
				$nometag="evalori_elemento_".$indice."_".$ordine."_".$zz;
				echo "<div id=\"".$nomediv."\">";
				echo "<img src=\"".$file_anteprima."\" width=\"200\">";
				echo "</div>";
				
				echo "<input type=\"text\" id=\"".$nometag."\" name=\"evalori_elemento_".$indice."[][]\">";
				?>
				<script language="Javascript">
					$('div#<?php echo $nomediv; ?> > img').cropper({
					  aspectRatio: 16 / 9,
					  autoCropArea: 0.65,
					  strict: false,
					  guides: false,
					  highlight: false,
					  dragCrop: false,
					  cropBoxMovable: false,
					  cropBoxResizable: false,
					  movable: false,
					  cropBoxMovable: true
					});
					
					$('div#<?php echo $nomediv; ?> > img').on('cropend.cropper', function (e) {
						var valore=$(e.target).cropper('getData');
						var coord_x=Math.round(valore.x);
						if (coord_x<0) coord_x=0;
						var coord_y=Math.round(valore.y);
						if (coord_y<0) coord_y=0;
						var x=Math.round(valore.width);
						if (x<0) x=0;
						var y=Math.round(valore.height);
						if (y<0) y=0;
						var stringa=coord_x+"-"+coord_y+"-"+x+"-"+y;
						$('#<?php echo $nometag; ?>').val(stringa);
					});
				</script>
				<?php
				echo "<br>";
			else:
				echo $rowDES->nome_file."<br>";
			endif;
			$zz++;
		endforeach;
	}
*/

function check_orario_layout() {
		// questa funzione verifica la coerenza degli orari dei layout, per evitare sovrapposizioni: è infatti
		// possibile creare layout "a cavallo" della mezzanotte tra il giorno precedente o quello successivo
		
		// error_reporting(E_ERROR);
		// display_errors(1);
		
		date_default_timezone_set('UTC');
		
		$sequenza_orari=$this->input->post('esequenza_orari');
		$id_dispositivo=$this->input->post('eid_dispositivo');
		$giorno_oggi=$this->input->post('egiorno_oggi');

		$timestamp_iniziale=0;
		$timestamp_oggi=$timestamp_iniziale+($giorno_oggi-1)*86400;
		
		$orari=explode("x", $sequenza_orari);
		$orario_inizio=Array();
		$orario_fine=Array();
		$stringa="";
		$xx=0;
		foreach ($orari as $orari_temp):
			$temp=explode("-", $orari_temp);
			$temp_inizio=explode(":", $temp[0]);
			$temp_fine=explode(":", $temp[1]);
			$orario_inizio[$xx]=$temp_inizio[0]*3600+$temp_inizio[1]*60+$timestamp_oggi;
			$orario_fine[$xx]=$temp_fine[0]*3600+$temp_fine[1]*60+$timestamp_oggi;
		//	if ($orario_fine[$xx]<($orario_inizio[$xx]+1))
		//		$orario_fine[$xx]=$orario_fine[$xx]+86400;
			$stringa=$stringa.$orario_inizio[$xx]." ";
			$stringa=$stringa.$orario_fine[$xx]."\n";
			$xx++;
		endforeach;
		$totale_layout=$xx;
		
		array_multisort($orario_inizio, $orario_fine);
		
		$xx=0;
		$errore=0;
		while ($xx<$totale_layout) {
			
			if ($orario_fine[$xx]<($orario_inizio[$xx]+1))
				$orario_fine[$xx]=$orario_fine[$xx]+86400;
			
			// controlla che l'orario di inizio del layout non coincida o non superi quello di fine;
			// questo è permesso solo nel caso dell'ultimo layout: in questo caso la riproduzione continuerà oltre la mezzanotte,
			// fino all'orario specificato del giorno successivo
			if ( (($orario_inizio[$xx]==$orario_fine[$xx]) || ($orario_inizio[$xx]>$orario_fine[$xx])) && ($xx<($totale_layout-1)) )
				$errore=1;
				
			// se non è l'ultimo layout della lista, esegue controlli aggiuntivi
			if ($xx<($totale_layout-1)) {
				// controlla che l'orario di fine del layout non superi quello di inizio del layout successivo
				if ($orario_fine[$xx]>$orario_inizio[($xx+1)])
					$errore=1;
			}
			
			$differenza_settimana=0;
			// se è il primo layout della lista, è necessario verificare che non ci siano sovrapposizioni con i layout del giorno precedente
			if ($xx==0):
				$giorno_precedente=$giorno_oggi-1;
				if ($giorno_precedente==0):
					$giorno_precedente=7;
					$differenza_settimana=7*86400;
				endif;
				// echo "> ".($orario_inizio[$xx]+$differenza_settimana)." <";
				$this->db->from('layout_playlist');
				$this->db->where('id_dispositivo', $id_dispositivo);
				$this->db->where('giorno', $giorno_precedente);
				$this->db->where('orario_fine >', ($orario_inizio[$xx]+$differenza_settimana));
			//	$this->db->order_by('ordine', 'desc');
			//	$this->db->limit(0, 1);
				$queryPREC=$this->db->get();
				if ($queryPREC->num_rows()>0):
					$errore=1;
				endif;
			endif;
			
			$differenza_settimana=0;
			// se è l'ultimo layout della lista, è necessario verificare che non ci siano sovrapposizioni con i layout del giorno successivo
			if ($xx==($totale_layout-1)):
				$giorno_successivo=$giorno_oggi+1;
				if ($giorno_successivo==8):
					$giorno_successivo=1;
					$differenza_settimana=7*86400;
				endif;
				// echo "> ".($orario_inizio[$xx]+$differenza_settimana)." <";
				$this->db->from('layout_playlist');
				$this->db->where('id_dispositivo', $id_dispositivo);
				$this->db->where('giorno', $giorno_successivo);
				$this->db->where('orario_inizio <', ($orario_fine[$xx]-$differenza_settimana));
			//	$this->db->order_by('ordine', 'desc');
			//	$this->db->limit(0, 1);
				$querySUCC=$this->db->get();
				if ($querySUCC->num_rows()>0):
					$errore=1;
				endif;
			endif;
			
			$xx++;
		}
		
	//	$errore=1;
		if ($errore==1)
			echo $id_dispositivo." ".$giorno_oggi." ".$giorno_successivo." ".$stringa." (".$totale_layout.")";
		echo $errore;
}

function layout_salva() {
	
	error_reporting(E_ERROR);

	$gruppo=$this->flexi_auth->get_user_group_id();
	$grupposelezionato=$this->strimy_model->accountselezionato();
	$operazione=$this->input->post('eoperazione');
	
	// variabile per il debugging
	// se =0 valore di produzione: non mostrerà alcun output
	// se =1 valore di test: visualizzerà i dati dei brani e gli eventuali errori
	// se =2 valore di test: visualizzerà solo gli echo del controller ma non quelli del model
	$test=0;
	
	$id_dispositivo_redirect=$this->input->post('eid_dispositivo');
	$giorno_oggi=$this->input->post('egiorno');
	$egiorno=$this->input->post('egiorno');
	$egiorno_selezionato=$this->input->post('egiorno_selezionato');
	
	// die($egiorno." ".$egiorno_selezionato);
	
	if ($this->input->post("econferma")=="ok"):
	
		$eid=$this->input->post('eid');
		$attiva=$this->input->post('eattiva');

		$eid_dispositivo_gruppo=$this->input->post("eid_dispositivo_gruppo");
		$eid_dispositivo_gruppo_selezionato=$this->input->post("eid_dispositivo_gruppo_selezionato");
		$kk=0;
		$eid_dispositivo=Array();
		foreach ($eid_dispositivo_gruppo as $eid_dispositivo_gruppo_temp):
			if ($eid_dispositivo_gruppo_selezionato[$kk]==1)
				$eid_dispositivo[]=$eid_dispositivo_gruppo_temp;
			$kk++;
		endforeach;
		
				
		$op_inizio_layout=$this->input->post('einizio');
		$op_fine_layout=$this->input->post('efine');
		$op_tipologia=$this->input->post('etipologia');
		
		$numero_layout=$this->input->post('enumero_layout');
		$layout_widget_totali=$this->input->post('elayout_widget_totali');
		
		$giorno=Array();
		$kk=1;
		foreach ($egiorno_selezionato as $giorno_selezionato_temp):
			if ($giorno_selezionato_temp==1)
				$giorno[]=$kk;
			$kk++;
		endforeach;

/*
		$layout_widget=Array();
		$zz=0;
		// recupera i layout
		while ($zz<$numero_layout) {
		//	$layout_widget_tipologia[$zz]=$this->input->post('elayout_widget_tipologia_'.$zz);
			$layout_widget[$zz]=$this->input->post('elayout_widget_'.$zz);
			$this->db->from('playlist_generica_indice');
			$this->db->where('id', $layout_widget[$zz]);
			$this->db->where('id_account', $grupposelezionato);
			$queryTIP=$this->db->get();
			$rowTIP=$queryTIP->row();
			$layout_widget_tipologia[$zz]=$rowTIP->tipologia;
			$zz++;
		}
		array_multisort($op_inizio_layout, $op_fine_layout, $attiva, $op_tipologia, $layout_widget_totali, $layout_widget, $layout_widget_tipologia);
*/		
		
		foreach ($giorno as $giorno_oggi):
		
			// calcola il timestamp di riferimento
			// tutti gli orari verranno computati considerando come "momento iniziale" le 00:00 di lunedì mattina
			// $timestamp_iniziale=strtotime('Last Monday', time());
			$timestamp_iniziale=0;
		
			// prosegue con la creazione della playlist layout per il primo dei dispositivi selezionati;
			// successivamente duplicherà i contenuti per tutti gli altri dispositivi: questo riduce i tempi di elaborazione
			foreach ($eid_dispositivo as $id_dispositivo):
			//	$this->db->where('id_account', $grupposelezionato);
				$this->db->where('id_dispositivo', $id_dispositivo);
			//	if ($giorno_oggi!=0)
				$this->db->where('giorno', $giorno_oggi);
				$this->db->delete('layout_playlist');
				
				$this->db->where('id_dispositivo', $id_dispositivo);
			//	if ($giorno_oggi!=0)
				$this->db->where('giorno', $giorno_oggi);
				$this->db->delete('layout_playlist_widget');
			endforeach;
			

			
			$zz=0;
			$zz1=0;
			
			
			// recupera i layout
			while ($zz<$numero_layout) {
				$layout_widget_tipologia[$zz]=$this->input->post('elayout_widget_tipologia_'.$zz);
				$layout_widget[$zz]=$this->input->post('elayout_widget_'.$zz);
				$zz++;
			}
			
			array_multisort($op_inizio_layout, $op_fine_layout, $attiva, $op_tipologia, $layout_widget_totali, $layout_widget, $layout_widget_tipologia);
			
			$zz=0;
			while ($zz<$numero_layout) {
				// verifica che le opzioni siano state inserite correttamente
				// prepara gli array per la successiva generazione dei singoli intervalli playlist
				// memorizza le opzioni nel database per poi riprenderle in fase di nuova creazione
			
				if ($attiva[$zz]==1):
			
					$timestamp_oggi=$timestamp_iniziale+($giorno_oggi-1)*86400;
					// echo $op_tipologia[$zz]."<br>";

					$dataLAY=array();
					
					
					$orario_temp=explode(":", $op_inizio_layout[$zz]);
					$giorno_temp=date('d', $timestamp_oggi);
					$mese_temp=date('m', $timestamp_oggi);
					$anno_temp=date('y', $timestamp_oggi);
					$orario_inizio=mktime($orario_temp[0],$orario_temp[1],0,$mese_temp,$giorno_temp,$anno_temp);
					
					$orario_inizio=$orario_temp[0]*3600+$orario_temp[1]*60+$timestamp_oggi;
					
					$orario_temp=explode(":", $op_fine_layout[$zz]);
					$giorno_temp=date('d', $timestamp_oggi);
					$mese_temp=date('m', $timestamp_oggi);
					$anno_temp=date('y', $timestamp_oggi);
					$orario_fine=mktime($orario_temp[0],$orario_temp[1],0,$mese_temp,$giorno_temp,$anno_temp);
				
					$orario_fine=$orario_temp[0]*3600+$orario_temp[1]*60+$timestamp_oggi;
					// se è l'ultimo layout della sequenza e se l'orario di fine è minore di quello di inizio
					// posticipa la fine al giorno successivo;
					if ($orario_fine<$orario_inizio+1)
						$orario_fine=$orario_fine+86400;
				
					// echo $zz1." ".date('d/m/y H:i:s', $orario_inizio)." - ".date('d/m/y H:i:s', $orario_fine);
				
					foreach ($eid_dispositivo as $id_dispositivo):
					
						$dataLAY['ordine']=$zz1;
						$dataLAY['id_account']=$grupposelezionato;
						$dataLAY['id_dispositivo']=$id_dispositivo;
						$dataLAY['orario_inizio']=$orario_inizio;
						$dataLAY['orario_fine']=$orario_fine;
						$dataLAY['giorno']=$giorno_oggi;
						$dataLAY['tipologia']=$op_tipologia[$zz];
					//	$dataLAY['programmazione_personalizzata']=$programmazione_personalizzata;
					//	$dataLAY['ultima_modifica']=$timestamp_attuale;
						$this->db->insert('layout_playlist', $dataLAY);
						
						$eid_layout=$this->db->insert_id();
						
						$yy=0;
						$dataWID=Array();
						while ($yy<$layout_widget_totali[$zz]) {
							if ($layout_widget[$zz][$yy]!=0):
							
								// recupera la tipologia_file della playlist;
								// utile, nel caso di Slideshow, per capire se si tratti di immagini, audio o video
								$this->db->from('playlist_generica_indice');
								$this->db->where('id', $layout_widget[$zz][$yy]);
								$this->db->where('id_account', $grupposelezionato);
								$queryTIP=$this->db->get();
								$rowTIP=$queryTIP->row();
							
								$dataWID['id_layout']=$eid_layout;
								$dataWID['ordine']=$yy;
								$dataWID['tipologia']=$rowTIP->tipologia;
								$dataWID['tipologia_file']=$rowTIP->tipologia_file;
								$dataWID['id_playlist']=$layout_widget[$zz][$yy];
								$dataWID['giorno']=$giorno_oggi;
								$dataWID['id_dispositivo']=$id_dispositivo;
								$this->db->insert('layout_playlist_widget', $dataWID);
							endif;
							$yy++;
						}
						
					endforeach;

					$zz1++;
				endif;

				$zz++;
			}

		endforeach;

		
		// aggiorna l'indice dei layout	
			
		// se si sta agendo su un gruppo, tutti i dispositivi avranno la stessa programmazione;
		// se si sta agendo su un singolo dispositivo, questo avrà una programmazione personalizzata
		if ($id_dispositivo_redirect<0)
			$programmazione_personalizzata=0;
		else
			$programmazione_personalizzata=1;
		$timestamp_attuale=time();
		
		foreach ($eid_dispositivo as $id_dispositivo):
			$dataIND=Array();
			$dataIND['ultima_modifica']=$timestamp_attuale;
			$dataIND['ultima_modifica_manuale']=$timestamp_attuale;
			$dataIND['programmazione_personalizzata']=$programmazione_personalizzata;
			$dataIND['id_dispositivo']=$id_dispositivo;
			$this->strimy_model->aggiorna_layout_indice_dispositivo($dataIND);
		endforeach;

		if ($egiorno==-1) $egiorno="0";
		$url="layout/indice/".$id_dispositivo_redirect."/".$giorno_oggi;
		if ($test>0):
			echo "<br>[ <a href=\"/index.php/".$url."\">prosegui</a> ]";
		else:
			redirect($url);
		endif;

	else:
		$url="layout/indice/".$id_dispositivo_redirect."/".$giorno_oggi;
		redirect($url);
	endif;

	}

}

