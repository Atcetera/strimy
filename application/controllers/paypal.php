<?php

error_reporting(0);

define("SIMULATION", 1);
define("SIMULATION_URL", "www.sandbox.paypal.com");
define("PRODUCTION_URL", "www.paypal.com");
define("PRIMARY_PAYPAL_EMAIL", "info@atcetera.it");
define("PRIMARY_SANDBOX_EMAIL", "admin@civicoweb.it");

//messages
define("ADMIN_MAIL", "info@atcetera.it");
define("NO_REPLY", "info@atcetera.it");


class Paypal extends CI_Controller {

// funzioni relative a Paypal (attivazione utenti, rinnovi ecc.)

	function test() {
		if($this->config->item('paypal_sandbox')==1):
		echo "simulazione";
			else:
		echo "reale";
		endif;

	}

    private function isVerifiedIPN()
    {

		// nel caso si ottenesse un errore di IPN INVALID, verificare che il charset del sito
		// coincida con charset specificato nell'account PayPal (es. UTF-8)

        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value)
        {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        // Modificando la costante SIMULATION nel file di configurazione
        // è possibile passare dall'ambiente di simulazione a quello di produzione
        if($this->config->item('paypal_sandbox')==1)
        {
            $url = SIMULATION_URL;
        }
        else
        {
            $url = PRODUCTION_URL;
        }

        $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Host: ".$url.":443\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";


        $fp = fsockopen ("ssl://".$url, 443, $errno, $errstr, 30);

        if (!$fp)
        {
            $this->sendReport(__LINE__);
            return FALSE;
        }
        else
        {
            fputs ($fp, $header . $req);
            while (!feof($fp))
            {
                $res = fgets ($fp, 1024);
                if (strcmp($res, "VERIFIED") == 0)
                {
                    fclose ($fp);
					$subject = "verify IPN!";
					$message = "test";
					$from = NO_REPLY;
					return TRUE;
                }
                else if (strcmp ($res, "INVALID") == 0)
                {
                    //se la procedura non è legittima invia un email all'amministratore
                    $dataSEND['post']=$_POST;
                    $dataSEND['errno']=$errno;
                    $dataSEND['errstr']=$errstr;
                    $dataSEND['header']=$header;
                    $dataSEND['res']=$res;
                    $dataSEND['req']=$req;
                    $this->sendReport(__LINE__ , $dataSEND);
                    fclose ($fp);
                    return FALSE;
                }
            }
        }
    }

    private function sendReport($line, $dataSEND=Array())
    {
        if($this->config->item('paypal_sandbox')==1)
        {
            $add = "- SIMULAZIONE - ";
        }
        else
        {
            $add = "";
        }
        //messaggio all'amministratore
        $subject = $add."Problema IPN";
        
        if (isset($dataSEND['subject']))
			$subject = $add.$dataSEND['subject'];
        
        $message = "Si è verificato un problema nella seguente transazione:\r\n\r\n";
        $message .= "Nome: " . $_POST['first_name'] . ' ' . $_POST['last_name'] . "\r\n";
        $message .= "email: " . $_POST['payer_email'] . "\r\n";
        $message .= "id transazione: " . $_POST['txn_id'] . "\r\n";
        $message .= "oggetto: " . $_POST['transaction_subject']."\r\n";
        $message .= "linea: " . $line."\r\n";
        
        if (isset($dataSEND['post'])):
				$message .= "-----\r\n";
				$message .= "errno: ".$dataSEND['errno']."\r\n";
				$message .= "errstr: ".$dataSEND['errstr']."\r\n";
				$message .= "res: ".$dataSEND['res']."\r\n";
				$message .= "req: ".$dataSEND['req']."\r\n";
				$message .= "post:\r\n";
                foreach ($dataSEND['post'] as $key => $value)
				{
					$value = urlencode(stripslashes($value));
					$req .= "&$key=$value";
					$message .= $key.": ".$value."\r\n";
				}
				$message .= "header:\r\n".$dataSEND['header']."\r\n";
        endif;

		if (isset($dataSEND['message']))
				$message .= "\r\n\r\n----------\r\n".$dataSEND['message']."\r\n";

        mail(ADMIN_MAIL,$subject,$message,"From: " . NO_REPLY);
        return;
    }

    private function isCompleted()
    {
        if(trim($_POST['payment_status']) === "Completed")
        {

            return TRUE;
        }
        return FALSE;
    }

    private function isPrimaryPayPalEmail()
    {

        if($this->config->item('paypal_sandbox')==1)
        {
            $email = PRIMARY_SANDBOX_EMAIL;
        }
        else
        {
            $email = PRIMARY_PAYPAL_EMAIL;
        }

        if(trim($_POST['receiver_email']) === $email)
        {
            $subject = "verify EMAIL!";
			$message = "test";
			$from = NO_REPLY;
			// mail(ADMIN_MAIL,$subject,$message,"From: noreply<".$from.">");

            return TRUE;
            
        }
        return FALSE;
    }


    protected function isReadyTransaction()
    {
	// $this->debugMail(__LINE__);
        if($this->isVerifiedIPN() AND
           $this->isPrimaryPayPalEmail() AND
           $this->isNotProcessed() AND
           $this->isVerifiedAmmount() AND
           $this->isCompleted())
           {
               return TRUE;
           }
		{
		
           return FALSE;
		}
    }

// protected $conn;

	protected function debugMail($line)
	{
	    mail("m.dagord@atcetera.it", "debug mail", "Riga: $line");
	}

        protected function isVerifiedAmmount()
        {
			
		//	mail(ADMIN_MAIL,"Westrim! Pagamento andato a buon fine!","Tutto a posto","From: " . NO_REPLY);
			
			// verifica che l'importo pagato sia corretto
			$dataPP = explode("///", $_POST['custom']);
			$importo=$dataPP[0];
			$gruppo=$dataPP[1];
		//	$crediti=$dataPP[2];
			$timestamp=$dataPP[3];
			
			$errore=0;
		//	$grupposelezionato=$this->Utenti_model->grupposelezionato();
			/*			
			if ($gruppo!=$grupposelezionato)
				$errore=1;
			if ($sessione!=get_cookie('idsessione'))
				$errore=1;
			*/
   			$subject = "verify ammount!";
			$message = "test";
			$from = NO_REPLY;
			// mail(ADMIN_MAIL,$subject,$message,"From: noreply<".$from.">");  
			
			$this->db->from('paypal_pagamenti');
		//	$this->db->where('session_id', $sessione);
			$this->db->where('id_account', $gruppo);
			$this->db->where('timestamp', $timestamp);
			$this->db->where('pagato', 0);
			$queryPA=$this->db->get();
			if ($queryPA->num_rows()==0):
				$errore=1;
				$importo_totale=-1;
			else:
				$rowPA=$queryPA->row();
				$importo_totale=$rowPA->importo_totale;
			endif;
			
			if($_POST['mc_gross'] != $importo_totale)
				$errore=1;

		
			if($errore==0)
				{

				$subject = "verify ammount complete!";
				$message = "test";
				$from = NO_REPLY;
			//	mail(ADMIN_MAIL,"Westrim! Pagamento andato a buon fine!","verify ammount complete","From: " . NO_REPLY);
				return TRUE;
				}

			else
			{
				$subject = "verify ammount errato!";
				$message = $_POST['mc_gross']." anziché ".$importo_totale."\n".$gruppo." anziché ".$grupposelezionato."\n".$sessione." anziché ".$this->session->userdata('idsessione');
				$from = NO_REPLY;
				$dataSEND['subject']=$subject;
				$dataSEND['message']=$message;
				$this->sendReport(__LINE__ , $dataSEND);
				return FALSE;
			}
        }

        protected function isNotProcessed()
        {
           // $this->dbConnect();
		$this->db->from('paypal_pagamenti');
		$this->db->where('transaction_id', $_POST['txn_id']);
		$query=$this->db->get();
            	if($query->num_rows()>0)
            {
                return FALSE;
            }
		// mail("m.dagord@atcetera.it", "debug mail", "processo verificato");
			$subject = "verify processed!";
			$message = "test";
			$from = NO_REPLY;
			// mail(ADMIN_MAIL,$subject,$message,"From: noreply<".$from.">");     
            return TRUE;
        }



		public function fatturazione_manuale() {
			// questa funzione serve a confermare manualmente un pagamento
			// e viene utilizzata quando c'è un errore di comunicazione con il server di PayPal
		
			$eid=$this->input->post('eid');
			$etransaction_id=trim($this->input->post('etransaction_id'));
			
			if (($eid!="") && ($etransaction_id!="")):
		
				$this->db->from('paypal_pagamenti');
				$this->db->where('id', $eid);
				$queryPAG=$this->db->get();
				$rowPAG=$queryPAG->row();

				$gruppo=$rowPAG->rif_gruppo;
				$timestamp=$rowPAG->timestamp;

				if ($rowPAG->sconto_codice!=""):  // se è stato utilizzato un codice sconto lo marca come utilizzato
					$dataANSC['utilizzato']=1;
					$this->db->where('codice', $rowPAG->sconto_codice);
					$this->db->where('rif_gruppo', $gruppo); // se è un codice generico il gruppo non corrisponde, e il codice sarà ancora utilizzabile
					$this->db->update('paypal_sconti', $dataANSC);			
				endif;

				$this->db->from('utenti_gruppi');
				$this->db->where('id', $gruppo);
				$queryGR=$this->db->get();
				$rowGR=$queryGR->row();
				$email_gruppo=$rowGR->ana_email;
				
				if ($rowGR->practice==1)
					$data_scadenza_temp=$rowGR->data_creazione-(24*60*60);
				else
					$data_scadenza_temp=$rowGR->data_scadenza;
					
				if ($rowGR->scadenza==0):  // è un account free: va convertito in pro e quindi soggetto a scadenza, che sarà a un anno dalla data odierna
					$data_scadenza_temp=time()-(24*60*60);
					$dataGR['scadenza']=1;
					$dataGR['tipologia']=$rowPAG->tipologia;
				endif;
				
				$data_scadenza=$this->Segreteria_model->data_aggiungi_anno($data_scadenza_temp);
				
				$dataGR['practice']=0;
				if ($rowPAG->upgrade==0) // la data di scadenza viene prorogata solo se è un rinnovo
					$dataGR['data_scadenza']=$data_scadenza;
				else  // altrimenti si tratta di un upgrade e imposta la nuova tipologia di account
					$dataGR['tipologia']=$rowPAG->tipologia;
				$this->db->where('id', $gruppo);
				$this->db->update('utenti_gruppi', $dataGR);

				$data_pagamento_riferimento="01/01/".(date('y', $timestamp));
				$datatemp=explode("/", $data_pagamento_riferimento);
				$data_pagamento_riferimento = mktime("0", "0", "0", $datatemp[1], $datatemp[0], $datatemp[2]);
				$data_pagamento_riferimento=$data_pagamento_riferimento-1;

				$this->db->from('paypal_pagamenti');
				$this->db->where('pagato', 1);
				$this->db->where('test_pagamento', 0);
				$this->db->where('data_pagamento >', $data_pagamento_riferimento);
				$this->db->orderby('codice', 'desc');
				$queryNF=$this->db->get();
				if ($queryNF->num_rows()==0):
					$numero_fattura=1;
				else:
					$rowNF=$queryNF->row();
					$numero_fattura=$rowNF->codice+1;
				endif;


				$dataPAG['pagato']=1;
				$dataPAG['codice']=$numero_fattura;
				$dataPAG['transaction_id']=$etransaction_id;
				$dataPAG['data_pagamento']=$timestamp;
				$dataPAG['ana_ragione_sociale']=$rowGR->ana_ditta;
				$dataPAG['ana_partita_iva']=$rowGR->ana_partita_iva;
				$dataPAG['ana_codice_fiscale']=$rowGR->ana_codice_fiscale;
				$this->db->where('id', $eid);
				$this->db->where('rif_gruppo', $gruppo);
				$this->db->where('pagato', 0);
				$this->db->update('paypal_pagamenti', $dataPAG);
			
				if ($rowPAG->upgrade==0):
					$subject = "[Civico] Abbonamento rinnovato";
					$message = "Gentile utente,<br>ti confermiamo che l'acquisto di Civico ".$account_descrizione." è andato a buon fine!<br>";
					$message .= "La nuova scadenza è: ".date('d/m/y', $data_scadenza)."<br><br>";
					$message .= "La fattura sarà disponibile a breve nella pagina <b>Il tuo account</b>";
					$message .= "<br><br>Buon lavoro,<br>lo staff Civico";
				else:
					$subject = "[Civico] Upgrade dell'abbonamento";
					$message = "Gentile utente,<br>ti confermiamo che l'upgrade a Civico ".$account_descrizione." è andato a buon fine!<br>";
					$message .= "La fattura sarà disponibile a breve nella pagina <b>Il tuo account</b>";	
					$message .= "<br><br>Buon lavoro,<br><br>lo staff Civico";		
				endif;
				$from = NO_REPLY;
				$message.$add2;

				$data_messaggio['oggetto']=$subject;
				$data_messaggio['destinatario']=$email_gruppo;
				$data_messaggio['messaggio_html']=$message;
				$data_messaggio['from_mail']=$from;
				$data_messaggio['from_nome']="Strimy";		
				$this->Segreteria_model->html_email($data_messaggio);
			
			endif;
			
			redirect('utenti/gruppi_index/fatturazione');
			
		}


        public function confirm_payment()
        {

		$dataPP = explode("///", $_POST['custom']);
		$importo=$dataPP[0];
		$gruppo=$dataPP[1];
		$crediti=$dataPP[2];
		$timestamp=$dataPP[3];

		// $this->debugMail(__LINE__);
            if($this->isReadyTransaction())
            {

				$subject = "pagamento completato!";
				$message = "test";
				$from = NO_REPLY;

				$this->db->from('paypal_pagamenti');
				$this->db->where('id_account', $gruppo);
				$this->db->where('timestamp', $timestamp);
				$queryPAG=$this->db->get();
				$rowPAG=$queryPAG->row();

/*
				$this->db->from('user_groups_dettagli');
				$this->db->where('id_account', $gruppo);
				$queryGR=$this->db->get();
				$rowGR=$queryGR->row();
				$email_gruppo=$rowGR->email;
*/
				$data_pagamento_riferimento="01/01/".(date('y', $timestamp));
				$datatemp=explode("/", $data_pagamento_riferimento);
				$data_pagamento_riferimento = mktime("0", "0", "0", $datatemp[1], $datatemp[0], $datatemp[2]);
				$data_pagamento_riferimento=$data_pagamento_riferimento-1;
				
				$this->db->from('paypal_pagamenti');
				$this->db->where('pagato', 1);
				$this->db->where('test_pagamento', $rowPAG->test_pagamento);
				$this->db->where('data_pagamento >', $data_pagamento_riferimento);
				$this->db->order_by('codice', 'desc');
				$queryNF=$this->db->get();
				if ($queryNF->num_rows()==0):
					$numero_fattura=1;
				else:
					$rowNF=$queryNF->row();
					$numero_fattura=$rowNF->codice+1;
				endif;

				$dataPAG['pagato']=1;
				$dataPAG['codice']=$numero_fattura;
				$dataPAG['transaction_id']=$_POST['txn_id'];
				$dataPAG['data_pagamento']=time();
				// $dataPAG['ana_ragione_sociale']=$rowGR->ragione_sociale;
				// $dataPAG['ana_partita_iva']=$rowGR->partita_iva;
				// $dataPAG['ana_codice_fiscale']=$rowGR->codice_fiscale;
				$this->db->where('id_account', $gruppo);
				$this->db->where('timestamp', $timestamp);
				$this->db->where('pagato', 0);
				$this->db->update('paypal_pagamenti', $dataPAG);

				$this->db->from('user_groups');
				$this->db->where('ugrp_id', $gruppo);
				$queryCRED=$this->db->get();
				$rowCRED=$queryCRED->row();
				$nuovi_crediti=$rowCRED->crediti+$rowPAG->crediti;
				$dataCRED['crediti']=$nuovi_crediti;
				
				$this->db->where('ugrp_id', $gruppo);
				$this->db->update('user_groups', $dataCRED);

				$email_acquirente=$rowCRED->ugrp_email;

				$from = NO_REPLY;


			/*
				$data_messaggio['oggetto']=$subject;
				$data_messaggio['destinatario']=$email_gruppo;
				$data_messaggio['messaggio_html']=$message;
				$data_messaggio['from_mail']=$from;
				$data_messaggio['from_nome']="Civico";		
				$this->Segreteria_model->html_email($data_messaggio);
			*/
				// mail($email_gruppo,"[Civico] ".$subject,$message,"From: noreply<".$from.">");	

				$message="Ti confermiamo di aver ricaricato i ".$rowPAG->crediti." crediti!";

				$subject="Strimy: procedura di pagamento ultimata!";
				mail($email_acquirente,$subject,$message,"From: noreply<".$from.">");


            }
            
            else
            {
				$subject = "[Strimy] Errore di pagamento su Strimy!";
				$from = NO_REPLY;
				$message = "C'è stato un problema con l'utente ".$_POST['payer_email']." (id gruppo/account: ".$gruppo.")\r\n";
				$message .= "Si prega di effettuare le opportune verifiche\r\n".$add2;

				mail(ADMIN_MAIL,$subject,$message,"From: noreply<".$from.">");	
				
			}
        }

	function ricevi_pagamento() {
		if ($this->config->item('paypal_sandbox')==1) echo "simulazione"; else echo "reale";
			echo "<br>";
		
			$subject = "test di pagamento su Strimy!";
			$message = "test";
			$from = NO_REPLY;
		
		if (!isset($_POST['txn_id'])) $tnx_id="";
			else $tnx_id=$_POST['txn_id'];
		if ($tnx_id!=""):
			$this->confirm_payment();
		else:
			die('errore');
		endif;

	}

}
?>
