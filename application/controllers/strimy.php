<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Strimy extends CI_Controller {

	public function index() {
	 
//        //Allow external script access
//        if (REQUEST == "external") {
  //          return;
//        } else {
            //Do other Things
		
			if ($this->config->item('base_url')=="http://www.westrim.it/"): // è il portale di Westrim
				$page="index_westrim";
			else: // è il portale di Strimy
				$page="frontpage";
			endif;
			
			$this->load->view('site_header');
			$this->load->view($page);
			$this->load->view('site_footer');

  //      }       
	
	}

	public function frontpage()
	{
		$this->load->view('site_header');
		$this->load->view('frontpage');
		$this->load->view('site_footer');
	}

	public function check_dispositivo($comando="", $device_id="", $status="")
	{
		
		$percorso_base=$this->config->item('archivio_real_path');
		$percorso_devices=$this->config->item('percorso_devices');
		$percorso_users=$this->config->item('percorso_users');

		$cartella_base=$percorso_base.$percorso_devices.$device_id;
	
		$nome_file="check.txt";
		$file=$cartella_base."/".$nome_file;
		$orario=time();
		$data=date('d/m/y', $orario);


		if ($comando=="config"):	// recupera i parametri di configurazione
			
			$this->db->select('dispositivi_configurazione.parametri_configurazione as parametri_configurazione, dispositivi.id_account as id_account');
			$this->db->from('dispositivi_configurazione');
			$this->db->join('dispositivi', 'dispositivi_configurazione.id_dispositivo=dispositivi.id_dispositivo');
			$this->db->where('dispositivi.id_dispositivo', $device_id);
			$this->db->where('dispositivi.attivato', 1);
			$query=$this->db->get();
			
			if ($query->num_rows()>0):
				$row=$query->row();
				$parametri_configurazione=unserialize($row->parametri_configurazione);
				$totale_parametri=count($parametri_configurazione);
				$xx=0;
				foreach ($parametri_configurazione as $key => $result):
					echo $key.";".$result;
				//	if ($xx<($totale_parametri-1))
						echo "\n";
					$xx++;
				endforeach;
			
				// recupera lo storage
				$dataSTORAGE=$this->strimy_model->recupera_storage($row->id_account);
				echo "storage_path;".$dataSTORAGE['full_path']."\n";
				
				// recupera l'email associata all'account, a cui inviare eventuali segnalazioni
				$this->db->select('user_groups.ugrp_email as ugrp_email');
				$this->db->from('user_groups');
				$this->db->join('dispositivi', 'dispositivi.id_account=user_groups.ugrp_id');
				$this->db->where('dispositivi.id_dispositivo', $device_id);
				$queryEGR=$this->db->get();
				if ($queryEGR->num_rows()>0):
					$rowEGR=$queryEGR->row();
					$email_account=$rowEGR->ugrp_email;
				else:
					$email_account="";
				endif;
				echo "email_account;".$email_account;
				
			endif;

		elseif ($comando=="scrivi"):	// riceve dal dispositivo l'informazione sul suo status e la scrive in un file di testo

			echo $device_id;

			$fp = fopen($file, 'w+');
			$riga_temp=$orario.";".$status."\n";
			fwrite($fp, $riga_temp);
			$riga_temp=$data.";0\n";
			fwrite($fp, $riga_temp);
			fclose($fp);
			chmod($file, 0666);

			if ($this->config->item('log_dispositivi')==1):
				// scrive lo stato del dispositivo nel database, per successivi controlli
				$status_temp=explode('_', $status);
				if (!isset($status_temp[2])) $status_temp[2]=$status_temp[1];
				$stato_dispositivo=$status_temp[0];
				$orario_server=$orario;

				$orario_temp=explode(":", $status_temp[1]);
				$orario_playlist=mktime($orario_temp[0],$orario_temp[1],$orario_temp[2],date('m', $orario), date('d', $orario), date('y', $orario));

				$orario_temp=explode(":", $status_temp[2]);
				$orario_dispositivo=mktime($orario_temp[0],$orario_temp[1],$orario_temp[2],date('m', $orario), date('d', $orario), date('y', $orario));

				$dataINS['id_dispositivo']=$device_id;
				$dataINS['stato_dispositivo']=$stato_dispositivo;
				$dataINS['orario_server']=$orario_server;
				$dataINS['orario_playlist']=$orario_playlist;
				$dataINS['orario_dispositivo']=$orario_dispositivo;
				
				$this->db->insert('log_dispositivi', $dataINS);
				
			endif;


		elseif ($comando=="leggi"):	// legge lo status del dispositivo precedentemente memorizzato e lo restituisce

		//	$data_check['device_id']=$device_id;
			$data_check['file']=$file;
			$data_risultato=$this->strimy_model->check_dispositivo($data_check);
			
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $device_id);
			$query=$this->db->get();
			$row=$query->row();
			
			$comando_remoto="";
			if ($row->comando_remoto!=0)
				$comando_remoto="<span style=\"color: red; font-weight: bold; font-size: 0.9em\">(".$row->comando_remoto.")</span> ";
			
			echo $comando_remoto.$data_risultato['messaggio'];
			
		elseif ($comando=="comando_remoto"):	// permette di eseguire un comando impartito da remoto
		
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $device_id);
			$this->db->where('attivato', 1);
			$query=$this->db->get();
			
			if ($query->num_rows()>0):
				$row=$query->row();
				echo $row->comando_remoto;
				
				$dataCOM['comando_remoto']=0;
		
				// dopo la lettura, il comando impartito va eliminato
				$this->db->where('id_dispositivo', $device_id);
				$this->db->update('dispositivi', $dataCOM);
				
			endif;
		
		elseif ($comando=="check"):	// controlla se il dispositivo è attivato; utile per la prima attivazione o future disattivazioni
		
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $device_id);
			$this->db->where('attivato', 1);
			$query=$this->db->get();
			
		//	if ( ($query->num_rows==1) && (file_exists($cartella_base)) )		// il dispositivo è presente nel database e la directory esiste
			if ($query->num_rows==1)	// il dispositivo è presente nel database e la directory esiste
				echo "1";
			else
				echo "0";
		
		elseif ($comando=="elenca_contenuti"):
		
			if ($status=="")
				$status=date('w');
				
			$giorno=$status;
			if ($giorno==0) $giorno=7; // se è domenica il sistema restituisce 0, ma strimy la gestisce come 7

			$dataRET=Array();
			$this->db->select('user_groups.ugrp_id as ugrp_id, user_groups.ugrp_stringa as ugrp_stringa');
			$this->db->from('user_groups');
			$this->db->join('dispositivi', 'dispositivi.id_account=user_groups.ugrp_id');
			$this->db->where('dispositivi.id_dispositivo', $device_id);
			$query=$this->db->get();
			if ($query->num_rows()>0):
				$row=$query->row();
				$dataGRUPPO['giorno']=$giorno;
				$dataGRUPPO['gruppo']=$row->ugrp_id;
				$dataGRUPPO['id_dispositivo']=$device_id;
				$dataGRUPPO['stringa']=$row->ugrp_stringa;
				$dataRET=$this->strimy_model->lista_file_contenuti($dataGRUPPO);
			endif;
		
			$xx=0;
			
			foreach ($dataRET['file'] as $file):
				echo $dataRET['percorso'][$xx].";".$file."\n";
				$xx++;
			endforeach;
		
		elseif (($comando=="playlist") || ($comando=="pretty_playlist")):	// restituisce la playlist formattatata
			
			///////////////////////////////////////////
			// verifica che il dispositivo sia attivato
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $device_id);
			$this->db->where('attivato', 1);
			$query=$this->db->get();
			
			if ($query->num_rows()==0)
				die();
			///////////////////////////////////////////
			
			if ($status=="")
				$status=date('w');
				
			$giorno=$status;
			
			if ($giorno==0) $giorno=7; // se è domenica il sistema restituisce "0", ma strimy la gestisce come "7"

			$timestamp_iniziale=0;
			$timestamp_oggi=$timestamp_iniziale+($giorno-1)*86400;

			// elenca i file di release (quello specifico per il dispositivo, se esiste, o quello generico);
			// in questo modo il dispositivo potrà effettuare l'upgrade
			$dataREL['id_dispositivo']=$device_id;
			$dataFIRMWARE = $this->strimy_model->lista_releases($dataREL);


			$dataDES=Array();
			
			$ultima_modifica=0;
			$this->db->from('layout_indice');
			$this->db->where('id_dispositivo', $device_id);
			$queryUMO=$this->db->get();
			if ($queryUMO->num_rows()>0):
				$rowUMO=$queryUMO->row();
				$ultima_modifica=$rowUMO->ultima_modifica;
			endif;
			
			$dataDES['device_id']=$device_id;
			$dataDES['timestamp']=$ultima_modifica;
			if (isset($dataFIRMWARE['file']))
				$dataDES['firmware']=$dataFIRMWARE;
			
			$dataDES['layout']=Array();
			$layout_temp=Array();

			// elenca i layout di ieri, in caso di sovrapposizioni antecedenti alla mezzanotte
			
			$differenza_settimana=0;
			$giorno_precedente=$giorno-1;
			if ($giorno_precedente==0):
				$giorno_precedente=7;
				$differenza_settimana=7*86400;
			endif;
			
			$this->db->from('layout_playlist');
			$this->db->where('id_dispositivo', $device_id);
			$this->db->where('giorno', $giorno_precedente);
			$this->db->where('orario_inizio <', $timestamp_oggi);
			$this->db->where('orario_fine >', $timestamp_oggi);
			$this->db->order_by('ordine', 'asc');
			$queryLAY0=$this->db->get();
			$dataQUERY['query']=$queryLAY0;
			$dataQUERY['differenza_settimana']=$differenza_settimana;
			$dataLAY=$this->strimy_model->playlist($dataQUERY);
			foreach ($dataLAY as $dataLAYtemp):
				if (!empty($dataLAYtemp))
					$layout_temp=array_merge($layout_temp, $dataLAYtemp);
			endforeach;
			
			// elenca i layout di oggi
			$this->db->from('layout_playlist');
			$this->db->where('id_dispositivo', $device_id);
			$this->db->where('giorno', $giorno);
			$this->db->order_by('ordine', 'asc');
			$queryLAY=$this->db->get();
			$dataQUERY['query']=$queryLAY;
			$dataQUERY['differenza_settimana']=0;
			$dataLAY=$this->strimy_model->playlist($dataQUERY);
			
			foreach ($dataLAY as $dataLAYtemp):
				if (!empty($dataLAYtemp))
					$layout_temp=array_merge($layout_temp, $dataLAYtemp);
			endforeach;
			
			
			// elenca i contenuti di sistema, comuni a tutti gli account: servono per la homescreen di default
			unset($dataQUERY);
			$dataQUERY['id_dispositivo']=$device_id;
			$dataLAY=$this->strimy_model->playlist_system($dataQUERY);
			foreach ($dataLAY as $dataLAYtemp):
				if (!empty($dataLAYtemp))
					// $layout_temp[]=$dataLAYtemp;
					$layout_temp=array_merge($layout_temp, $dataLAYtemp);
			endforeach;
			
			$dataDES['layout']=$layout_temp;
			
			if ($comando=="playlist"):
				echo json_encode($dataDES);
			elseif ($comando=="pretty_playlist"):
				echo "<pre>".json_encode($dataDES, JSON_PRETTY_PRINT)."</pre>";
			endif;
		
		elseif ($comando=="playlist_old"):	// restituisce la playlist formattatata (vecchio)
			
			$giorno=date('w');
			
			if ($giorno==0) $giorno=7; // se è domenica il sistema restituisce 0, ma strimy la gestisce come 7

			// verifica che la playlist giornaliera contenga effettivamente delle sequenze, escluse quelle di default "username" e "download"
			// in caso contrario carica la playlist generale "ogni giorno"
			$this->db->select('playlist_sequenze.id');
			$this->db->from('playlist_sequenze');
			$this->db->join('playlist', 'playlist.id=playlist_sequenze.id_playlist');
			$this->db->where('playlist.id_dispositivo', $device_id);
			$this->db->where('playlist.giorno', $giorno);
			$this->db->where('playlist_sequenze.tipologia !=', 'username');
			$this->db->where('playlist_sequenze.tipologia !=', 'download');
			$query=$this->db->get();
			
			if ($query->num_rows()==0)
				$giorno=0;
			
			if ($status!="") $giorno=$status;
			
			$dataDSP['id_dispositivo']=$device_id;
			$dataDSP['giorno']=$giorno;
			$dataRET=$this->strimy_model->playlist($dataDSP);
		
			$yy=0;
			foreach ($dataRET['riga'] as $riga_temp):
				if ($yy<($dataRET['totale_righe']-1))
					$riga_temp=$riga_temp."\n";
				echo $riga_temp;
				$yy++;
			endforeach;
	
		else: // comando sconosciuto
		
			die('unknown');
		
		endif;
		
	}

	public function attivazione_dispositivo()
	{
		$data['titolo']="Attiva nuovo dispositivo";
		$this->load->view('site_header_short', $data);
		$this->load->view('device_attivazione');
		$this->load->view('site_footer_short');
	}
	
	public function configurazione_dispositivo($device_id=0)
	{
		$data['device_id']=$device_id;
		$data['operazione']="configurazione";
		$data['titolo']="Configurazione dispositivo ".$device_id;
		
		$this->load->view('site_header_short', $data);
		$this->load->view('device_configurazione', $data);
		$this->load->view('site_footer_short');
	}

	public function configurazione_salva()
	{
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $grupposelezionato);
		$query=$this->db->get();
		$row=$query->row();
		$stringa=$row->ugrp_stringa;
		
		$elementi_form = $this->input->post();
		$device_id=$this->input->post('edevice_id');

		$percorso_base=$this->config->item('real_path');
		$percorso_devices=$this->config->item('percorso_devices');

		$cartella_base=$percorso_base.$percorso_devices.$device_id;

		$nome_file="config";

		$file=$cartella_base."/".$nome_file;

		// die($file);

		if ($elementi_form['econferma']=="ok"):		
		
			$dataSET['id_gruppo']=$elementi_form['egruppo'];
			$dataSET['descrizione']=$elementi_form['edescrizione_dispositivo'];
		
			unset($elementi_form['econferma']);
			unset($elementi_form['edevice_id']);
			unset($elementi_form['eaccount']);
			unset($elementi_form['edescrizione_dispositivo']);
			if ($elementi_form['name']=="")
				$elementi_form['name']=$device_id;

			$this->db->where('id_dispositivo', $device_id);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->update('dispositivi', $dataSET);

			$this->db->from('dispositivi_configurazione');
			$this->db->where('name', $device_id);
			$queryCHD=$this->db->get();
			
			if ($queryCHD->num_rows()==0):
				$dataCONF['id_dispositivo']=$device_id;
				$dataCONF['name']=$device_id;
				$dataCONF['parametri_configurazione']=serialize($elementi_form);
				$this->db->insert('dispositivi_configurazione', $dataCONF);
			else:
				$dataCONF['id_dispositivo']=$device_id;
				$dataCONF['parametri_configurazione']=serialize($elementi_form);
				$this->db->where('name', $device_id);
				$this->db->update('dispositivi_configurazione', $dataCONF);
			endif;

			$dataIND['ultima_modifica']=time();
			$dataIND['id_dispositivo']=$device_id;
			$this->strimy_model->aggiorna_layout_indice_dispositivo($dataIND);

			redirect('/strimy/frontpage');
		else:
			echo "errore";
		endif;
	}

	public function dispositivo_comando_remoto($device_id="") {
		$data['titolo']="Invia comando al dispositivo ".$device_id;
		$data['device_id']=$device_id;
		$data['operazione']="comando_remoto";
		$this->load->view('site_header_short', $data);
		$this->load->view('device_configurazione', $data);
		$this->load->view('site_footer_short');
	}

	public function dispositivo_comando_remoto_conferma() {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$elementi=$_REQUEST;
		$dataCOM['comando_remoto']=$elementi['ecomando'];
		$id_dispositivo=$elementi['edevice_id'];
		
		if ($elementi['econferma']=="ok"):
			$this->db->where('id_dispositivo', $id_dispositivo);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->update('dispositivi', $dataCOM);
			echo "0";
		endif;
		
	}


	public function dispositivo_elimina($device_id="")
	{
		// elimina il dispositivo dal database
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		$this->db->from('dispositivi');
		$this->db->where('id_dispositivo', $device_id);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		if ($query->num_rows()>0):
		
			$this->strimy_model->dispositivo_elimina($device_id);
			
		endif;
		redirect('strimy/frontpage');
	}


	public function dispositivo_attesa_attivazione($device_id="")
	{
		if ($device_id!=""):
			$this->db->from('dispositivi');
			$this->db->where('id_dispositivo', $device_id);
			$query=$this->db->get();
			
			if ($query->num_rows()==0):	// il dispositivo non esiste; lo inserisce in coda di attivazione
				$data['id_dispositivo']=$device_id;
				$data['attivato']=0;
				$data['data_preattivazione']=time();
				$this->db->insert('dispositivi', $data);
			endif;
		endif;
	}


	public function dispositivo_attivazione()
	{

	//	error_reporting(E_ERROR);

		$elementi=$_REQUEST;
		// $this->Strimy_model->dispositivo_attivazione($elementi);
		
		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

		$percorso_base=$this->config->item('archivio_real_path');
		$percorso_devices=$this->config->item('percorso_devices');

		$device_id=$elementi['eid_dispositivo'];
		$cartella_base=$percorso_base.$percorso_devices.$device_id;

		$account=$elementi['eaccount'];
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $grupposelezionato);
		$queryGRP=$this->db->get();
		$rowGRP=$queryGRP->row();

		$stringa_casuale=$rowGRP->ugrp_stringa;

		$localita=trim($elementi['elocalita']);

		if ($elementi['econferma']!="ok")
			die('5');	// errore generico

		if ($device_id=="")
			die('1');	// dispositivo non specificato

		$this->db->from('dispositivi');
		$this->db->where('id_dispositivo', $device_id);
		$queryDA=$this->db->get();
		if ($queryDA->num_rows()==0):
			die('2');	// dispositivo sconosciuto
		else:
			$rowDA=$queryDA->row();
			if ($rowDA->attivato==1):
				die('3');	// dispositivo già attivato
			endif;
		endif;

		if ($localita=="")
			die('4');	// non è stata indicata la località

		if (!file_exists($cartella_base)):  // la directory non esiste, in quanto è stata effettuata una preattivazione
			mkdir($cartella_base);
			chmod($cartella_base, 0775);
		endif;
		
		$nome_file="config";

		$file=$cartella_base."/".$nome_file;
		
		$parametro['id_dispositivo']=$device_id;
		$parametro['name']=$device_id;
		$parametro['user']=$grupposelezionato."_".$stringa_casuale;
		$parametro['city']=$localita;
		$parametro['background']="background_04.jpg";
		$parametro['rss_url']="http://www.repubblica.it/rss/homepage/rss2.0.xml";
		$parametro['tempo_aggiornamento_meteo']="600";
		$parametro['tempo_aggiornamento_rss']="300";
		$parametro['transizione_rss']="5";
		$parametro['intervallo_aggiornamento_playlist']="15";
		$parametro['intervallo_controllo_orari']="1";
		$parametro['download_configurazioni']="20";
		$parametro['colore_orologio']="255255255";
		$parametro['colore_dati_meteo']="255255255";
		$parametro['colore_rss']="255255255";
		$parametro['font_size']="100";
		$parametro['spacing']="1";
		$parametro['margin']="20.0";
		$parametro['background_color']="000000000200";
		$parametro['audio_output']="2";
		$parametro['font_color']="255255255255";
		$parametro_serializzato=serialize($parametro);
		$parametro['parametri_configurazione']=$parametro_serializzato;
		
		$this->db->insert('dispositivi_configurazione', $parametro);


		// crea un file config con i parametri dati
		$fp = fopen($file, 'w+'); //Apro il file in lettura, lo creo se non esiste
		foreach ($parametro as $parametro_temp => $valore):
			$riga_temp=$parametro_temp.";".$valore."\n";
			if ($parametro!="econferma")
				fwrite($fp, $riga_temp); //Scrivo una stringa sul file
			//	echo $riga_temp;
		endforeach;
		fclose($fp); //Chiudo il file aperto precedentemente
		chmod($file, 0666);


		// crea una playlist vuota
		$nome_file="playlist";
		$file=$cartella_base."/".$nome_file;
		$fp = fopen($file, 'w+'); //Apro il file prova.txt in lettura, lo creo se non esiste
		fclose($fp); //Chiudo il file aperto precedentemente
		chmod($file, 0666);


		// aggiunge il dispositivo al database
		$dataDB['id_account']=$account;
		$dataDB['descrizione']=$elementi['edescrizione'];
		$dataDB['attivato']=1;
		$dataDB['id_dispositivo']=$device_id;
		$dataDB['data_attivazione']=time();
		$this->db->where('id_dispositivo', $device_id);
		$this->db->update('dispositivi', $dataDB);
		
		// invia mail
		$data_mail['messaggio']="Attivato dispositivo n. ".$device_id;
		$data_mail['oggetto']="[strimy] attivazione nuovo dispositivo";
		$this->strimy_model->invia_mail($data_mail);
		
		echo "0";	// dispositivo attivato correttamente

	}


	public function gestisci_gruppi_dispositivo() {
		$data['titolo']="Gestione gruppi";
		$this->load->view('site_header_short', $data);
		$this->load->view('device_gruppi');
		$this->load->view('site_footer_short');
	}

	public function gestisci_gruppi_dispositivo_salva() {
			$grupposelezionato=$this->strimy_model->accountselezionato();
			$id=$this->input->post('eid_gruppo');
			$descrizione=$this->input->post('edescrizione_gruppo');
			$dataCAT['descrizione']=$descrizione;
			if ($id==0):
				$dataCAT['id_account']=$grupposelezionato;
				$this->db->insert('dispositivi_gruppi', $dataCAT);
			else:
				$this->db->where('id', $id);
				$this->db->update('dispositivi_gruppi', $dataCAT);
			endif;
			redirect('strimy/frontpage');
	}
	
	function gestisci_gruppi_dispositivo_elimina() {
		
		$id=$this->input->post('eid_gruppo');
		
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$this->db->from('dispositivi_gruppi');
		$this->db->where('id', $id);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();

		if ($query->num_rows()>0):
		
			$this->db->where('id', $id);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('dispositivi_gruppi');
			
			$device_id=$id*-1;
			$this->strimy_model->dispositivo_elimina($device_id);	// elimina i layout associati
			
			$dataGRP['id_gruppo']=0;
			$this->db->where('id_gruppo', $id);
			$this->db->where('id_account', $grupposelezionato);
			$this->db->update('dispositivi', $dataGRP);
			
			echo "0";

		endif;

	}

	public function gestione_archivio($tipologia_file=-1, $messaggio=0) {
		$data['messaggio']=$messaggio;
		$data['tipologia_file']=$tipologia_file;
		$data['origine']="archivio_file";
		$this->load->view('site_header');
		$this->load->view('archivio_gestione', $data);
		$this->load->view('site_footer');
	}
	
	public function gestione_archivio_sistema($tipologia_file=-1, $messaggio=0) {
		$data['messaggio']=$messaggio;
		$data['tipologia_file']=$tipologia_file;
		$data['origine']="archivio_sistema";
		$this->load->view('site_header');
		$this->load->view('archivio_gestione', $data);
		$this->load->view('site_footer');
	}

	public function file_upload() {

		ini_set('display_errors',1);
		ini_set('display_startup_errors',1);
		error_reporting(-1);

		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

		$percorso_base=$this->config->item('archivio_real_path');
		$percorso_users=$this->config->item('percorso_users');

		$percorso_temporaneo=$percorso_base.$percorso_users."tmp/";
		$percorso_temporaneo_thumb=$percorso_temporaneo;

		// dimensione massima (in pixel) del thumbnail
		$max_dim_thumb=150;


		$prefisso_file="";
		if ($this->input->post("eorigine")=="archivio_file"):
			$tabella_db="archivio_file";
			$directory_contenuto="content";
			$url_finale="gestione_archivio";
			$cartella_base=$percorso_base.$percorso_users.$stringa_casuale."/".$directory_contenuto;
			$prefisso_file="c";
		elseif ($this->input->post("eorigine")=="archivio_sistema"):
			$tabella_db="archivio_sistema";
			$directory_contenuto="../common";
			$url_finale="gestione_archivio_sistema";
			$cartella_base=$percorso_base.$percorso_users."common";
			$prefisso_file="s";
		endif;

		$errore=0;
		
		
		if ($grupposelezionato==0)
			$errore=1;
			
		$id_file=$this->input->post("eid_file");
		
		$tipologia_file_redirect=$this->input->post('etipologia_file_redirect');
		
		if ($this->input->post("econferma")=="ok"):
		
			if ( ($id_file==0) && (isset($_FILES["efile"]["name"])) && ($_FILES["efile"]["name"]!="") ):

				require_once($this->config->item('real_path').'/php-lib/getid3/getid3/getid3.php');

				$file_nomefile=$_FILES["efile"]["name"];
				$file_tipofile=$_FILES["efile"]["type"];
				$file_dimensione=($_FILES["efile"]["size"] / 1024);
				$file_temporaneo=$_FILES["efile"]["tmp_name"];

				$limite_upload=$this->config->item('limite_upload')*1024;

				if ($file_dimensione>$limite_upload)
					$errore=1;
				
				if ($errore==0):
				
				
				//	$pathInfo = pathinfo($file_nomefile);
				//	$file_nomefile=$pathInfo['filename'];
				//	die($file_nomefile);
				
					$file_nomefile=preg_replace("/[^a-z0-9\.]/", "_", strtolower($file_nomefile));

					$dataFL['nome_file']=$file_nomefile;
					$dataFL['descrizione']=$this->input->post('edescrizione');
					$dataFL['id_account']=$gruppo;
					$this->db->insert($tabella_db, $dataFL);
					$id_file=$this->db->insert_id();
					$file_nomefile=$id_file."_".$prefisso_file."_".$file_nomefile;

					$file_temporaneo_upload=$file_temporaneo;
					$thumbnail=0;
					$resized=0;
					$dim_x=0;
					$dim_y=0;
					$valori=Array();

					if (($file_tipofile=="image/jpeg") || ($file_tipofile=="image/jpg") || ($file_tipofile=="image/gif") || ($file_tipofile=="image/png")):
					
						// $image = new Imagick($file_temporaneo);
						$image = new Imagick(realpath($file_temporaneo));
						// $image_finale = new Imagick();

						$dimensioni = $image->getImageGeometry(); 
						$dim_x = $dimensioni['width']; 
						$dim_y = $dimensioni['height']; 

						$rapporto=$dim_x/$dim_y;


						// se altezza o larghezza dell'immagine supera i 2000px, viene ridimensionata
						if (($dim_x>2000) || ($dim_y>2000)):
							
							unset($file_temporaneo_upload);
							
							if ($dim_x>$dim_y):
								$dim_x=2000;
								$dim_y=round(2000/$rapporto);
							else:
								$dim_y=2000;
								$dim_x=round(2000*$rapporto);
							endif;
						
						endif;
						
						
						$image_resized = new Imagick(realpath($file_temporaneo));
						$image_resized->thumbnailImage($dim_x, $dim_y);
						
						// $image_resized->setColorspace(Imagick::COLORSPACE_RGB);
						$image_resized->writeImage($percorso_temporaneo."/".$file_nomefile.".jpg");
						$image_resized->destroy();
						
						
						/*
						$image_finale->newImage($dim_x, $dim_y, 'black', 'jpg' );

						$image_finale->setImageCompression(Imagick::COMPRESSION_JPEG);
						$image_finale->setImageCompressionQuality(100);
	
						$image->setImageCompression(Imagick::COMPRESSION_JPEG);
						$image->setImageCompressionQuality(100);

						$image->thumbnailImage($nuova_dim_x, $nuova_dim_y);
						$geometry = $image->getImageGeometry();
						$x = ( $finale_x - $geometry['width'] )/2;
						$y = ( $finale_y - $geometry['height'] )/2;
						$image_finale->compositeImage($image, imagick::COMPOSITE_OVER, $x, $y);
						*/
												
						$file_temporaneo_upload=$file_nomefile.".jpg";
						$file_nomefile=$file_nomefile.".jpg";
						
						$copia=1;

						if ($dim_x>$dim_y):
							$nuova_dim_y=$max_dim_thumb;
							$nuova_dim_x=round($max_dim_thumb*$rapporto);
						else:
							$nuova_dim_x=$max_dim_thumb;
							$nuova_dim_y=round($max_dim_thumb/$rapporto);
						endif;

						$thumb_image = new Imagick(realpath($file_temporaneo));
						$thumb_image->thumbnailImage($nuova_dim_x, $nuova_dim_y);
						// $image_resized->setColorspace(Imagick::COLORSPACE_RGB);
						$thumb_image->writeImage($percorso_temporaneo."/".$file_nomefile."_thumb.jpg");
						$thumb_image->destroy();
						$thumbnail=1;

						$tipologia_file=2;   // è un'immagine

					
					elseif (($file_tipofile=="video/avi") || ($file_tipofile=="video/mpg") || ($file_tipofile=="video/mkv") || ($file_tipofile=="video/x-msvideo")  || ($file_tipofile=="video/mp4")):

						$getID3 = new getID3;
						$ThisFileInfo = $getID3->analyze($file_temporaneo_upload);
						
						$dim_x=$ThisFileInfo['video']['resolution_x'];
						$dim_y=$ThisFileInfo['video']['resolution_y'];
					
						$rapporto=$dim_x/$dim_y;
					
						if ($dim_x>$dim_y):
							$nuova_dim_y=$max_dim_thumb;
							$nuova_dim_x=round($max_dim_thumb*$rapporto);
						else:
							$nuova_dim_x=$max_dim_thumb;
							$nuova_dim_y=round($max_dim_thumb/$rapporto);
						endif;

					
						$comando="ffmpeg -i ".$file_temporaneo_upload." -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg -s ".$nuova_dim_x."x".$nuova_dim_y." ".$percorso_temporaneo."/".$file_nomefile."_thumb.jpg 2>&1";
						shell_exec($comando);

						$thumbnail=1;
						$tipologia_file=1;	// è un video

					elseif ($file_tipofile=="audio/mp3"):
						
						$valori['nome_file_thumb']="../../archive_music.jpg";
						$tipologia_file=0;	// è un file audio

					else:
						
						// non è un file supportato;
						// elimina la voce temporanea dal database
						$errore=1;
						$this->db->where('id', $id_file);
						$this->db->delete($tabella_db);

					endif;
						
					
					if ($errore==0):
						
						if (!isset($copia))
							$copia=0;
						
						if ($copia==1):
							$dataFUP['copia']=1;
							$percorso_temporaneo=$percorso_temporaneo."/";
						else:
							$percorso_temporaneo="";
						endif;
						
						$dimensioni_file=filesize(realpath($file_temporaneo_upload));
						
						$dataFUP['file_da_caricare']=$percorso_temporaneo.$file_temporaneo_upload;
						$dataFUP['nome_del_file']=$file_nomefile;
						$dataFUP['cartella']=$directory_contenuto;
						$dataFUP['grupposelezionato']=$grupposelezionato;
						
						$this->strimy_model->file_upload($dataFUP);
						
						$valori['dimensioni']=$dim_x.";".$dim_y;
						
						unset($dataFUP);
						if ($thumbnail==1):

							$file_nomefile_thumb=$file_nomefile."_thumb.jpg";

							$dimensioni_file=$dimensioni_file+filesize(realpath($file_nomefile_thumb));
						
						//	die($percorso_temporaneo.$file_nomefile_thumb);
						
							$dataFUP['copia']=1;
							$dataFUP['file_da_caricare']=$percorso_temporaneo_thumb.$file_nomefile_thumb;
							$dataFUP['nome_del_file']=$file_nomefile_thumb;
							$dataFUP['cartella']=$directory_contenuto;
							$dataFUP['grupposelezionato']=$grupposelezionato;
							$this->strimy_model->file_upload($dataFUP);

							$valori['nome_file_thumb']=$file_nomefile_thumb;
							unset($dataFUP);
						endif;
											
						$dataFL2['nome_file']=$file_nomefile;
						$dataFL2['valori']=serialize($valori);
						$dataFL2['dimensioni_file']=$dimensioni_file;
						$dataFL2['tipologia_file']=$tipologia_file;
						$dataFL2['data_upload']=time();
						$this->db->where('id', $id_file);
						$this->db->where('id_account', $gruppo);
						$this->db->update($tabella_db, $dataFL2);
						unset($dataFL2);
					
						if ($tipologia_file_redirect!=-1):
							$tipologia_file_redirect=$tipologia_file;
						endif;
					
					endif;
					
				endif;
		
			elseif ($id_file!=0):
				
				$dataFL2['descrizione']=$this->input->post('edescrizione');
				$this->db->where('id', $id_file);
				$this->db->where('id_account', $gruppo);
				$this->db->update($tabella_db, $dataFL2);
			
			else:
				$errore=1;
			
			endif;
			
		endif;

		redirect('strimy/'.$url_finale.'/'.$tipologia_file_redirect."/".$errore);
		
	}

	
	public function file_gestisci($origine="", $id_file=0, $tipologia_file=-1) {
		$data['origine']=$origine;
		$data['id_file']=$id_file;
		$data['tipologia_file']=$tipologia_file;
		if ($id_file==0)
			$data['titolo']="Carica file";
		else
			$data['titolo']="Modifica";
		$this->load->view('site_header_short', $data);
		$this->load->view('archivio_file_gestione', $data);
		$this->load->view('site_footer_short');
	}


	public function file_cancella($origine="", $id_file=0) {
		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
		
		if ($origine=="archivio_file"):
			$tabella_db="archivio_file";
			$directory_contenuto="content";
			$url_finale="gestione_archivio";
		elseif ($origine=="archivio_sistema"):
			$tabella_db="archivio_sistema";
			$directory_contenuto="../common";
			$url_finale="gestione_archivio_sistema";
		endif;
		
		
		$this->db->from($tabella_db);
		$this->db->where('id', $id_file);
		if ($origine!="archivio_sistema")
			$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		if ($query->num_rows()>0):
			$row=$query->row();
			
			$valori=unserialize($row->valori);

			if ($origine=="archivio_sistema"):
				$dataARSI['ultima_modifica']=time();
				$this->db->where('ultima_modifica >', 0);
				$this->db->update('layout_indice', $dataARSI);
			else:
				$dataFILE['id_file']=$id_file;
				$dataFILE['id_account']=$grupposelezionato;
				$this->strimy_model->aggiorna_dispositivi_associati($dataFILE);
			endif;
			
			$dataFUP['file_da_cancellare']=$row->nome_file;
			$dataFUP['cartella']=$directory_contenuto;
			$dataFUP['grupposelezionato']=$grupposelezionato;
			$this->strimy_model->file_cancella($dataFUP);
			
			if ($row->tipologia_file!=0):	// se è un file audio non cancella il thumbnail, che è generico e comune a tutti gli utenti
				$dataFUP['file_da_cancellare']=$valori['nome_file_thumb'];
				$dataFUP['cartella']=$directory_contenuto;
				$dataFUP['grupposelezionato']=$grupposelezionato;
				$this->strimy_model->file_cancella($dataFUP);
			endif;
				
			// elimina il file dall'archivio
			$this->db->where('id', $id_file);
			if ($origine!="archivio_sistema")
				$this->db->where('id_account', $gruppo);
			$this->db->delete($tabella_db);
				
			// elimina il file dalle playlist
			if ($origine=="archivio_file"):
				$this->db->where('id_file', $id_file);
				$this->db->where('tipologia_file', 2);
				$this->db->delete('playlist_generica_elementi');
			endif;

		endif;
		
	}

	public function playlist_generica_gestisci($id=0, $tipologia_playlist=2, $tipologia_file=2) {
		$data['id_playlist']=$id;
		$data['tipologia_playlist']=$tipologia_playlist;  // indica che si tratta di una tipologia slideshow
		$data['tipologia_file']=$tipologia_file;	// indica se si tratta di una slideshow audio, video o immagini
		$this->load->view('site_header');
		$this->load->view('playlist_generica_gestisci', $data);
		$this->load->view('site_footer');
	}


	public function playlist_misc_gestisci($id=0, $tipologia_playlist=50) {
		$data['id_playlist']=$id;
		$data['tipologia_playlist']=$tipologia_playlist;
		if ($id==0)
			$prefisso="Inserisci";
		else
			$prefisso="Modifica";
		
		$widget=$this->config->item('widget');
		$widget_descrizione=$this->config->item('widget_descrizione');
		$data['tipologia_playlist']=$tipologia_playlist;
		$descrizione = array_search($tipologia_playlist, $widget);
		$data['titolo']=$prefisso." widget ".$widget_descrizione[$descrizione];
		$this->load->view('site_header_short', $data);
		$this->load->view('playlist_misc_gestisci', $data);
		$this->load->view('site_footer_short');
	}
	
	public function playlist_misc_gestisci_salva() {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$data=$this->input->post();
		// var_dump($data);
		unset($data['econferma']);
		unset($data['edescrizione']);
		unset($data['eid_playlist']);
		unset($data['etipologia_playlist']);
		if ($this->input->post('econferma')=="ok"):

			if (isset($data['background_color']))
				$data['background_color']=$this->strimy_model->hex2rgb($data['background_color']);

			$id=$this->input->post('eid_playlist');
			$dataPLY['descrizione']=$this->input->post('edescrizione');
			$dataPLY['tipologia']=$this->input->post('etipologia_playlist');
			$dataPLY['ultima_modifica']=time();
			$dataPLY['valori']=serialize($data);
			if ($id==0):
				$dataPLY['id_account']=$grupposelezionato;
				$this->db->insert('playlist_generica_indice', $dataPLY);
				$id=$this->db->insert_id();
			else:
				$this->db->where('id', $id);
				$this->db->update('playlist_generica_indice', $dataPLY);
			endif;
		endif;
		
		$dataPLAY['id_playlist']=$id;
		$this->strimy_model->aggiorna_dispositivi_associati($dataPLAY);
		
		redirect('strimy/gestione_playlist/'.$dataPLY['tipologia']);
	}


	public function playlist_generica_gestisci_aggiungi($ordine, $id=0, $id_file=0, $tipologia_playlist=2) {
		
		$widget_aspect_ratio=$this->config->item('widget_aspect_ratio');
		$widget_aspect_ratio_descrizione=$this->config->item('widget_aspect_ratio_descrizione');
		$grupposelezionato=$this->strimy_model->accountselezionato();
		
		if ($id!=0):
			// $this->db->select('playlist_generica_elementi.valori as valori');
			$this->db->from('playlist_generica_elementi');
			$this->db->where('id', $id);
			$this->db->where('id_account', $grupposelezionato);
			$query=$this->db->get();
			$row=$query->row();
			$id_file=$row->id_file;
			$valori=unserialize($row->valori);
			$valori_nascosti=unserialize($row->valori_nascosti);

		else:
			$valori['duration']=5;
			$valori['crop_ratio']=0;

		endif;
		
		$this->db->from('archivio_file');
		$this->db->where('id', $id_file);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		$row=$query->row();
		$valori_file=unserialize($row->valori);

		$dataLISTA['gruppo']=$grupposelezionato;
		$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);
		$dataSTORAGE=$this->strimy_model->recupera_storage($grupposelezionato);

		$valori['crop_mask']=$valori_nascosti['crop_mask'];

		$url_base=$dataSTORAGE['full_path'].$this->config->item('percorso_users').$stringa_casuale."/".$dataLISTA['directory_contenuto'];
	
		// echo "<div id=\"divfile_".$ordine."\" style=\"height: 110px; border-bottom: 1px solid #dedede; position: relative; float: left; padding-top: 4px; padding-bottom: 4px;\" class=\"contenuto_ordinabile ui-state-disabled\">";
		echo "<div id=\"divfile_".$ordine."\" class=\"contenuto_ordinabile panel panel-default ui-state-disabled\">";
		echo "<div class=\"panel-body\" style=\"height: 100px\">";
    	
			echo "<table class=\"table table-condensed table-hover\">";
			
			echo "<tr><td style=\"width: 20px\">";
			echo "<span class=\"campo_playlist sortable-reorder fa fa-reorder\" onmouseover=\"$('#divfile_".$ordine."').removeClass('ui-state-disabled')\" onmouseout=\"$('#divfile_".$ordine."').addClass('ui-state-disabled')\"></span>";
			echo "</td>";
			
			echo "<td style=\"width: 30px;\">";
			echo "<span class=\"campo_playlist campo_playlist_ordine\" style=\"font-size: 0.8em;\">".($ordine+1);
			echo "</span>";
			echo "</td>";
			
			echo "<td style=\"width: 200px; padding: 0px\">";
			echo "<span class=\"campo_playlist\">";
				
			$altezza=100;
			$file_anteprima=$dataSTORAGE['full_path'].$this->config->item('percorso_users').$stringa_casuale."/content/".$valori_file['nome_file_thumb'];
			list($img_x, $img_y) = getimagesize($file_anteprima);
			$larghezza=round($img_x/$img_y*$altezza)-100;
			$proporzione=$img_y/$altezza;
			
			$nomediv="divfile_interno_".$ordine;
			$nometag="evalori_crop_mask_".$ordine;
			echo "<div id=\"".$nomediv."\" style=\"max-width: 200px;\">"; // href=\"".$this->config->item('base_url')."index.php/strimy/player/".$id_file."\" data-toggle=\"modal\" data-target=\"#finestra-modale\">";
				echo "<img src=\"".$file_anteprima."\" style=\"height: ".$altezza."px;\">";
			echo "</div>";
			
			
			if ($tipologia_playlist!=0):
				
				if (($id==0) || ($valori['crop_mask']=="0;0;0;0") || (!isset($valori['crop_mask'])))
					$valori['crop_mask']="0;0;".$img_x.";".$img_y;
				
				if (!isset($valori['crop_ratio']))
					$valori['crop_ratio']=0;
				
				
				$valori_dimensioni_temp=explode(";", $valori_file['dimensioni']);
				
				echo "<input type=\"hidden\" id=\"evalori_crop_mask_".$ordine."\" name=\"evalori_crop_mask[]\" value=\"".$valori['crop_mask']."\">";
				echo "<input type=\"hidden\" id=\"evalori_crop_ratio_".$ordine."\" name=\"evalori_crop_ratio[]\" value=\"".$valori['crop_ratio']."\">";
				echo "<input type=\"hidden\" id=\"evalori_image_size_".$ordine."\" name=\"evalori_image_size[]\" value=\"".$valori_file['dimensioni']."\">";
				// questo campo indica il rapporto tra la larghezza dell'immagine full size e quella del thumbnail;
				// serve per ricalcolare, in fase di salvataggio, la reale dimensione della mascheratura
				echo "<input type=\"hidden\" name=\"erapporto[]\" value=\"".$valori_dimensioni_temp[0]/$img_x."\">";
				
				?>
				
				<script language="Javascript">
				
					$('div#<?php echo $nomediv; ?> > img').cropper({
					  strict: true,
					  highlight: false,
					  dragCrop: false,
					  autoDrag: false,
					  movable: false,
					  scalable: false,
					  cropBoxMovable: true,
					  mouseWheelZoom: false,
					  guides: true,
					  cropBoxResizable: true,
					  autoCropArea: 1,
					  checkImageOrigin: false
					});

					$('div#<?php echo $nomediv; ?> > img').on('cropend.cropper', function (e) {
						mask_recupera_dati('<?php echo $ordine; ?>');
					});

					$('div#<?php echo $nomediv; ?> > img').on('built.cropper', function (e) {

						var canvas=$(e.target).cropper('getContainerData');
						var image=$(e.target).cropper('getImageData');

						var margin_top=Math.round((canvas.height-image.height)/2);
						var margin_left=Math.round((canvas.width-image.width)/2);

						<?php if ($valori['crop_mask']!="0;0;0;0"):
						$valori_temp=explode(";", $valori['crop_mask']);
						$thumb_altezza=$altezza;
						$thumb_larghezza=$larghezza;
						$left=round($valori_temp[0]/$proporzione);
						$top=round($valori_temp[1]/$proporzione);
						$width=round($valori_temp[2]/$proporzione);
						$height=round($valori_temp[3]/$proporzione);
						?>

						var img_left=<?php echo $left; ?>;
						var img_top=<?php echo $top; ?>;
						var img_width=<?php echo $width; ?>;
						var img_height=<?php echo $height; ?>;

						if (img_width>image.widht)
							img_width=image.width;
						if (img_height>image.height)
							img_height=image.height;

						var dimensioni_crop = {
								left: (margin_left+img_left),
								top: (margin_top+img_top),
								width: img_width,
								height: img_height
						};

						$('div#<?php echo $nomediv; ?> > img').cropper('setAspectRatio', img_width / img_height);
						$(e.target).cropper('setCropBoxData', dimensioni_crop);

						<?php endif; ?>
						
					});
					
				</script>
				
				<?php
			
			endif;
			
			echo "</span>";
			echo "</td>";
			
			echo "<td style=\"width: 50%; max-width: 50%;\">";
			echo "<span class=\"campo_playlist\" style=\"width: 100%; max-width: 100%; height: 90%; overflow: hidden; text-overflow: break-word;\">"; 
			echo "<input type=\"hidden\" name=\"eid_file[]\" value=\"".$id_file."\">";
			echo "<input type=\"hidden\" id=\"eattivo_".$ordine."\" name=\"eattivo[]\" class=\"efile_attivo\" value=\"1\">";
			$descrizione=$row->nome_file;
			if (strlen($descrizione)>60)
				$descrizione=substr($descrizione, 0, 60)."...";
			echo str_replace("_", "_ ", $descrizione);
			// echo $descrizione;
			echo "</span>";
			echo "</td>";
			
			echo "<td>";
			if ($tipologia_playlist==2):
				echo "<span class=\"campo_playlist text-right\" style=\"width: 120px;\">";
				echo "durata (sec.): ";
				echo "<input type=\"text\" name=\"evalori_durata[]\" value=\"".$valori['duration']."\" style=\"width: 35px;\" maxlength=\"2\">";
				echo "</span>";
			else:
				echo "<input type=\"hidden\" name=\"evalori_durata[]\" value=\"0\">";
			endif;
			echo "</td>";

			echo "<td style=\"width: 40px; padding-right: 20px;\">";
			echo "<a onclick=\"cancella_file('".$ordine."');\" class=\"btn btn-default fa fa-trash\"></a>";
			echo "</td></tr>";

		echo "</table>";
		echo "</div>";
		echo "</div>";
		
		// se è una nuova immagine, viene eseguito il crop secondo le impostazioni della playlist
		if (($tipologia_playlist==2) && ($id==0)):
			?>
			<script language="Javascript">
				$('div#<?php echo $nomediv; ?> > img').on('built.cropper', function (e) {
					var tipo=$("#ecrop_ratio").val();
					var ordine=<?php echo $ordine; ?>;
					mask_ridimensiona(tipo, ordine);
				});
			</script>
			<?php
		endif;		
	}

	public function playlist_generica_gestisci_cancella($id_playlist=0) {
		$grupposelezionato=$this->strimy_model->accountselezionato();

		$dataDEL['id_playlist']=$id_playlist;
		$dataDEL['id_account']=$grupposelezionato;
		$this->strimy_model->playlist_generica_cancella($dataDEL);
		
	//	redirect('strimy/gestione_playlist');
	}

	public function playlist_generica_gestisci_salva() {
		$grupposelezionato=$this->strimy_model->accountselezionato();
		$id_playlist=$this->input->post('eid_playlist');
		$id_file=$this->input->post('eid_file');
		$attivo=$this->input->post('eattivo');
		$numero_file=$this->input->post('enumero_file');
		$descrizione_playlist=$this->input->post('edescrizione_playlist');
		$tipologia_playlist=$this->input->post('etipologia_playlist');
		$tipologia_file=$this->input->post('etipologia_file');
		$valori['crop_ratio']=$this->input->post('ecrop_ratio');
		
		$valori_durata=$this->input->post('evalori_durata');
		$valori_crop_mask=$this->input->post('evalori_crop_mask');
		$valori_image_size=$this->input->post('evalori_image_size');
		$valori_crop_ratio=$this->input->post('evalori_crop_ratio');
		$rapporto=$this->input->post('erapporto');		
		
		$dataPLY['descrizione']=$descrizione_playlist;
		$dataPLY['tipologia']=$tipologia_playlist;
		$dataPLY['tipologia_file']=$tipologia_file;
		$dataPLY['valori']=serialize($valori);
		$dataPLY['ultima_modifica']=time();
		
		if ($id_playlist==0):
			$dataPLY['id_account']=$grupposelezionato;
			$this->db->insert('playlist_generica_indice', $dataPLY);
			$id_playlist=$this->db->insert_id();
		else:
			$this->db->where('id', $id_playlist);
		//	$this->db->where('id_account', $grupposelezionato);
			$this->db->update('playlist_generica_indice', $dataPLY);

			$this->db->where('id_playlist', $id_playlist);
		//	$this->db->where('id_account', $grupposelezionato);
			$this->db->delete('playlist_generica_elementi');
		endif;
		
		$xx=0;
	//	die($id_playlist);
		while ($xx<$numero_file) {
		
			if ((isset($attivo[$xx])) && ($attivo[$xx]==1) && ($id_file[$xx]!="") && ($id_file[$xx]!=0)):
			
				unset($dataVALORI);
				unset($dataVALORInascosti);
				$dataFILE['ordine']=$xx;
				$dataFILE['id_account']=$grupposelezionato;
				$dataFILE['id_file']=$id_file[$xx];
				$dataFILE['id_playlist']=$id_playlist;
				$dataFILE['tipologia_file']=$tipologia_playlist;
				
				$dataVALORI['duration']=$valori_durata[$xx];
				if ($valori_crop_mask[$xx]!=""):
					$crop_temp=explode(";", $valori_crop_mask[$xx]);
					$crop_real[0]=round($crop_temp[0]*$rapporto[$xx]);
					$crop_real[1]=round($crop_temp[1]*$rapporto[$xx]);
					$crop_real[2]=round($crop_temp[2]*$rapporto[$xx]);
					$crop_real[3]=round($crop_temp[3]*$rapporto[$xx]);
					
					$valori_image_size_temp=explode(";", $valori_image_size[$xx]);
					
					if ($crop_real[2]>$valori_image_size_temp[0])
						$crop_real[2]=$valori_image_size_temp[0];
					if ($crop_real[3]>$valori_image_size_temp[1])
						$crop_real[3]=$valori_image_size_temp[1];
					
					$dataVALORI['crop_mask']=$crop_real[0].";".$crop_real[1].";".$crop_real[2].";".$crop_real[3];
					$dataVALORInascosti['crop_mask']=$valori_crop_mask[$xx];
				endif;
				if ($valori_image_size[$xx]!="")
					$dataVALORI['image_size']=$valori_image_size[$xx];
				if ($valori_crop_ratio[$xx]!="")
					$dataVALORI['crop_ratio']=$valori_crop_ratio[$xx];
				
				if (isset($dataVALORI))
					$dataFILE['valori']=serialize($dataVALORI);
				if (isset($dataVALORInascosti))
					$dataFILE['valori_nascosti']=serialize($dataVALORInascosti);
				
				$this->db->insert('playlist_generica_elementi', $dataFILE);
			endif;
			$xx++;
			
		}

		$dataPLAY['id_playlist']=$id_playlist;
		$this->strimy_model->aggiorna_dispositivi_associati($dataPLAY);
		
		redirect('strimy/gestione_playlist/'.$tipologia_playlist."/".$tipologia_file);
		
	}


	public function gestione_playlist($tipologia_playlist=2, $tipologia_file=2) {
		$data['tipologia_playlist']=$tipologia_playlist;	// se non specificato, è una slideshow
		$data['tipologia_file']=$tipologia_file;	// se non specificato, è una slideshow immagini
		$this->load->view('site_header');
		$this->load->view('playlist_gestione', $data);
		$this->load->view('site_footer');
	}

	public function gestione_playlist_old($device_id=0, $giorno=0) {
		$data['device_id']=$device_id;
		$data['giorno']=$giorno;
		$this->load->view('site_header');
		$this->load->view('playlist_gestione2', $data);
		$this->load->view('site_footer');
	}

	public function orario_playlist_check()
	{
		error_reporting(E_ALL);
		$elementi=$_REQUEST;
		$download=$elementi['edownload'];
		$numero_sequenze=$elementi['numero_sequenze'];
		$attiva_sequenza_temp=explode("-", $elementi['eattiva_sequenza']);
		$inizio_sequenza_temp=explode("-", $elementi['einizio']);
		$fine_sequenza_temp=explode("-", $elementi['efine']);
		$sequenza_cancella=explode("-", $elementi['ecancella_sequenza']);

		$xx=0; $yy=0;
		while ($yy<$numero_sequenze) {
			if ($sequenza_cancella[$yy]==0):
				
				if ($fine_sequenza_temp[$yy]=="")
					$fine_sequenza_temp[$yy]=$inizio_sequenza_temp[$yy];
				
				$attiva_sequenza[$xx]=$attiva_sequenza_temp[$yy];
				$orario_temp=$this->strimy_model->orario_check($inizio_sequenza_temp[$yy]);
				$inizio_sequenza[$xx]=$orario_temp['orario_formattato'];
				$inizio_timestamp[$xx]=$orario_temp['orario_timestamp'];
				$orario_temp=$this->strimy_model->orario_check($fine_sequenza_temp[$yy]);
				$fine_sequenza[$xx]=$orario_temp['orario_formattato'];
				$fine_timestamp[$xx]=$orario_temp['orario_timestamp'];
				
				$xx++;
				
			endif;
			$yy++;
		}
		$numero_sequenze=$xx;

		

		// verifica che l'orario di inizio non sia posteriore a quello di fine, o che non ci siano sovrapposizioni di orario
		$errore=0;
		$yy=0;
		while ($yy<$numero_sequenze) {
			if ($inizio_timestamp[$yy]>$fine_timestamp[$yy])
				$errore=1;
			
			$xx=0;
			while ($xx<$numero_sequenze) {
				if ($xx!=$yy):
					if (($inizio_timestamp[$xx]>$inizio_timestamp[$yy]) && ($inizio_timestamp[$xx]<$fine_timestamp[$yy]))
						$errore=1;
					if (($fine_timestamp[$xx]>$inizio_timestamp[$yy]) && ($fine_timestamp[$xx]<$fine_timestamp[$yy]))
						$errore=1;
					if (($inizio_timestamp[$xx]>($inizio_timestamp[$yy]-1)) && ($fine_timestamp[$xx]<($fine_timestamp[$yy]+1)))
						$errore=1;
				endif;
				$xx++;
			}
			$yy++;
		}
		
		if ($errore==1)
			die('1'); // sovrapposizione orari
		else
			die('0'); // tutto ok
	}


	public function playlist_salva()
	{
		
	//	error_reporting(E_ALL);
		error_reporting(E_ERROR);
		
	//	$elementi=$_REQUEST;
		$elementi=$this->input->post();

	//	ini_set('display_errors',1);
	//	ini_set('display_startup_errors',1);
	//	error_reporting(-1);

		$gruppo=$this->flexi_auth->get_user_group_id();
		$grupposelezionato=$this->strimy_model->accountselezionato();
		if ($grupposelezionato==0) die('logged out');
		$this->db->from('user_groups');
		$this->db->where('ugrp_id', $grupposelezionato);
		$queryUT=$this->db->get();
		$rowUT=$queryUT->row();
		$stringa_utente=$grupposelezionato."_".$rowUT->ugrp_stringa;
		
		$device_id=$elementi['edevice_id'];
		$giorno=$elementi['egiorno_sequenza'];
		$download=$elementi['edownload'];
		$numero_sequenze=$elementi['numero_sequenze'];
		$id_sequenza_temp=$elementi['eidsequenza'];
		$sequenza_tipologia_temp=$elementi['sequenza_tipologia'];
		$reset_sequenza_temp=$elementi['reset_sequenza'];
		$attiva_sequenza_temp=$elementi['eattiva_sequenza'];
		$titolo_sequenza_temp=$elementi['etitolo'];
		$inizio_sequenza_temp=$elementi['einizio'];
		$fine_sequenza_temp=$elementi['efine'];
		$sequenza_jingle_id_temp=$elementi['esequenza_jingle_id'];
		$sequenza_jingle_categoria_temp=$elementi['esequenza_jingle_categoria'];
		$sequenza_jingle_intervallo_temp=$elementi['esequenza_jingle_intervallo'];
		$sequenza_cancella=$elementi['ecancella_sequenza'];
		$elementi_sequenza_temp=$elementi['elementi_sequenza'];
		$elemento_durata_temp=$elementi['edurata'];
		$elemento_file_temp=$elementi['efile'];
		$id_elemento_temp=$elementi['eidelemento'];
		
		$elemento_file_id_temp=$elementi['efileid'];
		$elemento_cancella_temp=$elementi['ecancella'];

		$sovraimpressione_temp=$elementi['esovraimpressione'];
		$sovraimpressione_messaggio_temp=$elementi['esovraimpressione_messaggio'];
		$sovraimpressione_posizione_messaggio_temp=$elementi['esovraimpressione_posizione_messaggio'];

		
		// fa un primo parsing dei dati inseriti e li riordina
			
		$yy=0;
		$xx=0;
		$numero_sequenze_iniziali=$numero_sequenze;			
		while ($yy<$numero_sequenze) {
			if ($sequenza_cancella[$yy]==0):
				if (!isset($reset_sequenza_temp[$yy])) $reset_sequenza_temp[$yy]=0;
				if (!isset($sequenza_jingle_categoria_temp[$yy])) $sequenza_jingle_categoria_temp[$yy]=0;
				if (!isset($sequenza_jingle_intervallo_temp[$yy])) $sequenza_jingle_intervallo_temp[$yy]=0;
				if (!isset($sequenza_jingle_id_temp[$yy])) $sequenza_jingle_id_temp[$yy]=0;
				if (!isset($titolo_sequenza_temp[$yy])) $titolo_sequenza_temp[$yy]=" ";
				if (!isset($attiva_sequenza_temp[$yy])) $attiva_sequenza_temp[$yy]=1;
				if (!isset($elemento_durata_temp[$yy])) $elemento_durata_temp[$yy]=Array();
				if (!isset($elemento_file_temp[$yy])) $elemento_file_temp[$yy]=Array();
				if (!isset($elemento_file_id_temp[$yy])) $elemento_file_id_temp[$yy]=Array();
				if (!isset($elemento_cancella_temp[$yy])) $elemento_cancella_temp[$yy]=Array();
				if (!isset($id_elemento_temp[$yy])) $id_elemento_temp[$yy]=Array();
				if (!isset($sovraimpressione_temp[$yy])) $sovraimpressione_temp[$yy]=Array();
				if (!isset($sovraimpressione_messaggio_temp[$yy])) $sovraimpressione_messaggio_temp[$yy]=Array();
				if (!isset($sovraimpressione_posizione_messaggio_temp[$yy])) $sovraimpressione_posizione_messaggio_temp[$yy]=Array();
				
				if ($fine_sequenza_temp[$yy]=="")
					$fine_sequenza_temp[$yy]=$inizio_sequenza_temp[$yy];
				
				
				$id_sequenza[$xx]=$id_sequenza_temp[$yy];
				$titolo_sequenza[$xx]=$titolo_sequenza_temp[$yy];
				$reset_sequenza[$xx]=$reset_sequenza_temp[$yy];
				$attiva_sequenza[$xx]=$attiva_sequenza_temp[$yy];
				$sequenza_tipologia[$xx]=$sequenza_tipologia_temp[$yy];
				$orario_temp=$this->strimy_model->orario_check($inizio_sequenza_temp[$yy]);
				$inizio_sequenza[$xx]=$orario_temp['orario_formattato'];
				$inizio_timestamp[$xx]=$orario_temp['orario_timestamp'];
				$orario_temp=$this->strimy_model->orario_check($fine_sequenza_temp[$yy]);
				$fine_sequenza[$xx]=$orario_temp['orario_formattato'];
				$fine_timestamp[$xx]=$orario_temp['orario_timestamp'];
				$sequenza_jingle_id[$xx]=$sequenza_jingle_id_temp[$yy];
				$sequenza_jingle_categoria[$xx]=$sequenza_jingle_categoria_temp[$yy];
				$sequenza_jingle_intervallo[$xx]=$sequenza_jingle_intervallo_temp[$yy];
				$elementi_sequenza[$xx]=$elementi_sequenza_temp[$yy];
				$elemento_durata[$xx]=$elemento_durata_temp[$yy];
				$elemento_file[$xx]=$elemento_file_temp[$yy];
		//		$elemento_file_descrizione[$xx]=$elemento_file_descrizione_temp[$yy];
				$elemento_file_id[$xx]=$elemento_file_id_temp[$yy];
				$elemento_cancella[$xx]=$elemento_cancella_temp[$yy];
				$id_elemento[$xx]=$id_elemento_temp[$yy];
				
				$sovraimpressione[$xx]=$sovraimpressione_temp[$yy];
				$sovraimpressione_messaggio[$xx]=$sovraimpressione_messaggio_temp[$yy];
				$sovraimpressione_posizione_messaggio[$xx]=$sovraimpressione_posizione_messaggio_temp[$yy];
				
				$xx++;
				
			endif;
			$yy++;
		}
		$numero_sequenze=$xx;
		
		
		
		
		if ($elementi['econferma']=="ok"):

			$orario_temp=$this->strimy_model->orario_check($inizio_sequenza_temp[$yy]);
			$inizio_sequenza[$xx]=$orario_temp['orario_formattato'];
			$inizio_timestamp[$xx]=$orario_temp['orario_timestamp'];
			$orario_temp=$this->strimy_model->orario_check($fine_sequenza_temp[$yy]);
			$fine_sequenza[$xx]=$orario_temp['orario_formattato'];
			$fine_timestamp[$xx]=$orario_temp['orario_timestamp'];
			$orario_temp=$this->strimy_model->orario_check($download);
			$download=$orario_temp['orario_formattato'];
			$download_timestamp=$orario_temp['orario_timestamp'];

			$dataINS['numero_sequenze']=$numero_sequenze;
			$dataINS['numero_sequenze_iniziali']=$numero_sequenze_iniziali;
			$dataINS['device_id']=$device_id;
			$dataINS['grupposelezionato']=$grupposelezionato;
			$dataINS['giorno']=$giorno;
			$dataINS['download']=$download;
			$dataINS['download_timestamp']=$download_timestamp;

			$dataINS['id_sequenza']=$id_sequenza;
			$dataINS['id_sequenza_temp']=$id_sequenza_temp;
			$dataINS['titolo_sequenza']=$titolo_sequenza;
			$dataINS['reset_sequenza']=$reset_sequenza;
			$dataINS['sequenza_cancella']=$sequenza_cancella;
			$dataINS['attiva_sequenza']=$attiva_sequenza;
			$dataINS['sequenza_tipologia']=$sequenza_tipologia;
			$dataINS['inizio_sequenza']=$inizio_sequenza;
			$dataINS['inizio_timestamp']=$inizio_timestamp;
			$dataINS['fine_sequenza']=$fine_sequenza;
			$dataINS['fine_timestamp']=$fine_timestamp;
			$dataINS['sequenza_jingle_id']=$sequenza_jingle_id;
			$dataINS['sequenza_jingle_categoria']=$sequenza_jingle_categoria;
			$dataINS['sequenza_jingle_intervallo']=$sequenza_jingle_intervallo;
			$dataINS['elementi_sequenza']=$elementi_sequenza;
			$dataINS['elemento_durata']=$elemento_durata;
			$dataINS['elemento_file']=$elemento_file;
			$dataINS['elemento_file_id']=$elemento_file_id;
			$dataINS['elemento_cancella']=$elemento_cancella;
			$dataINS['id_elemento']=$id_elemento;
			$dataINS['sovraimpressione']=$sovraimpressione;
			$dataINS['sovraimpressione_messaggio']=$sovraimpressione_messaggio;
			$dataINS['sovraimpressione_posizione_messaggio']=$sovraimpressione_posizione_messaggio;

			$this->strimy_model->playlist_salva($dataINS);

			// playlist salvata correttamente
			redirect('strimy/frontpage');

		else:
			echo "2";	// errore generico
		endif;
	}


	public function carica_elemento($id_elemento, $id_file, $tipologia, $sequenza_selezionata, $numero_elementi)  // carica i singoli elementi della playlist
	{

	$gruppo=$this->flexi_auth->get_user_group_id();
	$grupposelezionato=$this->strimy_model->accountselezionato();
	$dataLISTA['gruppo']=$grupposelezionato;
	$stringa_casuale=$this->strimy_model->stringa_casuale($dataLISTA);

	$percorso_base=$this->config->item('real_path');

	$percorso_users=$this->config->item('percorso_users');
	$cartella_archivio=$percorso_base.$percorso_users.$stringa_casuale."/content";
	$url_archivio="/strimy/users/".$stringa_casuale."/content";
		
	error_reporting(E_ERROR);
	if ($id_elemento!=0):
		$this->db->select('playlist_elementi.id as id, playlist_elementi.durata as durata, playlist_elementi.descrizione_file as descrizione_file, playlist_elementi.file as file, playlist_elementi.id_file as id_file, playlist_elementi.sovraimpressione as sovraimpressione, playlist_elementi.sovraimpressione_messaggio as sovraimpressione_messaggio, playlist_elementi.sovraimpressione_posizione_messaggio as sovraimpressione_posizione_messaggio');
		$this->db->from('playlist_elementi');
		$this->db->join('playlist', 'playlist.id=playlist_elementi.id_playlist');
		$this->db->where('playlist.id_account', $grupposelezionato);
		$this->db->where('playlist_elementi.id', $id_elemento);
		$query=$this->db->get();
		$row=$query->row();
	
		$descrizione_file=$row->descrizione_file;
		$file=$row->file;
		$fileid=$row->id_file;
		$durata=$row->durata;
		$sovraimpressione=$row->sovraimpressione;
		$sovraimpressione_messaggio=$row->sovraimpressione_messaggio;
		$sovraimpressione_posizione_messaggio=$row->sovraimpressione_posizione_messaggio;
	
	else:
		$this->db->from('archivio_file');
		$this->db->where('id', $id_file);
		$this->db->where('id_account', $grupposelezionato);
		$query=$this->db->get();
		$row=$query->row();
	
		$file=$row->nome_file;
		$fileid=$row->id;
		$durata=5;
		
	endif;
	
	$anteprima=$url_archivio."/".$file;
	
	// echo $anteprima;
	
	$sovraimpressione_opzioni[0]='';
	$sovraimpressione_opzioni[1]='messaggio';
	$sovraimpressione_opzioni[2]='logo';
	$sovraimpressione_opzioni[3]='frame';

	$sovraimpressione_posizione_messaggio_opzioni[0]='';
	$sovraimpressione_posizione_messaggio_opzioni[1]='in alto';
	$sovraimpressione_posizione_messaggio_opzioni[2]='centrale';
	$sovraimpressione_posizione_messaggio_opzioni[3]='in basso';
	
	echo '<div style="position: relative; float: left; width: 100px; overflow: hidden;">';
	echo '<span style="font-size: 0.7em; white-space: nowrap;">'.$descrizione_file.'</span><br>';
	echo '<div class="elemento_sequenza_interno" style="background: url('.$anteprima.'); background-size: 100% 100%;"></div>';
	echo '</div>';
	
	echo '<span>';

	echo '<div style="float: left; position: relative; width: 80%; border: 1px solid #fff; top: 10px;">';
	if ($tipologia=="immagini"):

		// durata
		echo '<div style="position: relative; float: left; left: 50px; width: 200px;">';
		echo '<span style="font-size: 0.7em; padding-left: 45px;">durata</span><br>';
		echo '<div class="pulsante_sinistra" style="float:left;" onClick="durata_elemento(\'diminuisci\',\''.$sequenza_selezionata.'\',\''.$numero_elementi.'\')"><i class="fa fa-minus" style="width: 20px;"></i></div>';
		echo '<div style="float: left;"><input type="text" id="edurata_'.$sequenza_selezionata.'_'.$numero_elementi.'" name="edurata['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$durata.'" size="3" style="width: 40px;"></div>';
		echo '<div class="pulsante_destra" style="float:left;" onClick="durata_elemento(\'aumenta\',\''.$sequenza_selezionata.'\',\''.$numero_elementi.'\')"><i class="fa fa-plus" style="width: 20px;"></i></div>';
		echo '</div>';
		

		// caselle di selezione sovraimpressione		
		echo '<div style="position: relative; float: left; margin-left: 50px; width: 150px;">';
		echo '<span style="font-size: 0.7em;">sovraimpressione</span><br>';
		echo '<div>';
		echo '<select name="esovraimpressione['.$sequenza_selezionata.']['.$numero_elementi.']" class="styled" onChange="seleziona_sovraimpressione(this, \''.$sequenza_selezionata.'\', \''.$numero_elementi.'\')">';
	
		$xx=0;
		while ($xx<4) {
			echo '<option value="'.$xx.'"';
			if ($xx==$sovraimpressione)
				echo ' selected';
			echo '>'.$sovraimpressione_opzioni[$xx].'</option>';
			$xx++;
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		
		// div di sovraimpressione
		echo '<div style="position: relative; float: left; margin-left: 50px; width: 350px; border: 1px solid #fff">';
		
		// sovraimpressione messaggio
			echo '<div id="sovraimpressione_messaggio_'.$sequenza_selezionata.'_'.$numero_elementi.'" class="elemento_sovraimpressione"';
			if ($sovraimpressione!=1) {
					echo' style="display: none;"';
			}
			echo '>';
			echo '<span style="font-size: 0.7em;">messaggio</span><br>';
			echo '<span style="position: relative; float: left;">';
			echo ' <input type="text" name="esovraimpressione_messaggio['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$sovraimpressione_messaggio.'" size="20">';
			echo '<select name="esovraimpressione_posizione_messaggio['.$sequenza_selezionata.']['.$numero_elementi.']" class="styled">';
			$xx=1;
			while ($xx<4) {
				echo'<option value="'.$xx.'"';
				if ($xx==$sovraimpressione_posizione_messaggio)
					echo' selected';
				echo'>'.$sovraimpressione_posizione_messaggio_opzioni[$xx].'</option>';
				$xx++;
			}
			echo '</select>';
			echo '</span>';
			echo '</div>';
	
		echo '</div>';
	
	endif;
	echo '</div>';
	echo '<div class="pulsante_unico" onclick="cancella_elemento(\''.$sequenza_selezionata.'\',\''.$numero_elementi.'\')" style="margin-left: 30px; top: 20px;"><i class="fa fa-trash"></i></div>';

	echo '<input type="hidden" name="eidelemento['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$id_elemento.'" readonly="readonly">';
	echo '<input type="hidden" name="efile['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$file.'" readonly="readonly">';
	echo '<input type="hidden" name="efileid['.$sequenza_selezionata.']['.$numero_elementi.']" value="'.$fileid.'" readonly="readonly">';
	echo '<input type="hidden" id="ecancella_'.$sequenza_selezionata.'_'.$numero_elementi.'" name="ecancella['.$sequenza_selezionata.']['.$numero_elementi.']" value="0">';

	echo '</span>';
	
	}

	public function player($origine="archivio_file", $id=0) {
		$data['id']=$id;
		$data['titolo']="Anteprima";
		$data['origine']=$origine;
		$this->load->view('site_header_short', $data);
		$this->load->view('player', $data);
		$this->load->view('site_footer_short');
	}

	public function google() {
		$this->load->library('google');
		$this->google->upload_file();
	}

    
    public function aggiorna_stato_gruppi() {
		
		// questo script deve essere eseguito tramite cron solo una volta al giorno:
		// viene eseguito un controllo sul timestamp della mezzanotte di oggi per evitare doppie esecuzioni
		$timestamp = strtotime('today midnight');
		
		$this->db->from('last_cron');
		$this->db->where('time', $timestamp);
		$queryCRON=$this->db->get();
		
		if ($queryCRON->num_rows()==0):
		
			// aggiorna il credito degli account interessati
			$this->db->select('user_groups.ugrp_id as ugrp_id, user_groups.crediti as crediti');
			$this->db->from('user_groups');
			$this->db->join('dispositivi', 'dispositivi.id_account=user_groups.ugrp_id');
			$this->db->where('user_groups.crediti_scala', 1);
			$this->db->group_by('user_groups.ugrp_id');
			$queryGRP=$this->db->get();
			foreach ($queryGRP->result() as $rowGRP):
				$this->db->from('dispositivi');
				$this->db->where('id_account', $rowGRP->ugrp_id);
				$this->db->where('attivato', 1);
				$queryDISP=$this->db->get();
				$numero_dispositivi=$queryDISP->num_rows();
				$crediti_residui=$rowGRP->crediti;
				$crediti_residui=$crediti_residui-$numero_dispositivi;
				if ($crediti_residui<0)
					$crediti_residui=0;
				
				$dataCRE['crediti']=$crediti_residui;
				$this->db->where('ugrp_id', $rowGRP->ugrp_id);
				$this->db->update('user_groups', $dataCRE);
				
			endforeach;
		
			// elimina dal db tutti i dispositivi in attesa di attivazione da più di 24 ore
			$this->db->where('data_preattivazione <', ($timestamp-(24*3600)) );
			$this->db->where('attivato', 0);
			$this->db->delete('dispositivi');

			// aggiorna la tabella last_cron, per evitare che lo script venga nuovamente eseguito nello stesso giorno
			$dateCRON['time']=$timestamp;
			$this->db->where('id', 1);
			$this->db->update('last_cron', $dateCRON);
			
		else:
			die("script già eseguito");
		
		endif;
	}

}

