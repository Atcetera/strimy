<?php
/**
 * Load in codeigniter and return the instance.
 * The default controller and method should contain a return statement due to the REQUEST constant so that codeigniter
 * knows to return back to us.
 */
ob_start();
define("REQUEST", "external");
include("index.php");
ob_end_clean();
$CI =& get_instance();
$CI->db->from('dispositivi');
$CI->db->where('attivato', 1);
/*
$query=$CI->db->get();
foreach ($query->result() as $row):
	echo $row->id_dispositivo."<br>";
endforeach;
*/
?>
